#  compte rendu tp 2 Tom Bernard

**Configuration de router.tp2.efrei**

```
[tom@router ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:db:62:c4 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.254/24 brd 10.2.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fedb:62c4/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:2c:86:6e brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.117/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 440sec preferred_lft 440sec
    inet6 fe80::a23:154e:b9da:a6dd/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b2:5e:da brd ff:ff:ff:ff:ff:ff
    inet 192.168.159.133/24 brd 192.168.159.255 scope global dynamic noprefixroute enp0s9
       valid_lft 1340sec preferred_lft 1340sec
    inet6 fe80::44dd:7d15:fc07:6311/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
[tom@router ~]$
```
**preuve que le router a accès a internet**

```
[tom@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=128 time=28.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=128 time=29.0 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=128 time=31.2 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=128 time=28.1 ms
```
**preuve pour le fire wall**

```
[tom@router ~]$ sudo sysctl -w net.ipv4.ip_forward=1
net.ipv4.ip_forward = 1
[tom@router ~]$ sudo firewall-cmd --add-masquerade
success
[tom@router ~]$ sudo firewall-cmd --add-masquerade --permanent
success
```

**Configuration de node1.tp2.efrei**

**ip fixe 10.2.1.1**

```
[tom@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b2:b5:c3 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.1/24 brd 10.2.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feb2:b5c3/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:8b:bd:5d brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.114/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 377sec preferred_lft 377sec
    inet6 fe80::abf:a8cb:ff4:ddea/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

**ping de machine1 vers routeur 1**
```
[tom@node1 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=2.71 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=1.31 ms
64 bytes from 10.2.1.254: icmp_seq=3 ttl=64 time=1.78 ms
64 bytes from 10.2.1.254: icmp_seq=4 ttl=64 time=1.34 ms```

```

**ajoutez une route par défaut qui passe par router.tp2.efrei**
 ```
 [tom@node1 ~]$ sudo nano /etc/sysconfig/network
[sudo] password for tom:
# Created by anaconda
GATEWAY=10.2.1.254

[tom@node1 ~]$ ip route show
default via 10.2.1.254 dev enp0s3 proto static metric 100
10.2.1.0/24 dev enp0s3 proto kernel scope link src 10.2.1.1 metric 100
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.114 metric 101

```

**configuration de l'ip**
```
  GNU nano 5.6.1                         /etc/sysconfig/network-scripts/ifcfg-enp0s3                                    NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.1
NETMASK=255.255.255.0

GATEWAY=10.2.1.254
DNS1=1.1.1.1

```

**prouvez que vous avez un accès internet depuis node1.tp2.efrei désormais, avec une commande ping**

```
[tom@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=31.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=30.0 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=127 time=38.1 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=127 time=33.1 ms
64 bytes from 8.8.8.8: icmp_seq=5 ttl=127 time=33.2 ms
```

**utiliser la commande traceroute pour prouver**

```
[tom@node1 ~]$ traceroute google.fr
traceroute to google.fr (216.58.214.163), 30 hops max, 60 byte packets
 1  _gateway (10.2.1.254)  0.800 ms  0.996 ms  1.342 ms
```
**Install et conf du serveur DHCP sur dhcp.tp2.efrei**

**preuve que dhcp a accès a internet**
```
[tom@dhcp ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=18.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=19.2 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=127 time=18.9 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=127 time=19.0 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 18.371/18.871/19.173/0.300 ms
```
**on modifie la configuration du dhcp pour que la passerelle par default soit celle du routeur (10.2.1.254). Pour cela on modifie la ligne options routers**

```
commande : sudo nano /etc/dhcp/dhcpd.conf

default-lease-time 600;
max-lease-time 7200;

authoritative;
subnet 10.2.1.0 netmask 255.255.255.0 {
range 10.2.1.100 10.2.1.200;
option routers 10.2.1.254;
option subnet-mask 255.255.255.0;
}

```

**Test du DHCP sur node1.tp2.efrei**

on configure le node1 en dchp

```
commande :  sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3

intérieur fichier nano :

NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=dhcp
ONBOOT=yes

GATEWAY=10.2.1.254
DNS1=1.1.1.1
```

**Test du DHCP sur node1.tp2.efrei (tout)**

```
[tom@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b2:b5:c3 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.100/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 485sec preferred_lft 485sec
    inet6 fe80::a00:27ff:feb2:b5c3/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:8b:bd:5d brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.114/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 557sec preferred_lft 557sec
    inet6 fe80::abf:a8cb:ff4:ddea/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
[tom@node1 ~]$ ip route show
default via 10.2.1.254 dev enp0s3 proto dhcp src 10.2.1.100 metric 102
10.2.1.0/24 dev enp0s3 proto kernel scope link src 10.2.1.100 metric 102
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.114 metric 101
[tom@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=22.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=23.4 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 22.536/22.964/23.392/0.428 ms
```

 **Affichez la table ARP de router.tp2.efrei**

 ```
[tom@router ~]$ ip neigh show
192.168.159.254 dev enp0s9 lladdr 00:50:56:eb:86:ba STALE
192.168.56.100 dev enp0s8 lladdr 08:00:27:48:f6:a9 STALE
192.168.56.1 dev enp0s8 lladdr 0a:00:27:00:00:02 REACHABLE
10.2.1.100 dev enp0s3 lladdr 08:00:27:b2:b5:c3 STALE
10.2.1.253 dev enp0s3 lladdr 08:00:27:4d:59:f1 STALE
192.168.159.2 dev enp0s9 lladdr 00:50:56:fd:10:19 STALE
 ```
 on peut voir sur les deux avant dernière ligneles adresses ip et mac de nos deux machine (dchp et node)

 **Exécuter un simple ARP poisoning**

 on modifie l'adresse mac du routeur dans la table ARP de node 1, pour cela on la supprime puis la remplace :

**avant**

 ```
 [tom@node1 ~]$ ip neigh show
10.2.1.254 dev enp0s3 lladdr 08:00:27:db:62:c4 STALE
192.168.56.1 dev enp0s8 lladdr 0a:00:27:00:00:02 REACHABLE
10.2.1.253 dev enp0s3 lladdr 08:00:27:4d:59:f1 STALE
192.168.56.100 dev enp0s8 lladdr 08:00:27:48:f6:a9 STALE
 ```
**commande (l'adresse mac mise pour remplacer l'ancienne est aléatoire)**

```
[tom@node1 ~]$ sudo ip neigh del 10.2.1.254 lladdr 08:00:27:4d:59:f1 dev enp0s3 nud permanent
[tom@node1 ~]$ sudo ip neigh add 10.2.1.254 lladdr eb:ea:c1:77:ea:e9 dev enp0s3 nud permanent
```


 **après**

 ```
 [tom@node1 ~]$ ip neigh show
10.2.1.254 dev enp0s3 lladdr eb:ea:c1:77:ea:e9 PERMANENT
192.168.56.1 dev enp0s8 lladdr 0a:00:27:00:00:02 REACHABLE
10.2.1.253 dev enp0s3 lladdr 08:00:27:4d:59:f1 STALE
192.168.56.100 dev enp0s8 lladdr 08:00:27:48:f6:a9 STALE
 ```