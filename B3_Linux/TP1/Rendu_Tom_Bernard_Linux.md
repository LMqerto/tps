# Rendu Tom Bernard Linux TP1


**🌞 Prouvez que le schéma de partitionnement a bien été appliqué**
```
[tom@node1 ~]$ sudo fdisk -l
[sudo] password for tom:
Disk /dev/sda: 30 GiB, 32212254720 bytes, 62914560 sectors
Disk model: VBOX HARDDISK
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xc07eff1e

Device     Boot    Start      End  Sectors Size Id Type
/dev/sda1           2048 44058623 44056576  21G 8e Linux LVM
/dev/sda2  *    44058624 46155775  2097152   1G 83 Linux


Disk /dev/mapper/rl-root: 10 GiB, 10737418240 bytes, 20971520 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/rl-swap: 1 GiB, 1073741824 bytes, 2097152 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/rl-var: 5 GiB, 5368709120 bytes, 10485760 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/rl-home: 5 GiB, 5368709120 bytes, 10485760 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
[tom@node1 ~]$
```

**🌞 Mettre en évidence la ligne de configuration sudo qui concerne le groupe wheel**

```
[tom@node1 ~]$ sudo cat /etc/sudoers | grep 'wheel'
## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL
# %wheel        ALL=(ALL)       NOPASSWD: ALL
[tom@node1 ~]$
```

**🌞 Prouvez que votre utilisateur est bien dans le groupe wheel**
```
[tom@node1 ~]$ sudo grep 'wheel' /etc/group
wheel:x:10:tom
```

**🌞 Prouvez que la langue configurée pour l'OS est bien l'anglais**
```
[tom@node1 ~]$ locale
LANG=en_US.UTF-8
LC_CTYPE="en_US.UTF-8"
LC_NUMERIC="en_US.UTF-8"
LC_TIME="en_US.UTF-8"
LC_COLLATE="en_US.UTF-8"
LC_MONETARY="en_US.UTF-8"
LC_MESSAGES="en_US.UTF-8"
LC_PAPER="en_US.UTF-8"
LC_NAME="en_US.UTF-8"
LC_ADDRESS="en_US.UTF-8"
LC_TELEPHONE="en_US.UTF-8"
LC_MEASUREMENT="en_US.UTF-8"
LC_IDENTIFICATION="en_US.UTF-8"
LC_ALL=
```

**🌞 Prouvez que le firewall est déjà actif**
```
[tom@node1 ~]$ sudo systemctl status firewalld
[sudo] password for tom:
● firewalld.service - firewalld - dynamic firewall daemon
     Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; preset: enabled)
     Active: active (running) since Mon 2025-02-17 11:29:17 CET; 26min ago
       Docs: man:firewalld(1)
   Main PID: 555 (firewalld)
      Tasks: 2 (limit: 23177)
     Memory: 45.0M
        CPU: 350ms
     CGroup: /system.slice/firewalld.service
             └─555 /usr/bin/python3 -s /usr/sbin/firewalld --nofork --nopid

Feb 17 11:29:16 localhost systemd[1]: Starting firewalld - dynamic firewall daemon...
Feb 17 11:29:17 localhost systemd[1]: Started firewalld - dynamic firewall daemon.
[tom@node1 ~]$
```

ou

```
[tom@node1 ~]$ sudo firewall-cmd --state
running
```

**🌞 Attribuer l'adresse IP 10.1.1.11/24 à la VM**
```
[tom@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:af:f6:e6 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85736sec preferred_lft 85736sec
    inet6 fe80::a00:27ff:feaf:f6e6/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:10:33:e6 brd ff:ff:ff:ff:ff:ff
    inet 10.1.1.11/24 brd 10.1.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe10:33e6/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:16:d5:ff brd ff:ff:ff:ff:ff:ff
[tom@node1 ~]$
```

fichier conf 
```

DEVICE=enp0s8 # le nom de la carte
NAME=lan      # un nom arbitraire pas trop chiant à taper

BOOTPROTO=static # static ou dhcp
ONBOOT=yes       # la carte s'allume automatiquement au démarrage

IPADDR=10.1.1.11
NETMASK=255.255.255.0

```

**🌞 Attribuer le nom node1.tp1.b3 à la VM**
```
[tom@localhost ~]# sudo hostnamectl set-hostname node1.tp1.b3
[tom@localhost ~]# exit
logout
Connection to 192.168.141.4 closed.
C:\Users\tomto>ssh tom@192.168.141.4
tom@192.168.141.4's password:
Permission denied, please try again.
tom@192.168.141.4's password:
Last failed login: Mon Feb 17 11:33:42 CET 2025 from 192.168.141.3 on ssh:notty
There was 1 failed login attempt since the last successful login.
Last login: Mon Feb 17 11:32:57 2025 from 192.168.141.3
[tom@node1 ~]#
```

**🌞 Déterminer la liste des programmes qui écoutent sur un port TCP**
**🌞 Déterminer la liste des programmes qui écoutent sur un port UDP**

```
[tom@node1 ~]$ sudo ss -tulpn
Netid   State    Recv-Q   Send-Q       Local Address:Port       Peer Address:Port   Process
udp     UNCONN   0        0                127.0.0.1:323             0.0.0.0:*       users:(("chronyd",pid=697,fd=5))
udp     UNCONN   0        0                    [::1]:323                [::]:*       users:(("chronyd",pid=697,fd=6))
tcp     LISTEN   0        128                0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=732,fd=3))
tcp     LISTEN   0        128                   [::]:22                 [::]:*       users:(("sshd",pid=732,fd=4))
[tom@node1 ~]$
```
**🌞 Pour chacun des ports précédemment repérés..**

```
[tom@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client
  ports: 22/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```


**🌞 Fermez tous les ports inutilement ouverts dans le firewall**
```
[tom@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client
  ports: 22/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[tom@node1 ~]$ sudo firewall-cmd --remove-service=ssh
Warning: NOT_ENABLED: 'ssh' not in 'public'
success
[tom@node1 ~]$ sudo firewall-cmd --remove-service=cockpit
success
[tom@node1 ~]$ sudo firewall-cmd --remove-service=dhcpv6-client
success
[tom@node1 ~]$ sudo firewall-cmd --runtime-to-permanent
success
[tom@node1 ~]$ sudo firewall-cmd --reload
success
[tom@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services:
  ports: 22/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[tom@node1 ~]$
```

**🌞 Pour toutes les applications qui sont en écoute sur TOUTES les adresses IP**
```
[tom@node1 ~]$ sudo ss -tulpn
[sudo] password for tom:
Netid   State    Recv-Q   Send-Q       Local Address:Port       Peer Address:Port   Process
udp     UNCONN   0        0                127.0.0.1:323             0.0.0.0:*       users:(("chronyd",pid=697,fd=5))
udp     UNCONN   0        0                    [::1]:323                [::]:*       users:(("chronyd",pid=697,fd=6))
tcp     LISTEN   0        128              10.1.1.11:22              0.0.0.0:*       users:(("sshd",pid=1477,fd=3))
[tom@node1 ~]$
```


**🌞 Afficher l'état actuel de LVM**
```
[tom@node1 ~]$ sudo lvs
  LV   VG Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  home rl -wi-ao----  5.00g
  root rl -wi-ao---- 10.00g
  swap rl -wi-ao----  1.00g
  var  rl -wi-ao----  5.00g
[tom@node1 ~]$ sudo vgs
  VG #PV #LV #SN Attr   VSize  VFree
  rl   1   4   0 wz--n- 21.00g 4.00m
[tom@node1 ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda1  rl lvm2 a--  21.00g 4.00m
[tom@node1 ~]$ *
```
**🌞 Déterminer le type de système de fichiers**
```
[tom@node1 ~]$ df / /home
Filesystem          1K-blocks    Used Available Use% Mounted on
/dev/mapper/rl-root  10218772 1303928   8374172  14% /
/dev/mapper/rl-home   5074592      48   4796016   1% /home
[tom@node1 ~]$ sudo lsblk -f /home
[sudo] password for tom:
lsblk: /home: not a block device
[tom@node1 ~]$ sudo lsblk -f /
lsblk: /: not a block device
[tom@node1 ~]$ df / /
Filesystem          1K-blocks    Used Available Use% Mounted on
/dev/mapper/rl-root  10218772 1303928   8374172  14% /
/dev/mapper/rl-root  10218772 1303928   8374172  14% /
[tom@node1 ~]$ sudo lsblk -f /dev/mapper/rl-root
NAME    FSTYPE FSVER LABEL UUID                                 FSAVAIL FSUSE% MOUNTPOINTS
rl-root ext4   1.0         43742a2e-0995-473f-9203-25b7fda8e0ad      8G    13% /
[tom@node1 ~]$ sudo lsblk -f /dev/mapper/rl-home
NAME    FSTYPE FSVER LABEL UUID                                 FSAVAIL FSUSE% MOUNTPOINTS
rl-home ext4   1.0         3731ab4e-89b9-44ae-ba69-8d18e1afada5    4.6G     0% /home
[tom@node1 ~]$
```
**🌞 Remplissez votre partition /home**
```
[tom@node1 ~]$ dd if=/dev/zero of=/home/tom/bigfile bs=4M count=2500
dd: error writing '/home/tom/bigfile': No space left on device
1171+0 records in
1170+0 records out
4911116288 bytes (4.9 GB, 4.6 GiB) copied, 10.6183 s, 463 MB/s
[tom@node1 ~]$
```
**🌞 Constater que la partition est pleine**
```
[tom@node1 ~]$ df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             4.0M     0  4.0M   0% /dev
tmpfs                1.8G     0  1.8G   0% /dev/shm
tmpfs                732M  8.6M  724M   2% /run
/dev/mapper/rl-root  9.8G  1.3G  8.0G  14% /
/dev/sda2            974M  278M  629M  31% /boot
/dev/mapper/rl-home  4.9G  4.6G     0 100% /home
/dev/mapper/rl-var   4.9G  181M  4.4G   4% /var
tmpfs                366M     0  366M   0% /run/user/1000
[tom@node1 ~]$
```

**🌞Agrandir la partition**
```
[tom@node1 ~]$ sudo vgs
[sudo] password for tom:
  VG #PV #LV #SN Attr   VSize  VFree
  rl   1   4   0 wz--n- 21.00g 4.00m
[tom@node1 ~]$ sudo fdisk /dev/sda

Welcome to fdisk (util-linux 2.37.4).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

This disk is currently in use - repartitioning is probably a bad idea.
It's recommended to umount all file systems, and swapoff all swap
partitions on this disk.


Command (m for help): n
Partition type
   p   primary (2 primary, 0 extended, 2 free)
   e   extended (container for logical partitions)
Select (default p):

Using default response p.
Partition number (3,4, default 3):
First sector (46155776-62914559, default 46155776):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (46155776-62914559, default 62914559):

Created a new partition 3 of type 'Linux' and of size 8 GiB.

Command (m for help): w
The partition table has been altered.
Syncing disks.

[tom@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda           8:0    0   30G  0 disk
├─sda1        8:1    0   21G  0 part
│ ├─rl-root 253:0    0   10G  0 lvm  /
│ ├─rl-swap 253:1    0    1G  0 lvm  [SWAP]
│ ├─rl-var  253:2    0    5G  0 lvm  /var
│ └─rl-home 253:3    0    5G  0 lvm  /home
├─sda2        8:2    0    1G  0 part /boot
└─sda3        8:3    0    8G  0 part
sr0          11:0    1 1024M  0 rom
[tom@node1 ~]$ sudo pvcreate sda3
  No device found for sda3.
[tom@node1 ~]$ sudo pvcreate /dev/sda3
  Physical volume "/dev/sda3" successfully created.
[tom@node1 ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda1  rl lvm2 a--  21.00g 4.00m
  /dev/sda3     lvm2 ---   7.99g 7.99g
[tom@node1 ~]$ sudo vgextend rl /dev/sda3
  Volume group "rl" successfully extended
[tom@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda           8:0    0   30G  0 disk
├─sda1        8:1    0   21G  0 part
│ ├─rl-root 253:0    0   10G  0 lvm  /
│ ├─rl-swap 253:1    0    1G  0 lvm  [SWAP]
│ ├─rl-var  253:2    0    5G  0 lvm  /var
│ └─rl-home 253:3    0    5G  0 lvm  /home
├─sda2        8:2    0    1G  0 part /boot
└─sda3        8:3    0    8G  0 part
sr0          11:0    1 1024M  0 rom
[tom@node1 ~]$ sudo vgs
  VG #PV #LV #SN Attr   VSize  VFree
  rl   2   4   0 wz--n- 28.99g 7.99g
[tom@node1 ~]$ sudo lvextend -l +100%FREE /dev/rl/home
  Size of logical volume rl/home changed from 5.00 GiB (1280 extents) to 12.99 GiB (3326 extents).
  Logical volume rl/home successfully resized.
[tom@node1 ~]$ sudo resize2fs /dev/rl/home
resize2fs 1.46.5 (30-Dec-2021)
Filesystem at /dev/rl/home is mounted on /home; on-line resizing required
old_desc_blocks = 1, new_desc_blocks = 2
The filesystem on /dev/rl/home is now 3405824 (4k) blocks long.

[tom@node1 ~]$ df -h /home
Filesystem           Size  Used Avail Use% Mounted on
/dev/mapper/rl-home   13G  4.6G  7.6G  38% /home
[tom@node1 ~]$
```

**🌞 Remplissez votre partition /home**

```
[tom@node1 ~]$ df -h /home
Filesystem           Size  Used Avail Use% Mounted on
/dev/mapper/rl-home   13G  4.6G  7.6G  38% /home
[tom@node1 ~]$ dd if=/dev/zero of=/home/tom/bigfile bs=4M count=2500
2500+0 records in
2500+0 records out
10485760000 bytes (10 GB, 9.8 GiB) copied, 9.84181 s, 1.1 GB/s
[tom@node1 ~]$ df -h /home
Filesystem           Size  Used Avail Use% Mounted on
/dev/mapper/rl-home   13G  9.8G  2.4G  81% /home
[tom@node1 ~]$ 
```

**🌞 Utiliser ce nouveau disque pour étendre la partition /home de 20G**

```
[tom@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda           8:0    0   30G  0 disk
├─sda1        8:1    0   21G  0 part
│ ├─rl-root 253:0    0   10G  0 lvm  /
│ ├─rl-swap 253:1    0    1G  0 lvm  [SWAP]
│ ├─rl-var  253:2    0    5G  0 lvm  /var
│ └─rl-home 253:3    0   13G  0 lvm  /home
├─sda2        8:2    0    1G  0 part /boot
└─sda3        8:3    0    8G  0 part
  └─rl-home 253:3    0   13G  0 lvm  /home
sdb           8:16   0   40G  0 disk
sr0          11:0    1 1024M  0 rom
[tom@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for tom:
  Physical volume "/dev/sdb" successfully created.
[tom@node1 ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda1  rl lvm2 a--  21.00g     0
  /dev/sda3  rl lvm2 a--  <7.99g     0
  /dev/sdb      lvm2 ---  40.00g 40.00g
[tom@node1 ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda1
  VG Name               rl
  PV Size               <21.01 GiB / not usable 4.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              5377
  Free PE               0
  Allocated PE          5377
  PV UUID               RyuH2e-pXws-xgZ7-qcIq-XKUM-ge8u-fMaTUo

  --- Physical volume ---
  PV Name               /dev/sda3
  VG Name               rl
  PV Size               7.99 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              2045
  Free PE               0
  Allocated PE          2045
  PV UUID               scvms6-vvSI-9iLY-XQb6-4QFN-d3RR-1VlWWi

  "/dev/sdb" is a new physical volume of "40.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               40.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               ECfcnC-9VB5-qboe-AnJo-dUiw-84O7-RkNX54

[tom@node1 ~]$ sudo vgextend rl /dev/sdb
  Volume group "rl" successfully extended
[tom@node1 ~]$ sudo vgs
  VG #PV #LV #SN Attr   VSize   VFree
  rl   3   4   0 wz--n- <68.99g <40.00g
[tom@node1 ~]$ sudo lvextend -L +20G /dev/
Display all 164 possibilities? (y or n)
[tom@node1 ~]$ sudo lvextend -L +20G /dev/rl/home
  Size of logical volume rl/home changed from 12.99 GiB (3326 extents) to 32.99 GiB (8446 extents).
  Logical volume rl/home successfully resized.
[tom@node1 ~]$ sudo resize2fs /dev/rl/home
resize2fs 1.46.5 (30-Dec-2021)
Filesystem at /dev/rl/home is mounted on /home; on-line resizing required
old_desc_blocks = 2, new_desc_blocks = 5
The filesystem on /dev/rl/home is now 8648704 (4k) blocks long.

[tom@node1 ~]$ df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             4.0M     0  4.0M   0% /dev
tmpfs                1.8G     0  1.8G   0% /dev/shm
tmpfs                732M  8.6M  724M   2% /run
/dev/mapper/rl-root  9.8G  1.3G  8.0G  14% /
/dev/sda2            974M  278M  629M  31% /boot
/dev/mapper/rl-home   33G  9.8G   22G  32% /home
/dev/mapper/rl-var   4.9G  181M  4.4G   4% /var
tmpfs                366M     0  366M   0% /run/user/1000
[tom@node1 ~]$
```

**🌞 Créez une nouvelle partition**
```
[tom@node1 ~]$ sudo vgs
  VG #PV #LV #SN Attr   VSize   VFree
  rl   3   4   0 wz--n- <68.99g <20.00g
[tom@node1 ~]$ sudo lvcreate -L 20G web rl
  Volume group "web" not found
  Cannot process volume group web
[tom@node1 ~]$ sudo lvcreate -L 20G -n web rl
  Volume group "rl" has insufficient free space (5119 extents): 5120 required.
[tom@node1 ~]$ sudo lvcreate -L 19.9 -n web rl
  Rounding up size to full physical extent 20.00 MiB
  Logical volume "web" created.
[tom@node1 ~]$ sudo lvs
  LV   VG Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  home rl -wi-ao---- 32.99g
  root rl -wi-ao---- 10.00g
  swap rl -wi-ao----  1.00g
  var  rl -wi-ao----  5.00g
  web  rl -wi-a----- 20.00m
[tom@node1 ~]$ sudo lvs
  LV   VG Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  home rl -wi-ao---- 32.99g
  root rl -wi-ao---- 10.00g
  swap rl -wi-ao----  1.00g
  var  rl -wi-ao----  5.00g
  web  rl -wi-a----- 20.00m
[tom@node1 ~]$ sudo umount /dev/rl/web
umount: /dev/rl/web: not mounted.
[tom@node1 ~]$ sudo lvremove /dev/rl/web
Do you really want to remove active logical volume rl/web? [y/n]: y
  Logical volume "web" successfully removed.
[tom@node1 ~]$ sudo lvcreate -L 19.9G -n web rl
  Rounding up size to full physical extent 19.90 GiB
  Logical volume "web" created.
[tom@node1 ~]$ sudo lvs
  LV   VG Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  home rl -wi-ao---- 32.99g
  root rl -wi-ao---- 10.00g
  swap rl -wi-ao----  1.00g
  var  rl -wi-ao----  5.00g
  web  rl -wi-a----- 19.90g
[tom@node1 ~]$ sudo mkfs.ext4 /dev/rl/web
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 5217280 4k blocks and 1305600 inodes
Filesystem UUID: 5dc5ddd9-9d65-4d50-81c2-bcf669f4c6bb
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208,
        4096000

Allocating group tables: done
Writing inode tables: done
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done

[tom@node1 ~]$ sudo mkdir /var/www
[tom@node1 ~]$ sudo mount /dev/rl/web /var/www
[tom@node1 ~]$ df -h /var/www
Filesystem          Size  Used Avail Use% Mounted on
/dev/mapper/rl-web   20G   24K   19G   1% /var/www
[tom@node1 ~]$
```

**🌞 Proposez au moins une option de montage**

```
[tom@node1 ~]$ sudo nano /etc/fstab
[tom@node1 ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
/home                    : already mounted
/var                     : already mounted
none                     : ignored
/var/www                 : already mounted
[tom@node1 ~]$ sudo umount /var/www
[tom@node1 ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
/home                    : already mounted
/var                     : already mounted
none                     : ignored
mount: /var/www does not contain SELinux labels.
       You just mounted a file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
mount: (hint) your fstab has been modified, but systemd still uses
       the old version; use 'systemctl daemon-reload' to reload.
/var/www                 : successfully mounted
[tom@node1 ~]$
```

**fichier conf ligne à ajouter**
```
/dev/rl/web  /var/www  ext4 noexec  0 0
```
**🌞 Déterminer l'existant :**

```
[tom@node1 ~]$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin
tss:x:59:59:Account used for TPM access:/:/usr/sbin/nologin
systemd-coredump:x:999:997:systemd Core Dumper:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
sssd:x:998:996:User for sssd:/:/sbin/nologin
chrony:x:997:995:chrony system user:/var/lib/chrony:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/usr/share/empty.sshd:/usr/sbin/nologin
tom:x:1000:1000:tom:/home/tom:/bin/bash
tcpdump:x:72:72::/:/sbin/nologin
[tom@node1 ~]$ cat /etc/group
root:x:0:
bin:x:1:
daemon:x:2:
sys:x:3:
adm:x:4:
tty:x:5:
disk:x:6:
lp:x:7:
mem:x:8:
kmem:x:9:
wheel:x:10:tom
cdrom:x:11:
mail:x:12:
man:x:15:
dialout:x:18:
floppy:x:19:
games:x:20:
tape:x:33:
video:x:39:
ftp:x:50:
lock:x:54:
audio:x:63:
users:x:100:
nobody:x:65534:
utmp:x:22:
utempter:x:35:
ssh_keys:x:101:
tss:x:59:
input:x:999:
kvm:x:36:
render:x:998:
systemd-journal:x:190:
systemd-coredump:x:997:
dbus:x:81:
sssd:x:996:
chrony:x:995:
sshd:x:74:
sgx:x:994:
tom:x:1000:
tcpdump:x:72:
[tom@node1 ~]$ groups tom
tom : tom wheel
```
**🌞 Lister tous les processus qui sont actuellement en cours d'exécution, lancés par root**

```
[tom@node1 ~]$ ps -u root
    PID TTY          TIME CMD
      1 ?        00:00:01 systemd
      2 ?        00:00:00 kthreadd
      3 ?        00:00:00 pool_workqueue_
      4 ?        00:00:00 kworker/R-rcu_g
      5 ?        00:00:00 kworker/R-rcu_p
      6 ?        00:00:00 kworker/R-slub_
      7 ?        00:00:00 kworker/R-netns
     10 ?        00:00:00 kworker/u4:0-events_unbound
     11 ?        00:00:00 kworker/R-mm_pe
     12 ?        00:00:00 kworker/u4:1-events_unbound
     13 ?        00:00:00 rcu_tasks_kthre
     14 ?        00:00:00 rcu_tasks_rude_
     15 ?        00:00:00 rcu_tasks_trace
     16 ?        00:00:00 ksoftirqd/0
     17 ?        00:00:00 rcu_preempt
     18 ?        00:00:00 migration/0
     19 ?        00:00:00 idle_inject/0
     21 ?        00:00:00 cpuhp/0
     23 ?        00:00:00 kdevtmpfs
     24 ?        00:00:00 kworker/R-inet_
     25 ?        00:00:00 kauditd
     26 ?        00:00:00 khungtaskd
     28 ?        00:00:00 oom_reaper
     29 ?        00:00:00 kworker/R-write
     30 ?        00:00:00 kcompactd0
     31 ?        00:00:00 ksmd
     32 ?        00:00:00 khugepaged
     33 ?        00:00:00 kworker/R-crypt
     34 ?        00:00:00 kworker/R-kinte
     35 ?        00:00:00 kworker/R-kbloc
     36 ?        00:00:00 kworker/R-blkcg
     37 ?        00:00:00 kworker/R-tpm_d
     38 ?        00:00:00 kworker/R-md
     39 ?        00:00:00 kworker/R-md_bi
     40 ?        00:00:00 kworker/R-edac-
     41 ?        00:00:00 watchdogd
     42 ?        00:00:00 kworker/0:1H-kblockd
     43 ?        00:00:00 kswapd0
     48 ?        00:00:00 kworker/R-kthro
     49 ?        00:00:00 kworker/0:2-ata_sff
     53 ?        00:00:00 kworker/R-acpi_
     54 ?        00:00:00 kworker/R-kmpat
     55 ?        00:00:00 kworker/R-kalua
     56 ?        00:00:00 kworker/u4:3-events_unbound
     57 ?        00:00:00 kworker/R-mld
     58 ?        00:00:00 kworker/R-ipv6_
     67 ?        00:00:00 kworker/R-kstrp
    168 ?        00:00:00 kworker/u5:0
    217 ?        00:00:02 kworker/0:3-ata_sff
    377 ?        00:00:00 kworker/R-ata_s
    378 ?        00:00:00 scsi_eh_0
    379 ?        00:00:00 kworker/R-scsi_
    380 ?        00:00:00 scsi_eh_1
    381 ?        00:00:00 kworker/R-scsi_
    382 ?        00:00:00 scsi_eh_2
    383 ?        00:00:00 kworker/R-scsi_
    384 ?        00:00:00 scsi_eh_3
    385 ?        00:00:00 kworker/R-scsi_
    393 ?        00:00:00 kworker/0:2H-kblockd
    513 ?        00:00:00 kworker/R-kdmfl
    520 ?        00:00:00 kworker/R-kdmfl
    540 ?        00:00:00 jbd2/dm-0-8
    541 ?        00:00:00 kworker/R-ext4-
    604 ?        00:00:00 systemd-journal
    616 ?        00:00:00 systemd-udevd
    675 ?        00:00:00 jbd2/sda2-8
    676 ?        00:00:00 kworker/R-ext4-
    682 ?        00:00:00 kworker/R-kdmfl
    683 ?        00:00:00 kworker/R-kdmfl
    684 ?        00:00:00 kworker/R-kdmfl
    695 ?        00:00:00 irq/18-vmwgfx
    697 ?        00:00:00 kworker/R-ttm
    709 ?        00:00:00 jbd2/dm-3-8
    710 ?        00:00:00 kworker/R-ext4-
    715 ?        00:00:00 jbd2/dm-2-8
    717 ?        00:00:00 kworker/R-ext4-
    721 ?        00:00:00 jbd2/dm-4-8
    722 ?        00:00:00 kworker/R-ext4-
    729 ?        00:00:00 auditd
    763 ?        00:00:00 firewalld
    764 ?        00:00:00 systemd-logind
    772 ?        00:00:00 NetworkManager
    811 ?        00:00:00 crond
    812 tty1     00:00:00 agetty
    904 ?        00:00:00 sshd
    906 ?        00:00:00 rsyslogd
   5380 ?        00:00:00 sshd
   5423 ?        00:00:00 kworker/0:0-events
[tom@node1 ~]$
```
**🌞 Lister tous les processus qui sont actuellement en cours d'exécution, lancés par votre utilisateur**
```
[tom@node1 ~]$ ps -u tom
    PID TTY          TIME CMD
   5385 ?        00:00:00 systemd
   5387 ?        00:00:00 (sd-pam)
   5394 ?        00:00:00 sshd
   5395 pts/0    00:00:00 bash
   5451 pts/0    00:00:00 ps
[tom@node1 ~]$
```
**🌞 Déterminer le hash du mot de passe de root**
**🌞 Déterminer le hash du mot de passe de votre utilisateur**
**🌞 Déterminer la fonction de hachage qui a été utilisée**

$6$ = SHA512
```
[tom@node1 ~]$ sudo cat /etc/shadow | grep tom
tom:$6$jPDGJvxRdX3Gwtg/$ICqebRU1MO0QCNQtlHpOxZKlrsd.63uDcTbkhUy4SaHwVRWTA/aYaZ0vUi3nY6QSvJtEHkc6HhLCj01k4ZFYA.::0:99999:7:::
[tom@node1 ~]$ sudo cat /etc/shadow | grep root
root:$6$plVLKGzNevR6d.yn$eBPyMhtLwDbo69hTS5EfAl.2ADbiS0fcfJw4xwsUa0atNxe4lk3EJuEEY3.Y0Eh8tr994J1TSN9ufJGV0XLLC/::0:99999:7:::
[tom@node1 ~]$
```

**🌞 Déterminer, pour l'utilisateur root :**

**🌞 Déterminer, pour votre utilisateur :**

son shell par défaut

le chemin vers son répertoire personnel

```
[tom@node1 ~]$ sudo cat /etc/passwd | grep tom
[sudo] password for tom:
tom:x:1000:1000:tom:/home/tom:/bin/bash
[tom@node1 ~]$ sudo cat /etc/passwd | grep root
root:x:0:0:root:/root:/bin/bash
operator:x:11:0:operator:/root:/sbin/nologin
[tom@node1 ~]$
```
shell = /bin/bash

repertoir perso : 
tom = /home/tom
root = /root





**🌞 Afficher la ligne de configuration du fichier sudoers qui permet à votre utilisateur d'utiliser sudo**

```
[tom@node1 ~]$ sudo cat /etc/sudoers
[sudo] password for tom:
## Sudoers allows particular users to run various commands as
## the root user, without needing the root password.
##
## Examples are provided at the bottom of the file for collections
## of related commands, which can then be delegated out to particular
## users or groups.
##
## This file must be edited with the 'visudo' command.

## Host Aliases
## Groups of machines. You may prefer to use hostnames (perhaps using
## wildcards for entire domains) or IP addresses instead.
# Host_Alias     FILESERVERS = fs1, fs2
# Host_Alias     MAILSERVERS = smtp, smtp2

## User Aliases
## These aren't often necessary, as you can use regular groups
## (ie, from files, LDAP, NIS, etc) in this file - just use %groupname
## rather than USERALIAS
# User_Alias ADMINS = jsmith, mikem


## Command Aliases
## These are groups of related commands...

## Networking
# Cmnd_Alias NETWORKING = /sbin/route, /sbin/ifconfig, /bin/ping, /sbin/dhclient, /usr/bin/net, /sbin/iptables, /usr/bin/rfcomm, /usr/bin/wvdial, /sbin/iwconfig, /sbin/mii-tool

## Installation and management of software
# Cmnd_Alias SOFTWARE = /bin/rpm, /usr/bin/up2date, /usr/bin/yum

## Services
# Cmnd_Alias SERVICES = /sbin/service, /sbin/chkconfig, /usr/bin/systemctl start, /usr/bin/systemctl stop, /usr/bin/systemctl reload, /usr/bin/systemctl restart, /usr/bin/systemctl status, /usr/bin/systemctl enable, /usr/bin/systemctl disable

## Updating the locate database
# Cmnd_Alias LOCATE = /usr/bin/updatedb

## Storage
# Cmnd_Alias STORAGE = /sbin/fdisk, /sbin/sfdisk, /sbin/parted, /sbin/partprobe, /bin/mount, /bin/umount

## Delegating permissions
# Cmnd_Alias DELEGATING = /usr/sbin/visudo, /bin/chown, /bin/chmod, /bin/chgrp

## Processes
# Cmnd_Alias PROCESSES = /bin/nice, /bin/kill, /usr/bin/kill, /usr/bin/killall

## Drivers
# Cmnd_Alias DRIVERS = /sbin/modprobe

# Defaults specification

#
# Refuse to run if unable to disable echo on the tty.
#
Defaults   !visiblepw

#
# Preserving HOME has security implications since many programs
# use it when searching for configuration files. Note that HOME
# is already set when the the env_reset option is enabled, so
# this option is only effective for configurations where either
# env_reset is disabled or HOME is present in the env_keep list.
#
Defaults    always_set_home
Defaults    match_group_by_gid

# Prior to version 1.8.15, groups listed in sudoers that were not
# found in the system group database were passed to the group
# plugin, if any. Starting with 1.8.15, only groups of the form
# %:group are resolved via the group plugin by default.
# We enable always_query_group_plugin to restore old behavior.
# Disable this option for new behavior.
Defaults    always_query_group_plugin

Defaults    env_reset
Defaults    env_keep =  "COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS"
Defaults    env_keep += "MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE"
Defaults    env_keep += "LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES"
Defaults    env_keep += "LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE"
Defaults    env_keep += "LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY"

#
# Adding HOME to env_keep may enable a user to run unrestricted
# commands via sudo.
#
# Defaults   env_keep += "HOME"

Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin

## Next comes the main part: which users can run what software on
## which machines (the sudoers file can be shared between multiple
## systems).
## Syntax:
##
##      user    MACHINE=COMMANDS
##
## The COMMANDS section may have other options added to it.
##
## Allow root to run any commands anywhere
root    ALL=(ALL)       ALL

## Allows members of the 'sys' group to run networking, software,
## service management apps and more.
# %sys ALL = NETWORKING, SOFTWARE, SERVICES, STORAGE, DELEGATING, PROCESSES, LOCATE, DRIVERS

## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL

## Same thing without a password
# %wheel        ALL=(ALL)       NOPASSWD: ALL

## Allows members of the users group to mount and unmount the
## cdrom as root
# %users  ALL=/sbin/mount /mnt/cdrom, /sbin/umount /mnt/cdrom

## Allows members of the users group to shutdown this system
# %users  localhost=/sbin/shutdown -h now

## Read drop-in files from /etc/sudoers.d (the # here does not mean a comment)
#includedir /etc/sudoers.d
```


Ligne precise : 

```
## Allow root to run any commands anywhere
root    ALL=(ALL)       ALL

```

**🌞 Créer un utilisateur :**

aide useradd : 

```
Usage: useradd [options] LOGIN
       useradd -D
       useradd -D [options]

Options:
      --badname                 do not check for bad names
  -b, --base-dir BASE_DIR       base directory for the home directory of the
                                new account
      --btrfs-subvolume-home    use BTRFS subvolume for home directory
  -c, --comment COMMENT         GECOS field of the new account
  -d, --home-dir HOME_DIR       home directory of the new account
  -D, --defaults                print or change default useradd configuration
  -e, --expiredate EXPIRE_DATE  expiration date of the new account
  -f, --inactive INACTIVE       password inactivity period of the new account
  -g, --gid GROUP               name or ID of the primary group of the new
                                account
  -G, --groups GROUPS           list of supplementary groups of the new
                                account
  -h, --help                    display this help message and exit
  -k, --skel SKEL_DIR           use this alternative skeleton directory
  -K, --key KEY=VALUE           override /etc/login.defs defaults
  -l, --no-log-init             do not add the user to the lastlog and
                                faillog databases
  -m, --create-home             create the user's home directory
  -M, --no-create-home          do not create the user's home directory
  -N, --no-user-group           do not create a group with the same name as
                                the user
  -o, --non-unique              allow to create users with duplicate
                                (non-unique) UID
  -p, --password PASSWORD       encrypted password of the new account
  -r, --system                  create a system account
  -R, --root CHROOT_DIR         directory to chroot into
  -P, --prefix PREFIX_DIR       prefix directory where are located the /etc/* files
  -s, --shell SHELL             login shell of the new account
  -u, --uid UID                 user ID of the new account
  -U, --user-group              create a group with the same name as the user
  -Z, --selinux-user SEUSER     use a specific SEUSER for the SELinux user mapping

```
commande :
```
[tom@node1 ~]$ sudo groupadd admins
[tom@node1 ~]$ sudo useradd -s /sbin/nologin -M -g admins -N meow
[tom@node1 ~]$ sudo cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin
tss:x:59:59:Account used for TPM access:/:/usr/sbin/nologin
systemd-coredump:x:999:997:systemd Core Dumper:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
sssd:x:998:996:User for sssd:/:/sbin/nologin
chrony:x:997:995:chrony system user:/var/lib/chrony:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/usr/share/empty.sshd:/usr/sbin/nologin
tom:x:1000:1000:tom:/home/tom:/bin/bash
tcpdump:x:72:72::/:/sbin/nologin
meow:x:1001:1001::/home/meow:/sbin/nologin
[tom@node1 ~]$ groups meow
meow : admins
```

**🌞 Configuration sudoers**
```
[tom@node1 ~]$ sudo nano /etc/sudoers
```
ajouter contenue suivant : 

```
meow ALL=(tom) NOPASSWD: /bin/ls, /bin/cat, /bin/less, /bin/more

%admins ALL=(ALL) NOPASSWD: /usr/bin/dnf

tom ALL=(ALL) NOPASSWD: ALL
```

**preuve :**

```
C:\Users\tomto>ssh tom@10.1.1.11
tom@10.1.1.11's password:
Last login: Tue Feb 18 11:38:15 2025
[tom@node1 ~]$ sudo vgs
  VG #PV #LV #SN Attr   VSize   VFree
  rl   3   5   0 wz--n- <68.99g 96.00m
[tom@node1 ~]$ sudo cat /etc/shadow
root:$6$plVLKGzNevR6d.yn$eBPyMhtLwDbo69hTS5EfAl.2ADbiS0fcfJw4xwsUa0atNxe4lk3EJuEEY3.Y0Eh8tr994J1TSN9ufJGV0XLLC/::0:99999:7:::
bin:*:19820:0:99999:7:::
daemon:*:19820:0:99999:7:::
adm:*:19820:0:99999:7:::
lp:*:19820:0:99999:7:::
sync:*:19820:0:99999:7:::
shutdown:*:19820:0:99999:7:::
halt:*:19820:0:99999:7:::
mail:*:19820:0:99999:7:::
operator:*:19820:0:99999:7:::
games:*:19820:0:99999:7:::
ftp:*:19820:0:99999:7:::
nobody:*:19820:0:99999:7:::
tss:!!:20136::::::
systemd-coredump:!!:20136::::::
dbus:!!:20136::::::
sssd:!!:20136::::::
chrony:!!:20136::::::
sshd:!!:20136::::::
tom:$6$jPDGJvxRdX3Gwtg/$ICqebRU1MO0QCNQtlHpOxZKlrsd.63uDcTbkhUy4SaHwVRWTA/aYaZ0vUi3nY6QSvJtEHkc6HhLCj01k4ZFYA.::0:99999:7:::
tcpdump:!!:20136::::::
meow:!!:20137:0:99999:7:::
[tom@node1 ~]$ sudo passwd meow
Changing password for user meow.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
Sorry, passwords do not match.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.
[tom@node1 ~]$ sudo su - meow -s /bin/bash
Last login: Tue Feb 18 12:13:42 CET 2025 on pts/1
su: warning: cannot change directory to /home/meow: No such file or directory
[meow@node1 tom]$ ls /home
lost+found  tom
[meow@node1 tom]$ sudo nano /home/test.txt
[sudo] password for meow:
Sorry, user meow is not allowed to execute '/bin/nano /home/test.txt' as root on node1.tp1.b3.
[meow@node1 tom]$
```
**🌞 Déjà une configuration faible ?**

```
[meow@node1 tom]$ sudo -u tom less /etc/profile
```
on arrive dans le less suffit de taper !/bin/bash et on arrive en tant que notre user ou on  a tout les droit car pas besoins de rentrer le mdp pour sudo avec la regle qu'on a mis.

```
# /etc/profile

# System wide environment and startup programs, for login setup
# Functions and aliases go in /etc/bashrc

# It's NOT a good idea to change this file unless you know what you
# are doing. It's much better to create a custom.sh shell script in
# /etc/profile.d/ to make custom changes to your environment, as this
# will prevent the need for merging in future updates.

pathmunge () {
    case ":${PATH}:" in
        *:"$1":*)
            ;;
        *)
            if [ "$2" = "after" ] ; then
                PATH=$PATH:$1
            else
                PATH=$1:$PATH
            fi
!/bin/bash

[tom@node1 ~]$ sudo cat /etc/shadow
root:$6$plVLKGzNevR6d.yn$eBPyMhtLwDbo69hTS5EfAl.2ADbiS0fcfJw4xwsUa0atNxe4lk3EJuEEY3.Y0Eh8tr994J1TSN9ufJGV0XLLC/::0:99999:7:::
bin:*:19820:0:99999:7:::
daemon:*:19820:0:99999:7:::
adm:*:19820:0:99999:7:::
lp:*:19820:0:99999:7:::
sync:*:19820:0:99999:7:::
shutdown:*:19820:0:99999:7:::
halt:*:19820:0:99999:7:::
mail:*:19820:0:99999:7:::
operator:*:19820:0:99999:7:::
games:*:19820:0:99999:7:::
ftp:*:19820:0:99999:7:::
nobody:*:19820:0:99999:7:::
tss:!!:20136::::::
systemd-coredump:!!:20136::::::
dbus:!!:20136::::::
sssd:!!:20136::::::
chrony:!!:20136::::::
sshd:!!:20136::::::
tom:$6$jPDGJvxRdX3Gwtg/$ICqebRU1MO0QCNQtlHpOxZKlrsd.63uDcTbkhUy4SaHwVRWTA/aYaZ0vUi3nY6QSvJtEHkc6HhLCj01k4ZFYA.::0:99999:7:::
tcpdump:!!:20136::::::
meow:$6$rounds=100000$tcg2z4IEulhlZbgI$mtN0xrhlAsNyizpSoFcUNg.3i1grfwCPwAeL6j//3UzBo8jR1In.fRTYxbt3TnI88Oc8JFhe3XOY0EIO5ak/N0:20137:0:99999:7:::
[tom@node1 ~]$
```

comment regler le pb : 

modifier /etc/sudoers, il suffit d'enlever le droit d'utiliser less et more car cat suffit pour lire le contenue d'un fichier: 

```
meow ALL=(tom) NOPASSWD: /bin/ls, /bin/cat
```

**🌞 Déterminer les permissions des fichiers/dossiers...**

```
[tom@node1 ~]$ ls -l /etc/passwd
-rw-r--r--. 1 root root 1008 Feb 18 10:54 /etc/passwd
[tom@node1 ~]$ ls -l /etc/shadow
----------. 1 root root 872 Feb 18 11:53 /etc/shadow
[tom@node1 ~]$  ls -l /etc/ssh/sshd_config
-rw-------. 1 root root 3668 Feb 17 15:39 /etc/ssh/sshd_config
[tom@node1 ~]$ sudo ls -l /root
total 4
-rw-------. 1 root root 1642 Feb 17 12:34 anaconda-ks.cfg
[tom@node1 ~]$ sudo ls -l /home/tom
total 10240004
-rw-r--r--. 1 tom tom 10485760000 Feb 17 16:19 bigfile
[tom@node1 ~]$ sudo ls -l /bin/ls
-rwxr-xr-x. 1 root root 140952 Nov  6 17:29 /bin/ls
[tom@node1 ~]$ sudo ls -l /bin/systemctl
-rwxr-xr-x. 1 root root 305744 Nov 16 02:22 /bin/systemctl
[tom@node1 ~]$
```


**🌞 Restreindre l'accès à un fichier personnel**


```
[tom@node1 ~]$ mkdir passepartout
[tom@node1 ~]$ chmod passepartout/ 777
chmod: invalid mode: ‘passepartout/’
Try 'chmod --help' for more information.
[tom@node1 ~]$ chmod 777 passepartout/
[tom@node1 ~]$ nano passepartout/dont_readme.txt
[tom@node1 ~]$ chmod 600 passepartout/dont_readme.txt
[tom@node1 ~]$ sudo su root
[root@node1 tom]# pwd
/home/tom
[root@node1 tom]# cat passepartout/dont_readme.txt


[root@node1 tom]# ls -l passepartout/dont_readme.txt
-rw-------. 1 tom tom 16 Feb 18 14:44 passepartout/dont_readme.txt
[root@node1 tom]#
```
Preuve 

```
[tom@node1 ~]$ nano passepartout/dont_readme.txt
[tom@node1 ~]$ cat passepartout/dont_readme.txt
visiere iridium
[tom@node1 ~]$ sudo su root
[root@node1 tom]# cat passepartout/dont_readme.txt
visiere iridium
[tom@node1 ~]$ sudo -u meow cat passepartout/dont_readme.txt
cat: passepartout/dont_readme.txt: Permission denied


```
**🌞 Lister tous les programmes qui ont le bit SUID activé**

```
[tom@node1 /]$ sudo find . -perm /4000
find: ‘./proc/6592/task/6592/fd/6’: No such file or directory
find: ‘./proc/6592/task/6592/fdinfo/6’: No such file or directory
find: ‘./proc/6592/fd/5’: No such file or directory
find: ‘./proc/6592/fdinfo/5’: No such file or directory
./usr/bin/newgrp
./usr/bin/su
./usr/bin/gpasswd
./usr/bin/chage
./usr/bin/umount
./usr/bin/crontab
./usr/bin/sudo
./usr/bin/passwd
./usr/bin/mount
./usr/sbin/grub2-set-bootflag
./usr/sbin/pam_timestamp_check
./usr/sbin/unix_chkpwd
[tom@node1 /]$
```

**🌞Rendre le fichier dont_readme.txt immuable**

```
[tom@node1 ~]$ lsattr passepartout/dont_readme.txt
--------------e------- passepartout/dont_readme.txt
[tom@node1 ~]$ sudo chattr +i passepartout/dont_readme.txt
[tom@node1 ~]$ lsattr passepartout/dont_readme.txt
----i---------e------- passepartout/dont_readme.txt
[tom@node1 ~]$ sudo su root
[root@node1 tom]# cat passepartout/dont_readme.txt
visiere iridium
[root@node1 tom]# nano passepartout/dont_readme.txt
```

en mode nano en tant que root :
```
visiere iridium
















                [ File 'passepartout/dont_readme.txt' is unwritable ]
```


**🌞 Afficher l'identifiant du processus serveur OpenSSH en cours d'exécution**
```
[tom@node1 ~]$ ps -ef | grep sshd
root         904       1  0 09:13 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
root        5908     904  0 12:36 ?        00:00:00 sshd: tom [priv]
tom         5912    5908  0 12:36 ?        00:00:01 sshd: tom@pts/0
tom         6653    5913  0 15:16 pts/0    00:00:00 grep --color=auto sshd
[tom@node1 ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; preset: enabled)
     Active: active (running) since Tue 2025-02-18 09:13:22 CET; 6h ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 904 (sshd)
      Tasks: 1 (limit: 23160)
     Memory: 4.5M
        CPU: 125ms
     CGroup: /system.slice/sshd.service
             └─904 "sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups"

Feb 18 09:13:22 node1.tp1.b3 systemd[1]: Starting OpenSSH server daemon...
Feb 18 09:13:22 node1.tp1.b3 sshd[904]: Server listening on 10.1.1.11 port 22.
Feb 18 09:13:22 node1.tp1.b3 systemd[1]: Started OpenSSH server daemon.
Feb 18 09:14:43 node1.tp1.b3 sshd[5380]: Accepted password for tom from 10.1.1.1 por>
Feb 18 09:14:43 node1.tp1.b3 sshd[5380]: pam_unix(sshd:session): session opened for >
Feb 18 11:38:36 node1.tp1.b3 sshd[5693]: Accepted password for tom from 10.1.1.1 por>
Feb 18 11:38:36 node1.tp1.b3 sshd[5693]: pam_unix(sshd:session): session opened for >
Feb 18 12:36:46 node1.tp1.b3 sshd[5908]: Accepted password for tom from 10.1.1.1 por>
lines 1-20
^C
[tom@node1 ~]$

```

**🌞 Changer le port d'écoute du serveur OpenSSH**


```
[tom@node1 ~]$ cat /etc/ssh/sshd_config
cat: /etc/ssh/sshd_config: Permission denied
[tom@node1 ~]$ sudo cat /etc/ssh/sshd_config
#       $OpenBSD: sshd_config,v 1.104 2021/07/02 05:11:21 dtucker Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# To modify the system-wide sshd configuration, create a  *.conf  file under
#  /etc/ssh/sshd_config.d/  which will be automatically included below
Include /etc/ssh/sshd_config.d/*.conf

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
#Port 22
#AddressFamily any
ListenAddress 10.1.1.11
#ListenAddress ::

#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
#HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# Logging
#SyslogFacility AUTH
#LogLevel INFO

# Authentication:

#LoginGraceTime 2m
#PermitRootLogin prohibit-password
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

#PubkeyAuthentication yes

# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile      .ssh/authorized_keys

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication yes
#PermitEmptyPasswords no

# Change to no to disable s/key passwords
#KbdInteractiveAuthentication yes

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no
#KerberosUseKuserok yes

# GSSAPI options
#GSSAPIAuthentication no
#GSSAPICleanupCredentials yes
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no
#GSSAPIEnablek5users no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the KbdInteractiveAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via KbdInteractiveAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and KbdInteractiveAuthentication to 'no'.
# WARNING: 'UsePAM no' is not supported in RHEL and may cause several
# problems.
#UsePAM no

#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
#X11Forwarding no
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
#PrintMotd yes
#PrintLastLog yes
#TCPKeepAlive yes
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
#UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# override default of no subsystems
Subsystem       sftp    /usr/libexec/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#       X11Forwarding no
#       AllowTcpForwarding no
#       PermitTTY no
#       ForceCommand cvs server


[tom@node1 ~]$ sudo nano /etc/ssh/sshd_config
[tom@node1 ~]$ systemctl reload sshd
Failed to reload sshd.service: Access denied
See system logs and 'systemctl status sshd.service' for details.
[tom@node1 ~]$ sudo systemctl reload sshd
[tom@node1 ~]$ sudo cat /etc/ssh/sshd_config
#       $OpenBSD: sshd_config,v 1.104 2021/07/02 05:11:21 dtucker Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# To modify the system-wide sshd configuration, create a  *.conf  file under
#  /etc/ssh/sshd_config.d/  which will be automatically included below
Include /etc/ssh/sshd_config.d/*.conf

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
Port 2000
#AddressFamily any
ListenAddress 10.1.1.11
#ListenAddress ::

#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
#HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# Logging
#SyslogFacility AUTH
#LogLevel INFO

# Authentication:

#LoginGraceTime 2m
#PermitRootLogin prohibit-password
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

#PubkeyAuthentication yes

# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile      .ssh/authorized_keys

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication yes
#PermitEmptyPasswords no

# Change to no to disable s/key passwords
#KbdInteractiveAuthentication yes

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no
#KerberosUseKuserok yes

# GSSAPI options
#GSSAPIAuthentication no
#GSSAPICleanupCredentials yes
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no
#GSSAPIEnablek5users no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the KbdInteractiveAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via KbdInteractiveAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and KbdInteractiveAuthentication to 'no'.
# WARNING: 'UsePAM no' is not supported in RHEL and may cause several
# problems.
#UsePAM no

#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
#X11Forwarding no
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
#PrintMotd yes
#PrintLastLog yes
#TCPKeepAlive yes
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
#UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# override default of no subsystems
Subsystem       sftp    /usr/libexec/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#       X11Forwarding no
#       AllowTcpForwarding no
#       PermitTTY no
#       ForceCommand cvs server
[tom@node1 ~]$ ss -plnt
State        Recv-Q       Send-Q             Local Address:Port             Peer Address:Port       Process
LISTEN       0            128                    10.1.1.11:2000                  0.0.0.0:*
[tom@node1 ~]$

[tom@node1 ~]$ exit
logout
Connection to 10.1.1.11 closed.
PS C:\Users\tomto> ssh tom@10.1.1.11

PS C:\Users\tomto> ssh tom@10.1.1.11 -p 2000
ssh: connect to host 10.1.1.11 port 2000: Connection timed out

```
cependant impossible de se connecter il faut ouvir le port que l'on  a mit dans la config en tcp

```
sudo firewall-cmd --permanent --add-port=4000/tcp 
sudo firewall-cmd --reload
```

maintenant c'est bon 

```
PS C:\Users\tomto> ssh tom@10.1.1.11 -p 2000
tom@10.1.1.11's password:
Last login: Tue Feb 18 12:41:44 2025
[tom@node1 ~]$
```

**🌞 Configurer une authentification par clé**

sur gitbash :
```
$ ssh-copy-id -p 2000 tom@10.1.1.11
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/c/Users/tomto/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
tom@10.1.1.11's password:

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh -p 2000 'tom@10.1.1.11'"
and check to make sure that only the key(s) you wanted were added.

```


preuve : 

```
PS C:\Users\tomto> ssh tom@10.1.1.11 -p 2000
Last login: Tue Feb 18 16:16:56 2025 from 10.1.1.1
[tom@node1 ~]$
```

**🌞 Désactiver la connexion par password**

```
[tom@node1 ~]$ sudo nano /etc/ssh/sshd_config
```

on passe dans le fichier conf de : 

PasswordAuthentication yes

à

PasswordAuthentication no

```
[tom@node1 ~]$ sudo cat /etc/ssh/sshd_config
#       $OpenBSD: sshd_config,v 1.104 2021/07/02 05:11:21 dtucker Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# To modify the system-wide sshd configuration, create a  *.conf  file under
#  /etc/ssh/sshd_config.d/  which will be automatically included below
Include /etc/ssh/sshd_config.d/*.conf

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
Port 2000
#AddressFamily any
ListenAddress 10.1.1.11
#ListenAddress ::

#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
#HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# Logging
#SyslogFacility AUTH
#LogLevel INFO

# Authentication:

#LoginGraceTime 2m
#PermitRootLogin prohibit-password
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

#PubkeyAuthentication yes

# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile      .ssh/authorized_keys

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
PasswordAuthentication no
#PermitEmptyPasswords no

# Change to no to disable s/key passwords
#KbdInteractiveAuthentication yes

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no
#KerberosUseKuserok yes

# GSSAPI options
#GSSAPIAuthentication no
#GSSAPICleanupCredentials yes
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no
#GSSAPIEnablek5users no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the KbdInteractiveAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via KbdInteractiveAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and KbdInteractiveAuthentication to 'no'.
# WARNING: 'UsePAM no' is not supported in RHEL and may cause several
# problems.
#UsePAM no

#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
#X11Forwarding no
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
#PrintMotd yes
#PrintLastLog yes
#TCPKeepAlive yes
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
#UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# override default of no subsystems
Subsystem       sftp    /usr/libexec/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#       X11Forwarding no
#       AllowTcpForwarding no
#       PermitTTY no
        #       ForceCommand cvs server
[tom@node1 ~]$
```

**🌞 Désactiver la connexion en tant que root**

on modifie la conf ssh avec : 

PermitRootLogin no

```
[tom@node1 ~]$ sudo nano /etc/ssh/sshd_config
[tom@node1 ~]$ sudo cat /etc/ssh/sshd_config
#       $OpenBSD: sshd_config,v 1.104 2021/07/02 05:11:21 dtucker Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# To modify the system-wide sshd configuration, create a  *.conf  file under
#  /etc/ssh/sshd_config.d/  which will be automatically included below
Include /etc/ssh/sshd_config.d/*.conf

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
Port 2000
#AddressFamily any
ListenAddress 10.1.1.11
#ListenAddress ::

#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
#HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# Logging
#SyslogFacility AUTH
#LogLevel INFO

# Authentication:

#LoginGraceTime 2m
PermitRootLogin no
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

#PubkeyAuthentication yes

# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile      .ssh/authorized_keys

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
PasswordAuthentication no
#PermitEmptyPasswords no

# Change to no to disable s/key passwords
#KbdInteractiveAuthentication yes

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no
#KerberosUseKuserok yes

# GSSAPI options
#GSSAPIAuthentication no
#GSSAPICleanupCredentials yes
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no
#GSSAPIEnablek5users no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the KbdInteractiveAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via KbdInteractiveAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and KbdInteractiveAuthentication to 'no'.
# WARNING: 'UsePAM no' is not supported in RHEL and may cause several
# problems.
#UsePAM no

#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
#X11Forwarding no
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
#PrintMotd yes
#PrintLastLog yes
#TCPKeepAlive yes
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
#UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# override default of no subsystems
Subsystem       sftp    /usr/libexec/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#       X11Forwarding no
#       AllowTcpForwarding no
#       PermitTTY no
        #       ForceCommand cvs server
[tom@node1 ~]$ systemctl relaod sshd
Unknown command verb relaod.


[tom@node1 ~]$ sudo systemctl reload sshd
[tom@node1 ~]$
```


**🌞 Proposer au moins 5 configurations supplémentaires qui permettent de renforcer la sécurité du serveur OpenSSH**

```
ClientAliveInterval 0
ClientAliveCountMax 2
PermitTunnel no
LoginGraceTime 300
MaxAuthTries 3
```
```
[tom@node1 ~]$ sudo nano /etc/ssh/sshd_config
[tom@node1 ~]$ sudo cat /etc/ssh/sshd_config
#       $OpenBSD: sshd_config,v 1.104 2021/07/02 05:11:21 dtucker Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# To modify the system-wide sshd configuration, create a  *.conf  file under
#  /etc/ssh/sshd_config.d/  which will be automatically included below
Include /etc/ssh/sshd_config.d/*.conf

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
Port 2000
#AddressFamily any
ListenAddress 10.1.1.11
#ListenAddress ::

#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
#HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# Logging
#SyslogFacility AUTH
#LogLevel INFO

# Authentication:

LoginGraceTime 300
PermitRootLogin no
#StrictModes yes
MaxAuthTries 3
#MaxSessions

#PubkeyAuthentication yes

# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile      .ssh/authorized_keys

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
PasswordAuthentication no
#PermitEmptyPasswords no

# Change to no to disable s/key passwords
#KbdInteractiveAuthentication yes

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no
#KerberosUseKuserok yes

# GSSAPI options
#GSSAPIAuthentication no
#GSSAPICleanupCredentials yes
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no
#GSSAPIEnablek5users no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the KbdInteractiveAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via KbdInteractiveAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and KbdInteractiveAuthentication to 'no'.
# WARNING: 'UsePAM no' is not supported in RHEL and may cause several
# problems.
#UsePAM no

#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
#X11Forwarding no
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
#PrintMotd yes
#PrintLastLog yes
#TCPKeepAlive yes
#PermitUserEnvironment no
#Compression delayed
ClientAliveInterval 0
ClientAliveCountMax 2
#UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# override default of no subsystems
Subsystem       sftp    /usr/libexec/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#       X11Forwarding no
#       AllowTcpForwarding no
#       PermitTTY no
        #       ForceCommand cvs server
[tom@node1 ~]$ sudo systemctl reload sshd
```


**🌞 Installer fail2ban sur la machine**

```
[tom@node1 ~]$ sudo dnf install fail2ban -y
Rocky Linux 9 - BaseOS                                                             10 kB/s | 4.1 kB     00:00
Rocky Linux 9 - AppStream                                                          12 kB/s | 4.5 kB     00:00
Rocky Linux 9 - Extras                                                            6.3 kB/s | 2.9 kB     00:00
Dependencies resolved.
==================================================================================================================
 Package                                 Architecture      Version                     Repository            Size
==================================================================================================================
Installing:
 fail2ban                                noarch            1.1.0-6.el9                 epel                 9.3 k
Installing dependencies:
 checkpolicy                             x86_64            3.6-1.el9                   appstream            352 k
 esmtp                                   x86_64            1.2-19.el9                  epel                  52 k
 fail2ban-firewalld                      noarch            1.1.0-6.el9                 epel                 9.5 k
 fail2ban-selinux                        noarch            1.1.0-6.el9                 epel                  31 k
 fail2ban-sendmail                       noarch            1.1.0-6.el9                 epel                  12 k
 fail2ban-server                         noarch            1.1.0-6.el9                 epel                 465 k
 libesmtp                                x86_64            1.0.6-24.el9                epel                  66 k
 liblockfile                             x86_64            1.14-10.el9.0.1             baseos                27 k
 policycoreutils-python-utils            noarch            3.6-2.1.el9                 appstream             71 k
 python3-audit                           x86_64            3.1.5-1.el9                 appstream             82 k
 python3-distro                          noarch            1.5.0-7.el9                 appstream             36 k
 python3-libsemanage                     x86_64            3.6-2.1.el9_5               appstream             78 k
 python3-policycoreutils                 noarch            3.6-2.1.el9                 appstream            2.0 M
 python3-setools                         x86_64            4.4.4-1.el9                 baseos               551 k
 python3-setuptools                      noarch            53.0.0-13.el9               baseos               838 k

Transaction Summary
==================================================================================================================
Install  16 Packages

Total download size: 4.7 M
Installed size: 16 M
Downloading Packages:
(1/16): fail2ban-firewalld-1.1.0-6.el9.noarch.rpm                                  45 kB/s | 9.5 kB     00:00
(2/16): fail2ban-1.1.0-6.el9.noarch.rpm                                            30 kB/s | 9.3 kB     00:00
(3/16): fail2ban-selinux-1.1.0-6.el9.noarch.rpm                                   151 kB/s |  31 kB     00:00
(4/16): fail2ban-sendmail-1.1.0-6.el9.noarch.rpm                                   74 kB/s |  12 kB     00:00
(5/16): fail2ban-server-1.1.0-6.el9.noarch.rpm                                    292 kB/s | 465 kB     00:01
(6/16): esmtp-1.2-19.el9.x86_64.rpm                                                16 kB/s |  52 kB     00:03
(7/16): libesmtp-1.0.6-24.el9.x86_64.rpm                                           22 kB/s |  66 kB     00:02
(8/16): python3-setuptools-53.0.0-13.el9.noarch.rpm                               355 kB/s | 838 kB     00:02
(9/16): python3-distro-1.5.0-7.el9.noarch.rpm                                      78 kB/s |  36 kB     00:00
(10/16): python3-setools-4.4.4-1.el9.x86_64.rpm                                   218 kB/s | 551 kB     00:02
(11/16): liblockfile-1.14-10.el9.0.1.x86_64.rpm                                   9.0 kB/s |  27 kB     00:03
(12/16): python3-audit-3.1.5-1.el9.x86_64.rpm                                      47 kB/s |  82 kB     00:01
(13/16): checkpolicy-3.6-1.el9.x86_64.rpm                                         118 kB/s | 352 kB     00:02
(14/16): policycoreutils-python-utils-3.6-2.1.el9.noarch.rpm                      178 kB/s |  71 kB     00:00
(15/16): python3-policycoreutils-3.6-2.1.el9.noarch.rpm                           1.2 MB/s | 2.0 MB     00:01
(16/16): python3-libsemanage-3.6-2.1.el9_5.x86_64.rpm                              43 kB/s |  78 kB     00:01
------------------------------------------------------------------------------------------------------------------
Total                                                                             448 kB/s | 4.7 MB     00:10
Extra Packages for Enterprise Linux 9 - x86_64                                    1.3 MB/s | 1.6 kB     00:00
Importing GPG key 0x3228467C:
 Userid     : "Fedora (epel9) <epel@fedoraproject.org>"
 Fingerprint: FF8A D134 4597 106E CE81 3B91 8A38 72BF 3228 467C
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-9
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                          1/1
  Installing       : python3-setuptools-53.0.0-13.el9.noarch                                                 1/16
  Installing       : python3-setools-4.4.4-1.el9.x86_64                                                      2/16
  Installing       : python3-distro-1.5.0-7.el9.noarch                                                       3/16
  Installing       : python3-libsemanage-3.6-2.1.el9_5.x86_64                                                4/16
  Installing       : python3-audit-3.1.5-1.el9.x86_64                                                        5/16
  Installing       : checkpolicy-3.6-1.el9.x86_64                                                            6/16
  Installing       : python3-policycoreutils-3.6-2.1.el9.noarch                                              7/16
  Installing       : policycoreutils-python-utils-3.6-2.1.el9.noarch                                         8/16
  Running scriptlet: fail2ban-selinux-1.1.0-6.el9.noarch                                                     9/16
  Installing       : fail2ban-selinux-1.1.0-6.el9.noarch                                                     9/16
  Running scriptlet: fail2ban-selinux-1.1.0-6.el9.noarch                                                     9/16
libsemanage.semanage_direct_install_info: Overriding fail2ban module at lower priority 100 with module at priority 200.

  Installing       : fail2ban-server-1.1.0-6.el9.noarch                                                     10/16
  Running scriptlet: fail2ban-server-1.1.0-6.el9.noarch                                                     10/16
  Installing       : fail2ban-firewalld-1.1.0-6.el9.noarch                                                  11/16
  Installing       : liblockfile-1.14-10.el9.0.1.x86_64                                                     12/16
  Installing       : libesmtp-1.0.6-24.el9.x86_64                                                           13/16
  Installing       : esmtp-1.2-19.el9.x86_64                                                                14/16
  Running scriptlet: esmtp-1.2-19.el9.x86_64                                                                14/16
  Installing       : fail2ban-sendmail-1.1.0-6.el9.noarch                                                   15/16
  Installing       : fail2ban-1.1.0-6.el9.noarch                                                            16/16
  Running scriptlet: fail2ban-selinux-1.1.0-6.el9.noarch                                                    16/16
  Running scriptlet: fail2ban-1.1.0-6.el9.noarch                                                            16/16
  Verifying        : esmtp-1.2-19.el9.x86_64                                                                 1/16
  Verifying        : fail2ban-1.1.0-6.el9.noarch                                                             2/16
  Verifying        : fail2ban-firewalld-1.1.0-6.el9.noarch                                                   3/16
  Verifying        : fail2ban-selinux-1.1.0-6.el9.noarch                                                     4/16
  Verifying        : fail2ban-sendmail-1.1.0-6.el9.noarch                                                    5/16
  Verifying        : fail2ban-server-1.1.0-6.el9.noarch                                                      6/16
  Verifying        : libesmtp-1.0.6-24.el9.x86_64                                                            7/16
  Verifying        : python3-setuptools-53.0.0-13.el9.noarch                                                 8/16
  Verifying        : python3-setools-4.4.4-1.el9.x86_64                                                      9/16
  Verifying        : liblockfile-1.14-10.el9.0.1.x86_64                                                     10/16
  Verifying        : python3-distro-1.5.0-7.el9.noarch                                                      11/16
  Verifying        : checkpolicy-3.6-1.el9.x86_64                                                           12/16
  Verifying        : python3-audit-3.1.5-1.el9.x86_64                                                       13/16
  Verifying        : python3-policycoreutils-3.6-2.1.el9.noarch                                             14/16
  Verifying        : policycoreutils-python-utils-3.6-2.1.el9.noarch                                        15/16
  Verifying        : python3-libsemanage-3.6-2.1.el9_5.x86_64                                               16/16

Installed:
  checkpolicy-3.6-1.el9.x86_64                         esmtp-1.2-19.el9.x86_64
  fail2ban-1.1.0-6.el9.noarch                          fail2ban-firewalld-1.1.0-6.el9.noarch
  fail2ban-selinux-1.1.0-6.el9.noarch                  fail2ban-sendmail-1.1.0-6.el9.noarch
  fail2ban-server-1.1.0-6.el9.noarch                   libesmtp-1.0.6-24.el9.x86_64
  liblockfile-1.14-10.el9.0.1.x86_64                   policycoreutils-python-utils-3.6-2.1.el9.noarch
  python3-audit-3.1.5-1.el9.x86_64                     python3-distro-1.5.0-7.el9.noarch
  python3-libsemanage-3.6-2.1.el9_5.x86_64             python3-policycoreutils-3.6-2.1.el9.noarch
  python3-setools-4.4.4-1.el9.x86_64                   python3-setuptools-53.0.0-13.el9.noarch

Complete!
[tom@node1 ~]$
```


**🌞 Configurer fail2ban**
c'est la partie config SSH qui nous interesse : 

```
[tom@node1 ~]$ cd /etc/fail2ban/
[tom@node1 fail2ban]$ sudo cp jail.conf jail.local
[tom@node1 fail2ban]$ cd /home/tom
[tom@node1 ~]$ sudo nano /etc/fail2ban/jail.local


# SSH servers
#

[sshd]

# To use more aggressive sshd modes set filter parameter "mode" in jail.local:
# normal (default), ddos, extra or aggressive (combines all).
# See "tests/files/logs/sshd" or "filter.d/sshd.conf" for usage example and details.
#mode   = normal
enabled = true
filter = sshd
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s
bantime = 3600
maxretry = 7
findtime = 300

```



**🌞 Prouvez que fail2ban est effectif**


pour vérifier on réactive la connexion par mdp et on enleve notre clé de authorized_keys : 

```
[tom@node1 ~]$ sudo nano /etc/ssh/sshd_config
[tom@node1 ~]$ sudo systemctl reload sshd
[tom@node1 ~]$ sudo nano .ssh/authorized_keys
```

```
PS C:\Users\tomto> ssh tom@10.1.1.11 -p 2000
tom@10.1.1.11's password:
Permission denied, please try again.
tom@10.1.1.11's password:
Received disconnect from 10.1.1.11 port 2000:2: Too many authentication failures
Disconnected from 10.1.1.11 port 2000
PS C:\Users\tomto> ssh tom@10.1.1.11 -p 2000
tom@10.1.1.11's password:
Permission denied, please try again.
tom@10.1.1.11's password:
Received disconnect from 10.1.1.11 port 2000:2: Too many authentication failures
Disconnected from 10.1.1.11 port 2000
PS C:\Users\tomto> ssh tom@10.1.1.11 -p 2000
tom@10.1.1.11's password:
Permission denied, please try again.
tom@10.1.1.11's password:
Received disconnect from 10.1.1.11 port 2000:2: Too many authentication failures
Disconnected from 10.1.1.11 port 2000
PS C:\Users\tomto> ssh tom@10.1.1.11 -p 2000
tom@10.1.1.11's password:
Permission denied, please try again.
tom@10.1.1.11's password:
Received disconnect from 10.1.1.11 port 2000:2: Too many authentication failures
Disconnected from 10.1.1.11 port 2000
PS C:\Users\tomto> ssh tom@10.1.1.11 -p 2000
tom@10.1.1.11's password:
Permission denied, please try again.
tom@10.1.1.11's password:
Received disconnect from 10.1.1.11 port 2000:2: Too many authentication failures
Disconnected from 10.1.1.11 port 2000
PS C:\Users\tomto> ssh tom@10.1.1.11 -p 2000
tom@10.1.1.11's password:
Permission denied, please try again.
tom@10.1.1.11's password:
Received disconnect from 10.1.1.11 port 2000:2: Too many authentication failures
Disconnected from 10.1.1.11 port 2000
PS C:\Users\tomto> ssh tom@10.1.1.11 -p 2000
tom@10.1.1.11's password:
Permission denied, please try again.
tom@10.1.1.11's password:
Received disconnect from 10.1.1.11 port 2000:2: Too many authentication failures
Disconnected from 10.1.1.11 port 2000
PS C:\Users\tomto> ssh tom@10.1.1.11 -p 2000
tom@10.1.1.11's password:
Permission denied, please try again.
tom@10.1.1.11's password:
Received disconnect from 10.1.1.11 port 2000:2: Too many authentication failures
Disconnected from 10.1.1.11 port 2000
PS C:\Users\tomto>
```

```
[tom@node1 ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     24
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd + _COMM=sshd-session
`- Actions
   |- Currently banned: 1
   |- Total banned:     1
   `- Banned IP list:   10.1.1.1
[tom@node1 ~]$ fail2ban-client set sshd unbanip 10.1.1.1
2025-02-18 19:39:19,102 fail2ban                [2608]: ERROR   Permission denied to socket: /var/run/fail2ban/fail2ban.sock, (you must be root)
[tom@node1 ~]$ sudo fail2ban-client set sshd unbanip 10.1.1.1
1
[tom@node1 ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     24
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd + _COMM=sshd-session
`- Actions
   |- Currently banned: 0
   |- Total banned:     1
   `- Banned IP list:
[tom@node1 ~]$
```

**🌞 Ecrire le script harden.sh**



```
[tom@node1 ~]$ vi harden.sh


if ! systemctl is-active --quiet sshd; then
  echo "Le service OpenSSH n'est pas démarré. Démarrage en cours..."
  systemctl start sshd
  echo "Le service OpenSSH a été démarré."
else
  echo "Le service OpenSSH est déjà démarré."
fi
SSH_PORT=$(grep -E "^Port" /etc/ssh/sshd_config | awk '{print $2}')
if [ "$SSH_PORT" == "22" ]; then
  echo "Le port d'écoute SSH est toujours 22. Changement en cours..."
  NEW_PORT=2000
  sed -i "s/^#*Port 22/Port $NEW_PORT/" /etc/ssh/sshd_config
  systemctl restart sshd
  echo "Le port SSH a été changé à $NEW_PORT."
else
  echo "Le port SSH est déjà configuré sur $SSH_PORT."
fi
echo "Configuration de la sécurité OpenSSH..."
sed -i 's/^#*PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
sed -i 's/^#*PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config
systemctl restart sshd
echo "Configuration de la sécurité OpenSSH terminée."
if ! rpm -q fail2ban &> /dev/null; then
  echo "Fail2ban n'est pas installé. Installation en cours..."
  dnf install -y fail2ban
  systemctl start fail2ban
  systemctl enable fail2ban
  echo "Fail2ban a été installé et démarré."
  else
  echo "Fail2ban est déjà installé."
fi
if ! grep -q "\[sshd\]" /etc/fail2ban/jail.d/sshd.local 2>/dev/null; then
  echo "Configuration de fail2ban pour surveiller les logs SSH..."
  cat <<EOL > /etc/fail2ban/jail.d/sshd.local
[sshd]
enabled = true
filter = sshd
action = iptables[name=SSH, port=$SSH_PORT, protocol=tcp]
logpath = /var/log/securechmod +x harden.sh
maxretry = 7
findtime = 300
bantime = 3600
EOL
  systemctl restart fail2ban
  echo "Fail2ban a été configuré pour surveiller les logs SSH."
else
  echo "Fail2ban surveille déjà les logs SSH."
fi

echo "Le script a terminé avec succès."
                                         


[tom@node1 ~]$ chmod +x harden.sh
[tom@node1 ~]$ sudo ./harden.sh
Le service OpenSSH est déjà démarré.
Le port SSH est déjà configuré sur 2000.
Configuration de la sécurité OpenSSH...
Configuration de la sécurité OpenSSH terminée.
Fail2ban est déjà installé.
Fail2ban surveille déjà les logs SSH.
Le script a terminé avec succès.
[tom@node1 ~]$
```







