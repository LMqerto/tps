
# COMPTE RENDU TP TOM BERNARD

# Mettre en place la topologie dans GS3

**Configuration des machine**

config node1 réseau 1

```
[tom@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3a:6c:e5 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.11/24 brd 10.3.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe3a:6ce5/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:03:ad:77 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.119/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 507sec preferred_lft 507sec
    inet6 fe80::9061:9f40:950:c289/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
 TOM BERNARD
config node2 réseau 1

```
[tom@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:1a:44:21 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/24 brd 10.3.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe1a:4421/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3e:92:75 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.121/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 387sec preferred_lft 387sec
    inet6 fe80::613e:9516:5694:6ff3/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

```


config node1 réseau 2

```
 Oct  5 16:39:55 2023 from 192.168.56.1
[tom@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:8a:0c:37 brd ff:ff:ff:ff:ff:ff
    inet 10.3.2.11/24 brd 10.3.2.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe8a:c37/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d5:d5:f8 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.120/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 331sec preferred_lft 331sec
    inet6 fe80::9df3:a6ea:b7d2:bea/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

config node2 réseau2

```
[tom@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d9:16:b6 brd ff:ff:ff:ff:ff:ff
    inet 10.3.2.12/24 brd 10.3.2.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fed9:16b6/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:9a:0b:97 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.122/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 309sec preferred_lft 309sec
    inet6 fe80::e21c:ff1:ccde:6b/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

config routeur1

```
[tom@router1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:8c:0a:db brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.254/24 brd 10.3.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe8c:adb/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:54:43:8c brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.123/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 576sec preferred_lft 576sec
    inet6 fe80::c535:aeb0:6b6e:d53d/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a0:df:3c brd ff:ff:ff:ff:ff:ff
    inet 10.3.100.1/24 brd 10.3.100.255 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fea0:df3c/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f5:b7:5e brd ff:ff:ff:ff:ff:ff
    inet 192.168.159.135/24 brd 192.168.159.255 scope global dynamic noprefixroute enp0s10
       valid_lft 1476sec preferred_lft 1476sec
    inet6 fe80::2cb0:5d02:c596:8c8f/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

config routeur 2

```
[tom@router2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:bd:e1:ee brd ff:ff:ff:ff:ff:ff
    inet 10.3.2.254/24 brd 10.3.2.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:febd:e1ee/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:bc:5a:bd brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.124/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 488sec preferred_lft 488sec
    inet6 fe80::b7ff:ff6c:ddcb:2cde/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ba:45:a7 brd ff:ff:ff:ff:ff:ff
    inet 10.3.100.2/24 brd 10.3.100.255 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feba:45a7/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 08:00:27:da:c0:36 brd ff:ff:ff:ff:ff:ff
```

**ping machine réseau  entre elles**

ping intérieur réseau 1

```
[tom@node1 ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=8.92 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=1.25 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=1.61 ms
64 bytes from 10.3.1.12: icmp_seq=4 ttl=64 time=0.840 ms
^C
--- 10.3.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 0.840/3.154/8.915/3.337 ms
```

ping intérieur réseau 2

```
[tom@node1 ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=64 time=1.38 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=64 time=1.28 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=64 time=1.45 ms
64 bytes from 10.3.2.12: icmp_seq=4 ttl=64 time=1.39 ms
64 bytes from 10.3.2.12: icmp_seq=5 ttl=64 time=0.946 ms
^C
--- 10.3.2.12 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4007ms
rtt min/avg/max/mdev = 0.946/1.289/1.452/0.180 ms
```

ping intérieur réseau 3 

```
[tom@router1 ~]$ ping 10.3.100.2
PING 10.3.100.2 (10.3.100.2) 56(84) bytes of data.
64 bytes from 10.3.100.2: icmp_seq=1 ttl=64 time=1.59 ms
64 bytes from 10.3.100.2: icmp_seq=2 ttl=64 time=1.15 ms
64 bytes from 10.3.100.2: icmp_seq=3 ttl=64 time=1.17 ms
64 bytes from 10.3.100.2: icmp_seq=4 ttl=64 time=1.10 ms
64 bytes from 10.3.100.2: icmp_seq=5 ttl=64 time=1.26 ms
^C
--- 10.3.100.2 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4007ms
rtt min/avg/max/mdev = 1.100/1.255/1.589/0.174 ms
```
**le router1.tp3 doit avoir un accès internet normal**

```
[tom@router1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=128 time=15.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=128 time=15.8 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=128 time=15.9 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=128 time=15.5 ms
64 bytes from 8.8.8.8: icmp_seq=5 ttl=128 time=15.5 ms
^C
--- 8.8.8.8 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4007ms
rtt min/avg/max/mdev = 15.377/15.604/15.919/0.210 ms
```

```
[tom@router1 ~]$ ping efrei.fr
PING efrei.fr (51.255.68.208) 56(84) bytes of data.
64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=1 ttl=128 time=19.0 ms
64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=2 ttl=128 time=20.3 ms
64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=3 ttl=128 time=19.1 ms
64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=4 ttl=128 time=18.9 ms
64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=5 ttl=128 time=19.8 ms
^C64 bytes from 51.255.68.208: icmp_seq=6 ttl=128 time=21.3 ms

--- efrei.fr ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5011ms
rtt min/avg/max/mdev = 18.893/19.719/21.268/0.858 ms
```

**II. Routes routes routes**

 **Activer le routage sur les deux machines router**
router 1
```
[tom@router1 ~]$ sudo sysctl -w net.ipv4.ip_forward=1
[sudo] password for tom:
net.ipv4.ip_forward = 1
[tom@router1 ~]$ sudo firewall-cmd --add-masquerade
success
[tom@router1 ~]$ sudo firewall-cmd --add-masquerade --permanent
success
[tom@router1 ~]$
```

router 2
 ```
 [tom@router2 ~]$ sudo sysctl -w net.ipv4.ip_forward=1
net.ipv4.ip_forward = 1
[tom@router2 ~]$ sudo firewall-cmd --add-masquerade
success
[tom@router2 ~]$ sudo firewall-cmd --add-masquerade --permanent
success
[tom@router2 ~]$
 ```

**Mettre en place les routes locales**

Sur le routeur 1

```
[tom@router1 ~]$ sudo ip route add 10.3.2.11 via 10.3.100.2 dev enp0s9
[tom@router1 ~]$ sudo ip route add 10.3.2.12 via 10.3.100.2 dev enp0s9
[tom@router1 ~]$ ip route show
10.3.2.11 via 10.3.100.2 dev enp0s9
10.3.2.12 via 10.3.100.2 dev enp0s9
```

résultat :

```
[tom@node1 ~]$ ping 10.3.2.11
PING 10.3.2.11 (10.3.2.11) 56(84) bytes of data.
64 bytes from 10.3.2.11: icmp_seq=1 ttl=62 time=2.87 ms
64 bytes from 10.3.2.11: icmp_seq=2 ttl=62 time=3.02 ms
64 bytes from 10.3.2.11: icmp_seq=3 ttl=62 time=2.95 ms
64 bytes from 10.3.2.11: icmp_seq=4 ttl=62 time=2.73 ms
64 bytes from 10.3.2.11: icmp_seq=5 ttl=62 time=3.32 ms
64 bytes from 10.3.2.11: icmp_seq=6 ttl=62 time=1.15 ms
^C
--- 10.3.2.11 ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5007ms
rtt min/avg/max/mdev = 1.145/2.672/3.318/0.706 ms
[tom@node1 ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=62 time=2.48 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=62 time=3.07 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=62 time=2.65 ms
64 bytes from 10.3.2.12: icmp_seq=4 ttl=62 time=2.81 ms
64 bytes from 10.3.2.12: icmp_seq=5 ttl=62 time=3.11 ms
```

Sur le routeur 2

```
[tom@router2 ~]$ ip route show
10.3.1.11 via 10.3.100.1 dev enp0s9
10.3.1.12 via 10.3.100.1 dev enp0s9
[tom@router1 ~]$ sudo ip route add 10.3.1.11 via 10.3.100.1 dev enp0s9
[tom@router1 ~]$ sudo ip route add 10.3.1.12 via 10.3.100.1 dev enp0s9
```

résultat:
```
[tom@node1 ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=62 time=2.94 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=62 time=4.58 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=62 time=2.78 ms
64 bytes from 10.3.1.11: icmp_seq=4 ttl=62 time=3.47 ms
^C
--- 10.3.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 2.782/3.443/4.583/0.705 ms
[tom@node1 ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=62 time=3.31 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=62 time=2.79 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=62 time=2.17 ms
64 bytes from 10.3.1.12: icmp_seq=4 ttl=62 time=1.17 ms
^C
--- 10.3.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 1.166/2.357/3.312/0.798 ms
[tom@node1 ~]$
```

**Mettre en place les routes par défaut**

ping de node 1 réseau ver internet :

```
[tom@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=16.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=18.0 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=127 time=16.7 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=127 time=17.5 ms
64 bytes from 8.8.8.8: icmp_seq=5 ttl=127 time=15.9 ms
^C
--- 8.8.8.8 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4008ms
rtt min/avg/max/mdev = 15.934/16.913/17.986/0.724 ms
```

ping de node 1 réseau 2 vers internet :

pour cela on doit definir la route par defaut de router 2 vers routeur 1 en modifiant le fichier /etc/sysconfig/network-scripts/route-enp0s9 : 

```
default via 10.3.100.1 dev enp0s9
```
résultat
```
[tom@node1 ~]$ traceroute 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  _gateway (10.3.2.254)  0.987 ms  22.246 ms  22.233 ms
 2  * * *
 3  192.168.159.2 (192.168.159.2)  2.520 ms  2.497 ms  2.478 ms
```
# DHCP

**Setup de la machine dhcp.net1.tp3**

commande pour acceder a la configuration du DHCP : sudo nano /etc/dhcp/dhcpd.conf

**fichier configuration dhcp**
```
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page

default-lease-time 600;
max-lease-time 7200;

authoritative;
subnet 10.3.1.0 netmask 255.255.255.0 {
range 10.3.1.50 10.3.1.99;
option routers 10.3.1.254;
option subnet-mask 255.255.255.0;
option domain-name-servers 1.1.1.1;
}
```

**Preuve !**

config ip node 1 :

```
DEVICE=enp0s3

BOOTPROTO=dhcp
ONBOOT=yes

GATEWAY=10.3.1.254
DNS1=1.1.1.1
```
ip obtenue :
```
[tom@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3a:6c:e5 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.50/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 594sec preferred_lft 594sec
    inet6 fe80::a00:27ff:fe3a:6ce5/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:03:ad:77 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.119/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 492sec preferred_lft 492sec
    inet6 fe80::9061:9f40:950:c289/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
**table de routeage**

```
[tom@node1 ~]$ ip route show
default via 10.3.1.254 dev enp0s3 proto dhcp src 10.3.1.50 metric 102
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.50 metric 102
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.119 metric 101
```

commande dig pour montrer DNS : 

```
[tom@node1 ~]$ dig google.com

; <<>> DiG 9.16.23-RH <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 4314
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             212     IN      A       142.250.179.110

;; Query time: 16 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Fri Oct 06 09:18:02 CEST 2023
;; MSG SIZE  rcvd: 55

```

**prouvez que vous avez un accès internet normal avec un ping**

```
[tom@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=17.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=16.2 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=127 time=16.2 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=127 time=16.1 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 16.074/16.446/17.268/0.478 ms
```

# SERVER WEB

**Installation du serveur web NGINX**

```
[tom@web ~]$ sudo dnf install nginx
[sudo] password for tom:
Rocky Linux 9 - BaseOS                                                                  8.4 kB/s | 4.1 kB     00:00
Rocky Linux 9 - BaseOS                                                                  1.0 MB/s | 1.9 MB     00:01
Rocky Linux 9 - AppStream                                                                12 kB/s | 4.5 kB     00:00
Rocky Linux 9 - AppStream                                                               3.0 MB/s | 7.1 MB     00:02
Rocky Linux 9 - Extras                                                                  7.5 kB/s | 2.9 kB     00:00
Rocky Linux 9 - Extras                                                                   11 kB/s |  11 kB     00:01
Dependencies resolved.
========================================================================================================================
 Package                          Architecture          Version                          Repository                Size
========================================================================================================================
Installing:
 nginx                            x86_64                1:1.20.1-14.el9                  appstream                 38 k
Installing dependencies:
 nginx-core                       x86_64                1:1.20.1-14.el9                  appstream                567 k
 nginx-filesystem                 noarch                1:1.20.1-14.el9                  appstream                 10 k
 rocky-logos-httpd                noarch                90.14-1.el9                      appstream                 24 k

Transaction Summary
========================================================================================================================
Install  4 Packages

Total download size: 640 k
Installed size: 1.8 M
Is this ok [y/N]: y
Downloading Packages:
(1/4): nginx-filesystem-1.20.1-14.el9.noarch.rpm                                         44 kB/s |  10 kB     00:00
(2/4): nginx-1.20.1-14.el9.x86_64.rpm                                                   134 kB/s |  38 kB     00:00
(3/4): rocky-logos-httpd-90.14-1.el9.noarch.rpm                                          80 kB/s |  24 kB     00:00
(4/4): nginx-core-1.20.1-14.el9.x86_64.rpm                                              1.0 MB/s | 567 kB     00:00
------------------------------------------------------------------------------------------------------------------------
Total                                                                                   610 kB/s | 640 kB     00:01
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Running scriptlet: nginx-filesystem-1:1.20.1-14.el9.noarch                                                        1/4
  Installing       : nginx-filesystem-1:1.20.1-14.el9.noarch                                                        1/4
  Installing       : nginx-core-1:1.20.1-14.el9.x86_64                                                              2/4
  Installing       : rocky-logos-httpd-90.14-1.el9.noarch                                                           3/4
  Installing       : nginx-1:1.20.1-14.el9.x86_64                                                                   4/4
  Running scriptlet: nginx-1:1.20.1-14.el9.x86_64                                                                   4/4
  Verifying        : rocky-logos-httpd-90.14-1.el9.noarch                                                           1/4
  Verifying        : nginx-filesystem-1:1.20.1-14.el9.noarch                                                        2/4
  Verifying        : nginx-1:1.20.1-14.el9.x86_64                                                                   3/4
  Verifying        : nginx-core-1:1.20.1-14.el9.x86_64                                                              4/4

Installed:
  nginx-1:1.20.1-14.el9.x86_64           nginx-core-1:1.20.1-14.el9.x86_64   nginx-filesystem-1:1.20.1-14.el9.noarch
  rocky-logos-httpd-90.14-1.el9.noarch

Complete!
```

**création d'une bete page html**
**création du dossier**

```
[tom@web ~]$ sudo mkdir /var/www/
[tom@web ~]$ sudo mkdir /var/www/efrei_site_nul
```
création index.html et changement propriétaire

```
[tom@web ~]$ sudo chown nginx /var/www/efrei_site_nul/
[tom@web~]$ sudo nano /var/www/efrei_site_nul/index.html
[tom@web ~]$ sudo chown nginx /var/www/efrei_site_nul/index.html
```
**Config de NGINX**

```
[tom@web ~]$ sudo nano /etc/nginx/conf.d/web.net2.tp3.conf

server {
      # on indique le nom d'hôte du serveur
      server_name   web.net2.tp3;

      # on précise sur quelle IP et quel port on veut que le site soit dispo
      listen        10.3.2.101:80;

      location      / {
          # on indique l'endroit où se trouve notre racine web
          root      /var/www/efrei_site_nul;

          # et on indique le nom de la page d'accueil, pour pas que le client ait besoin de le préciser explicitement
          index index.html;
      }
  }

```

**FIREWALL**

ouvrir le port et le prouver :

```
sudo firewall-cmd --add-port=80/tcp --permanent
success
[tom@web ~]$ sudo firewall-cmd --reload
success
[tom@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[tom@web ~]$
```

**Démmarer le service Nginx**

```
[tom@web ~]$  sudo systemctl start nginx
[tom@web ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[tom@web ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; preset: disabled)
     Active: active (running) since Fri 2023-10-06 10:16:35 CEST; 23s ago
   Main PID: 11040 (nginx)
      Tasks: 2 (limit: 11052)
     Memory: 2.0M
        CPU: 16ms
     CGroup: /system.slice/nginx.service
             ├─11040 "nginx: master process /usr/sbin/nginx"
             └─11041 "nginx: worker process"

Oct 06 10:16:35 node1.net2.tp3 systemd[1]: Starting The nginx HTTP and reverse proxy server...
Oct 06 10:16:35 node1.net2.tp3 nginx[11038]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Oct 06 10:16:35 node1.net2.tp3 nginx[11038]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Oct 06 10:16:35 node1.net2.tp3 systemd[1]: Started The nginx HTTP and reverse proxy server.
```



**Test local**

```
[tom@web efrei_site_nul]$ curl http://10.3.2.101
coucou efrei
```

**Test depuis client 1**

```
[tom@web ~]$ curl http://10.3.2.101
coucou efrei
```
**Changement de hosts** 
```
[tom@web ~]$ sudo nano /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.3.2.101 web.net2.tp3
```
**test avec nom domaine**
```
[tom@web ~]$ curl http://web.net2.tp3
coucou efrei
[tom@web ~]$
```

# SERVER DNS

**INSTALL**

mise a jour de la machine 

```
[tom@dns ~]$ sudo dnf update -y
[sudo] password for tom:
Rocky Linux 9 - BaseOS                                                                   10 kB/s | 4.1 kB     00:00
Rocky Linux 9 - BaseOS                                                                  1.9 MB/s | 1.9 MB     00:00
Rocky Linux 9 - AppStream                                                                14 kB/s | 4.5 kB     00:00
Rocky Linux 9 - AppStream                                                               3.8 MB/s | 7.1 MB     00:01
Rocky Linux 9 - Extras                                                                  8.6 kB/s | 2.9 kB     00:00
Rocky Linux 9 - Extras                                                                   28 kB/s |  11 kB     00:00
Dependencies resolved.
========================================================================================================================
 Package                                 Architecture        Version                       Repository              Size
========================================================================================================================
Upgrading:
 glibc                                   x86_64              2.34-60.el9_2.7               baseos                 1.9 M
 glibc-common                            x86_64              2.34-60.el9_2.7               baseos                 306 k
 glibc-gconv-extra                       x86_64              2.34-60.el9_2.7               baseos                 1.6 M
 glibc-langpack-en                       x86_64              2.34-60.el9_2.7               baseos                 564 k
 python-unversioned-command              noarch              3.9.16-1.el9_2.2              appstream              8.6 k
 python3                                 x86_64              3.9.16-1.el9_2.2              baseos                  25 k
 python3-libs                            x86_64              3.9.16-1.el9_2.2              baseos                 7.3 M

Transaction Summary
========================================================================================================================
Upgrade  7 Packages

Total download size: 12 M
Downloading Packages:
(1/7): glibc-langpack-en-2.34-60.el9_2.7.x86_64.rpm                                     1.4 MB/s | 564 kB     00:00
(2/7): python3-3.9.16-1.el9_2.2.x86_64.rpm                                               49 kB/s |  25 kB     00:00
(3/7): glibc-common-2.34-60.el9_2.7.x86_64.rpm                                          534 kB/s | 306 kB     00:00
(4/7): glibc-gconv-extra-2.34-60.el9_2.7.x86_64.rpm                                     1.2 MB/s | 1.6 MB     00:01
[MIRROR] python-unversioned-command-3.9.16-1.el9_2.2.noarch.rpm: Status code: 404 for https://rocky-linux-europe-west9.production.gcp.mirrors.ctrliq.cloud/pub/rocky//9.2/AppStream/x86_64/os/Packages/p/python-unversioned-command-3.9.16-1.el9_2.2.noarch.rpm (IP: 34.149.52.168)
(5/7): python-unversioned-command-3.9.16-1.el9_2.2.noarch.rpm                            31 kB/s | 8.6 kB     00:00
(6/7): glibc-2.34-60.el9_2.7.x86_64.rpm                                                 1.7 MB/s | 1.9 MB     00:01
(7/7): python3-libs-3.9.16-1.el9_2.2.x86_64.rpm                                         1.4 MB/s | 7.3 MB     00:05
------------------------------------------------------------------------------------------------------------------------
Total                                                                                   2.0 MB/s |  12 MB     00:05
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Upgrading        : glibc-gconv-extra-2.34-60.el9_2.7.x86_64                                                      1/14
  Running scriptlet: glibc-gconv-extra-2.34-60.el9_2.7.x86_64                                                      1/14
  Upgrading        : glibc-common-2.34-60.el9_2.7.x86_64                                                           2/14
  Running scriptlet: glibc-2.34-60.el9_2.7.x86_64                                                                  3/14
  Upgrading        : glibc-2.34-60.el9_2.7.x86_64                                                                  3/14
  Running scriptlet: glibc-2.34-60.el9_2.7.x86_64                                                                  3/14
  Upgrading        : glibc-langpack-en-2.34-60.el9_2.7.x86_64                                                      4/14
  Upgrading        : python-unversioned-command-3.9.16-1.el9_2.2.noarch                                            5/14
  Upgrading        : python3-3.9.16-1.el9_2.2.x86_64                                                               6/14
  Upgrading        : python3-libs-3.9.16-1.el9_2.2.x86_64                                                          7/14
  Cleanup          : python3-libs-3.9.16-1.el9_2.1.x86_64                                                          8/14
  Cleanup          : python3-3.9.16-1.el9_2.1.x86_64                                                               9/14
  Cleanup          : python-unversioned-command-3.9.16-1.el9_2.1.noarch                                           10/14
  Cleanup          : glibc-langpack-en-2.34-60.el9.x86_64                                                         11/14
  Cleanup          : glibc-common-2.34-60.el9.x86_64                                                              12/14
  Cleanup          : glibc-gconv-extra-2.34-60.el9.x86_64                                                         13/14
  Running scriptlet: glibc-gconv-extra-2.34-60.el9.x86_64                                                         13/14
  Cleanup          : glibc-2.34-60.el9.x86_64                                                                     14/14
  Running scriptlet: glibc-2.34-60.el9.x86_64                                                                     14/14
  Verifying        : python3-libs-3.9.16-1.el9_2.2.x86_64                                                          1/14
  Verifying        : python3-libs-3.9.16-1.el9_2.1.x86_64                                                          2/14
  Verifying        : python3-3.9.16-1.el9_2.2.x86_64                                                               3/14
  Verifying        : python3-3.9.16-1.el9_2.1.x86_64                                                               4/14
  Verifying        : glibc-langpack-en-2.34-60.el9_2.7.x86_64                                                      5/14
  Verifying        : glibc-langpack-en-2.34-60.el9.x86_64                                                          6/14
  Verifying        : glibc-gconv-extra-2.34-60.el9_2.7.x86_64                                                      7/14
  Verifying        : glibc-gconv-extra-2.34-60.el9.x86_64                                                          8/14
  Verifying        : glibc-common-2.34-60.el9_2.7.x86_64                                                           9/14
  Verifying        : glibc-common-2.34-60.el9.x86_64                                                              10/14
  Verifying        : glibc-2.34-60.el9_2.7.x86_64                                                                 11/14
  Verifying        : glibc-2.34-60.el9.x86_64                                                                     12/14
  Verifying        : python-unversioned-command-3.9.16-1.el9_2.2.noarch                                           13/14
  Verifying        : python-unversioned-command-3.9.16-1.el9_2.1.noarch                                           14/14

Upgraded:
  glibc-2.34-60.el9_2.7.x86_64                                    glibc-common-2.34-60.el9_2.7.x86_64
  glibc-gconv-extra-2.34-60.el9_2.7.x86_64                        glibc-langpack-en-2.34-60.el9_2.7.x86_64
  python-unversioned-command-3.9.16-1.el9_2.2.noarch              python3-3.9.16-1.el9_2.2.x86_64
  python3-libs-3.9.16-1.el9_2.2.x86_64

Complete!
```

**installation du server dns**

```
[tom@dns ~]$  sudo dnf install -y bind bind-utils
Last metadata expiration check: 0:01:14 ago on Fri 06 Oct 2023 11:08:20 AM CEST.
Dependencies resolved.
========================================================================================================================
 Package                                 Architecture      Version                           Repository            Size
========================================================================================================================
Installing:
 bind                                    x86_64            32:9.16.23-11.el9_2.1             appstream            488 k
 bind-utils                              x86_64            32:9.16.23-11.el9_2.1             appstream            199 k
Installing dependencies:
 bind-dnssec-doc                         noarch            32:9.16.23-11.el9_2.1             appstream             45 k
 bind-libs                               x86_64            32:9.16.23-11.el9_2.1             appstream            1.2 M
 bind-license                            noarch            32:9.16.23-11.el9_2.1             appstream             13 k
 checkpolicy                             x86_64            3.5-1.el9                         appstream            345 k
 fstrm                                   x86_64            0.6.1-3.el9                       appstream             27 k
 libmaxminddb                            x86_64            1.5.2-3.el9                       appstream             33 k
 libuv                                   x86_64            1:1.42.0-1.el9                    appstream            149 k
 policycoreutils-python-utils            noarch            3.5-1.el9                         appstream             71 k
 protobuf-c                              x86_64            1.3.3-12.el9                      baseos                35 k
 python3-audit                           x86_64            3.0.7-103.el9                     appstream             83 k
 python3-bind                            noarch            32:9.16.23-11.el9_2.1             appstream             61 k
 python3-distro                          noarch            1.5.0-7.el9                       appstream             36 k
 python3-libsemanage                     x86_64            3.5-1.el9                         appstream             79 k
 python3-ply                             noarch            3.11-14.el9.0.1                   baseos               103 k
 python3-policycoreutils                 noarch            3.5-1.el9                         appstream            2.0 M
 python3-setools                         x86_64            4.4.1-1.el9                       baseos               543 k
 python3-setuptools                      noarch            53.0.0-12.el9                     baseos               839 k
Installing weak dependencies:
 bind-dnssec-utils                       x86_64            32:9.16.23-11.el9_2.1             appstream            113 k

Transaction Summary
========================================================================================================================
Install  20 Packages

Total download size: 6.5 M
Installed size: 21 M
Downloading Packages:
(1/20): protobuf-c-1.3.3-12.el9.x86_64.rpm                                              248 kB/s |  35 kB     00:00
(2/20): python3-ply-3.11-14.el9.0.1.noarch.rpm                                          641 kB/s | 103 kB     00:00
(3/20): python3-distro-1.5.0-7.el9.noarch.rpm                                           252 kB/s |  36 kB     00:00
(4/20): checkpolicy-3.5-1.el9.x86_64.rpm                                                777 kB/s | 345 kB     00:00
(5/20): python3-setools-4.4.1-1.el9.x86_64.rpm                                          581 kB/s | 543 kB     00:00
(6/20): python3-setuptools-53.0.0-12.el9.noarch.rpm                                     713 kB/s | 839 kB     00:01
(7/20): python3-audit-3.0.7-103.el9.x86_64.rpm                                          160 kB/s |  83 kB     00:00
(8/20): policycoreutils-python-utils-3.5-1.el9.noarch.rpm                               281 kB/s |  71 kB     00:00
(9/20): bind-utils-9.16.23-11.el9_2.1.x86_64.rpm                                        1.4 MB/s | 199 kB     00:00
(10/20): bind-dnssec-utils-9.16.23-11.el9_2.1.x86_64.rpm                                315 kB/s | 113 kB     00:00
(11/20): python3-policycoreutils-3.5-1.el9.noarch.rpm                                   1.7 MB/s | 2.0 MB     00:01
(12/20): python3-bind-9.16.23-11.el9_2.1.noarch.rpm                                     298 kB/s |  61 kB     00:00
(13/20): bind-license-9.16.23-11.el9_2.1.noarch.rpm                                     312 kB/s |  13 kB     00:00
(14/20): bind-libs-9.16.23-11.el9_2.1.x86_64.rpm                                        1.2 MB/s | 1.2 MB     00:01
(15/20): bind-9.16.23-11.el9_2.1.x86_64.rpm                                             814 kB/s | 488 kB     00:00
(16/20): bind-dnssec-doc-9.16.23-11.el9_2.1.noarch.rpm                                  316 kB/s |  45 kB     00:00
(17/20): libmaxminddb-1.5.2-3.el9.x86_64.rpm                                            736 kB/s |  33 kB     00:00
(18/20): fstrm-0.6.1-3.el9.x86_64.rpm                                                   419 kB/s |  27 kB     00:00
(19/20): libuv-1.42.0-1.el9.x86_64.rpm                                                  886 kB/s | 149 kB     00:00
(20/20): python3-libsemanage-3.5-1.el9.x86_64.rpm                                       198 kB/s |  79 kB     00:00
------------------------------------------------------------------------------------------------------------------------
Total                                                                                   1.9 MB/s | 6.5 MB     00:03
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : bind-license-32:9.16.23-11.el9_2.1.noarch                                                     1/20
  Installing       : protobuf-c-1.3.3-12.el9.x86_64                                                                2/20
  Installing       : fstrm-0.6.1-3.el9.x86_64                                                                      3/20
  Installing       : libmaxminddb-1.5.2-3.el9.x86_64                                                               4/20
  Installing       : libuv-1:1.42.0-1.el9.x86_64                                                                   5/20
  Installing       : bind-libs-32:9.16.23-11.el9_2.1.x86_64                                                        6/20
  Installing       : bind-utils-32:9.16.23-11.el9_2.1.x86_64                                                       7/20
  Installing       : python3-setuptools-53.0.0-12.el9.noarch                                                       8/20
  Installing       : python3-setools-4.4.1-1.el9.x86_64                                                            9/20
  Installing       : python3-distro-1.5.0-7.el9.noarch                                                            10/20
  Installing       : bind-dnssec-doc-32:9.16.23-11.el9_2.1.noarch                                                 11/20
  Installing       : python3-libsemanage-3.5-1.el9.x86_64                                                         12/20
  Installing       : python3-audit-3.0.7-103.el9.x86_64                                                           13/20
  Installing       : checkpolicy-3.5-1.el9.x86_64                                                                 14/20
  Installing       : python3-policycoreutils-3.5-1.el9.noarch                                                     15/20
  Installing       : policycoreutils-python-utils-3.5-1.el9.noarch                                                16/20
  Installing       : python3-ply-3.11-14.el9.0.1.noarch                                                           17/20
  Installing       : python3-bind-32:9.16.23-11.el9_2.1.noarch                                                    18/20
  Installing       : bind-dnssec-utils-32:9.16.23-11.el9_2.1.x86_64                                               19/20
  Running scriptlet: bind-32:9.16.23-11.el9_2.1.x86_64                                                            20/20
  Installing       : bind-32:9.16.23-11.el9_2.1.x86_64                                                            20/20
  Running scriptlet: bind-32:9.16.23-11.el9_2.1.x86_64                                                            20/20
  Verifying        : python3-setuptools-53.0.0-12.el9.noarch                                                       1/20
  Verifying        : python3-setools-4.4.1-1.el9.x86_64                                                            2/20
  Verifying        : protobuf-c-1.3.3-12.el9.x86_64                                                                3/20
  Verifying        : python3-ply-3.11-14.el9.0.1.noarch                                                            4/20
  Verifying        : python3-distro-1.5.0-7.el9.noarch                                                             5/20
  Verifying        : checkpolicy-3.5-1.el9.x86_64                                                                  6/20
  Verifying        : python3-audit-3.0.7-103.el9.x86_64                                                            7/20
  Verifying        : python3-policycoreutils-3.5-1.el9.noarch                                                      8/20
  Verifying        : policycoreutils-python-utils-3.5-1.el9.noarch                                                 9/20
  Verifying        : bind-utils-32:9.16.23-11.el9_2.1.x86_64                                                      10/20
  Verifying        : bind-libs-32:9.16.23-11.el9_2.1.x86_64                                                       11/20
  Verifying        : bind-dnssec-utils-32:9.16.23-11.el9_2.1.x86_64                                               12/20
  Verifying        : bind-32:9.16.23-11.el9_2.1.x86_64                                                            13/20
  Verifying        : python3-bind-32:9.16.23-11.el9_2.1.noarch                                                    14/20
  Verifying        : bind-license-32:9.16.23-11.el9_2.1.noarch                                                    15/20
  Verifying        : bind-dnssec-doc-32:9.16.23-11.el9_2.1.noarch                                                 16/20
  Verifying        : libuv-1:1.42.0-1.el9.x86_64                                                                  17/20
  Verifying        : python3-libsemanage-3.5-1.el9.x86_64                                                         18/20
  Verifying        : libmaxminddb-1.5.2-3.el9.x86_64                                                              19/20
  Verifying        : fstrm-0.6.1-3.el9.x86_64                                                                     20/20

Installed:
  bind-32:9.16.23-11.el9_2.1.x86_64                           bind-dnssec-doc-32:9.16.23-11.el9_2.1.noarch
  bind-dnssec-utils-32:9.16.23-11.el9_2.1.x86_64              bind-libs-32:9.16.23-11.el9_2.1.x86_64
  bind-license-32:9.16.23-11.el9_2.1.noarch                   bind-utils-32:9.16.23-11.el9_2.1.x86_64
  checkpolicy-3.5-1.el9.x86_64                                fstrm-0.6.1-3.el9.x86_64
  libmaxminddb-1.5.2-3.el9.x86_64                             libuv-1:1.42.0-1.el9.x86_64
  policycoreutils-python-utils-3.5-1.el9.noarch               protobuf-c-1.3.3-12.el9.x86_64
  python3-audit-3.0.7-103.el9.x86_64                          python3-bind-32:9.16.23-11.el9_2.1.noarch
  python3-distro-1.5.0-7.el9.noarch                           python3-libsemanage-3.5-1.el9.x86_64
  python3-ply-3.11-14.el9.0.1.noarch                          python3-policycoreutils-3.5-1.el9.noarch
  python3-setools-4.4.1-1.el9.x86_64                          python3-setuptools-53.0.0-12.el9.noarch

Complete!
```

**CONFIG**

configuration fichier principal :

```
[tom@dns ~]$  sudo nano /etc/named.conf

options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
[...] # je zappe les lignes pas importantes, vous pouvez les laisser dans votre fichier
        allow-query     { localhost; any; };
        allow-query-cache { localhost; any; };

        recursion yes; # cette ligne autorise la recursion, voir la note en dessous de cette conf
[...]
# référence vers notre fichier de zone
zone "net2.tp3" IN {
     type master;
     file "net2.tp3.db";
     allow-update { none; };
     allow-query {any; };
};
# référence vers notre fichier de zone inverse (notez la notation à l'envers de l'IP)
zone "2.3.10.in-addr.arpa" IN {
     type master;
     file "net2.tp3.rev";
     allow-update { none; };
     allow-query { any; };
};
```

**modification fichier de zones**

```
sudo nano /var/named/net2.tp3.db

$TTL 86400
@ IN SOA dns.net2.tp3. admin.net2.tp3. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.net2.tp3.

; Enregistrements DNS pour faire correspondre des noms à des IPs
dns        IN A 10.3.2.102
web        IN A 10.3.2.101




sudo nano /var/named/net2.tp3/rev

$TTL 86400
@ IN SOA dns.net2.tp3. admin.net2.tp3. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.net2.tp3.

;Reverse lookup for Name Server
102   IN PTR dns.net2.tp3.
101   IN PTR web.net2.tp3.

```
 
 **demarrage du service DNS**

 ```
 [tom@dns ~]$ sudo systemctl start named
[tom@dns ~]$ sudo systemctl enable named
Created symlink /etc/systemd/system/multi-user.target.wants/named.service → /usr/lib/systemd/system/named.service.
[tom@dns ~]$ sudo systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; preset: disabled)
     Active: active (running) since Fri 2023-10-06 11:34:44 CEST; 14s ago
   Main PID: 12528 (named)
      Tasks: 4 (limit: 11052)
     Memory: 16.8M
        CPU: 29ms
     CGroup: /system.slice/named.service
             └─12528 /usr/sbin/named -u named -c /etc/named.conf

Oct 06 11:34:44 dns.net2.tp3 named[12528]: zone localhost.localdomain/IN: loaded serial 0
Oct 06 11:34:44 dns.net2.tp3 named[12528]: all zones loaded
Oct 06 11:34:44 dns.net2.tp3 systemd[1]: Started Berkeley Internet Name Domain (DNS).
Oct 06 11:34:44 dns.net2.tp3 named[12528]: running
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:7fd::1#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:500:200::b#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:7fe::53#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:500:9f::42#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: managed-keys-zone: Initializing automatic trust anchor management for zone '.'; DNSKEY ID 20326 is now trusted, waiving the normal 30-day waiting period.
Oct 06 11:34:44 dns.net2.tp3 named[12528]: resolver priming query complete
[tom@dns ~]$ sudo journalctl -xe -u named
Oct 06 11:34:44 dns.net2.tp3 named[12528]: zone 2.3.10.in-addr.arpa/IN: loaded serial 2019061800
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:503:ba3e::2:30#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './NS/IN': 2001:503:ba3e::2:30#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:500:9f::42#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './NS/IN': 2001:500:9f::42#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:500:200::b#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './NS/IN': 2001:500:200::b#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:dc3::35#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './NS/IN': 2001:dc3::35#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:500:a8::e#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './NS/IN': 2001:500:a8::e#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:503:c27::2:30#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './NS/IN': 2001:503:c27::2:30#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:500:2d::d#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './NS/IN': 2001:500:2d::d#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:500:1::53#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './NS/IN': 2001:500:1::53#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:500:2f::f#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './NS/IN': 2001:500:2f::f#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:7fe::53#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './NS/IN': 2001:7fe::53#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:7fd::1#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './NS/IN': 2001:7fd::1#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:500:2::c#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './NS/IN': 2001:500:2::c#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:500:12::d0d#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './NS/IN': 2001:500:12::d0d#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: zone 1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa/IN: loaded serial 0
Oct 06 11:34:44 dns.net2.tp3 named[12528]: zone 1.0.0.127.in-addr.arpa/IN: loaded serial 0
Oct 06 11:34:44 dns.net2.tp3 named[12528]: zone localhost/IN: loaded serial 0
Oct 06 11:34:44 dns.net2.tp3 named[12528]: zone net2.tp3/IN: loaded serial 2019061800
Oct 06 11:34:44 dns.net2.tp3 named[12528]: zone localhost.localdomain/IN: loaded serial 0
Oct 06 11:34:44 dns.net2.tp3 named[12528]: all zones loaded
Oct 06 11:34:44 dns.net2.tp3 systemd[1]: Started Berkeley Internet Name Domain (DNS).
░░ Subject: A start job for unit named.service has finished successfully
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ A start job for unit named.service has finished successfully.
░░
░░ The job identifier is 2383.
Oct 06 11:34:44 dns.net2.tp3 named[12528]: running
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:7fd::1#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:500:200::b#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:7fe::53#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: network unreachable resolving './DNSKEY/IN': 2001:500:9f::42#53
Oct 06 11:34:44 dns.net2.tp3 named[12528]: managed-keys-zone: Initializing automatic trust anchor management for zone '.'; DNSKEY ID 20326 is now trusted, waiving the normal 30-day waiting period.
Oct 06 11:34:44 dns.net2.tp3 named[12528]: resolver priming query complete
 ```

**ouverture du port du firewall**

```
[tom@dns ~]$ sudo firewall-cmd --add-port=53/udp --permanent
success
[tom@dns ~]$ sudo firewall-cmd --reload
success
[tom@dns ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 53/udp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

**TEST**

Depuis la machine clientes du réseau 1 :

```
[tom@node1 ~]$ dig web.net2.tp3 +short
10.3.2.101
[tom@node1 ~]$ curl http://web.net2.tp3
coucou efrei
```

**DHCP my old FRIEND**

changement du dns dans le dhcp

```
sudo nano /etc/dhcp/dhcpd.conf

#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page

default-lease-time 600;
max-lease-time 7200;

authoritative;
subnet 10.3.1.0 netmask 255.255.255.0 {
range 10.3.1.50 10.3.1.99;
option routers 10.3.1.254;
option subnet-mask 255.255.255.0;
option domain-name-servers 10.3.2.102;
}

 sudo systemctl restart dhcpd

```
il faut maintenant également redemmarer le réseau de client 1 :

```
[tom@node1 ~]$ sudo nmcli con reload enp0s3
[sudo] password for tom:
[tom@node1 ~]$ sudo nmcli con up enp0s3
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/9)

[tom@node1 ~]$ dig web.net2.tp3 +short
10.3.2.101
```


