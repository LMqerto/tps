# compte rendu TP1 Tom Bernard

# 1 INSTALL

**istallation de docker sur la machine:**

1 : on ajoute le répertoire docker
```
[tom@efrei-xmg4agau1 ~]$ sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
[sudo] password for tom:
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo
```

2 : on met a jour le repertoire :

```
[tom@efrei-xmg4agau1 ~]$ sudo dnf update
Docker CE Stable - x86_64                                                               141 kB/s |  32 kB     00:00
Rocky Linux 9 - BaseOS                                                                   15 kB/s | 4.1 kB     00:00
Rocky Linux 9 - BaseOS                                                                  2.0 MB/s | 1.9 MB     00:00
Rocky Linux 9 - AppStream                                                                16 kB/s | 4.5 kB     00:00
Rocky Linux 9 - AppStream                                                               2.7 MB/s | 7.1 MB     00:02
Rocky Linux 9 - Extras                                                                   12 kB/s | 2.9 kB     00:00
Rocky Linux 9 - Extras                                                                   17 kB/s |  11 kB     00:00
Dependencies resolved.
========================================================================================================================
 Package                                Architecture       Version                          Repository             Size
========================================================================================================================
Upgrading:
 curl                                   x86_64             7.76.1-23.el9_2.4                baseos                294 k
 glibc                                  x86_64             2.34-60.el9_2.7                  baseos                1.9 M
 glibc-common                           x86_64             2.34-60.el9_2.7                  baseos                306 k
 glibc-gconv-extra                      x86_64             2.34-60.el9_2.7                  baseos                1.6 M
 glibc-langpack-en                      x86_64             2.34-60.el9_2.7                  baseos                564 k
 libcurl                                x86_64             7.76.1-23.el9_2.4                baseos                283 k
 libnghttp2                             x86_64             1.43.0-5.el9_2.1                 baseos                 72 k
 python-unversioned-command             noarch             3.9.16-1.el9_2.2                 appstream             8.6 k
 python3                                x86_64             3.9.16-1.el9_2.2                 baseos                 25 k
 python3-libs                           x86_64             3.9.16-1.el9_2.2                 baseos                7.3 M

Transaction Summary
========================================================================================================================
Upgrade  10 Packages

Total download size: 12 M
Is this ok [y/N]: y
Downloading Packages:
(1/10): libnghttp2-1.43.0-5.el9_2.1.x86_64.rpm                                          440 kB/s |  72 kB     00:00
(2/10): python3-3.9.16-1.el9_2.2.x86_64.rpm                                             150 kB/s |  25 kB     00:00
(3/10): curl-7.76.1-23.el9_2.4.x86_64.rpm                                               2.4 MB/s | 294 kB     00:00
(4/10): libcurl-7.76.1-23.el9_2.4.x86_64.rpm                                            1.9 MB/s | 283 kB     00:00
(5/10): glibc-langpack-en-2.34-60.el9_2.7.x86_64.rpm                                    4.1 MB/s | 564 kB     00:00
(6/10): glibc-common-2.34-60.el9_2.7.x86_64.rpm                                         3.1 MB/s | 306 kB     00:00
(7/10): glibc-2.34-60.el9_2.7.x86_64.rpm                                                3.8 MB/s | 1.9 MB     00:00
(8/10): python-unversioned-command-3.9.16-1.el9_2.2.noarch.rpm                          328 kB/s | 8.6 kB     00:00
(9/10): glibc-gconv-extra-2.34-60.el9_2.7.x86_64.rpm                                    2.1 MB/s | 1.6 MB     00:00
(10/10): python3-libs-3.9.16-1.el9_2.2.x86_64.rpm                                       3.6 MB/s | 7.3 MB     00:02
------------------------------------------------------------------------------------------------------------------------
Total                                                                                   5.1 MB/s |  12 MB     00:02
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Upgrading        : glibc-gconv-extra-2.34-60.el9_2.7.x86_64                                                      1/20
  Running scriptlet: glibc-gconv-extra-2.34-60.el9_2.7.x86_64                                                      1/20
  Upgrading        : glibc-common-2.34-60.el9_2.7.x86_64                                                           2/20
  Running scriptlet: glibc-2.34-60.el9_2.7.x86_64                                                                  3/20
  Upgrading        : glibc-2.34-60.el9_2.7.x86_64                                                                  3/20
  Running scriptlet: glibc-2.34-60.el9_2.7.x86_64                                                                  3/20
  Upgrading        : glibc-langpack-en-2.34-60.el9_2.7.x86_64                                                      4/20
  Upgrading        : libnghttp2-1.43.0-5.el9_2.1.x86_64                                                            5/20
  Upgrading        : libcurl-7.76.1-23.el9_2.4.x86_64                                                              6/20
  Upgrading        : python-unversioned-command-3.9.16-1.el9_2.2.noarch                                            7/20
  Upgrading        : python3-3.9.16-1.el9_2.2.x86_64                                                               8/20
  Upgrading        : python3-libs-3.9.16-1.el9_2.2.x86_64                                                          9/20
  Upgrading        : curl-7.76.1-23.el9_2.4.x86_64                                                                10/20
  Cleanup          : python3-libs-3.9.16-1.el9_2.1.x86_64                                                         11/20
  Cleanup          : curl-7.76.1-23.el9_2.2.x86_64                                                                12/20
  Cleanup          : libcurl-7.76.1-23.el9_2.2.x86_64                                                             13/20
  Cleanup          : libnghttp2-1.43.0-5.el9.x86_64                                                               14/20
  Cleanup          : python3-3.9.16-1.el9_2.1.x86_64                                                              15/20
  Cleanup          : python-unversioned-command-3.9.16-1.el9_2.1.noarch                                           16/20
  Cleanup          : glibc-langpack-en-2.34-60.el9.x86_64                                                         17/20
  Cleanup          : glibc-common-2.34-60.el9.x86_64                                                              18/20
  Cleanup          : glibc-gconv-extra-2.34-60.el9.x86_64                                                         19/20
  Running scriptlet: glibc-gconv-extra-2.34-60.el9.x86_64                                                         19/20
  Cleanup          : glibc-2.34-60.el9.x86_64                                                                     20/20
  Running scriptlet: glibc-2.34-60.el9.x86_64                                                                     20/20
  Verifying        : libnghttp2-1.43.0-5.el9_2.1.x86_64                                                            1/20
  Verifying        : libnghttp2-1.43.0-5.el9.x86_64                                                                2/20
  Verifying        : python3-libs-3.9.16-1.el9_2.2.x86_64                                                          3/20
  Verifying        : python3-libs-3.9.16-1.el9_2.1.x86_64                                                          4/20
  Verifying        : python3-3.9.16-1.el9_2.2.x86_64                                                               5/20
  Verifying        : python3-3.9.16-1.el9_2.1.x86_64                                                               6/20
  Verifying        : libcurl-7.76.1-23.el9_2.4.x86_64                                                              7/20
  Verifying        : libcurl-7.76.1-23.el9_2.2.x86_64                                                              8/20
  Verifying        : curl-7.76.1-23.el9_2.4.x86_64                                                                 9/20
  Verifying        : curl-7.76.1-23.el9_2.2.x86_64                                                                10/20
  Verifying        : glibc-langpack-en-2.34-60.el9_2.7.x86_64                                                     11/20
  Verifying        : glibc-langpack-en-2.34-60.el9.x86_64                                                         12/20
  Verifying        : glibc-gconv-extra-2.34-60.el9_2.7.x86_64                                                     13/20
  Verifying        : glibc-gconv-extra-2.34-60.el9.x86_64                                                         14/20
  Verifying        : glibc-common-2.34-60.el9_2.7.x86_64                                                          15/20
  Verifying        : glibc-common-2.34-60.el9.x86_64                                                              16/20
  Verifying        : glibc-2.34-60.el9_2.7.x86_64                                                                 17/20
  Verifying        : glibc-2.34-60.el9.x86_64                                                                     18/20
  Verifying        : python-unversioned-command-3.9.16-1.el9_2.2.noarch                                           19/20
  Verifying        : python-unversioned-command-3.9.16-1.el9_2.1.noarch                                           20/20

Upgraded:
  curl-7.76.1-23.el9_2.4.x86_64                         glibc-2.34-60.el9_2.7.x86_64
  glibc-common-2.34-60.el9_2.7.x86_64                   glibc-gconv-extra-2.34-60.el9_2.7.x86_64
  glibc-langpack-en-2.34-60.el9_2.7.x86_64              libcurl-7.76.1-23.el9_2.4.x86_64
  libnghttp2-1.43.0-5.el9_2.1.x86_64                    python-unversioned-command-3.9.16-1.el9_2.2.noarch
  python3-3.9.16-1.el9_2.2.x86_64                       python3-libs-3.9.16-1.el9_2.2.x86_64

Complete!
```

3 : on install les packet :

```
[tom@efrei-xmg4agau1 ~]$ sudo dnf install docker-ce docker-ce-cli containerd.io
Last metadata expiration check: 0:00:20 ago on Wed 15 Nov 2023 04:19:45 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                                Architecture     Version                       Repository                  Size
========================================================================================================================
Installing:
 containerd.io                          x86_64           1.6.24-3.1.el9                docker-ce-stable            33 M
 docker-ce                              x86_64           3:24.0.7-1.el9                docker-ce-stable            24 M
 docker-ce-cli                          x86_64           1:24.0.7-1.el9                docker-ce-stable           7.1 M
Installing dependencies:
 checkpolicy                            x86_64           3.5-1.el9                     appstream                  345 k
 container-selinux                      noarch           3:2.205.0-1.el9_2             appstream                   50 k
 fuse-common                            x86_64           3.10.2-5.el9.0.1              baseos                     8.1 k
 fuse-overlayfs                         x86_64           1.11-1.el9_2                  appstream                   71 k
 fuse3                                  x86_64           3.10.2-5.el9.0.1              appstream                   53 k
 fuse3-libs                             x86_64           3.10.2-5.el9.0.1              appstream                   91 k
 libslirp                               x86_64           4.4.0-7.el9                   appstream                   68 k
 policycoreutils-python-utils           noarch           3.5-1.el9                     appstream                   71 k
 python3-audit                          x86_64           3.0.7-103.el9                 appstream                   83 k
 python3-distro                         noarch           1.5.0-7.el9                   appstream                   36 k
 python3-libsemanage                    x86_64           3.5-1.el9                     appstream                   79 k
 python3-policycoreutils                noarch           3.5-1.el9                     appstream                  2.0 M
 python3-setools                        x86_64           4.4.1-1.el9                   baseos                     543 k
 python3-setuptools                     noarch           53.0.0-12.el9                 baseos                     839 k
 slirp4netns                            x86_64           1.2.0-3.el9                   appstream                   45 k
 tar                                    x86_64           2:1.34-6.el9_1                baseos                     876 k
Installing weak dependencies:
 docker-buildx-plugin                   x86_64           0.11.2-1.el9                  docker-ce-stable            13 M
 docker-ce-rootless-extras              x86_64           24.0.7-1.el9                  docker-ce-stable           3.9 M
 docker-compose-plugin                  x86_64           2.21.0-1.el9                  docker-ce-stable            13 M

Transaction Summary
========================================================================================================================
Install  22 Packages

Total download size: 98 M
Installed size: 385 M
Is this ok [y/N]: y
Downloading Packages:
(1/22): docker-buildx-plugin-0.11.2-1.el9.x86_64.rpm                                    4.4 MB/s |  13 MB     00:02
(2/22): docker-ce-cli-24.0.7-1.el9.x86_64.rpm                                           1.0 MB/s | 7.1 MB     00:07
(3/22): docker-ce-24.0.7-1.el9.x86_64.rpm                                               2.0 MB/s |  24 MB     00:11
(4/22): docker-ce-rootless-extras-24.0.7-1.el9.x86_64.rpm                               1.4 MB/s | 3.9 MB     00:02
(5/22): docker-compose-plugin-2.21.0-1.el9.x86_64.rpm                                   2.0 MB/s |  13 MB     00:06
(6/22): containerd.io-1.6.24-3.1.el9.x86_64.rpm                                         1.8 MB/s |  33 MB     00:18
(7/22): python3-setuptools-53.0.0-12.el9.noarch.rpm                                     113 kB/s | 839 kB     00:07
(8/22): fuse-common-3.10.2-5.el9.0.1.x86_64.rpm                                          59 kB/s | 8.1 kB     00:00
(9/22): python3-distro-1.5.0-7.el9.noarch.rpm                                           112 kB/s |  36 kB     00:00
(10/22): python3-setools-4.4.1-1.el9.x86_64.rpm                                         198 kB/s | 543 kB     00:02
(11/22): tar-1.34-6.el9_1.x86_64.rpm                                                    214 kB/s | 876 kB     00:04
(12/22): checkpolicy-3.5-1.el9.x86_64.rpm                                               155 kB/s | 345 kB     00:02
(13/22): fuse-overlayfs-1.11-1.el9_2.x86_64.rpm                                         136 kB/s |  71 kB     00:00
(14/22): python3-audit-3.0.7-103.el9.x86_64.rpm                                          63 kB/s |  83 kB     00:01
(15/22): policycoreutils-python-utils-3.5-1.el9.noarch.rpm                               89 kB/s |  71 kB     00:00
(16/22): container-selinux-2.205.0-1.el9_2.noarch.rpm                                    11 kB/s |  50 kB     00:04
(17/22): slirp4netns-1.2.0-3.el9.x86_64.rpm                                              96 kB/s |  45 kB     00:00
(18/22): python3-libsemanage-3.5-1.el9.x86_64.rpm                                        79 kB/s |  79 kB     00:00
(19/22): libslirp-4.4.0-7.el9.x86_64.rpm                                                 55 kB/s |  68 kB     00:01
(20/22): fuse3-3.10.2-5.el9.0.1.x86_64.rpm                                              104 kB/s |  53 kB     00:00
(21/22): fuse3-libs-3.10.2-5.el9.0.1.x86_64.rpm                                          69 kB/s |  91 kB     00:01
(22/22): python3-policycoreutils-3.5-1.el9.noarch.rpm                                   264 kB/s | 2.0 MB     00:07
------------------------------------------------------------------------------------------------------------------------
Total                                                                                   3.1 MB/s |  98 MB     00:31
Docker CE Stable - x86_64                                                                16 kB/s | 1.6 kB     00:00
Importing GPG key 0x621E9F35:
 Userid     : "Docker Release (CE rpm) <docker@docker.com>"
 Fingerprint: 060A 61C5 1B55 8A7F 742B 77AA C52F EB6B 621E 9F35
 From       : https://download.docker.com/linux/centos/gpg
Is this ok [y/N]: y
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : fuse3-libs-3.10.2-5.el9.0.1.x86_64                                                            1/22
  Installing       : python3-setuptools-53.0.0-12.el9.noarch                                                       2/22
  Installing       : docker-compose-plugin-2.21.0-1.el9.x86_64                                                     3/22
  Running scriptlet: docker-compose-plugin-2.21.0-1.el9.x86_64                                                     3/22
  Installing       : python3-setools-4.4.1-1.el9.x86_64                                                            4/22
  Installing       : python3-distro-1.5.0-7.el9.noarch                                                             5/22
  Installing       : python3-libsemanage-3.5-1.el9.x86_64                                                          6/22
  Installing       : libslirp-4.4.0-7.el9.x86_64                                                                   7/22
  Installing       : slirp4netns-1.2.0-3.el9.x86_64                                                                8/22
  Installing       : python3-audit-3.0.7-103.el9.x86_64                                                            9/22
  Installing       : checkpolicy-3.5-1.el9.x86_64                                                                 10/22
  Installing       : python3-policycoreutils-3.5-1.el9.noarch                                                     11/22
  Installing       : policycoreutils-python-utils-3.5-1.el9.noarch                                                12/22
  Running scriptlet: container-selinux-3:2.205.0-1.el9_2.noarch                                                   13/22
  Installing       : container-selinux-3:2.205.0-1.el9_2.noarch                                                   13/22
  Running scriptlet: container-selinux-3:2.205.0-1.el9_2.noarch                                                   13/22
  Installing       : containerd.io-1.6.24-3.1.el9.x86_64                                                          14/22
  Running scriptlet: containerd.io-1.6.24-3.1.el9.x86_64                                                          14/22
  Installing       : fuse-common-3.10.2-5.el9.0.1.x86_64                                                          15/22
  Installing       : fuse3-3.10.2-5.el9.0.1.x86_64                                                                16/22
  Installing       : fuse-overlayfs-1.11-1.el9_2.x86_64                                                           17/22
  Running scriptlet: fuse-overlayfs-1.11-1.el9_2.x86_64                                                           17/22
  Installing       : tar-2:1.34-6.el9_1.x86_64                                                                    18/22
  Installing       : docker-buildx-plugin-0.11.2-1.el9.x86_64                                                     19/22
  Running scriptlet: docker-buildx-plugin-0.11.2-1.el9.x86_64                                                     19/22
  Installing       : docker-ce-cli-1:24.0.7-1.el9.x86_64                                                          20/22
  Running scriptlet: docker-ce-cli-1:24.0.7-1.el9.x86_64                                                          20/22
  Installing       : docker-ce-rootless-extras-24.0.7-1.el9.x86_64                                                21/22
  Running scriptlet: docker-ce-rootless-extras-24.0.7-1.el9.x86_64                                                21/22
  Installing       : docker-ce-3:24.0.7-1.el9.x86_64                                                              22/22
  Running scriptlet: docker-ce-3:24.0.7-1.el9.x86_64                                                              22/22
  Running scriptlet: container-selinux-3:2.205.0-1.el9_2.noarch                                                   22/22
  Running scriptlet: docker-ce-3:24.0.7-1.el9.x86_64                                                              22/22
  Verifying        : containerd.io-1.6.24-3.1.el9.x86_64                                                           1/22
  Verifying        : docker-buildx-plugin-0.11.2-1.el9.x86_64                                                      2/22
  Verifying        : docker-ce-3:24.0.7-1.el9.x86_64                                                               3/22
  Verifying        : docker-ce-cli-1:24.0.7-1.el9.x86_64                                                           4/22
  Verifying        : docker-ce-rootless-extras-24.0.7-1.el9.x86_64                                                 5/22
  Verifying        : docker-compose-plugin-2.21.0-1.el9.x86_64                                                     6/22
  Verifying        : python3-setuptools-53.0.0-12.el9.noarch                                                       7/22
  Verifying        : python3-setools-4.4.1-1.el9.x86_64                                                            8/22
  Verifying        : tar-2:1.34-6.el9_1.x86_64                                                                     9/22
  Verifying        : fuse-common-3.10.2-5.el9.0.1.x86_64                                                          10/22
  Verifying        : python3-distro-1.5.0-7.el9.noarch                                                            11/22
  Verifying        : container-selinux-3:2.205.0-1.el9_2.noarch                                                   12/22
  Verifying        : checkpolicy-3.5-1.el9.x86_64                                                                 13/22
  Verifying        : fuse-overlayfs-1.11-1.el9_2.x86_64                                                           14/22
  Verifying        : python3-audit-3.0.7-103.el9.x86_64                                                           15/22
  Verifying        : python3-policycoreutils-3.5-1.el9.noarch                                                     16/22
  Verifying        : policycoreutils-python-utils-3.5-1.el9.noarch                                                17/22
  Verifying        : slirp4netns-1.2.0-3.el9.x86_64                                                               18/22
  Verifying        : libslirp-4.4.0-7.el9.x86_64                                                                  19/22
  Verifying        : python3-libsemanage-3.5-1.el9.x86_64                                                         20/22
  Verifying        : fuse3-libs-3.10.2-5.el9.0.1.x86_64                                                           21/22
  Verifying        : fuse3-3.10.2-5.el9.0.1.x86_64                                                                22/22

Installed:
  checkpolicy-3.5-1.el9.x86_64                               container-selinux-3:2.205.0-1.el9_2.noarch
  containerd.io-1.6.24-3.1.el9.x86_64                        docker-buildx-plugin-0.11.2-1.el9.x86_64
  docker-ce-3:24.0.7-1.el9.x86_64                            docker-ce-cli-1:24.0.7-1.el9.x86_64
  docker-ce-rootless-extras-24.0.7-1.el9.x86_64              docker-compose-plugin-2.21.0-1.el9.x86_64
  fuse-common-3.10.2-5.el9.0.1.x86_64                        fuse-overlayfs-1.11-1.el9_2.x86_64
  fuse3-3.10.2-5.el9.0.1.x86_64                              fuse3-libs-3.10.2-5.el9.0.1.x86_64
  libslirp-4.4.0-7.el9.x86_64                                policycoreutils-python-utils-3.5-1.el9.noarch
  python3-audit-3.0.7-103.el9.x86_64                         python3-distro-1.5.0-7.el9.noarch
  python3-libsemanage-3.5-1.el9.x86_64                       python3-policycoreutils-3.5-1.el9.noarch
  python3-setools-4.4.1-1.el9.x86_64                         python3-setuptools-53.0.0-12.el9.noarch
  slirp4netns-1.2.0-3.el9.x86_64                             tar-2:1.34-6.el9_1.x86_64

Complete!
```

**on demarre docker**

```
[tom@efrei-xmg4agau1 ~]$ sudo systemctl enable docker
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
[tom@efrei-xmg4agau1 ~]$
```

**on ajoute l'utilisateur au groupe docker**

```
[tom@efrei-xmg4agau1 ~]$ sudo usermod -aG docker tom
[sudo] password for tom:
[tom@efrei-xmg4agau1 ~]$
```

pour finir on relance la machine


# lancement de conteneur

commande de lancement qui prend en compte toute les consignes : 

```
docker run -d --name tp1 --memory 256m --cpus 0.5 -p 8000:9999 -v $(pwd)/tp.conf:/etc/nginx/conf.d/tp.conf  --rm -v $(pwd)/index.html:/var/www/tp_docker/index.html    nginx
```

# construire sa propre image 

```
tom@efrei-xmg4agau1 ~]$ docker buildx build -t test10 /home/tom/dockerfile
[+] Building 611.3s (10/10) FINISHED                                                                     docker:default
 => [internal] load build definition from Dockerfile                                                               0.0s
 => => transferring dockerfile: 290B                                                                               0.0s
 => [internal] load .dockerignore                                                                                  0.0s
 => => transferring context: 2B                                                                                    0.0s
 => [internal] load metadata for docker.io/library/debian:10                                                       2.8s
 => [1/5] FROM docker.io/library/debian:10@sha256:53cf4f4dbe6f827072bde99045671754cca8174d0464d829c194a26e7ba2c1  48.4s
 => => resolve docker.io/library/debian:10@sha256:53cf4f4dbe6f827072bde99045671754cca8174d0464d829c194a26e7ba2c13  0.0s
 => => sha256:e9452e7328bacf0f29bb3ebda2280fd492cec7feaa2db70e75a5e12381b114b4 1.46kB / 1.46kB                     0.0s
 => => sha256:711706b827bb26857b90ceb32b653a05be0e06459342cc05124da0f97f9b6ad9 50.50MB / 50.50MB                  47.0s
 => => sha256:53cf4f4dbe6f827072bde99045671754cca8174d0464d829c194a26e7ba2c134 984B / 984B                         0.0s
 => => sha256:6d307788805b89ceb86f31fbe1c5c7df4db77ae23514646598f47e545b8ffa67 529B / 529B                         0.0s
 => => extracting sha256:711706b827bb26857b90ceb32b653a05be0e06459342cc05124da0f97f9b6ad9                          1.3s
 => [internal] load build context                                                                                  0.0s
 => => transferring context: 93B                                                                                   0.0s
 => [2/5] RUN apt update -y                                                                                      107.4s
 => [3/5] RUN apt install -y apache2                                                                             305.1s
 => [4/5] RUN apt install -y nginx                                                                               147.0s
 => [5/5] COPY apache2.conf /etc/apache2/apache2.conf                                                              0.0s
 => exporting to image                                                                                             0.6s
 => => exporting layers                                                                                            0.6s
 => => writing image sha256:0d0d8dbcb5c3215ac9d9f002858cf018fd6bd753ec8ebdf7454a68a87e4a8416                       0.0s
 => => naming to docker.io/library/test10                                                                          0.0s
[tom@efrei-xmg4agau1 ~]$ docker images
REPOSITORY   TAG       IMAGE ID       CREATED         SIZE
test10       latest    0d0d8dbcb5c3   2 minutes ago   270MB
nginx        latest    c20060033e06   2 weeks ago     187MB
```

**contenue dockerfile**

```
FROM debian:10

RUN apt update -y

RUN apt install -y apache2

RUN apt install -y nginx

CMD [ "/usr/sbin/nginx", "-g", "daemon off;" ]

COPY apache2.conf /etc/apache2/apache2.conf

EXPOSE 80
```

**preuve que sa fonctionne**

```
[tom@efrei-xmg4agau1 dockerfile]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                                               NAMES
433b85db217d   test10    "/usr/sbin/nginx -g …"   13 minutes ago   Up 13 minutes   80/tcp, 0.0.0.0:8000->9999/tcp, :::8000->9999/tcp   tp1
```

  **fichier apache2.conf**

  ```
# on définit un port sur lequel écouter
Listen 80

# on charge certains modules Apache strictement nécessaires à son bon fonctionnement
LoadModule mpm_event_module "/usr/lib/apache2/modules/mod_mpm_event.so"
LoadModule dir_module "/usr/lib/apache2/modules/mod_dir.so"
LoadModule authz_core_module "/usr/lib/apache2/modules/mod_authz_core.so"

# on indique le nom du fichier HTML à charger par défaut
DirectoryIndex index.html
# on indique le chemin où se trouve notre site
DocumentRoot "/var/www/html/"

# quelques paramètres pour les logs
ErrorLog "logs/error.log"
LogLevel warn
  ```

# docker copose

**installer wiki JS**

```
[tom@efrei-xmg4agau1 ~]$ mkdir docker_compose1
[tom@efrei-xmg4agau1 ~]$ cd docker_compose1/
[tom@efrei-xmg4agau1 docker_compose1]$ sudo nano docker-compose.yml
[tom@efrei-xmg4agau1 docker_compose1]$ docker compose up -d
[+] Running 22/23
[+] Running 22/23 [⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulled                                                        1387.1s
[+] Running 22/23 [⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulled                                                        1387.1s
[+] Running 22/23 [⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulled                                                        1387.1s
 ✔ wiki 13 layers [⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulled                                                        1387.1s
   ✔ 31e352740f53 Pull complete                                                                                   43.2s
   ✔ 2629b68d4311 Pull complete                                                                                  722.6s
   ✔ ddb7cc70f260 Pull complete                                                                                   43.7s
   ✔ 18afe6373474 Pull complete                                                                                   44.0s
   ✔ 88ec59e3b80e Pull complete                                                                                  252.6s
   ✔ 4f4fb700ef54 Pull complete                                                                                   44.5s
   ✔ 710fc81a2a82 Pull complete                                                                                  227.0s
   ✔ 4e75fc591abd Pull complete                                                                                 1379.2s
   ✔ d11f75e9538b Pull complete                                                                                  255.0s
   ✔ 37595d564d08 Pull complete                                                                                  255.9s
   ✔ b6015734afb2 Pull complete                                                                                  256.7s
   ✔ 6c63ee65d20b Pull complete                                                                                  257.7s
   ✔ 2442912e7d55 Pull complete                                                                                  259.0s
 ⠴ db 8 layers [⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulling                                                               1407.5s
   ✔ 96526aa774ef Pull complete                                                                                  258.2s
   ✔ f3836ccc9a67 Pull complete                                                                                  258.8s
   ✔ 3625732fb368 Pull complete                                                                                  259.7s
   ✔ db2cf8153bfe Pull complete                                                                                  638.9s
   ✔ 57d1e4d158ff Pull complete                                                                                  646.9s
   ✔ d4b037398667 Pull complete                                                                                  647.5s
   ✔ fffdf7fd1f65 Pull complete                                                                                  648.7s
   ✔ 6fbe9502899e Pull complete                                                                                  649.7s
canceled
[tom@efrei-xmg4agau1 docker_compose1]$ docker compose up -d
[+] Running 9/9
 ✔ db 8 layers [⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulled                                                                   5.1s
   ✔ 96526aa774ef Already exists                                                                                   0.0s
   ✔ f3836ccc9a67 Already exists                                                                                   0.0s
   ✔ 3625732fb368 Already exists                                                                                   0.0s
   ✔ db2cf8153bfe Already exists                                                                                   0.0s
   ✔ 57d1e4d158ff Already exists                                                                                   0.0s
   ✔ d4b037398667 Already exists                                                                                   0.0s
   ✔ fffdf7fd1f65 Already exists                                                                                   0.0s
   ✔ 6fbe9502899e Already exists                                                                                   0.0s
[+] Running 4/4
 ✔ Network docker_compose1_default   Created                                                                       0.1s
 ✔ Volume "docker_compose1_db-data"  Created                                                                       0.0s
 ✔ Container docker_compose1-db-1    Started                                                                       0.2s
 ✔ Container docker_compose1-wiki-1  Started                                                                       0.0s
[tom@efrei-xmg4agau1 docker_compose1]$
```
**contenue docker-compose.yml**

```
version: "3"
services:

  db:
    image: postgres:15-alpine
    environment:
      POSTGRES_DB: wiki
      POSTGRES_PASSWORD: wikijsrocks
      POSTGRES_USER: wikijs
    logging:
      driver: "none"
    restart: unless-stopped
    volumes:
      - db-data:/var/lib/postgresql/data

  wiki:
    image: ghcr.io/requarks/wiki:2
    depends_on:
      - db
    environment:
      DB_TYPE: postgres
      DB_HOST: db
      DB_PORT: 5432
      DB_USER: wikijs
      DB_PASS: wikijsrocks
      DB_NAME: wiki
    restart: unless-stopped
    ports:
      - "80:3000"

volumes:
  db-data:

```


**créer image personalisé wikijs**


```
[tom@efrei-xmg4agau1 ~]$ mkdir docker-compose1
[tom@efrei-xmg4agau1 ~]$ cd docker-compose1/
[tom@efrei-xmg4agau1 dockerfilejs]$ sudo nano Dockerfile.wikijs
[tom@efrei-xmg4agau1 docker_compose1]$ sudo nano docker-compose.yml
[tom@efrei-xmg4agau1 docker_compose1]$ cd ..
[sudo] password for tom:
[tom@efrei-xmg4agau1 docker_compose1]$ docker build -t wikijs-local -f Dockerfile.wikijs .
[+] Building 1.4s (9/9) FINISHED                                                                         docker:default
 => [internal] load build definition from Dockerfile.wikijs                                                        0.0s
 => => transferring dockerfile: 268B                                                                               0.0s
 => [internal] load .dockerignore                                                                                  0.0s
 => => transferring context: 2B                                                                                    0.0s
 => [internal] load metadata for docker.io/requarks/wiki:latest                                                    1.0s
 => CACHED [1/4] FROM docker.io/requarks/wiki:latest@sha256:5419ec3f4ac6a0bc9fd1b6b126249fe5ad72c02f7a47e885a15e8  0.0s
 => [internal] load build context                                                                                  0.0s
 => => transferring context: 685B                                                                                  0.0s
 => [2/4] WORKDIR /var/wiki                                                                                        0.1s
 => [3/4] RUN mkdir keys                                                                                           0.1s
 => [4/4] ADD docker-compose.yml config.yml                                                                        0.0s
 => exporting to image                                                                                             0.1s
 => => exporting layers                                                                                            0.0s
 => => writing image sha256:4200667b960e9729aef8273e29c7aef0819faf2532575352ef41b176c8cbf2fc                       0.0s
 => => naming to docker.io/library/wikijs-local

[tom@efrei-xmg4agau1 docker_compose1]$ [tom@efrei-xmg4agau1 docker_compose1]$ docker compose up -d
[+] Building 6.1s (9/9) FINISHED                                                                         docker:default
 => [wikijs internal] load build definition from Dockerfile.wikijs                                                 0.0s
 => => transferring dockerfile: 268B                                                                               0.0s
 => [wikijs internal] load .dockerignore                                                                           0.0s
 => => transferring context: 2B                                                                                    0.0s
 => [wikijs internal] load metadata for docker.io/requarks/wiki:latest                                             6.0s
 => [wikijs 1/4] FROM docker.io/requarks/wiki:latest@sha256:5419ec3f4ac6a0bc9fd1b6b126249fe5ad72c02f7a47e885a15e8  0.0s
 => [wikijs internal] load build context                                                                           0.0s
 => => transferring context: 792B                                                                                  0.0s
 => CACHED [wikijs 2/4] WORKDIR /var/wiki                                                                          0.0s
 => CACHED [wikijs 3/4] RUN mkdir keys                                                                             0.0s
 => [wikijs 4/4] ADD docker-compose.yml config.yml                                                                 0.0s
 => [wikijs] exporting to image                                                                                    0.0s
 => => exporting layers                                                                                            0.0s
 => => writing image sha256:e2901044348f22b2e4a7edfeb4316a464e07654261660b704e6e1145de4e8f58                       0.0s
 => => naming to docker.io/library/docker_compose1-wikijs                                                          0.0s
[+] Running 3/3
 ✔ Container docker_compose1-wikijs-1  Started                                                                     0.1s
 ✔ Container docker_compose1-db-1      Running                                                                     0.0s
 ✔ Container docker_compose1-wiki-1    Running


[tom@efrei-xmg4agau1 docker_compose1]$ docker ps
CONTAINER ID   IMAGE                     COMMAND                  CREATED             STATUS          PORTS                                             NAMES
27512dc86c57   ghcr.io/requarks/wiki:2   "docker-entrypoint.s…"   About an hour ago   Up 23 seconds   3443/tcp, 0.0.0.0:80->3000/tcp, :::80->3000/tcp   docker_compose1-wiki-1
0b4c002d337b   postgres:15-alpine        "docker-entrypoint.s…"   About an hour ago   Up 23 seconds   5432/tcp                                          docker_compose1-db-1
[tom@efrei-xmg4agau1 docker_compose1]$                                               docker_compose1-wikijs-1
```

**contenue docker-compose.yml :**

```
version: "3"

services:
  wikijs:
    build:
      context: .
      dockerfile: Dockerfile.wikijs
    ports:
      - "3000:3000"

  db:
    image: postgres:15-alpine
    environment:
      POSTGRES_DB: wiki
      POSTGRES_PASSWORD: wikijsrocks
      POSTGRES_USER: wikijs
    logging:
      driver: "none"
    restart: unless-stopped
    volumes:
      - db-data:/var/lib/postgresql/data
  wiki:
    image: ghcr.io/requarks/wiki:2
    depends_on:
      - db
    environment:
      DB_TYPE: postgres
      DB_HOST: db
      DB_PORT: 5432
      DB_USER: wikijs
      DB_PASS: wikijsrocks
      DB_NAME: wiki
    restart: unless-stopped
    ports:
      - "80:3000"

volumes:
  db-data:


```
**contenue dockerfile.wikijs :**

```
FROM requarks/wiki:latest

WORKDIR /var/wiki

RUN mkdir keys
ADD docker-compose.yml config.yml

VOLUME /var/wiki/keys

EXPOSE 3000
ENTRYPOINT [ "node", "server" ]

```


# conteneuriser notre application

**premièrement on installe les commande git**

```
[tom@efrei-xmg4agau1 ~]$ sudo dnf install git-all
Last metadata expiration check: 2:53:53 ago on Thu 16 Nov 2023 03:07:49 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                               Architecture        Version                         Repository              Size
========================================================================================================================
Installing:
 git-all                               noarch              2.39.3-1.el9_2                  appstream              7.5 k
Installing dependencies:
 apr                                   x86_64              1.7.0-11.el9                    appstream              123 k
 apr-util                              x86_64              1.6.1-20.el9_2.1                appstream               94 k
 apr-util-bdb                          x86_64              1.6.1-20.el9_2.1                appstream               12 k
 emacs-filesystem                      noarch              1:27.2-8.el9_2.1                appstream              7.9 k
 fontconfig                            x86_64              2.14.0-2.el9_1                  appstream              274 k
 git                                   x86_64              2.39.3-1.el9_2                  appstream               61 k
 git-core                              x86_64              2.39.3-1.el9_2                  appstream              4.2 M
 git-core-doc                          noarch              2.39.3-1.el9_2                  appstream              2.6 M
 git-credential-libsecret              x86_64              2.39.3-1.el9_2                  appstream               14 k
 git-daemon                            x86_64              2.39.3-1.el9_2                  appstream              315 k
 git-email                             noarch              2.39.3-1.el9_2                  appstream               53 k
 git-gui                               noarch              2.39.3-1.el9_2                  appstream              242 k
 git-instaweb                          noarch              2.39.3-1.el9_2                  appstream               25 k
 git-subtree                           x86_64              2.39.3-1.el9_2                  appstream               34 k
 git-svn                               noarch              2.39.3-1.el9_2                  appstream               69 k
 gitk                                  noarch              2.39.3-1.el9_2                  appstream              157 k
 gitweb                                noarch              2.39.3-1.el9_2                  appstream              143 k
 httpd                                 x86_64              2.4.53-11.el9_2.5               appstream               47 k
 httpd-core                            x86_64              2.4.53-11.el9_2.5               appstream              1.4 M
 httpd-filesystem                      noarch              2.4.53-11.el9_2.5               appstream               14 k
 httpd-tools                           x86_64              2.4.53-11.el9_2.5               appstream               81 k
 libX11                                x86_64              1.7.0-7.el9                     appstream              652 k
 libX11-common                         noarch              1.7.0-7.el9                     appstream              152 k
 libXau                                x86_64              1.0.9-8.el9                     appstream               30 k
 libXft                                x86_64              2.3.3-8.el9                     appstream               61 k
 libXrender                            x86_64              0.9.10-16.el9                   appstream               27 k
 libsecret                             x86_64              0.20.4-4.el9                    appstream              157 k
 libserf                               x86_64              1.3.9-26.el9                    appstream               57 k
 libxcb                                x86_64              1.13.1-9.el9                    appstream              224 k
 mailcap                               noarch              2.1.49-5.el9                    baseos                  32 k
 perl-Authen-SASL                      noarch              2.16-25.el9                     appstream               52 k
 perl-AutoLoader                       noarch              5.74-480.el9                    appstream               21 k
 perl-B                                x86_64              1.80-480.el9                    appstream              179 k
 perl-CGI                              noarch              4.51-5.el9                      appstream              198 k
 perl-Carp                             noarch              1.50-460.el9                    appstream               29 k
 perl-Class-Struct                     noarch              0.66-480.el9                    appstream               22 k
 perl-Compress-Raw-Bzip2               x86_64              2.101-5.el9                     appstream               34 k
 perl-Compress-Raw-Zlib                x86_64              2.101-5.el9                     appstream               60 k
 perl-Data-Dumper                      x86_64              2.174-462.el9                   appstream               55 k
 perl-Digest                           noarch              1.19-4.el9                      appstream               25 k
 perl-Digest-HMAC                      noarch              1.03-29.el9                     appstream               16 k
 perl-Digest-MD5                       x86_64              2.58-4.el9                      appstream               36 k
 perl-Digest-SHA                       x86_64              1:6.02-461.el9                  appstream               61 k
 perl-DynaLoader                       x86_64              1.47-480.el9                    appstream               26 k
 perl-Encode                           x86_64              4:3.08-462.el9                  appstream              1.7 M
 perl-Encode-Locale                    noarch              1.05-21.el9                     appstream               19 k
 perl-Errno                            x86_64              1.30-480.el9                    appstream               15 k
 perl-Error                            noarch              1:0.17029-7.el9                 appstream               41 k
 perl-Exporter                         noarch              5.74-461.el9                    appstream               31 k
 perl-Fcntl                            x86_64              1.13-480.el9                    appstream               20 k
 perl-File-Basename                    noarch              2.85-480.el9                    appstream               17 k
 perl-File-Find                        noarch              1.37-480.el9                    appstream               26 k
 perl-File-Path                        noarch              2.18-4.el9                      appstream               35 k
 perl-File-Temp                        noarch              1:0.231.100-4.el9               appstream               59 k
 perl-File-stat                        noarch              1.09-480.el9                    appstream               17 k
 perl-FileHandle                       noarch              2.03-480.el9                    appstream               16 k
 perl-GSSAPI                           x86_64              0.28-37.el9                     appstream               59 k
 perl-Getopt-Long                      noarch              1:2.52-4.el9                    appstream               60 k
 perl-Getopt-Std                       noarch              1.12-480.el9                    appstream               16 k
 perl-Git                              noarch              2.39.3-1.el9_2                  appstream               37 k
 perl-Git-SVN                          noarch              2.39.3-1.el9_2                  appstream               52 k
 perl-HTML-Parser                      x86_64              3.76-3.el9                      appstream              119 k
 perl-HTML-Tagset                      noarch              3.20-47.el9                     appstream               18 k
 perl-HTTP-Date                        noarch              6.05-7.el9                      appstream               24 k
 perl-HTTP-Message                     noarch              6.29-3.el9                      appstream               96 k
 perl-HTTP-Tiny                        noarch              0.076-460.el9                   appstream               54 k
 perl-I18N-Langinfo                    x86_64              0.19-480.el9                    appstream               23 k
 perl-IO                               x86_64              1.43-480.el9                    appstream               87 k
 perl-IO-Compress                      noarch              2.102-4.el9                     appstream              256 k
 perl-IO-HTML                          noarch              1.004-4.el9                     appstream               28 k
 perl-IO-Socket-IP                     noarch              0.41-5.el9                      appstream               42 k
 perl-IO-Socket-SSL                    noarch              2.073-1.el9                     appstream              217 k
 perl-IPC-Open3                        noarch              1.21-480.el9                    appstream               23 k
 perl-LWP-MediaTypes                   noarch              6.04-9.el9                      appstream               33 k
 perl-MIME-Base64                      x86_64              3.16-4.el9                      appstream               30 k
 perl-MailTools                        noarch              2.21-9.el9                      appstream              101 k
 perl-Memoize                          noarch              1.03-480.el9                    appstream               57 k
 perl-NDBM_File                        x86_64              1.15-480.el9                    appstream               22 k
 perl-Net-SMTP-SSL                     noarch              1.04-16.el9                     appstream               12 k
 perl-Net-SSLeay                       x86_64              1.92-2.el9                      appstream              365 k
 perl-POSIX                            x86_64              1.94-480.el9                    appstream               96 k
 perl-PathTools                        x86_64              3.78-461.el9                    appstream               85 k
 perl-Pod-Escapes                      noarch              1:1.07-460.el9                  appstream               20 k
 perl-Pod-Perldoc                      noarch              3.28.01-461.el9                 appstream               83 k
 perl-Pod-Simple                       noarch              1:3.42-4.el9                    appstream              215 k
 perl-Pod-Usage                        noarch              4:2.01-4.el9                    appstream               40 k
 perl-Scalar-List-Utils                x86_64              4:1.56-461.el9                  appstream               71 k
 perl-SelectSaver                      noarch              1.02-480.el9                    appstream               12 k
 perl-Socket                           x86_64              4:2.031-4.el9                   appstream               54 k
 perl-Storable                         x86_64              1:3.21-460.el9                  appstream               95 k
 perl-Symbol                           noarch              1.08-480.el9                    appstream               14 k
 perl-Sys-Hostname                     x86_64              1.23-480.el9                    appstream               17 k
 perl-Term-ANSIColor                   noarch              5.01-461.el9                    appstream               48 k
 perl-Term-Cap                         noarch              1.17-460.el9                    appstream               22 k
 perl-Term-ReadLine                    noarch              1.17-480.el9                    appstream               19 k
 perl-TermReadKey                      x86_64              2.38-11.el9                     appstream               36 k
 perl-Text-ParseWords                  noarch              3.30-460.el9                    appstream               16 k
 perl-Text-Tabs+Wrap                   noarch              2013.0523-460.el9               appstream               23 k
 perl-Tie                              noarch              4.6-480.el9                     appstream               32 k
 perl-Time-HiRes                       x86_64              4:1.9764-462.el9                appstream               57 k
 perl-Time-Local                       noarch              2:1.300-7.el9                   appstream               33 k
 perl-TimeDate                         noarch              1:2.33-6.el9                    appstream               51 k
 perl-URI                              noarch              5.09-3.el9                      appstream              108 k
 perl-YAML                             noarch              1.30-8.el9                      appstream               81 k
 perl-base                             noarch              2.27-480.el9                    appstream               16 k
 perl-constant                         noarch              1.33-461.el9                    appstream               23 k
 perl-filetest                         noarch              1.03-480.el9                    appstream               15 k
 perl-if                               noarch              0.60.800-480.el9                appstream               14 k
 perl-interpreter                      x86_64              4:5.32.1-480.el9                appstream               71 k
 perl-lib                              x86_64              0.65-480.el9                    appstream               15 k
 perl-libnet                           noarch              3.13-4.el9                      appstream              125 k
 perl-libs                             x86_64              4:5.32.1-480.el9                appstream              2.0 M
 perl-mro                              x86_64              1.23-480.el9                    appstream               28 k
 perl-overload                         noarch              1.31-480.el9                    appstream               46 k
 perl-overloading                      noarch              0.02-480.el9                    appstream               13 k
 perl-parent                           noarch              1:0.238-460.el9                 appstream               14 k
 perl-podlators                        noarch              1:4.14-460.el9                  appstream              112 k
 perl-subs                             noarch              1.03-480.el9                    appstream               12 k
 perl-vars                             noarch              1.05-480.el9                    appstream               13 k
 rocky-logos-httpd                     noarch              90.14-1.el9                     appstream               24 k
 subversion                            x86_64              1.14.1-5.el9_0                  appstream              1.0 M
 subversion-libs                       x86_64              1.14.1-5.el9_0                  appstream              1.5 M
 subversion-perl                       x86_64              1.14.1-5.el9_0                  appstream              1.0 M
 tcl                                   x86_64              1:8.6.10-7.el9                  baseos                 1.1 M
 tk                                    x86_64              1:8.6.10-9.el9                  appstream              1.6 M
 utf8proc                              x86_64              2.6.1-4.el9                     appstream               79 k
 xml-common                            noarch              0.6.3-58.el9                    appstream               31 k
Installing weak dependencies:
 apr-util-openssl                      x86_64              1.6.1-20.el9_2.1                appstream               14 k
 mod_http2                             x86_64              1.15.19-4.el9_2.4               appstream              149 k
 mod_lua                               x86_64              2.4.53-11.el9_2.5               appstream               61 k
 perl-Clone                            x86_64              0.45-6.el9                      appstream               22 k
 perl-Mozilla-CA                       noarch              20200520-6.el9                  appstream               12 k

Transaction Summary
========================================================================================================================
Install  133 Packages

Total download size: 26 M
Installed size: 103 M
Is this ok [y/N]: y
Downloading Packages:
(1/133): rocky-logos-httpd-90.14-1.el9.noarch.rpm                                        44 kB/s |  24 kB     00:00
(2/133): perl-Net-SMTP-SSL-1.04-16.el9.noarch.rpm                                        45 kB/s |  12 kB     00:00
(3/133): perl-Text-ParseWords-3.30-460.el9.noarch.rpm                                    49 kB/s |  16 kB     00:00
(4/133): mailcap-2.1.49-5.el9.noarch.rpm                                                 22 kB/s |  32 kB     00:01
(5/133): perl-Exporter-5.74-461.el9.noarch.rpm                                           22 kB/s |  31 kB     00:01
(6/133): perl-File-Path-2.18-4.el9.noarch.rpm                                            11 kB/s |  35 kB     00:03
(7/133): perl-Pod-Perldoc-3.28.01-461.el9.noarch.rpm                                     28 kB/s |  83 kB     00:02
(8/133): perl-Encode-Locale-1.05-21.el9.noarch.rpm                                       42 kB/s |  19 kB     00:00
(9/133): perl-Pod-Simple-3.42-4.el9.noarch.rpm                                           25 kB/s | 215 kB     00:08
(10/133): perl-Getopt-Long-2.52-4.el9.noarch.rpm                                         18 kB/s |  60 kB     00:03
(11/133): perl-TimeDate-2.33-6.el9.noarch.rpm                                            28 kB/s |  51 kB     00:01
(12/133): perl-MailTools-2.21-9.el9.noarch.rpm                                           25 kB/s | 101 kB     00:04
(13/133): perl-IO-HTML-1.004-4.el9.noarch.rpm                                            22 kB/s |  28 kB     00:01
(14/133): perl-Pod-Usage-2.01-4.el9.noarch.rpm                                           16 kB/s |  40 kB     00:02
(15/133): perl-HTML-Tagset-3.20-47.el9.noarch.rpm                                        16 kB/s |  18 kB     00:01
(16/133): perl-Digest-HMAC-1.03-29.el9.noarch.rpm                                        18 kB/s |  16 kB     00:00
(17/133): perl-IO-Socket-IP-0.41-5.el9.noarch.rpm                                        12 kB/s |  42 kB     00:03
(18/133): perl-Time-Local-1.300-7.el9.noarch.rpm                                         19 kB/s |  33 kB     00:01
(19/133): perl-libnet-3.13-4.el9.noarch.rpm                                              39 kB/s | 125 kB     00:03
(20/133): mod_lua-2.4.53-11.el9_2.5.x86_64.rpm                                           53 kB/s |  61 kB     00:01
(21/133): httpd-tools-2.4.53-11.el9_2.5.x86_64.rpm                                       37 kB/s |  81 kB     00:02
(22/133): httpd-2.4.53-11.el9_2.5.x86_64.rpm                                             16 kB/s |  47 kB     00:03
(23/133): httpd-filesystem-2.4.53-11.el9_2.5.noarch.rpm                                  26 kB/s |  14 kB     00:00
(24/133): apr-util-openssl-1.6.1-20.el9_2.1.x86_64.rpm                                   18 kB/s |  14 kB     00:00
(25/133): perl-IO-Compress-2.102-4.el9.noarch.rpm                                        20 kB/s | 256 kB     00:12
(26/133): apr-util-bdb-1.6.1-20.el9_2.1.x86_64.rpm                                       18 kB/s |  12 kB     00:00
(27/133): apr-util-1.6.1-20.el9_2.1.x86_64.rpm                                           36 kB/s |  94 kB     00:02
(28/133): tcl-8.6.10-7.el9.x86_64.rpm                                                    30 kB/s | 1.1 MB     00:36
(29/133): libserf-1.3.9-26.el9.x86_64.rpm                                                64 kB/s |  57 kB     00:00
(30/133): libsecret-0.20.4-4.el9.x86_64.rpm                                              42 kB/s | 157 kB     00:03
(31/133): perl-Carp-1.50-460.el9.noarch.rpm                                              17 kB/s |  29 kB     00:01
(32/133): fontconfig-2.14.0-2.el9_1.x86_64.rpm                                           32 kB/s | 274 kB     00:08
(33/133): libX11-common-1.7.0-7.el9.noarch.rpm                                           39 kB/s | 152 kB     00:03
(34/133): perl-LWP-MediaTypes-6.04-9.el9.noarch.rpm                                      58 kB/s |  33 kB     00:00
(35/133): perl-Term-Cap-1.17-460.el9.noarch.rpm                                          58 kB/s |  22 kB     00:00
(36/133): perl-podlators-4.14-460.el9.noarch.rpm                                        112 kB/s | 112 kB     00:00
(37/133): perl-CGI-4.51-5.el9.noarch.rpm                                                 79 kB/s | 198 kB     00:02
(38/133): perl-Term-ANSIColor-5.01-461.el9.noarch.rpm                                    66 kB/s |  48 kB     00:00
(39/133): perl-HTTP-Tiny-0.076-460.el9.noarch.rpm                                        92 kB/s |  54 kB     00:00
(40/133): perl-Digest-1.19-4.el9.noarch.rpm                                              46 kB/s |  25 kB     00:00
(41/133): perl-constant-1.33-461.el9.noarch.rpm                                          33 kB/s |  23 kB     00:00
(42/133): perl-URI-5.09-3.el9.noarch.rpm                                                 96 kB/s | 108 kB     00:01
(43/133): perl-Compress-Raw-Bzip2-2.101-5.el9.x86_64.rpm                                 47 kB/s |  34 kB     00:00
(44/133): perl-Clone-0.45-6.el9.x86_64.rpm                                               37 kB/s |  22 kB     00:00
(45/133): perl-Data-Dumper-2.174-462.el9.x86_64.rpm                                      23 kB/s |  55 kB     00:02
(46/133): perl-HTML-Parser-3.76-3.el9.x86_64.rpm                                         52 kB/s | 119 kB     00:02
(47/133): perl-Compress-Raw-Zlib-2.101-5.el9.x86_64.rpm                                  49 kB/s |  60 kB     00:01
(48/133): perl-Scalar-List-Utils-1.56-461.el9.x86_64.rpm                                 42 kB/s |  71 kB     00:01
(49/133): perl-Storable-3.21-460.el9.x86_64.rpm                                          30 kB/s |  95 kB     00:03
(50/133): perl-PathTools-3.78-461.el9.x86_64.rpm                                         40 kB/s |  85 kB     00:02
(51/133): perl-IO-Socket-SSL-2.073-1.el9.noarch.rpm                                      33 kB/s | 217 kB     00:06
(52/133): perl-Mozilla-CA-20200520-6.el9.noarch.rpm                                      28 kB/s |  12 kB     00:00
(53/133): perl-Digest-SHA-6.02-461.el9.x86_64.rpm                                        37 kB/s |  61 kB     00:01
(54/133): perl-Text-Tabs+Wrap-2013.0523-460.el9.noarch.rpm                               51 kB/s |  23 kB     00:00
(55/133): perl-Digest-MD5-2.58-4.el9.x86_64.rpm                                          37 kB/s |  36 kB     00:00
(56/133): perl-Pod-Escapes-1.07-460.el9.noarch.rpm                                       47 kB/s |  20 kB     00:00
(57/133): perl-Authen-SASL-2.16-25.el9.noarch.rpm                                        36 kB/s |  52 kB     00:01
(58/133): perl-Error-0.17029-7.el9.noarch.rpm                                            41 kB/s |  41 kB     00:00
(59/133): perl-MIME-Base64-3.16-4.el9.x86_64.rpm                                         41 kB/s |  30 kB     00:00
(60/133): utf8proc-2.6.1-4.el9.x86_64.rpm                                                44 kB/s |  79 kB     00:01
(61/133): perl-GSSAPI-0.28-37.el9.x86_64.rpm                                             42 kB/s |  59 kB     00:01
(62/133): perl-YAML-1.30-8.el9.noarch.rpm                                                45 kB/s |  81 kB     00:01
(63/133): perl-HTTP-Date-6.05-7.el9.noarch.rpm                                           27 kB/s |  24 kB     00:00
(64/133): perl-File-Temp-0.231.100-4.el9.noarch.rpm                                      46 kB/s |  59 kB     00:01
(65/133): perl-HTTP-Message-6.29-3.el9.noarch.rpm                                        45 kB/s |  96 kB     00:02
(66/133): libXau-1.0.9-8.el9.x86_64.rpm                                                  16 kB/s |  30 kB     00:01
(67/133): libXrender-0.9.10-16.el9.x86_64.rpm                                            17 kB/s |  27 kB     00:01
(68/133): libxcb-1.13.1-9.el9.x86_64.rpm                                                 32 kB/s | 224 kB     00:06
(69/133): mod_http2-1.15.19-4.el9_2.4.x86_64.rpm                                         34 kB/s | 149 kB     00:04
(70/133): libXft-2.3.3-8.el9.x86_64.rpm                                                  24 kB/s |  61 kB     00:02
(71/133): apr-1.7.0-11.el9.x86_64.rpm                                                    33 kB/s | 123 kB     00:03
(72/133): perl-Socket-2.031-4.el9.x86_64.rpm                                             28 kB/s |  54 kB     00:01
(73/133): perl-mro-1.23-480.el9.x86_64.rpm                                               25 kB/s |  28 kB     00:01
(74/133): perl-lib-0.65-480.el9.x86_64.rpm                                               45 kB/s |  15 kB     00:00
(75/133): perl-interpreter-5.32.1-480.el9.x86_64.rpm                                     30 kB/s |  71 kB     00:02
(76/133): perl-Sys-Hostname-1.23-480.el9.x86_64.rpm                                      19 kB/s |  17 kB     00:00
(77/133): perl-POSIX-1.94-480.el9.x86_64.rpm                                             22 kB/s |  96 kB     00:04
(78/133): perl-NDBM_File-1.15-480.el9.x86_64.rpm                                         21 kB/s |  22 kB     00:01
(79/133): perl-IO-1.43-480.el9.x86_64.rpm                                                19 kB/s |  87 kB     00:04
(80/133): perl-I18N-Langinfo-0.19-480.el9.x86_64.rpm                                     18 kB/s |  23 kB     00:01
(81/133): perl-Fcntl-1.13-480.el9.x86_64.rpm                                             21 kB/s |  20 kB     00:00
(82/133): perl-Errno-1.30-480.el9.x86_64.rpm                                             11 kB/s |  15 kB     00:01
(83/133): perl-DynaLoader-1.47-480.el9.x86_64.rpm                                        24 kB/s |  26 kB     00:01
(84/133): perl-Encode-3.08-462.el9.x86_64.rpm                                            34 kB/s | 1.7 MB     00:50
(85/133): perl-vars-1.05-480.el9.noarch.rpm                                              12 kB/s |  13 kB     00:01
(86/133): perl-subs-1.03-480.el9.noarch.rpm                                              29 kB/s |  12 kB     00:00
(87/133): perl-overloading-0.02-480.el9.noarch.rpm                                       30 kB/s |  13 kB     00:00
(88/133): perl-overload-1.31-480.el9.noarch.rpm                                          28 kB/s |  46 kB     00:01
(89/133): perl-if-0.60.800-480.el9.noarch.rpm                                            21 kB/s |  14 kB     00:00
(90/133): perl-filetest-1.03-480.el9.noarch.rpm                                          23 kB/s |  15 kB     00:00
(91/133): perl-base-2.27-480.el9.noarch.rpm                                              39 kB/s |  16 kB     00:00
(92/133): perl-Tie-4.6-480.el9.noarch.rpm                                                30 kB/s |  32 kB     00:01
(93/133): perl-Term-ReadLine-1.17-480.el9.noarch.rpm                                     21 kB/s |  19 kB     00:00
(94/133): perl-B-1.80-480.el9.x86_64.rpm                                                 23 kB/s | 179 kB     00:07
(95/133): perl-SelectSaver-1.02-480.el9.noarch.rpm                                       16 kB/s |  12 kB     00:00
(96/133): perl-Symbol-1.08-480.el9.noarch.rpm                                            13 kB/s |  14 kB     00:01
(97/133): perl-IPC-Open3-1.21-480.el9.noarch.rpm                                         50 kB/s |  23 kB     00:00
(98/133): perl-Getopt-Std-1.12-480.el9.noarch.rpm                                        37 kB/s |  16 kB     00:00
(99/133): perl-FileHandle-2.03-480.el9.noarch.rpm                                        48 kB/s |  16 kB     00:00
(100/133): perl-File-stat-1.09-480.el9.noarch.rpm                                        52 kB/s |  17 kB     00:00
(101/133): perl-Memoize-1.03-480.el9.noarch.rpm                                          30 kB/s |  57 kB     00:01
(102/133): perl-File-Basename-2.85-480.el9.noarch.rpm                                    12 kB/s |  17 kB     00:01
(103/133): perl-File-Find-1.37-480.el9.noarch.rpm                                        14 kB/s |  26 kB     00:01
(104/133): perl-AutoLoader-5.74-480.el9.noarch.rpm                                       30 kB/s |  21 kB     00:00
(105/133): perl-Class-Struct-0.66-480.el9.noarch.rpm                                     13 kB/s |  22 kB     00:01
(106/133): subversion-perl-1.14.1-5.el9_0.x86_64.rpm                                     33 kB/s | 1.0 MB     00:31
(107/133): perl-libs-5.32.1-480.el9.x86_64.rpm                                           29 kB/s | 2.0 MB     01:11
(108/133): perl-Net-SSLeay-1.92-2.el9.x86_64.rpm                                         80 kB/s | 365 kB     00:04
(109/133): xml-common-0.6.3-58.el9.noarch.rpm                                            42 kB/s |  31 kB     00:00
(110/133): emacs-filesystem-27.2-8.el9_2.1.noarch.rpm                                    31 kB/s | 7.9 kB     00:00
(111/133): perl-TermReadKey-2.38-11.el9.x86_64.rpm                                       58 kB/s |  36 kB     00:00
(112/133): subversion-libs-1.14.1-5.el9_0.x86_64.rpm                                     34 kB/s | 1.5 MB     00:45
(113/133): perl-Git-SVN-2.39.3-1.el9_2.noarch.rpm                                       108 kB/s |  52 kB     00:00
(114/133): perl-Git-2.39.3-1.el9_2.noarch.rpm                                            35 kB/s |  37 kB     00:01
(115/133): gitweb-2.39.3-1.el9_2.noarch.rpm                                             120 kB/s | 143 kB     00:01
(116/133): git-svn-2.39.3-1.el9_2.noarch.rpm                                             60 kB/s |  69 kB     00:01
(117/133): git-instaweb-2.39.3-1.el9_2.noarch.rpm                                        33 kB/s |  25 kB     00:00
(118/133): subversion-1.14.1-5.el9_0.x86_64.rpm                                          56 kB/s | 1.0 MB     00:18
(119/133): gitk-2.39.3-1.el9_2.noarch.rpm                                                48 kB/s | 157 kB     00:03
(120/133): git-email-2.39.3-1.el9_2.noarch.rpm                                           43 kB/s |  53 kB     00:01
(121/133): git-all-2.39.3-1.el9_2.noarch.rpm                                             30 kB/s | 7.5 kB     00:00
(122/133): perl-parent-0.238-460.el9.noarch.rpm                                          49 kB/s |  14 kB     00:00
(123/133): git-gui-2.39.3-1.el9_2.noarch.rpm                                             54 kB/s | 242 kB     00:04
(124/133): perl-Time-HiRes-1.9764-462.el9.x86_64.rpm                                     70 kB/s |  57 kB     00:00
(125/133): git-subtree-2.39.3-1.el9_2.x86_64.rpm                                         48 kB/s |  34 kB     00:00
(126/133): libX11-1.7.0-7.el9.x86_64.rpm                                                103 kB/s | 652 kB     00:06
(127/133): git-credential-libsecret-2.39.3-1.el9_2.x86_64.rpm                            23 kB/s |  14 kB     00:00
(128/133): git-daemon-2.39.3-1.el9_2.x86_64.rpm                                          70 kB/s | 315 kB     00:04
(129/133): git-2.39.3-1.el9_2.x86_64.rpm                                                 41 kB/s |  61 kB     00:01
(130/133): git-core-doc-2.39.3-1.el9_2.noarch.rpm                                        92 kB/s | 2.6 MB     00:28
(131/133): tk-8.6.10-9.el9.x86_64.rpm                                                    77 kB/s | 1.6 MB     00:20
(132/133): httpd-core-2.4.53-11.el9_2.5.x86_64.rpm                                       77 kB/s | 1.4 MB     00:17
(133/133): git-core-2.39.3-1.el9_2.x86_64.rpm                                            83 kB/s | 4.2 MB     00:52
------------------------------------------------------------------------------------------------------------------------
Total                                                                                   123 kB/s |  26 MB     03:37
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : apr-1.7.0-11.el9.x86_64                                                                      1/133
  Installing       : apr-util-bdb-1.6.1-20.el9_2.1.x86_64                                                         2/133
  Installing       : apr-util-1.6.1-20.el9_2.1.x86_64                                                             3/133
  Installing       : apr-util-openssl-1.6.1-20.el9_2.1.x86_64                                                     4/133
  Installing       : perl-Digest-SHA-1:6.02-461.el9.x86_64                                                        5/133
  Installing       : perl-Digest-1.19-4.el9.noarch                                                                6/133
  Installing       : perl-GSSAPI-0.28-37.el9.x86_64                                                               7/133
  Installing       : perl-Tie-4.6-480.el9.noarch                                                                  8/133
  Installing       : perl-Digest-HMAC-1.03-29.el9.noarch                                                          9/133
  Installing       : perl-Digest-MD5-2.58-4.el9.x86_64                                                           10/133
  Installing       : perl-B-1.80-480.el9.x86_64                                                                  11/133
  Installing       : perl-FileHandle-2.03-480.el9.noarch                                                         12/133
  Installing       : perl-Authen-SASL-2.16-25.el9.noarch                                                         13/133
  Installing       : perl-Data-Dumper-2.174-462.el9.x86_64                                                       14/133
  Installing       : perl-AutoLoader-5.74-480.el9.noarch                                                         15/133
  Installing       : perl-libnet-3.13-4.el9.noarch                                                               16/133
  Installing       : perl-base-2.27-480.el9.noarch                                                               17/133
  Installing       : perl-Net-SSLeay-1.92-2.el9.x86_64                                                           18/133
  Installing       : perl-URI-5.09-3.el9.noarch                                                                  19/133
  Installing       : perl-Mozilla-CA-20200520-6.el9.noarch                                                       20/133
  Installing       : perl-Text-Tabs+Wrap-2013.0523-460.el9.noarch                                                21/133
  Installing       : perl-Pod-Escapes-1:1.07-460.el9.noarch                                                      22/133
  Installing       : perl-if-0.60.800-480.el9.noarch                                                             23/133
  Installing       : perl-File-Path-2.18-4.el9.noarch                                                            24/133
  Installing       : perl-IO-Socket-IP-0.41-5.el9.noarch                                                         25/133
  Installing       : perl-Time-Local-2:1.300-7.el9.noarch                                                        26/133
  Installing       : perl-IO-Socket-SSL-2.073-1.el9.noarch                                                       27/133
  Installing       : perl-Term-ANSIColor-5.01-461.el9.noarch                                                     28/133
  Installing       : perl-POSIX-1.94-480.el9.x86_64                                                              29/133
  Installing       : perl-Term-Cap-1.17-460.el9.noarch                                                           30/133
  Installing       : perl-subs-1.03-480.el9.noarch                                                               31/133
  Installing       : perl-Pod-Simple-1:3.42-4.el9.noarch                                                         32/133
  Installing       : perl-IPC-Open3-1.21-480.el9.noarch                                                          33/133
  Installing       : perl-Class-Struct-0.66-480.el9.noarch                                                       34/133
  Installing       : perl-File-Temp-1:0.231.100-4.el9.noarch                                                     35/133
  Installing       : perl-HTTP-Tiny-0.076-460.el9.noarch                                                         36/133
  Installing       : perl-Socket-4:2.031-4.el9.x86_64                                                            37/133
  Installing       : perl-Symbol-1.08-480.el9.noarch                                                             38/133
  Installing       : perl-SelectSaver-1.02-480.el9.noarch                                                        39/133
  Installing       : perl-File-stat-1.09-480.el9.noarch                                                          40/133
  Installing       : perl-podlators-1:4.14-460.el9.noarch                                                        41/133
  Installing       : perl-Pod-Perldoc-3.28.01-461.el9.noarch                                                     42/133
  Installing       : perl-mro-1.23-480.el9.x86_64                                                                43/133
  Installing       : perl-Fcntl-1.13-480.el9.x86_64                                                              44/133
  Installing       : perl-overloading-0.02-480.el9.noarch                                                        45/133
  Installing       : perl-IO-1.43-480.el9.x86_64                                                                 46/133
  Installing       : perl-Pod-Usage-4:2.01-4.el9.noarch                                                          47/133
  Installing       : perl-Text-ParseWords-3.30-460.el9.noarch                                                    48/133
  Installing       : perl-constant-1.33-461.el9.noarch                                                           49/133
  Installing       : perl-Scalar-List-Utils-4:1.56-461.el9.x86_64                                                50/133
  Installing       : perl-MIME-Base64-3.16-4.el9.x86_64                                                          51/133
  Installing       : perl-Errno-1.30-480.el9.x86_64                                                              52/133
  Installing       : perl-vars-1.05-480.el9.noarch                                                               53/133
  Installing       : perl-overload-1.31-480.el9.noarch                                                           54/133
  Installing       : perl-Getopt-Std-1.12-480.el9.noarch                                                         55/133
  Installing       : perl-File-Basename-2.85-480.el9.noarch                                                      56/133
  Installing       : perl-Storable-1:3.21-460.el9.x86_64                                                         57/133
  Installing       : perl-parent-1:0.238-460.el9.noarch                                                          58/133
  Installing       : perl-Getopt-Long-1:2.52-4.el9.noarch                                                        59/133
  Installing       : perl-Exporter-5.74-461.el9.noarch                                                           60/133
  Installing       : perl-Carp-1.50-460.el9.noarch                                                               61/133
  Installing       : perl-NDBM_File-1.15-480.el9.x86_64                                                          62/133
  Installing       : perl-PathTools-3.78-461.el9.x86_64                                                          63/133
  Installing       : perl-Encode-4:3.08-462.el9.x86_64                                                           64/133
  Installing       : perl-libs-4:5.32.1-480.el9.x86_64                                                           65/133
  Installing       : perl-interpreter-4:5.32.1-480.el9.x86_64                                                    66/133
  Installing       : git-core-2.39.3-1.el9_2.x86_64                                                              67/133
  Installing       : perl-lib-0.65-480.el9.x86_64                                                                68/133
  Installing       : perl-DynaLoader-1.47-480.el9.x86_64                                                         69/133
  Installing       : perl-TermReadKey-2.38-11.el9.x86_64                                                         70/133
  Installing       : perl-Net-SMTP-SSL-1.04-16.el9.noarch                                                        71/133
  Installing       : perl-TimeDate-1:2.33-6.el9.noarch                                                           72/133
  Installing       : perl-Compress-Raw-Zlib-2.101-5.el9.x86_64                                                   73/133
  Installing       : perl-Error-1:0.17029-7.el9.noarch                                                           74/133
  Installing       : perl-Memoize-1.03-480.el9.noarch                                                            75/133
  Installing       : perl-File-Find-1.37-480.el9.noarch                                                          76/133
  Installing       : utf8proc-2.6.1-4.el9.x86_64                                                                 77/133
  Installing       : mailcap-2.1.49-5.el9.noarch                                                                 78/133
  Installing       : perl-LWP-MediaTypes-6.04-9.el9.noarch                                                       79/133
  Installing       : perl-MailTools-2.21-9.el9.noarch                                                            80/133
  Installing       : perl-HTTP-Date-6.05-7.el9.noarch                                                            81/133
  Installing       : perl-Clone-0.45-6.el9.x86_64                                                                82/133
  Installing       : git-core-doc-2.39.3-1.el9_2.noarch                                                          83/133
  Installing       : git-subtree-2.39.3-1.el9_2.x86_64                                                           84/133
  Installing       : git-daemon-2.39.3-1.el9_2.x86_64                                                            85/133
  Running scriptlet: git-daemon-2.39.3-1.el9_2.x86_64                                                            85/133
  Installing       : perl-IO-HTML-1.004-4.el9.noarch                                                             86/133
  Installing       : perl-HTML-Tagset-3.20-47.el9.noarch                                                         87/133
  Installing       : perl-Compress-Raw-Bzip2-2.101-5.el9.x86_64                                                  88/133
  Installing       : perl-IO-Compress-2.102-4.el9.noarch                                                         89/133
  Installing       : perl-YAML-1.30-8.el9.noarch                                                                 90/133
  Installing       : perl-Sys-Hostname-1.23-480.el9.x86_64                                                       91/133
  Installing       : perl-I18N-Langinfo-0.19-480.el9.x86_64                                                      92/133
  Installing       : perl-Encode-Locale-1.05-21.el9.noarch                                                       93/133
  Installing       : perl-HTTP-Message-6.29-3.el9.noarch                                                         94/133
  Installing       : perl-HTML-Parser-3.76-3.el9.x86_64                                                          95/133
  Installing       : perl-CGI-4.51-5.el9.noarch                                                                  96/133
  Installing       : perl-filetest-1.03-480.el9.noarch                                                           97/133
  Installing       : perl-Term-ReadLine-1.17-480.el9.noarch                                                      98/133
  Installing       : perl-Time-HiRes-4:1.9764-462.el9.x86_64                                                     99/133
  Installing       : httpd-tools-2.4.53-11.el9_2.5.x86_64                                                       100/133
  Installing       : libserf-1.3.9-26.el9.x86_64                                                                101/133
  Installing       : subversion-libs-1.14.1-5.el9_0.x86_64                                                      102/133
  Installing       : subversion-perl-1.14.1-5.el9_0.x86_64                                                      103/133
  Installing       : subversion-1.14.1-5.el9_0.x86_64                                                           104/133
  Running scriptlet: subversion-1.14.1-5.el9_0.x86_64                                                           104/133
  Installing       : emacs-filesystem-1:27.2-8.el9_2.1.noarch                                                   105/133
  Installing       : perl-Git-2.39.3-1.el9_2.noarch                                                             106/133
  Installing       : git-2.39.3-1.el9_2.x86_64                                                                  107/133
  Installing       : perl-Git-SVN-2.39.3-1.el9_2.noarch                                                         108/133
  Installing       : git-svn-2.39.3-1.el9_2.noarch                                                              109/133
  Installing       : gitweb-2.39.3-1.el9_2.noarch                                                               110/133
  Installing       : git-email-2.39.3-1.el9_2.noarch                                                            111/133
  Running scriptlet: xml-common-0.6.3-58.el9.noarch                                                             112/133
  Installing       : xml-common-0.6.3-58.el9.noarch                                                             112/133
  Installing       : fontconfig-2.14.0-2.el9_1.x86_64                                                           113/133
  Running scriptlet: fontconfig-2.14.0-2.el9_1.x86_64                                                           113/133
  Installing       : libXau-1.0.9-8.el9.x86_64                                                                  114/133
  Installing       : libxcb-1.13.1-9.el9.x86_64                                                                 115/133
  Installing       : libX11-common-1.7.0-7.el9.noarch                                                           116/133
  Installing       : libX11-1.7.0-7.el9.x86_64                                                                  117/133
  Installing       : libXrender-0.9.10-16.el9.x86_64                                                            118/133
  Installing       : libXft-2.3.3-8.el9.x86_64                                                                  119/133
  Installing       : libsecret-0.20.4-4.el9.x86_64                                                              120/133
  Installing       : git-credential-libsecret-2.39.3-1.el9_2.x86_64                                             121/133
  Running scriptlet: httpd-filesystem-2.4.53-11.el9_2.5.noarch                                                  122/133
  Installing       : httpd-filesystem-2.4.53-11.el9_2.5.noarch                                                  122/133
  Installing       : httpd-core-2.4.53-11.el9_2.5.x86_64                                                        123/133
  Installing       : mod_lua-2.4.53-11.el9_2.5.x86_64                                                           124/133
  Installing       : mod_http2-1.15.19-4.el9_2.4.x86_64                                                         125/133
  Installing       : rocky-logos-httpd-90.14-1.el9.noarch                                                       126/133
  Installing       : httpd-2.4.53-11.el9_2.5.x86_64                                                             127/133
  Running scriptlet: httpd-2.4.53-11.el9_2.5.x86_64                                                             127/133
  Installing       : git-instaweb-2.39.3-1.el9_2.noarch                                                         128/133
  Installing       : tcl-1:8.6.10-7.el9.x86_64                                                                  129/133
  Running scriptlet: tk-1:8.6.10-9.el9.x86_64                                                                   130/133
  Installing       : tk-1:8.6.10-9.el9.x86_64                                                                   130/133
  Installing       : gitk-2.39.3-1.el9_2.noarch                                                                 131/133
  Installing       : git-gui-2.39.3-1.el9_2.noarch                                                              132/133
  Installing       : git-all-2.39.3-1.el9_2.noarch                                                              133/133
  Running scriptlet: fontconfig-2.14.0-2.el9_1.x86_64                                                           133/133
  Running scriptlet: httpd-2.4.53-11.el9_2.5.x86_64                                                             133/133
  Running scriptlet: git-all-2.39.3-1.el9_2.noarch                                                              133/133
  Verifying        : mailcap-2.1.49-5.el9.noarch                                                                  1/133
  Verifying        : tcl-1:8.6.10-7.el9.x86_64                                                                    2/133
  Verifying        : rocky-logos-httpd-90.14-1.el9.noarch                                                         3/133
  Verifying        : perl-Net-SMTP-SSL-1.04-16.el9.noarch                                                         4/133
  Verifying        : perl-Text-ParseWords-3.30-460.el9.noarch                                                     5/133
  Verifying        : perl-Exporter-5.74-461.el9.noarch                                                            6/133
  Verifying        : perl-Pod-Simple-1:3.42-4.el9.noarch                                                          7/133
  Verifying        : perl-File-Path-2.18-4.el9.noarch                                                             8/133
  Verifying        : perl-Pod-Perldoc-3.28.01-461.el9.noarch                                                      9/133
  Verifying        : perl-Encode-Locale-1.05-21.el9.noarch                                                       10/133
  Verifying        : perl-Getopt-Long-1:2.52-4.el9.noarch                                                        11/133
  Verifying        : perl-MailTools-2.21-9.el9.noarch                                                            12/133
  Verifying        : perl-TimeDate-1:2.33-6.el9.noarch                                                           13/133
  Verifying        : perl-Pod-Usage-4:2.01-4.el9.noarch                                                          14/133
  Verifying        : perl-IO-HTML-1.004-4.el9.noarch                                                             15/133
  Verifying        : perl-IO-Socket-IP-0.41-5.el9.noarch                                                         16/133
  Verifying        : perl-HTML-Tagset-3.20-47.el9.noarch                                                         17/133
  Verifying        : perl-Digest-HMAC-1.03-29.el9.noarch                                                         18/133
  Verifying        : perl-IO-Compress-2.102-4.el9.noarch                                                         19/133
  Verifying        : perl-Time-Local-2:1.300-7.el9.noarch                                                        20/133
  Verifying        : perl-libnet-3.13-4.el9.noarch                                                               21/133
  Verifying        : mod_lua-2.4.53-11.el9_2.5.x86_64                                                            22/133
  Verifying        : httpd-tools-2.4.53-11.el9_2.5.x86_64                                                        23/133
  Verifying        : httpd-2.4.53-11.el9_2.5.x86_64                                                              24/133
  Verifying        : httpd-filesystem-2.4.53-11.el9_2.5.noarch                                                   25/133
  Verifying        : apr-util-openssl-1.6.1-20.el9_2.1.x86_64                                                    26/133
  Verifying        : apr-util-bdb-1.6.1-20.el9_2.1.x86_64                                                        27/133
  Verifying        : apr-util-1.6.1-20.el9_2.1.x86_64                                                            28/133
  Verifying        : fontconfig-2.14.0-2.el9_1.x86_64                                                            29/133
  Verifying        : libsecret-0.20.4-4.el9.x86_64                                                               30/133
  Verifying        : libserf-1.3.9-26.el9.x86_64                                                                 31/133
  Verifying        : libX11-common-1.7.0-7.el9.noarch                                                            32/133
  Verifying        : perl-Carp-1.50-460.el9.noarch                                                               33/133
  Verifying        : perl-CGI-4.51-5.el9.noarch                                                                  34/133
  Verifying        : perl-LWP-MediaTypes-6.04-9.el9.noarch                                                       35/133
  Verifying        : perl-podlators-1:4.14-460.el9.noarch                                                        36/133
  Verifying        : perl-Term-Cap-1.17-460.el9.noarch                                                           37/133
  Verifying        : perl-Term-ANSIColor-5.01-461.el9.noarch                                                     38/133
  Verifying        : perl-HTTP-Tiny-0.076-460.el9.noarch                                                         39/133
  Verifying        : perl-Digest-1.19-4.el9.noarch                                                               40/133
  Verifying        : perl-URI-5.09-3.el9.noarch                                                                  41/133
  Verifying        : perl-constant-1.33-461.el9.noarch                                                           42/133
  Verifying        : perl-Encode-4:3.08-462.el9.x86_64                                                           43/133
  Verifying        : perl-Compress-Raw-Bzip2-2.101-5.el9.x86_64                                                  44/133
  Verifying        : perl-Clone-0.45-6.el9.x86_64                                                                45/133
  Verifying        : perl-Data-Dumper-2.174-462.el9.x86_64                                                       46/133
  Verifying        : perl-HTML-Parser-3.76-3.el9.x86_64                                                          47/133
  Verifying        : perl-Scalar-List-Utils-4:1.56-461.el9.x86_64                                                48/133
  Verifying        : perl-Compress-Raw-Zlib-2.101-5.el9.x86_64                                                   49/133
  Verifying        : perl-IO-Socket-SSL-2.073-1.el9.noarch                                                       50/133
  Verifying        : perl-Storable-1:3.21-460.el9.x86_64                                                         51/133
  Verifying        : perl-PathTools-3.78-461.el9.x86_64                                                          52/133
  Verifying        : perl-Digest-SHA-1:6.02-461.el9.x86_64                                                       53/133
  Verifying        : perl-Mozilla-CA-20200520-6.el9.noarch                                                       54/133
  Verifying        : perl-Text-Tabs+Wrap-2013.0523-460.el9.noarch                                                55/133
  Verifying        : perl-Digest-MD5-2.58-4.el9.x86_64                                                           56/133
  Verifying        : perl-Authen-SASL-2.16-25.el9.noarch                                                         57/133
  Verifying        : perl-Pod-Escapes-1:1.07-460.el9.noarch                                                      58/133
  Verifying        : perl-Error-1:0.17029-7.el9.noarch                                                           59/133
  Verifying        : utf8proc-2.6.1-4.el9.x86_64                                                                 60/133
  Verifying        : perl-MIME-Base64-3.16-4.el9.x86_64                                                          61/133
  Verifying        : perl-GSSAPI-0.28-37.el9.x86_64                                                              62/133
  Verifying        : perl-YAML-1.30-8.el9.noarch                                                                 63/133
  Verifying        : perl-HTTP-Date-6.05-7.el9.noarch                                                            64/133
  Verifying        : perl-HTTP-Message-6.29-3.el9.noarch                                                         65/133
  Verifying        : perl-File-Temp-1:0.231.100-4.el9.noarch                                                     66/133
  Verifying        : libXau-1.0.9-8.el9.x86_64                                                                   67/133
  Verifying        : libxcb-1.13.1-9.el9.x86_64                                                                  68/133
  Verifying        : libXrender-0.9.10-16.el9.x86_64                                                             69/133
  Verifying        : mod_http2-1.15.19-4.el9_2.4.x86_64                                                          70/133
  Verifying        : libXft-2.3.3-8.el9.x86_64                                                                   71/133
  Verifying        : apr-1.7.0-11.el9.x86_64                                                                     72/133
  Verifying        : perl-Socket-4:2.031-4.el9.x86_64                                                            73/133
  Verifying        : perl-mro-1.23-480.el9.x86_64                                                                74/133
  Verifying        : perl-libs-4:5.32.1-480.el9.x86_64                                                           75/133
  Verifying        : perl-lib-0.65-480.el9.x86_64                                                                76/133
  Verifying        : perl-interpreter-4:5.32.1-480.el9.x86_64                                                    77/133
  Verifying        : perl-Sys-Hostname-1.23-480.el9.x86_64                                                       78/133
  Verifying        : perl-POSIX-1.94-480.el9.x86_64                                                              79/133
  Verifying        : perl-NDBM_File-1.15-480.el9.x86_64                                                          80/133
  Verifying        : perl-IO-1.43-480.el9.x86_64                                                                 81/133
  Verifying        : perl-I18N-Langinfo-0.19-480.el9.x86_64                                                      82/133
  Verifying        : perl-Fcntl-1.13-480.el9.x86_64                                                              83/133
  Verifying        : perl-Errno-1.30-480.el9.x86_64                                                              84/133
  Verifying        : perl-DynaLoader-1.47-480.el9.x86_64                                                         85/133
  Verifying        : perl-B-1.80-480.el9.x86_64                                                                  86/133
  Verifying        : perl-vars-1.05-480.el9.noarch                                                               87/133
  Verifying        : perl-subs-1.03-480.el9.noarch                                                               88/133
  Verifying        : perl-overloading-0.02-480.el9.noarch                                                        89/133
  Verifying        : perl-overload-1.31-480.el9.noarch                                                           90/133
  Verifying        : perl-if-0.60.800-480.el9.noarch                                                             91/133
  Verifying        : perl-filetest-1.03-480.el9.noarch                                                           92/133
  Verifying        : perl-base-2.27-480.el9.noarch                                                               93/133
  Verifying        : perl-Tie-4.6-480.el9.noarch                                                                 94/133
  Verifying        : perl-Term-ReadLine-1.17-480.el9.noarch                                                      95/133
  Verifying        : perl-Symbol-1.08-480.el9.noarch                                                             96/133
  Verifying        : perl-SelectSaver-1.02-480.el9.noarch                                                        97/133
  Verifying        : perl-Memoize-1.03-480.el9.noarch                                                            98/133
  Verifying        : perl-IPC-Open3-1.21-480.el9.noarch                                                          99/133
  Verifying        : perl-Getopt-Std-1.12-480.el9.noarch                                                        100/133
  Verifying        : perl-FileHandle-2.03-480.el9.noarch                                                        101/133
  Verifying        : perl-File-stat-1.09-480.el9.noarch                                                         102/133
  Verifying        : perl-File-Find-1.37-480.el9.noarch                                                         103/133
  Verifying        : perl-File-Basename-2.85-480.el9.noarch                                                     104/133
  Verifying        : perl-Class-Struct-0.66-480.el9.noarch                                                      105/133
  Verifying        : perl-AutoLoader-5.74-480.el9.noarch                                                        106/133
  Verifying        : subversion-perl-1.14.1-5.el9_0.x86_64                                                      107/133
  Verifying        : subversion-libs-1.14.1-5.el9_0.x86_64                                                      108/133
  Verifying        : subversion-1.14.1-5.el9_0.x86_64                                                           109/133
  Verifying        : perl-Net-SSLeay-1.92-2.el9.x86_64                                                          110/133
  Verifying        : xml-common-0.6.3-58.el9.noarch                                                             111/133
  Verifying        : emacs-filesystem-1:27.2-8.el9_2.1.noarch                                                   112/133
  Verifying        : perl-TermReadKey-2.38-11.el9.x86_64                                                        113/133
  Verifying        : perl-Git-SVN-2.39.3-1.el9_2.noarch                                                         114/133
  Verifying        : perl-Git-2.39.3-1.el9_2.noarch                                                             115/133
  Verifying        : gitweb-2.39.3-1.el9_2.noarch                                                               116/133
  Verifying        : gitk-2.39.3-1.el9_2.noarch                                                                 117/133
  Verifying        : git-svn-2.39.3-1.el9_2.noarch                                                              118/133
  Verifying        : git-instaweb-2.39.3-1.el9_2.noarch                                                         119/133
  Verifying        : git-gui-2.39.3-1.el9_2.noarch                                                              120/133
  Verifying        : git-email-2.39.3-1.el9_2.noarch                                                            121/133
  Verifying        : git-core-doc-2.39.3-1.el9_2.noarch                                                         122/133
  Verifying        : git-all-2.39.3-1.el9_2.noarch                                                              123/133
  Verifying        : perl-parent-1:0.238-460.el9.noarch                                                         124/133
  Verifying        : libX11-1.7.0-7.el9.x86_64                                                                  125/133
  Verifying        : perl-Time-HiRes-4:1.9764-462.el9.x86_64                                                    126/133
  Verifying        : git-subtree-2.39.3-1.el9_2.x86_64                                                          127/133
  Verifying        : git-daemon-2.39.3-1.el9_2.x86_64                                                           128/133
  Verifying        : git-credential-libsecret-2.39.3-1.el9_2.x86_64                                             129/133
  Verifying        : git-core-2.39.3-1.el9_2.x86_64                                                             130/133
  Verifying        : git-2.39.3-1.el9_2.x86_64                                                                  131/133
  Verifying        : tk-1:8.6.10-9.el9.x86_64                                                                   132/133
  Verifying        : httpd-core-2.4.53-11.el9_2.5.x86_64                                                        133/133

Installed:
  apr-1.7.0-11.el9.x86_64                                     apr-util-1.6.1-20.el9_2.1.x86_64
  apr-util-bdb-1.6.1-20.el9_2.1.x86_64                        apr-util-openssl-1.6.1-20.el9_2.1.x86_64
  emacs-filesystem-1:27.2-8.el9_2.1.noarch                    fontconfig-2.14.0-2.el9_1.x86_64
  git-2.39.3-1.el9_2.x86_64                                   git-all-2.39.3-1.el9_2.noarch
  git-core-2.39.3-1.el9_2.x86_64                              git-core-doc-2.39.3-1.el9_2.noarch
  git-credential-libsecret-2.39.3-1.el9_2.x86_64              git-daemon-2.39.3-1.el9_2.x86_64
  git-email-2.39.3-1.el9_2.noarch                             git-gui-2.39.3-1.el9_2.noarch
  git-instaweb-2.39.3-1.el9_2.noarch                          git-subtree-2.39.3-1.el9_2.x86_64
  git-svn-2.39.3-1.el9_2.noarch                               gitk-2.39.3-1.el9_2.noarch
  gitweb-2.39.3-1.el9_2.noarch                                httpd-2.4.53-11.el9_2.5.x86_64
  httpd-core-2.4.53-11.el9_2.5.x86_64                         httpd-filesystem-2.4.53-11.el9_2.5.noarch
  httpd-tools-2.4.53-11.el9_2.5.x86_64                        libX11-1.7.0-7.el9.x86_64
  libX11-common-1.7.0-7.el9.noarch                            libXau-1.0.9-8.el9.x86_64
  libXft-2.3.3-8.el9.x86_64                                   libXrender-0.9.10-16.el9.x86_64
  libsecret-0.20.4-4.el9.x86_64                               libserf-1.3.9-26.el9.x86_64
  libxcb-1.13.1-9.el9.x86_64                                  mailcap-2.1.49-5.el9.noarch
  mod_http2-1.15.19-4.el9_2.4.x86_64                          mod_lua-2.4.53-11.el9_2.5.x86_64
  perl-Authen-SASL-2.16-25.el9.noarch                         perl-AutoLoader-5.74-480.el9.noarch
  perl-B-1.80-480.el9.x86_64                                  perl-CGI-4.51-5.el9.noarch
  perl-Carp-1.50-460.el9.noarch                               perl-Class-Struct-0.66-480.el9.noarch
  perl-Clone-0.45-6.el9.x86_64                                perl-Compress-Raw-Bzip2-2.101-5.el9.x86_64
  perl-Compress-Raw-Zlib-2.101-5.el9.x86_64                   perl-Data-Dumper-2.174-462.el9.x86_64
  perl-Digest-1.19-4.el9.noarch                               perl-Digest-HMAC-1.03-29.el9.noarch
  perl-Digest-MD5-2.58-4.el9.x86_64                           perl-Digest-SHA-1:6.02-461.el9.x86_64
  perl-DynaLoader-1.47-480.el9.x86_64                         perl-Encode-4:3.08-462.el9.x86_64
  perl-Encode-Locale-1.05-21.el9.noarch                       perl-Errno-1.30-480.el9.x86_64
  perl-Error-1:0.17029-7.el9.noarch                           perl-Exporter-5.74-461.el9.noarch
  perl-Fcntl-1.13-480.el9.x86_64                              perl-File-Basename-2.85-480.el9.noarch
  perl-File-Find-1.37-480.el9.noarch                          perl-File-Path-2.18-4.el9.noarch
  perl-File-Temp-1:0.231.100-4.el9.noarch                     perl-File-stat-1.09-480.el9.noarch
  perl-FileHandle-2.03-480.el9.noarch                         perl-GSSAPI-0.28-37.el9.x86_64
  perl-Getopt-Long-1:2.52-4.el9.noarch                        perl-Getopt-Std-1.12-480.el9.noarch
  perl-Git-2.39.3-1.el9_2.noarch                              perl-Git-SVN-2.39.3-1.el9_2.noarch
  perl-HTML-Parser-3.76-3.el9.x86_64                          perl-HTML-Tagset-3.20-47.el9.noarch
  perl-HTTP-Date-6.05-7.el9.noarch                            perl-HTTP-Message-6.29-3.el9.noarch
  perl-HTTP-Tiny-0.076-460.el9.noarch                         perl-I18N-Langinfo-0.19-480.el9.x86_64
  perl-IO-1.43-480.el9.x86_64                                 perl-IO-Compress-2.102-4.el9.noarch
  perl-IO-HTML-1.004-4.el9.noarch                             perl-IO-Socket-IP-0.41-5.el9.noarch
  perl-IO-Socket-SSL-2.073-1.el9.noarch                       perl-IPC-Open3-1.21-480.el9.noarch
  perl-LWP-MediaTypes-6.04-9.el9.noarch                       perl-MIME-Base64-3.16-4.el9.x86_64
  perl-MailTools-2.21-9.el9.noarch                            perl-Memoize-1.03-480.el9.noarch
  perl-Mozilla-CA-20200520-6.el9.noarch                       perl-NDBM_File-1.15-480.el9.x86_64
  perl-Net-SMTP-SSL-1.04-16.el9.noarch                        perl-Net-SSLeay-1.92-2.el9.x86_64
  perl-POSIX-1.94-480.el9.x86_64                              perl-PathTools-3.78-461.el9.x86_64
  perl-Pod-Escapes-1:1.07-460.el9.noarch                      perl-Pod-Perldoc-3.28.01-461.el9.noarch
  perl-Pod-Simple-1:3.42-4.el9.noarch                         perl-Pod-Usage-4:2.01-4.el9.noarch
  perl-Scalar-List-Utils-4:1.56-461.el9.x86_64                perl-SelectSaver-1.02-480.el9.noarch
  perl-Socket-4:2.031-4.el9.x86_64                            perl-Storable-1:3.21-460.el9.x86_64
  perl-Symbol-1.08-480.el9.noarch                             perl-Sys-Hostname-1.23-480.el9.x86_64
  perl-Term-ANSIColor-5.01-461.el9.noarch                     perl-Term-Cap-1.17-460.el9.noarch
  perl-Term-ReadLine-1.17-480.el9.noarch                      perl-TermReadKey-2.38-11.el9.x86_64
  perl-Text-ParseWords-3.30-460.el9.noarch                    perl-Text-Tabs+Wrap-2013.0523-460.el9.noarch
  perl-Tie-4.6-480.el9.noarch                                 perl-Time-HiRes-4:1.9764-462.el9.x86_64
  perl-Time-Local-2:1.300-7.el9.noarch                        perl-TimeDate-1:2.33-6.el9.noarch
  perl-URI-5.09-3.el9.noarch                                  perl-YAML-1.30-8.el9.noarch
  perl-base-2.27-480.el9.noarch                               perl-constant-1.33-461.el9.noarch
  perl-filetest-1.03-480.el9.noarch                           perl-if-0.60.800-480.el9.noarch
  perl-interpreter-4:5.32.1-480.el9.x86_64                    perl-lib-0.65-480.el9.x86_64
  perl-libnet-3.13-4.el9.noarch                               perl-libs-4:5.32.1-480.el9.x86_64
  perl-mro-1.23-480.el9.x86_64                                perl-overload-1.31-480.el9.noarch
  perl-overloading-0.02-480.el9.noarch                        perl-parent-1:0.238-460.el9.noarch
  perl-podlators-1:4.14-460.el9.noarch                        perl-subs-1.03-480.el9.noarch
  perl-vars-1.05-480.el9.noarch                               rocky-logos-httpd-90.14-1.el9.noarch
  subversion-1.14.1-5.el9_0.x86_64                            subversion-libs-1.14.1-5.el9_0.x86_64
  subversion-perl-1.14.1-5.el9_0.x86_64                       tcl-1:8.6.10-7.el9.x86_64
  tk-1:8.6.10-9.el9.x86_64                                    utf8proc-2.6.1-4.el9.x86_64
  xml-common-0.6.3-58.el9.noarch

Complete!
[tom@efrei-xmg4agau1 ~]$
```

**ensuite on créer un répertoire pour y mettre notre .yml et notre Dockerfile**

```
[tom@efrei-xmg4agau1 ~]$ mkdir appli
[tom@efrei-xmg4agau1 ~]$ cd appli/
[tom@efrei-xmg4agau1 appli]$ sudo nano Dockerfile
[tom@efrei-xmg4agau1 appli]$ sudo nano docker-compose;yml
-bash: yml: command not found-bash: yml: command not found
[tom@efrei-xmg4agau1 appli]$ sudo nano docker-compose.yml
```

**contenue .yml**

```
version: "3"

services:
  app:
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - "3000:3000"

```

**contenue Dockerfile**

```
FROM node:lts

WORKDIR /appli

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "start"]

```

**je clone git**

```
[tom@efrei-xmg4agau1 appli]$ git clone https://github.com/LMqerto/appli.git
Cloning into 'appli'...
remote: Enumerating objects: 4, done.
remote: Counting objects: 100% (4/4), done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 4 (delta 0), reused 4 (delta 0), pack-reused 0
Receiving objects: 100% (4/4), done.
```

**je lance le docker build mais avant je décale les fichiers au bon endroit**

```
[tom@efrei-xmg4agau1 appli]$ sudo mv appli/app.js  /home/tom/appli/
[tom@efrei-xmg4agau1 appli]$ sudo mv appli/package.json  /home/tom/appli/
[tom@efrei-xmg4agau1 appli]$ ls
app.js  appli  docker-compose.yml  Dockerfile  package.json
[tom@efrei-xmg4agau1 appli]$ docker build -t mon-application .
[+] Building 8.7s (10/10) FINISHED                                                                       docker:default
 => [internal] load build definition from Dockerfile                                                               0.0s
 => => transferring dockerfile: 212B                                                                               0.0s
 => [internal] load .dockerignore                                                                                  0.0s
 => => transferring context: 2B                                                                                    0.0s
 => [internal] load metadata for docker.io/library/node:lts                                                        0.4s
 => [1/5] FROM docker.io/library/node:lts@sha256:5f21943fe97b24ae1740da6d7b9c56ac43fe3495acb47c1b232b0a352b02a25c  0.0s
 => [internal] load build context                                                                                  0.0s
 => => transferring context: 5.57kB                                                                                0.0s
 => CACHED [2/5] WORKDIR appli/                                                                                    0.0s
 => [3/5] COPY package*.json ./                                                                                    0.0s
 => [4/5] RUN npm install                                                                                          8.0s
 => [5/5] COPY . .                                                                                                 0.0s
 => exporting to image                                                                                             0.1s
 => => exporting layers                                                                                            0.1s
 => => writing image sha256:699c03390af3db8356c5bc5eea3e514ea3865cf74726d4418e12f7cf2d084bb9                       0.0s
 => => naming to docker.io/library/mon-application                                                                 0.0s
[tom@efrei-xmg4agau1 appli]$


```

**je lance l'appli**

```
[tom@efrei-xmg4agau1 appli]$ docker compose up
[+] Building 1.0s (10/10) FINISHED                                                                       docker:default
 => [app internal] load build definition from Dockerfile                                                           0.0s
 => => transferring dockerfile: 212B                                                                               0.0s
 => [app internal] load .dockerignore                                                                              0.0s
 => => transferring context: 2B                                                                                    0.0s
 => [app internal] load metadata for docker.io/library/node:lts                                                    0.9s
 => [app 1/5] FROM docker.io/library/node:lts@sha256:5f21943fe97b24ae1740da6d7b9c56ac43fe3495acb47c1b232b0a352b02  0.0s
 => [app internal] load build context                                                                              0.0s
 => => transferring context: 5.22kB                                                                                0.0s
 => CACHED [app 2/5] WORKDIR appli/                                                                                0.0s
 => CACHED [app 3/5] COPY package*.json ./                                                                         0.0s
 => CACHED [app 4/5] RUN npm install                                                                               0.0s
 => CACHED [app 5/5] COPY . .                                                                                      0.0s
 => [app] exporting to image                                                                                       0.0s
 => => exporting layers                                                                                            0.0s
 => => writing image sha256:ab8c232e407a7bb3cc2b4565fa6ac9337c329e155b9d2dae1a51e504dfe6c11e                       0.0s
 => => naming to docker.io/library/appli-app                                                                       0.0s
[+] Running 2/1
 ✔ Network appli_default  Created                                                                                  0.1s
 ✔ Container appli-app-1  Created                                                                                  0.0s
Attaching to appli-app-1
appli-app-1  |
appli-app-1  | > mon-application@1.0.0 start
appli-app-1  | > node app.js
appli-app-1  |
appli-app-1  | L'application écoute sur le port 3000

```
























