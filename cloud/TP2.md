# compte rendu TP2 cloud 
# partie 1
**Verification bonne instalation**

```
az group create --name efreitp2 --location francecentral
{
  "id": "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2",
  "location": "francecentral",
  "managedBy": null,
  "name": "efreitp2",
  "properties": {
    "provisioningState": "Succeeded"
  },
  "tags": null,
  "type": "Microsoft.Resources/resourceGroups"
}
az>>
```

**créer vm avec UI**

```
PS C:\Users\tomto> ssh tom@20.199.91.162
The authenticity of host '20.199.91.162 (20.199.91.162)' can't be established.
ECDSA key fingerprint is SHA256:lKQbg1czDGx8Ik6fN7VeQ1SV4u2Sly9Yzfsh+PvPw6w.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '20.199.91.162' (ECDSA) to the list of known hosts.
Welcome to Ubuntu 22.04.3 LTS (GNU/Linux 6.2.0-1018-azure x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Tue Jan  9 14:59:26 UTC 2024

  System load:  0.498046875       Processes:             104
  Usage of /:   5.1% of 28.89GB   Users logged in:       0
  Memory usage: 32%               IPv4 address for eth0: 10.0.0.4
  Swap usage:   0%

Expanded Security Maintenance for Applications is not enabled.

0 updates can be applied immediately.

Enable ESM Apps to receive additional future security updates.
See https://ubuntu.com/esm or run: sudo pro status


The list of available updates is more than a week old.
To check for new updates run: sudo apt update


The programs included with the Ubuntu system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
applicable law.

To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

tom@pc1:~$
```

**création vm avec azure CLI**

```
PS C:\Users\tomto> az interactive
This command is in preview and under development. Reference and support levels: https://aka.ms/CLI_refstatus
Installing the Interactive extension...
Default enabled including preview versions for extension installation now. Disabled in May 2024. Use '--allow-preview true' to enable it specifically if needed. Use '--allow-preview false' to install stable version only.
The installed extension 'interactive' is in preview.

Do you agree to sending telemetry (yes/no)? Default answer is yes:

A new Recommender is added which can make the completion ability more intelligent and provide the scenario completion!
If you want to disable this feature, you can use 'az config set interactive.enable_recommender=False' to disable it.

A command preload mechanism was added to prevent lagging and command run errors.
You can skip preloading in a single pass by CTRL+C or turn it off by setting 'az config set interactive.enable_preloading=False'

Loading command table... Expected time around 1 minute.

az>>
vm create --name pc2 -g efreitp2 --image Ubuntu2204 --admin-username tom --ssh-key-values "C:\Users\tomto\.ssh\id_rsa.p"
Selecting "northeurope" may reduce your costs. The region you've selected may cost more for the same services. You can disable this message in the future with the command "az config set core.display_region_identified=false". Learn more at https://go.microsoft.com/fwlink/?linkid=222571

{
  "fqdns": "",
  "id": "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/virtualMachines/pc2",
  "location": "francecentral",
  "macAddress": "60-45-BD-6B-D1-64",
  "powerState": "VM running",
  "privateIpAddress": "10.0.0.5",
  "publicIpAddress": "20.216.160.249",
  "resourceGroup": "efreitp2",
  "zones": ""
}
az>>
```


**créer deux vm avec CLi et ping**
création Vm (a faire deux fois en changeant le nom) 

```
vm create --name pc3 -g efreitp2 --image Ubuntu2204 --admin-username tom --ssh-key-values "C:\Users\tomto\.ssh\id_rsa.p"
Selecting "northeurope" may reduce your costs. The region you've selected may cost more for the same services. You can disable this message in the future with the command "az config set core.display_region_identified=false". Learn more at https://go.microsoft.com/fwlink/?linkid=222571

{
  "fqdns": "",
  "id": "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/virtualMachines/pc2",
  "location": "francecentral",
  "macAddress": "60-45-BD-6B-D1-64",
  "powerState": "VM running",
  "privateIpAddress": "10.0.0.5",
  "publicIpAddress": "20.216.160.249",
  "resourceGroup": "efreitp2",
  "zones": ""
}
az>>
```


ping d'une machine a l'autre
```
PS C:\Users\tomto> ssh tom@20.199.91.162
Welcome to Ubuntu 22.04.3 LTS (GNU/Linux 6.2.0-1018-azure x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Tue Jan  9 15:08:42 UTC 2024

  System load:  0.0               Processes:             100
  Usage of /:   5.2% of 28.89GB   Users logged in:       0
  Memory usage: 35%               IPv4 address for eth0: 10.0.0.4
  Swap usage:   0%


Expanded Security Maintenance for Applications is not enabled.

0 updates can be applied immediately.

Enable ESM Apps to receive additional future security updates.
See https://ubuntu.com/esm or run: sudo pro status


The list of available updates is more than a week old.
To check for new updates run: sudo apt update

Last login: Tue Jan  9 14:59:28 2024 from 217.26.204.244
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

tom@pc1:~$ ping 10.0.0.5
PING 10.0.0.5 (10.0.0.5) 56(84) bytes of data.
64 bytes from 10.0.0.5: icmp_seq=1 ttl=64 time=3.37 ms
64 bytes from 10.0.0.5: icmp_seq=2 ttl=64 time=2.34 ms
64 bytes from 10.0.0.5: icmp_seq=3 ttl=64 time=2.50 ms
64 bytes from 10.0.0.5: icmp_seq=4 ttl=64 time=2.23 ms
^C
--- 10.0.0.5 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 2.230/2.612/3.371/0.448 ms
```
**tester cloud init**

```
--admin-username tom --ssh-key-values "C:\Users\tomto\.ssh\id_rsa.pub" --custom-data "C:\cour\cloud\#cloud-config.txt"
Selecting "northeurope" may reduce your costs. The region you've selected may cost more for the same services. You can disable this message in the future with the command "az config set core.display_region_identified=false". Learn more at https://go.microsoft.com/fwlink/?linkid=222571

{
  "fqdns": "",
  "id": "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/virtualMachines/pc3",
  "location": "francecentral",
  "macAddress": "60-45-BD-6E-BC-90",
  "powerState": "VM running",
  "privateIpAddress": "10.0.0.6",
  "publicIpAddress": "20.216.188.137",
  "resourceGroup": "efreitp2",
  "zones": ""
}
az>>
```

**vérifier que le cloud init fonctionne**
fichier init
```
#cloud-config
users:
  - name: <tom>
    primary_group: <tom>
    groups: sudo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    lock_passwd: false
    passwd: <$6$YE7RpAG.OxgXvTXe$L.zPZWHtHmLpYh7IH3QzOpbVpE4g.wuo8J/YoEgwkPKKpkRM.zPCTjCtyFHgjDsquWDq/DUM/NXY95d5RtVM9/>
    ssh_authorized_keys:
      - <ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCe0FRYpkm54jl913sRyM8ispSvS14pmMLGTxsEZHiwwSaHyIeUbVOpF9826lEMy0Ks0jgFoXGEFp3K05jFdCrmHte3/1+39CSv/xtxyACQOk1C8oLKbahnspZ+FR9gVBd8/QPsHin997/fQ+8ST6LmorP+NYQ4vZP8drIkwXPnk+EctsI8KYdZf6s1+Fj04gkfdQpH7pF4s4SuawOpAY5EC73Lm77fkw9o6npQCwr5EIFAeUbaI/ZLtgAPRKAtDveOGeWHga/GIDAC66gx1uC4ztFUzoM+bz1lcBWYdsYTuy4KpqC8zApN0LHiOVIJR/NZZtt7fw0dqgcYJie0NiSprQz78QP88DYQl7d1CKydtWo0sNeZWjkkNQlpsSzhIhu5cuxwOS+OXyRQ4rqW59rpXal6FFioGfPk4Zw4embHETE33xEmUVyGxx8jLLVs4N+qLIjoAcCuVXI3s7tbB9kDJ1RTvrL1ibZWLvZQD7us/tbph7rUIqqFu5OgOWjQ+tOMzMQr9pdc7B/8WYrZdMSm5cR/P+DD6z7DciBKohc7xBGG9uuwD3J8R7Ui661n2SrUgo4hAbRZiaNru4VBIv+hJ4M8nsh/o7XO+KAYakVwbkuOCXQJ4txI6wQpV2rBLvc5p4iwOUhOkK4yW0PWWoTmq05p25oKkd1ow4SNJfWJzQ== tomto@MSI
>

```

verifier si l'utilisateur existe et la clé publique existe

```
tom@pc3:~$ less /etc/passwd|grep "/bin/bash"
root:x:0:0:root:/root:/bin/bash
tom:x:1000:1000:Ubuntu:/home/tom:/bin/bash
tom@pc3:~$ cd .ssh/
tom@pc3:~/.ssh$ ls
authorized_keys
tom@pc3:~/.ssh$ cat authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCe0FRYpkm54jl913sRyM8ispSvS14pmMLGTxsEZHiwwSaHyIeUbVOpF9826lEMy0Ks0jgFoXGEFp3K05jFdCrmHte3/1+39CSv/xtxyACQOk1C8oLKbahnspZ+FR9gVBd8/QPsHin997/fQ+8ST6LmorP+NYQ4vZP8drIkwXPnk+EctsI8KYdZf6s1+Fj04gkfdQpH7pF4s4SuawOpAY5EC73Lm77fkw9o6npQCwr5EIFAeUbaI/ZLtgAPRKAtDveOGeWHga/GIDAC66gx1uC4ztFUzoM+bz1lcBWYdsYTuy4KpqC8zApN0LHiOVIJR/NZZtt7fw0dqgcYJie0NiSprQz78QP88DYQl7d1CKydtWo0sNeZWjkkNQlpsSzhIhu5cuxwOS+OXyRQ4rqW59rpXal6FFioGfPk4Zw4embHETE33xEmUVyGxx8jLLVs4N+qLIjoAcCuVXI3s7tbB9kDJ1RTvrL1ibZWLvZQD7us/tbph7rUIqqFu5OgOWjQ+tOMzMQr9pdc7B/8WYrZdMSm5cR/P+DD6z7DciBKohc7xBGG9uuwD3J8R7Ui661n2SrUgo4hAbRZiaNru4VBIv+hJ4M8nsh/o7XO+KAYakVwbkuOCXQJ4txI6wQpV2rBLvc5p4iwOUhOkK4yW0PWWoTmq05p25oKkd1ow4SNJfWJzQ== tomto@MSI
tom@pc3:~/.ssh$
```

# partie 2

**créer la vm**

```
}
vm create --name pc1 -g efreitp2 --image Ubuntu2204 --admin-username tom --ssh-key-values "C:\Users\tomto\.ssh\id_rsa.pub" --
Selecting "northeurope" may reduce your costs. The region you've selected may cost more for the same services. You can disable this message in the future with the command "az config set core.display_region_identified=false". Learn more at https://go.microsoft.com/fwlink/?linkid=222571

{
  "fqdns": "",
  "id": "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/virtualMachines/pc1",
  "location": "francecentral",
  "macAddress": "60-45-BD-19-38-0B",
  "powerState": "VM running",
  "privateIpAddress": "10.0.0.4",
  "publicIpAddress": "20.43.63.221",
  "resourceGroup": "efreitp2",
  "zones": ""
}

```

**installer docker**

```
PS C:\Users\tomto> ssh tom@4.233.69.210
The authenticity of host '4.233.69.210 (4.233.69.210)' can't be established.
ECDSA key fingerprint is SHA256:v8R03PLZnPLQYci6yJbr8y7TmX07N7FZhh1GftFilT8.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '4.233.69.210' (ECDSA) to the list of known hosts.
Welcome to Ubuntu 22.04.3 LTS (GNU/Linux 6.2.0-1018-azure x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Tue Jan  9 16:10:00 UTC 2024

  System load:  0.25732421875     Processes:             107
  Usage of /:   5.1% of 28.89GB   Users logged in:       0
  Memory usage: 8%                IPv4 address for eth0: 10.0.0.4
  Swap usage:   0%

Expanded Security Maintenance for Applications is not enabled.

0 updates can be applied immediately.

Enable ESM Apps to receive additional future security updates.
See https://ubuntu.com/esm or run: sudo pro status

tom@pc1:~$
The list of available updates is more than a week old.
To check for new updates run: sudo apt update


The programs included with the Ubuntu system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
applicable law.

To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

tom@pc1:~$ sudo apt-get update
Hit:1 http://azure.archive.ubuntu.com/ubuntu jammy InRelease
Get:2 http://azure.archive.ubuntu.com/ubuntu jammy-updates InRelease [119 kB]
Get:3 http://azure.archive.ubuntu.com/ubuntu jammy-backports InRelease [109 kB]
Get:4 http://azure.archive.ubuntu.com/ubuntu jammy-security InRelease [110 kB]
Get:5 http://azure.archive.ubuntu.com/ubuntu jammy/universe amd64 Packages [14.1 MB]
Get:6 http://azure.archive.ubuntu.com/ubuntu jammy/universe Translation-en [5652 kB]
Get:7 http://azure.archive.ubuntu.com/ubuntu jammy/universe amd64 c-n-f Metadata [286 kB]
Get:8 http://azure.archive.ubuntu.com/ubuntu jammy/multiverse amd64 Packages [217 kB]
Get:9 http://azure.archive.ubuntu.com/ubuntu jammy/multiverse Translation-en [112 kB]
Get:10 http://azure.archive.ubuntu.com/ubuntu jammy/multiverse amd64 c-n-f Metadata [8372 B]
Get:11 http://azure.archive.ubuntu.com/ubuntu jammy-updates/main amd64 Packages [1268 kB]
Get:12 http://azure.archive.ubuntu.com/ubuntu jammy-updates/main Translation-en [260 kB]
Get:13 http://azure.archive.ubuntu.com/ubuntu jammy-updates/restricted amd64 Packages [1259 kB]
Get:14 http://azure.archive.ubuntu.com/ubuntu jammy-updates/restricted Translation-en [205 kB]
Get:15 http://azure.archive.ubuntu.com/ubuntu jammy-updates/universe amd64 Packages [1022 kB]
Get:16 http://azure.archive.ubuntu.com/ubuntu jammy-updates/universe Translation-en [227 kB]
Get:17 http://azure.archive.ubuntu.com/ubuntu jammy-updates/universe amd64 c-n-f Metadata [22.1 kB]
Get:18 http://azure.archive.ubuntu.com/ubuntu jammy-updates/multiverse amd64 Packages [42.1 kB]
Get:19 http://azure.archive.ubuntu.com/ubuntu jammy-updates/multiverse Translation-en [10.1 kB]
Get:20 http://azure.archive.ubuntu.com/ubuntu jammy-updates/multiverse amd64 c-n-f Metadata [472 B]
Get:21 http://azure.archive.ubuntu.com/ubuntu jammy-backports/main amd64 Packages [41.7 kB]
Get:22 http://azure.archive.ubuntu.com/ubuntu jammy-backports/main Translation-en [10.5 kB]
Get:23 http://azure.archive.ubuntu.com/ubuntu jammy-backports/main amd64 c-n-f Metadata [388 B]
Get:24 http://azure.archive.ubuntu.com/ubuntu jammy-backports/restricted amd64 c-n-f Metadata [116 B]
Get:25 http://azure.archive.ubuntu.com/ubuntu jammy-backports/universe amd64 Packages [24.3 kB]
Get:26 http://azure.archive.ubuntu.com/ubuntu jammy-backports/universe Translation-en [16.5 kB]
Get:27 http://azure.archive.ubuntu.com/ubuntu jammy-backports/universe amd64 c-n-f Metadata [644 B]
Get:28 http://azure.archive.ubuntu.com/ubuntu jammy-backports/multiverse amd64 c-n-f Metadata [116 B]
Get:29 http://azure.archive.ubuntu.com/ubuntu jammy-security/main amd64 Packages [1058 kB]
Get:30 http://azure.archive.ubuntu.com/ubuntu jammy-security/main Translation-en [200 kB]
Get:31 http://azure.archive.ubuntu.com/ubuntu jammy-security/restricted amd64 Packages [1237 kB]
Get:32 http://azure.archive.ubuntu.com/ubuntu jammy-security/restricted Translation-en [202 kB]
Get:33 http://azure.archive.ubuntu.com/ubuntu jammy-security/universe amd64 Packages [824 kB]
Get:34 http://azure.archive.ubuntu.com/ubuntu jammy-security/universe Translation-en [156 kB]
Get:35 http://azure.archive.ubuntu.com/ubuntu jammy-security/universe amd64 c-n-f Metadata [16.8 kB]
Get:36 http://azure.archive.ubuntu.com/ubuntu jammy-security/multiverse amd64 Packages [37.1 kB]
Get:37 http://azure.archive.ubuntu.com/ubuntu jammy-security/multiverse Translation-en [7476 B]
Get:38 http://azure.archive.ubuntu.com/ubuntu jammy-security/multiverse amd64 c-n-f Metadata [260 B]
Fetched 28.9 MB in 5s (6015 kB/s)
Reading package lists... Done
tom@pc1:~$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add –
Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
gpg: can't open '–': No such file or directory
tom@pc1:~$ sudo apt-get update
Hit:1 http://azure.archive.ubuntu.com/ubuntu jammy InRelease
Hit:2 http://azure.archive.ubuntu.com/ubuntu jammy-updates InRelease
Hit:3 http://azure.archive.ubuntu.com/ubuntu jammy-backports InRelease
Hit:4 http://azure.archive.ubuntu.com/ubuntu jammy-security InRelease
Reading package lists... Done
tom@pc1:~$ sudo apt-get install ca-certificates curl gnupg
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
ca-certificates is already the newest version (20230311ubuntu0.22.04.1).
ca-certificates set to manually installed.
gnupg is already the newest version (2.2.27-3ubuntu2.1).
gnupg set to manually installed.
The following additional packages will be installed:
  libcurl4
The following packages will be upgraded:
  curl libcurl4
2 upgraded, 0 newly installed, 0 to remove and 38 not upgraded.
Need to get 483 kB of archives.
After this operation, 0 B of additional disk space will be used.
Do you want to continue? [Y/n] yes
Get:1 http://azure.archive.ubuntu.com/ubuntu jammy-updates/main amd64 curl amd64 7.81.0-1ubuntu1.15 [194 kB]
Get:2 http://azure.archive.ubuntu.com/ubuntu jammy-updates/main amd64 libcurl4 amd64 7.81.0-1ubuntu1.15 [289 kB]
Fetched 483 kB in 0s (10.2 MB/s)
(Reading database ... 61587 files and directories currently installed.)
Preparing to unpack .../curl_7.81.0-1ubuntu1.15_amd64.deb ...
Unpacking curl (7.81.0-1ubuntu1.15) over (7.81.0-1ubuntu1.14) ...
Preparing to unpack .../libcurl4_7.81.0-1ubuntu1.15_amd64.deb ...
Unpacking libcurl4:amd64 (7.81.0-1ubuntu1.15) over (7.81.0-1ubuntu1.14) ...
Setting up libcurl4:amd64 (7.81.0-1ubuntu1.15) ...
Setting up curl (7.81.0-1ubuntu1.15) ...
Processing triggers for man-db (2.10.2-1) ...
Processing triggers for libc-bin (2.35-0ubuntu3.4) ...
Scanning processes...
Scanning linux images...

Running kernel seems to be up-to-date.

No services need to be restarted.

No containers need to be restarted.

No user sessions are running outdated binaries.

No VM guests are running outdated hypervisor (qemu) binaries on this host.
tom@pc1:~$ # Add Docker's official GPG key:
tom@pc1:~$ sudo apt-get update
ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get updatesudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
Hit:1 http://azure.archive.ubuntu.com/ubuntu jammy InRelease
Hit:2 http://azure.archive.ubuntu.com/ubuntu jammy-updates InRelease
Hit:3 http://azure.archive.ubuntu.com/ubuntu jammy-backports InRelease
Hit:4 http://azure.archive.ubuntu.com/ubuntu jammy-security InRelease
Reading package lists... Done
tom@pc1:~$ sudo install -m 0755 -d /etc/apt/keyrings
tom@pc1:~$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
tom@pc1:~$ sudo chmod a+r /etc/apt/keyrings/docker.gpg
tom@pc1:~$ echo \
>   "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
>   $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
>   sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
tom@pc1:~$ sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Package docker-ce is not available, but is referred to by another package.
This may mean that the package is missing, has been obsoleted, or
is only available from another source

E: Package 'docker-ce' has no installation candidate
E: Unable to locate package docker-ce-cli
E: Unable to locate package containerd.io
E: Couldn't find any package by glob 'containerd.io'
E: Couldn't find any package by regex 'containerd.io'
E: Unable to locate package docker-buildx-plugin
E: Unable to locate package docker-compose-plugin
tom@pc1:~$ sudo apt-get update
Hit:1 http://azure.archive.ubuntu.com/ubuntu jammy InRelease
Hit:2 http://azure.archive.ubuntu.com/ubuntu jammy-updates InRelease
Hit:3 http://azure.archive.ubuntu.com/ubuntu jammy-backports InRelease
Hit:4 http://azure.archive.ubuntu.com/ubuntu jammy-security InRelease
Get:5 https://download.docker.com/linux/ubuntu jammy InRelease [48.8 kB]
Get:6 https://download.docker.com/linux/ubuntu jammy/stable amd64 Packages [23.0 kB]
Fetched 71.8 kB in 1s (98.3 kB/s)
Reading package lists... Done
tom@pc1:~$ install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
install: target 'docker-compose-plugin' is not a directory
tom@pc1:~$ sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  docker-ce-rootless-extras libltdl7 libslirp0 pigz slirp4netns
Suggested packages:
  aufs-tools cgroupfs-mount | cgroup-lite
The following NEW packages will be installed:
  containerd.io docker-buildx-plugin docker-ce docker-ce-cli docker-ce-rootless-extras docker-compose-plugin libltdl7
  libslirp0 pigz slirp4netns
0 upgraded, 10 newly installed, 0 to remove and 38 not upgraded.
Need to get 115 MB of archives.
After this operation, 411 MB of additional disk space will be used.
Do you want to continue? [Y/n] yes
Get:1 http://azure.archive.ubuntu.com/ubuntu jammy/universe amd64 pigz amd64 2.6-1 [63.6 kB]
Get:2 http://azure.archive.ubuntu.com/ubuntu jammy/main amd64 libltdl7 amd64 2.4.6-15build2 [39.6 kB]
Get:3 http://azure.archive.ubuntu.com/ubuntu jammy/main amd64 libslirp0 amd64 4.6.1-1build1 [61.5 kB]
Get:4 http://azure.archive.ubuntu.com/ubuntu jammy/universe amd64 slirp4netns amd64 1.0.1-2 [28.2 kB]
Get:5 https://download.docker.com/linux/ubuntu jammy/stable amd64 containerd.io amd64 1.6.26-1 [29.5 MB]
Get:6 https://download.docker.com/linux/ubuntu jammy/stable amd64 docker-buildx-plugin amd64 0.11.2-1~ubuntu.22.04~jammy [28.2 MB]
Get:7 https://download.docker.com/linux/ubuntu jammy/stable amd64 docker-ce-cli amd64 5:24.0.7-1~ubuntu.22.04~jammy [13.3 MB]
Get:8 https://download.docker.com/linux/ubuntu jammy/stable amd64 docker-ce amd64 5:24.0.7-1~ubuntu.22.04~jammy [22.6 MB]
Get:9 https://download.docker.com/linux/ubuntu jammy/stable amd64 docker-ce-rootless-extras amd64 5:24.0.7-1~ubuntu.22.04~jammy [9030 kB]
Get:10 https://download.docker.com/linux/ubuntu jammy/stable amd64 docker-compose-plugin amd64 2.21.0-1~ubuntu.22.04~jammy [11.9 MB]
Fetched 115 MB in 1s (92.9 MB/s)
Selecting previously unselected package pigz.
(Reading database ... 61587 files and directories currently installed.)
Preparing to unpack .../0-pigz_2.6-1_amd64.deb ...
Unpacking pigz (2.6-1) ...
Selecting previously unselected package containerd.io.
Preparing to unpack .../1-containerd.io_1.6.26-1_amd64.deb ...
Unpacking containerd.io (1.6.26-1) ...
Selecting previously unselected package docker-buildx-plugin.
Preparing to unpack .../2-docker-buildx-plugin_0.11.2-1~ubuntu.22.04~jammy_amd64.deb ...
Unpacking docker-buildx-plugin (0.11.2-1~ubuntu.22.04~jammy) ...
Selecting previously unselected package docker-ce-cli.
Preparing to unpack .../3-docker-ce-cli_5%3a24.0.7-1~ubuntu.22.04~jammy_amd64.deb ...
Unpacking docker-ce-cli (5:24.0.7-1~ubuntu.22.04~jammy) ...
Selecting previously unselected package docker-ce.
Preparing to unpack .../4-docker-ce_5%3a24.0.7-1~ubuntu.22.04~jammy_amd64.deb ...
Unpacking docker-ce (5:24.0.7-1~ubuntu.22.04~jammy) ...
Selecting previously unselected package docker-ce-rootless-extras.
Preparing to unpack .../5-docker-ce-rootless-extras_5%3a24.0.7-1~ubuntu.22.04~jammy_amd64.deb ...
Unpacking docker-ce-rootless-extras (5:24.0.7-1~ubuntu.22.04~jammy) ...
Selecting previously unselected package docker-compose-plugin.
Preparing to unpack .../6-docker-compose-plugin_2.21.0-1~ubuntu.22.04~jammy_amd64.deb ...
Unpacking docker-compose-plugin (2.21.0-1~ubuntu.22.04~jammy) ...
Selecting previously unselected package libltdl7:amd64.
Preparing to unpack .../7-libltdl7_2.4.6-15build2_amd64.deb ...
Unpacking libltdl7:amd64 (2.4.6-15build2) ...
Selecting previously unselected package libslirp0:amd64.
Preparing to unpack .../8-libslirp0_4.6.1-1build1_amd64.deb ...
Unpacking libslirp0:amd64 (4.6.1-1build1) ...
Selecting previously unselected package slirp4netns.
Preparing to unpack .../9-slirp4netns_1.0.1-2_amd64.deb ...
Unpacking slirp4netns (1.0.1-2) ...
Setting up docker-buildx-plugin (0.11.2-1~ubuntu.22.04~jammy) ...
Setting up containerd.io (1.6.26-1) ...
Created symlink /etc/systemd/system/multi-user.target.wants/containerd.service → /lib/systemd/system/containerd.service.Setting up docker-compose-plugin (2.21.0-1~ubuntu.22.04~jammy) ...
Setting up libltdl7:amd64 (2.4.6-15build2) ...
Setting up docker-ce-cli (5:24.0.7-1~ubuntu.22.04~jammy) ...
Setting up libslirp0:amd64 (4.6.1-1build1) ...
Setting up pigz (2.6-1) ...
Setting up docker-ce-rootless-extras (5:24.0.7-1~ubuntu.22.04~jammy) ...
Setting up slirp4netns (1.0.1-2) ...
Setting up docker-ce (5:24.0.7-1~ubuntu.22.04~jammy) ...
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /lib/systemd/system/docker.service.
Created symlink /etc/systemd/system/sockets.target.wants/docker.socket → /lib/systemd/system/docker.socket.
Processing triggers for man-db (2.10.2-1) ...
Processing triggers for libc-bin (2.35-0ubuntu3.4) ...
Scanning processes...
Scanning linux images...

Running kernel seems to be up-to-date.

No services need to be restarted.

No containers need to be restarted.

No user sessions are running outdated binaries.

No VM guests are running outdated hypervisor (qemu) binaries on this host.
tom@pc1:~$ docker version
Client: Docker Engine - Community
 Version:           24.0.7
 API version:       1.43
 Go version:        go1.20.10
 Git commit:        afdd53b
 Built:             Thu Oct 26 09:07:41 2023
 OS/Arch:           linux/amd64
 Context:           default
permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get "http://%2Fvar%2Frun%2Fdocker.sock/v1.24/version": dial unix /var/run/docker.sock: connect: permission denied
tom@pc1:~$
```
demmarage automatique
```
tom@pc1:~$ sudo systemctl enable docker
Synchronizing state of docker.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable docker
```

**AMELIORER SSH**

**empecher connexion utilisateur par mot de passe ou sans mot de passe => PasswordAuthentication et PermitEmptyPasswords en no et pas yes**
```
sudo nano /etc/ssh/sshd_config

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
PasswordAuthentication no 
#PermitEmptyPasswords no

# Change to yes to enable challenge-response passwords (beware issues with
# some PAM modules and threads)
KbdInteractiveAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no

# GSSAPI options

```

**on peut également changer #ClientAliveInterval 0 pour definir un temps de deconnexion au bout d'un moment (en seconde) (meme fichier qu'avant)**

```
#ClientAliveInterval 500
```

**diminuer nombre tentative connexion**

```
#MaxAuthTries 3
```

**GENERALISER LA VM**

```
tom@pc1:~$ sudo rm -f /etc/cloud/cloud.cfg.d/50-curtin-networking.cfg /etc/cloud/cloud.cfg.d/curtin-preserve-sources.cfg /etc/cloud/cloud.cfg.d/99-installer.cfg /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg
tom@pc1:~$ sudo rm -f /etc/cloud/ds-identify.cfg
tom@pc1:~$ sudo rm -f /etc/netplan/*.yaml
tom@pc1:~$ sudo sed -i 's/Provisioning.Enabled=y/Provisioning.Enabled=n/g' /etc/waagent.conf
tom@pc1:~$ sudo sed -i 's/Provisioning.UseCloudInit=n/Provisioning.UseCloudInit=y/g' /etc/waagent.conf
tom@pc1:~$ sudo sed -i 's/ResourceDisk.Format=y/ResourceDisk.Format=n/g' /etc/waagent.conf
tom@pc1:~$ sudo sed -i 's/ResourceDisk.EnableSwap=y/ResourceDisk.EnableSwap=n/g' /etc/waagent.conf
tom@pc1:~$ cat <<EOF | sudo tee -a /etc/waagent.conf
> Provisioning.Agent=auto
> EOF
Provisioning.Agent=auto
tom@pc1:~$ sudo cloud-init clean --logs --seed
tom@pc1:~$ sudo rm -rf /var/lib/cloud/
tom@pc1:~$ sudo systemctl stop walinuxagent.service
tom@pc1:~$ sudo rm -rf /var/lib/waagent/
tom@pc1:~$ sudo rm -f /var/log/waagent.log
tom@pc1:~$ sudo waagent -force -deprovision+user
/usr/sbin/waagent:27: DeprecationWarning: the imp module is deprecated in favour of importlib and slated for removal in Python 3.12; see the module's documentation for alternative uses
  import imp
WARNING! The waagent service will be stopped.
WARNING! Cached DHCP leases will be deleted.
WARNING! root password will be disabled. You will not be able to login as root.
WARNING! /etc/resolv.conf will NOT be removed, this is a behavior change to earlier versions of Ubuntu.
WARNING! ovf-env.xml is not found.
WARNING! Skip delete user.
tom@pc1:~$sudo rm -f ~/.bash_history

```

**image custom**

ID : 30fd1508-1141-4c8a-880b-cfcd61006778

suivi des commande : 

```
az>> az vm list --output table

Name    ResourceGroup    Location       Zones
------  ---------------  -------------  -------
pc1     EFREITP2         francecentral
az>> az vm get-instance-view -g <efreitp2> -n <pc1>
La syntaxe de la commande n’est pas correcte.
az>> az vm get-instance-view -g efreitp2 -n pc1
{
  "additionalCapabilities": null,
  "applicationProfile": null,
  "availabilitySet": null,
  "billingProfile": null,
  "capacityReservation": null,
  "diagnosticsProfile": null,
  "etag": null,
  "evictionPolicy": null,
  "extendedLocation": null,
  "extensionsTimeBudget": null,
  "hardwareProfile": {
    "vmSize": "Standard_DS1_v2",
    "vmSizeProperties": null
  },
  "host": null,
  "hostGroup": null,
  "id": "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/virtualMachines/pc1",
  "identity": null,
  "instanceView": {
    "assignedHost": null,
    "bootDiagnostics": null,
    "computerName": null,
    "disks": [
      {
        "encryptionSettings": null,
        "name": "pc1_disk1_748643d54ceb4609929608f2b98ead1e",
        "statuses": [
          {
            "code": "ProvisioningState/succeeded",
            "displayStatus": "Provisioning succeeded",
            "level": "Info",
            "message": null,
            "time": "2024-01-09T16:37:10.685398+00:00"
          }
        ]
      }
    ],
    "extensions": null,
    "hyperVGeneration": "V2",
    "isVmInStandbyPool": null,
    "maintenanceRedeployStatus": null,
    "osName": null,
    "osVersion": null,
    "patchStatus": null,
    "platformFaultDomain": null,
    "platformUpdateDomain": null,
    "rdpThumbPrint": null,
    "statuses": [
      {
        "code": "ProvisioningState/succeeded",
        "displayStatus": "Provisioning succeeded",
        "level": "Info",
        "message": null,
        "time": "2024-01-09T16:37:10.701041+00:00"
      },
      {
        "code": "PowerState/deallocated",
        "displayStatus": "VM deallocated",
        "level": "Info",
        "message": null,
        "time": null
      }
    ],
    "vmAgent": null,
    "vmHealth": null
  },
  "licenseType": null,
  "location": "francecentral",
  "managedBy": null,
  "name": "pc1",
  "networkProfile": {
    "networkApiVersion": null,
    "networkInterfaceConfigurations": null,
    "networkInterfaces": [
      {
        "deleteOption": null,
        "id": "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Network/networkInterfaces/pc1VMNic",
        "primary": null,
        "resourceGroup": "efreitp2"
      }
    ]
  },
  "osProfile": {
    "adminPassword": null,
    "adminUsername": "tom",
    "allowExtensionOperations": true,
    "computerName": "pc1",
    "customData": null,
    "linuxConfiguration": {
      "disablePasswordAuthentication": true,
      "enableVmAgentPlatformUpdates": false,
      "patchSettings": {
        "assessmentMode": "ImageDefault",
        "automaticByPlatformSettings": null,
        "patchMode": "ImageDefault"
      },
      "provisionVmAgent": true,
      "ssh": {
        "publicKeys": [
          {
            "keyData": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCe0FRYpkm54jl913sRyM8ispSvS14pmMLGTxsEZHiwwSaHyIeUbVOpF9826lEMy0Ks0jgFoXGEFp3K05jFdCrmHte3/1+39CSv/xtxyACQOk1C8oLKbahnspZ+FR9gVBd8/QPsHin997/fQ+8ST6LmorP+NYQ4vZP8drIkwXPnk+EctsI8KYdZf6s1+Fj04gkfdQpH7pF4s4SuawOpAY5EC73Lm77fkw9o6npQCwr5EIFAeUbaI/ZLtgAPRKAtDveOGeWHga/GIDAC66gx1uC4ztFUzoM+bz1lcBWYdsYTuy4KpqC8zApN0LHiOVIJR/NZZtt7fw0dqgcYJie0NiSprQz78QP88DYQl7d1CKydtWo0sNeZWjkkNQlpsSzhIhu5cuxwOS+OXyRQ4rqW59rpXal6FFioGfPk4Zw4embHETE33xEmUVyGxx8jLLVs4N+qLIjoAcCuVXI3s7tbB9kDJ1RTvrL1ibZWLvZQD7us/tbph7rUIqqFu5OgOWjQ+tOMzMQr9pdc7B/8WYrZdMSm5cR/P+DD6z7DciBKohc7xBGG9uuwD3J8R7Ui661n2SrUgo4hAbRZiaNru4VBIv+hJ4M8nsh/o7XO+KAYakVwbkuOCXQJ4txI6wQpV2rBLvc5p4iwOUhOkK4yW0PWWoTmq05p25oKkd1ow4SNJfWJzQ== tomto@MSI\n",
            "path": "/home/tom/.ssh/authorized_keys"
          }
        ]
      }
    },
    "requireGuestProvisionSignal": true,
    "secrets": [],
    "windowsConfiguration": null
  },
  "plan": null,
  "platformFaultDomain": null,
  "priority": null,
  "provisioningState": "Succeeded",
  "proximityPlacementGroup": null,
  "resourceGroup": "efreitp2",
  "resources": null,
  "scheduledEventsProfile": null,
  "securityProfile": {
    "encryptionAtHost": null,
    "encryptionIdentity": null,
    "proxyAgentSettings": null,
    "securityType": "TrustedLaunch",
    "uefiSettings": {
      "secureBootEnabled": true,
      "vTpmEnabled": true
    }
  },
  "storageProfile": {
    "dataDisks": [],
    "diskControllerType": "SCSI",
    "imageReference": {
      "communityGalleryImageId": null,
      "exactVersion": "22.04.202312060",
      "id": null,
      "offer": "0001-com-ubuntu-server-jammy",
      "publisher": "Canonical",
      "sharedGalleryImageId": null,
      "sku": "22_04-lts-gen2",
      "version": "latest"
    },
    "osDisk": {
      "caching": "ReadWrite",
      "createOption": "FromImage",
      "deleteOption": "Detach",
      "diffDiskSettings": null,
      "diskSizeGb": null,
      "encryptionSettings": null,
      "image": null,
      "managedDisk": {
        "diskEncryptionSet": null,
        "id": "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/disks/pc1_disk1_748643d54ceb4609929608f2b98ead1e",
        "resourceGroup": "efreitp2",
        "securityProfile": null,
        "storageAccountType": null
      },
      "name": "pc1_disk1_748643d54ceb4609929608f2b98ead1e",
      "osType": "Linux",
      "vhd": null,
      "writeAcceleratorEnabled": null
    }
  },
  "tags": {},
  "timeCreated": "2024-01-09T16:08:44.042237+00:00",
  "type": "Microsoft.Compute/virtualMachines",
  "userData": null,
  "virtualMachineScaleSet": null,
  "vmId": "cb84b3d4-39c5-4bb1-b762-c28835265b59",
  "zones": null
}

az>> az sig create --resource-group efreitp2 --gallery-name efreigallery
{
  "description": null,
  "id": "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/galleries/efreigallery",
  "identifier": {
    "uniqueName": "30fd1508-1141-4c8a-880b-cfcd61006778-EFREIGALLERY"
  },
  "location": "francecentral",
  "name": "efreigallery",
  "provisioningState": "Succeeded",
  "resourceGroup": "efreitp2",
  "sharingProfile": null,
  "sharingStatus": null,
  "softDeletePolicy": null,
  "tags": {},
  "type": "Microsoft.Compute/galleries"
}

az sig image-definition create --resource-group efreitp2 --gallery-name efreigallery --gallery-image-definition efreidef --publisher efreipub --offer efreioffer --sku efreisku --os-type Linux --os-state Generalized --hyper-v-generation V2   --features SecurityType=TrustedLaunch

{
  "architecture": "x64",
  "description": null,
  "disallowed": null,
  "endOfLifeDate": null,
  "eula": null,
  "features": [
    {
      "name": "SecurityType",
      "value": "TrustedLaunch"
    }
  ],
  "hyperVGeneration": "V2",
  "id": "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/galleries/efreigallery/images/efreidef",
  "identifier": {
    "offer": "efreioffer",
    "publisher": "efreipub",
    "sku": "efreisku"
  },
  "location": "francecentral",
  "name": "efreidef",
  "osState": "Generalized",
  "osType": "Linux",
  "privacyStatementUri": null,
  "provisioningState": "Succeeded",
  "purchasePlan": null,
  "recommended": null,
  "releaseNoteUri": null,
  "resourceGroup": "efreitp2",
  "tags": {},
  "type": "Microsoft.Compute/galleries/images"
}


az sig image-version create --resource-group efreitp2 --gallery-name efreigallery --gallery-image-definition efreidef --gallery-image-version 1.0.0 --target-regions "francecentral" --replica-count 1 --managed-image "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/virtualMachines/pc1"
{
  "id": "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/galleries/efreigallery/images/efreidef/versions/1.0.0",
  "location": "francecentral",
  "name": "1.0.0",
  "provisioningState": "Succeeded",
  "publishingProfile": {
    "endOfLifeDate": null,
    "excludeFromLatest": false,
    "publishedDate": "2024-01-09T17:07:07.141801+00:00",
    "replicaCount": 1,
    "replicationMode": null,
    "storageAccountType": "Standard_LRS",
    "targetExtendedLocations": null,
    "targetRegions": [
      {
        "encryption": null,
        "excludeFromLatest": null,
        "name": "France Central",
        "regionalReplicaCount": 1,
        "storageAccountType": "Standard_LRS"
      }
    ]
  },
  "replicationStatus": null,
  "resourceGroup": "efreitp2",
  "safetyProfile": {
    "allowDeletionOfReplicatedLocations": false,
    "policyViolations": null,
    "reportedForPolicyViolation": false
  },
  "storageProfile": {
    "dataDiskImages": null,
    "osDiskImage": {
      "hostCaching": "ReadWrite",
      "sizeInGb": 30,
      "source": {
        "id": null,
        "storageAccountId": null,
        "uri": null
      }
    },
    "source": {
      "communityGalleryImageId": null,
      "id": "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/virtualMachines/pc1",
      "resourceGroup": "efreitp2"
    }
  },
  "tags": {},
  "type": "Microsoft.Compute/galleries/images/versions"
}
```

**création vm**

```
az vm create --resource-group efreitp2 --name pc3  --image "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/galleries/efreigallery/images/efreidef/versions/1.0.0"--security-type TrustedLaunch --ssh-key-values "C:\Users\tomto\.ssh\id_rsa.pub" --admin-username tom

Selecting "northeurope" may reduce your costs. The region you've selected may cost more for the same services. You can disable this message in the future with the command "az config set core.display_region_identified=false". Learn more at https://go.microsoft.com/fwlink/?linkid=222571

{
  "fqdns": "",
  "id": "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/virtualMachines/pc3",
  "location": "francecentral",
  "macAddress": "60-45-BD-6D-EE-D3",
  "powerState": "VM running",
  "privateIpAddress": "10.0.0.5",
  "publicIpAddress": "20.111.22.65",
  "resourceGroup": "efreitp2",
  "zones": ""
}
```

**vérification nouvelle vm**

docker on et ssh gardé

```
PS C:\Users\tomto> ssh tom@20.111.22.65
The authenticity of host '20.111.22.65 (20.111.22.65)' can't be established.
ECDSA key fingerprint is SHA256:49o4AQLPos2MFCuZd4cy0jaxR0OgqZ2TrYOTrCbWaHs.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '20.111.22.65' (ECDSA) to the list of known hosts.
Welcome to Ubuntu 22.04.3 LTS (GNU/Linux 6.2.0-1018-azure x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Tue Jan  9 17:26:54 UTC 2024

  System load:  0.22412109375     Processes:                106
  Usage of /:   7.5% of 28.89GB   Users logged in:          0
  Memory usage: 9%                IPv4 address for docker0: 172.17.0.1
  Swap usage:   0%                IPv4 address for eth0:    10.0.0.5


Expanded Security Maintenance for Applications is not enabled.

37 updates can be applied immediately.
24 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable

Enable ESM Apps to receive additional future security updates.
See https://ubuntu.com/esm or run: sudo pro status


Last login: Tue Jan  9 16:10:03 2024 from 217.26.204.244

yestom@pc3:~$
tom@pc3:~$ docker version
Client: Docker Engine - Community
 Version:           24.0.7
 API version:       1.43
 Go version:        go1.20.10
 Git commit:        afdd53b
 Built:             Thu Oct 26 09:07:41 2023
 OS/Arch:           linux/amd64
 Context:           default
permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get "http://%2Fvar%2Frun%2Fdocker.sock/v1.24/version": dial unix /var/run/docker.sock: connect: permission denied
tom@pc3:~$ sudo nano /etc/ssh/sshd_config

# Authentication:

#LoginGraceTime 2m
#PermitRootLogin prohibit-password
#StrictModes yes
#MaxAuthTries 3
#MaxSessions 10

#PubkeyAuthentication yes

# Expect .ssh/authorized_keys2 to be disregarded by default in future.
#AuthorizedKeysFile     .ssh/authorized_keys .ssh/authorized_keys2

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody
```

**création fichier cloud.init (une partie est deja faite grace au étape précédente)**

```
#cloud-config
users:
  - name: tom
    primary_group: tom
    groups: sudo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    lock_passwd: false
    passwd: $6$YE7RpAG.OxgXvTXe$L.zPZWHtHmLpYh7IH3QzOpbVpE4g.wuo8J/YoEgwkPKKpkRM.zPCTjCtyFHgjDsquWDq/DUM/NXY95d5RtVM9/
    ssh_authorized_keys:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCe0FRYpkm54jl913sRyM8ispSvS14pmMLGTxsEZHiwwSaHyIeUbVOpF9826lEMy0Ks0jgFoXGEFp3K05jFdCrmHte3/1+39CSv/xtxyACQOk1C8oLKbahnspZ+FR9gVBd8/QPsHin997/fQ+8ST6LmorP+NYQ4vZP8drIkwXPnk+EctsI8KYdZf6s1+Fj04gkfdQpH7pF4s4SuawOpAY5EC73Lm77fkw9o6npQCwr5EIFAeUbaI/ZLtgAPRKAtDveOGeWHga/GIDAC66gx1uC4ztFUzoM+bz1lcBWYdsYTuy4KpqC8zApN0LHiOVIJR/NZZtt7fw0dqgcYJie0NiSprQz78QP88DYQl7d1CKydtWo0sNeZWjkkNQlpsSzhIhu5cuxwOS+OXyRQ4rqW59rpXal6FFioGfPk4Zw4embHETE33xEmUVyGxx8jLLVs4N+qLIjoAcCuVXI3s7tbB9kDJ1RTvrL1ibZWLvZQD7us/tbph7rUIqqFu5OgOWjQ+tOMzMQr9pdc7B/8WYrZdMSm5cR/P+DD6z7DciBKohc7xBGG9uuwD3J8R7Ui661n2SrUgo4hAbRZiaNru4VBIv+hJ4M8nsh/o7XO+KAYakVwbkuOCXQJ4txI6wQpV2rBLvc5p4iwOUhOkK4yW0PWWoTmq05p25oKkd1ow4SNJfWJzQ== tomto@MSI
runcmd:
  - |
  docker run d -p 80:80 nginx

```

**Avec une comande az, créez une nouvelle machine pour nginx automatique**

```
az vm create --resource-group efreitp2 --name pc2  --image "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/galleries/efreigallery/images/efreidef/versions/1.0.0"--security-type TrustedLaunch --ssh-key-values "C:\Users\tomto\.ssh\id_rsa.pub" --admin-username tom --custom-data C:\cour\cloud\#cloud-config.txt

Selecting "northeurope" may reduce your costs. The region you've selected may cost more for the same services. You can disable this message in the future with the command "az config set core.display_region_identified=false". Learn more at https://go.microsoft.com/fwlink/?linkid=222571

{
  "fqdns": "",
  "id": "/subscriptions/30fd1508-1141-4c8a-880b-cfcd61006778/resourceGroups/efreitp2/providers/Microsoft.Compute/virtualMachines/pc2",
  "location": "francecentral",
  "macAddress": "60-45-BD-1A-27-5F",
  "powerState": "VM running",
  "privateIpAddress": "10.0.0.6",
  "publicIpAddress": "20.199.125.14",
  "resourceGroup": "efreitp2",
  "zones": ""
}
```
le serveur nginx tourne on peut y acceder!





