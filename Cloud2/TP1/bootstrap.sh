#!/bin/bash

# Installation des paquets vim et python3
sudo dnf update -y
sudo dnf install -y vim python3

# Mise à jour du système avec dnf update -y (facultatif)
# sudo dnf update -y
