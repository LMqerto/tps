# TP1 Tom Bernard

**repackager la VM créée précédement**
``` 
$ vagrant up
Bringing machine 'ezconf.tp1.efrei' up with 'virtualbox' provider...
==> ezconf.tp1.efrei: Importing base box 'generic/rocky9'...
==> ezconf.tp1.efrei: Matching MAC address for NAT networking...
==> ezconf.tp1.efrei: Checking if box 'generic/rocky9' version '4.3.12' is up to date...
==> ezconf.tp1.efrei: Setting the name of the VM: Cloud2_ezconftp1efrei_1712066948810_14281
==> ezconf.tp1.efrei: Clearing any previously set network interfaces...
==> ezconf.tp1.efrei: Preparing network interfaces based on configuration...
    ezconf.tp1.efrei: Adapter 1: nat
    ezconf.tp1.efrei: Adapter 2: hostonly
==> ezconf.tp1.efrei: Forwarding ports...
    ezconf.tp1.efrei: 22 (guest) => 2222 (host) (adapter 1)
==> ezconf.tp1.efrei: Configuring storage mediums...
==> ezconf.tp1.efrei: VirtualBox does not support shrinking disk sizes. Cannot shrink 'vagrant_primary' disks size.
==> ezconf.tp1.efrei: Running 'pre-boot' VM customizations...
==> ezconf.tp1.efrei: Booting VM...
==> ezconf.tp1.efrei: Waiting for machine to boot. This may take a few minutes...
    ezconf.tp1.efrei: SSH address: 127.0.0.1:2222
    ezconf.tp1.efrei: SSH username: vagrant
    ezconf.tp1.efrei: SSH auth method: private key
    ezconf.tp1.efrei:
    ezconf.tp1.efrei: Vagrant insecure key detected. Vagrant will automatically replace
    ezconf.tp1.efrei: this with a newly generated keypair for better security.
    ezconf.tp1.efrei:
    ezconf.tp1.efrei: Inserting generated public key within guest...
    ezconf.tp1.efrei: Removing insecure key from the guest if it's present...
    ezconf.tp1.efrei: Key inserted! Disconnecting and reconnecting using new SSH key...
==> ezconf.tp1.efrei: Machine booted and ready!
==> ezconf.tp1.efrei: Checking for guest additions in VM...
    ezconf.tp1.efrei: The guest additions on this VM do not match the installed version of
    ezconf.tp1.efrei: VirtualBox! In most cases this is fine, but in rare cases it can
    ezconf.tp1.efrei: prevent things such as shared folders from working properly. If you see
    ezconf.tp1.efrei: shared folder errors, please make sure the guest additions within the
    ezconf.tp1.efrei: virtual machine match the version of VirtualBox you have installed on
    ezconf.tp1.efrei: your host and reload your VM.
    ezconf.tp1.efrei:
    ezconf.tp1.efrei: Guest Additions Version: 6.1.48
    ezconf.tp1.efrei: VirtualBox Version: 7.0
==> ezconf.tp1.efrei: Setting hostname...
==> ezconf.tp1.efrei: Configuring and enabling network interfaces...
==> ezconf.tp1.efrei: Running provisioner: shell...
    ezconf.tp1.efrei: Running: C:/Users/tomto/AppData/Local/Temp/vagrant-shell20240402-7500-c0yqlm.sh
    ezconf.tp1.efrei: Extra Packages for Enterprise Linux 9 - x86_64  8.5 MB/s |  21 MB     00:02
    ezconf.tp1.efrei: Extra Packages for Enterprise Linux 9 openh264  2.5 kB/s | 2.5 kB     00:00
    ezconf.tp1.efrei: Rocky Linux 9 - BaseOS                          1.4 MB/s | 2.2 MB     00:01
    ezconf.tp1.efrei: Rocky Linux 9 - AppStream                       5.8 MB/s | 7.4 MB     00:01
    ezconf.tp1.efrei: Rocky Linux 9 - Extras                           14 kB/s |  14 kB     00:01
    ezconf.tp1.efrei: Dependencies resolved.
    ezconf.tp1.efrei: ================================================================================
    ezconf.tp1.efrei:  Package                    Arch   Version                      Repo       Size
    ezconf.tp1.efrei: ================================================================================
    ezconf.tp1.efrei: Installing:
    ezconf.tp1.efrei:  kernel                     x86_64 5.14.0-362.24.1.el9_3        baseos    4.6 M
    ezconf.tp1.efrei:  kernel-devel               x86_64 5.14.0-362.24.1.el9_3        appstream  20 M
    ezconf.tp1.efrei: Upgrading:
    ezconf.tp1.efrei:  NetworkManager             x86_64 1:1.44.0-5.el9_3             baseos    2.2 M
    ezconf.tp1.efrei:  NetworkManager-libnm       x86_64 1:1.44.0-5.el9_3             baseos    1.8 M
    ezconf.tp1.efrei:  NetworkManager-team        x86_64 1:1.44.0-5.el9_3             baseos     38 k
    ezconf.tp1.efrei:  NetworkManager-tui         x86_64 1:1.44.0-5.el9_3             baseos    243 k
    ezconf.tp1.efrei:  basesystem                 noarch 11-13.el9.0.1                baseos    6.4 k
    ezconf.tp1.efrei:  binutils                   x86_64 2.35.2-42.el9_3.1            baseos    4.5 M
    ezconf.tp1.efrei:  binutils-gold              x86_64 2.35.2-42.el9_3.1            baseos    732 k
    ezconf.tp1.efrei:  curl                       x86_64 7.76.1-26.el9_3.3            baseos    293 k
    ezconf.tp1.efrei:  expat                      x86_64 2.5.0-1.el9_3.1              baseos    115 k
    ezconf.tp1.efrei:  glibc                      x86_64 2.34-83.el9.12               baseos    1.9 M
    ezconf.tp1.efrei:  glibc-common               x86_64 2.34-83.el9.12               baseos    303 k
    ezconf.tp1.efrei:  glibc-devel                x86_64 2.34-83.el9.12               appstream  43 k
    ezconf.tp1.efrei:  glibc-gconv-extra          x86_64 2.34-83.el9.12               baseos    1.5 M
    ezconf.tp1.efrei:  glibc-headers              x86_64 2.34-83.el9.12               appstream 444 k
    ezconf.tp1.efrei:  glibc-langpack-en          x86_64 2.34-83.el9.12               baseos    561 k
    ezconf.tp1.efrei:  gnutls                     x86_64 3.7.6-23.el9_3.3             baseos    1.0 M
    ezconf.tp1.efrei:  grub2-common               noarch 1:2.06-70.el9_3.2.rocky.0.4  baseos    903 k
    ezconf.tp1.efrei:  grub2-pc                   x86_64 1:2.06-70.el9_3.2.rocky.0.4  baseos     13 k
    ezconf.tp1.efrei:  grub2-pc-modules           noarch 1:2.06-70.el9_3.2.rocky.0.4  baseos    909 k
    ezconf.tp1.efrei:  grub2-tools                x86_64 1:2.06-70.el9_3.2.rocky.0.4  baseos    1.8 M
    ezconf.tp1.efrei:  grub2-tools-minimal        x86_64 1:2.06-70.el9_3.2.rocky.0.4  baseos    601 k
    ezconf.tp1.efrei:  kernel-headers             x86_64 5.14.0-362.24.1.el9_3        appstream 6.1 M
    ezconf.tp1.efrei:  kernel-tools               x86_64 5.14.0-362.24.1.el9_3        baseos    4.9 M
    ezconf.tp1.efrei:  kernel-tools-libs          x86_64 5.14.0-362.24.1.el9_3        baseos    4.6 M
    ezconf.tp1.efrei:  libcurl                    x86_64 7.76.1-26.el9_3.3            baseos    283 k
    ezconf.tp1.efrei:  libssh                     x86_64 0.10.4-12.el9_3              baseos    215 k
    ezconf.tp1.efrei:  libssh-config              noarch 0.10.4-12.el9_3              baseos    9.2 k
    ezconf.tp1.efrei:  libsss_certmap             x86_64 2.9.1-4.el9_3.5              baseos     92 k
    ezconf.tp1.efrei:  libsss_idmap               x86_64 2.9.1-4.el9_3.5              baseos     43 k
    ezconf.tp1.efrei:  libsss_nss_idmap           x86_64 2.9.1-4.el9_3.5              baseos     47 k
    ezconf.tp1.efrei:  libsss_sudo                x86_64 2.9.1-4.el9_3.5              baseos     37 k
    ezconf.tp1.efrei:  linux-firmware             noarch 20230814-142.el9_3           baseos    307 M
    ezconf.tp1.efrei:  linux-firmware-whence      noarch 20230814-142.el9_3           baseos     77 k
    ezconf.tp1.efrei:  openssh                    x86_64 8.7p1-34.el9_3.3             baseos    456 k
    ezconf.tp1.efrei:  openssh-clients            x86_64 8.7p1-34.el9_3.3             baseos    712 k
    ezconf.tp1.efrei:  openssh-server             x86_64 8.7p1-34.el9_3.3             baseos    458 k
    ezconf.tp1.efrei:  openssl                    x86_64 1:3.0.7-25.el9_3             baseos    1.2 M
    ezconf.tp1.efrei:  openssl-devel              x86_64 1:3.0.7-25.el9_3             appstream 3.0 M
    ezconf.tp1.efrei:  openssl-libs               x86_64 1:3.0.7-25.el9_3             baseos    2.1 M
    ezconf.tp1.efrei:  python-unversioned-command noarch 3.9.18-1.el9_3.1             appstream 8.6 k
    ezconf.tp1.efrei:  python3                    x86_64 3.9.18-1.el9_3.1             baseos     25 k
    ezconf.tp1.efrei:  python3-libs               x86_64 3.9.18-1.el9_3.1             baseos    7.3 M
    ezconf.tp1.efrei:  python3-perf               x86_64 5.14.0-362.24.1.el9_3        baseos    4.7 M
    ezconf.tp1.efrei:  python3-pip-wheel          noarch 21.2.3-7.el9_3.1             baseos    1.1 M
    ezconf.tp1.efrei:  python3-rpm                x86_64 4.16.1.3-27.el9_3            baseos     64 k
    ezconf.tp1.efrei:  rpm                        x86_64 4.16.1.3-27.el9_3            baseos    485 k
    ezconf.tp1.efrei:  rpm-build-libs             x86_64 4.16.1.3-27.el9_3            baseos     87 k
    ezconf.tp1.efrei:  rpm-libs                   x86_64 4.16.1.3-27.el9_3            baseos    307 k
    ezconf.tp1.efrei:  rpm-plugin-audit           x86_64 4.16.1.3-27.el9_3            baseos     15 k
    ezconf.tp1.efrei:  rpm-plugin-selinux         x86_64 4.16.1.3-27.el9_3            baseos     15 k
    ezconf.tp1.efrei:  rpm-plugin-systemd-inhibit x86_64 4.16.1.3-27.el9_3            appstream  15 k
    ezconf.tp1.efrei:  rpm-sign-libs              x86_64 4.16.1.3-27.el9_3            baseos     19 k
    ezconf.tp1.efrei:  selinux-policy             noarch 38.1.23-1.el9_3.2            baseos     52 k
    ezconf.tp1.efrei:  selinux-policy-targeted    noarch 38.1.23-1.el9_3.2            baseos    6.5 M
    ezconf.tp1.efrei:  sqlite-libs                x86_64 3.34.1-7.el9_3               baseos    618 k
    ezconf.tp1.efrei:  sssd-client                x86_64 2.9.1-4.el9_3.5              baseos    162 k
    ezconf.tp1.efrei:  sssd-common                x86_64 2.9.1-4.el9_3.5              baseos    1.6 M
    ezconf.tp1.efrei:  sssd-kcm                   x86_64 2.9.1-4.el9_3.5              baseos    110 k
    ezconf.tp1.efrei:  sudo                       x86_64 1.9.5p2-10.el9_3             baseos    1.0 M
    ezconf.tp1.efrei:  systemd                    x86_64 252-18.el9.0.1.rocky         baseos    3.9 M
    ezconf.tp1.efrei:  systemd-libs               x86_64 252-18.el9.0.1.rocky         baseos    651 k
    ezconf.tp1.efrei:  systemd-pam                x86_64 252-18.el9.0.1.rocky         baseos    259 k
    ezconf.tp1.efrei:  systemd-rpm-macros         noarch 252-18.el9.0.1.rocky         baseos     47 k
    ezconf.tp1.efrei:  systemd-udev               x86_64 252-18.el9.0.1.rocky         baseos    1.8 M
    ezconf.tp1.efrei:  tzdata                     noarch 2024a-1.el9                  baseos    430 k
    ezconf.tp1.efrei: Installing dependencies:
    ezconf.tp1.efrei:  freetype                   x86_64 2.10.4-9.el9                 baseos    387 k
    ezconf.tp1.efrei:  graphite2                  x86_64 1.3.14-9.el9                 baseos     94 k
    ezconf.tp1.efrei:  grub2-tools-efi            x86_64 1:2.06-70.el9_3.2.rocky.0.4  baseos    539 k
    ezconf.tp1.efrei:  grub2-tools-extra          x86_64 1:2.06-70.el9_3.2.rocky.0.4  baseos    838 k
    ezconf.tp1.efrei:  harfbuzz                   x86_64 2.7.4-8.el9                  baseos    624 k
    ezconf.tp1.efrei:  kernel-core                x86_64 5.14.0-362.24.1.el9_3        baseos     19 M
    ezconf.tp1.efrei:  kernel-modules             x86_64 5.14.0-362.24.1.el9_3        baseos     37 M
    ezconf.tp1.efrei:  kernel-modules-core        x86_64 5.14.0-362.24.1.el9_3        baseos     31 M
    ezconf.tp1.efrei:  libpng                     x86_64 2:1.6.37-12.el9              baseos    116 k
    ezconf.tp1.efrei:
    ezconf.tp1.efrei: Transaction Summary
    ezconf.tp1.efrei: ================================================================================
    ezconf.tp1.efrei: Install  11 Packages
    ezconf.tp1.efrei: Upgrade  65 Packages
    ezconf.tp1.efrei:
    ezconf.tp1.efrei: Total download size: 497 M
    ezconf.tp1.efrei: Downloading Packages:
    ezconf.tp1.efrei: [MIRROR] graphite2-1.3.14-9.el9.x86_64.rpm: Status code: 403 for http://rocky.quelquesmots.fr/9.3/BaseOS/x86_64/os/Packages/g/graphite2-1.3.14-9.el9.x86_64.rpm (IP: 137.74.202.137)
    ezconf.tp1.efrei: [MIRROR] freetype-2.10.4-9.el9.x86_64.rpm: Status code: 403 for http://rocky.quelquesmots.fr/9.3/BaseOS/x86_64/os/Packages/f/freetype-2.10.4-9.el9.x86_64.rpm (IP: 137.74.202.137)
    ezconf.tp1.efrei: [MIRROR] harfbuzz-2.7.4-8.el9.x86_64.rpm: Status code: 403 for http://rocky.quelquesmots.fr/9.3/BaseOS/x86_64/os/Packages/h/harfbuzz-2.7.4-8.el9.x86_64.rpm (IP: 137.74.202.137)
    ezconf.tp1.efrei: (1/76): graphite2-1.3.14-9.el9.x86_64.rpm       268 kB/s |  94 kB     00:00
    ezconf.tp1.efrei: (2/76): freetype-2.10.4-9.el9.x86_64.rpm        895 kB/s | 387 kB     00:00
    ezconf.tp1.efrei: (3/76): libpng-1.6.37-12.el9.x86_64.rpm         1.1 MB/s | 116 kB     00:00
    ezconf.tp1.efrei: (4/76): harfbuzz-2.7.4-8.el9.x86_64.rpm         1.0 MB/s | 624 kB     00:00
    ezconf.tp1.efrei: (5/76): kernel-5.14.0-362.24.1.el9_3.x86_64.rpm 4.0 MB/s | 4.6 MB     00:01
    ezconf.tp1.efrei: (6/76): kernel-modules-5.14.0-362.24.1.el9_3.x8  16 MB/s |  37 MB     00:02
    ezconf.tp1.efrei: (7/76): grub2-tools-extra-2.06-70.el9_3.2.rocky 3.5 MB/s | 838 kB     00:00
    ezconf.tp1.efrei: (8/76): grub2-tools-efi-2.06-70.el9_3.2.rocky.0 4.1 MB/s | 539 kB     00:00
    ezconf.tp1.efrei: (9/76): kernel-devel-5.14.0-362.24.1.el9_3.x86_ 9.5 MB/s |  20 MB     00:02
    ezconf.tp1.efrei: (10/76): basesystem-11-13.el9.0.1.noarch.rpm    128 kB/s | 6.4 kB     00:00
    ezconf.tp1.efrei: (11/76): kernel-core-5.14.0-362.24.1.el9_3.x86_ 3.8 MB/s |  19 MB     00:05
    ezconf.tp1.efrei: (12/76): selinux-policy-38.1.23-1.el9_3.2.noarc 536 kB/s |  52 kB     00:00
    ezconf.tp1.efrei: (13/76): expat-2.5.0-1.el9_3.1.x86_64.rpm       637 kB/s | 115 kB     00:00
    ezconf.tp1.efrei: (14/76): kernel-modules-core-5.14.0-362.24.1.el 7.1 MB/s |  31 MB     00:04
    ezconf.tp1.efrei: (15/76): binutils-gold-2.35.2-42.el9_3.1.x86_64 2.2 MB/s | 732 kB     00:00
    ezconf.tp1.efrei: (16/76): selinux-policy-targeted-38.1.23-1.el9_ 5.7 MB/s | 6.5 MB     00:01
    ezconf.tp1.efrei: (17/76): python3-pip-wheel-21.2.3-7.el9_3.1.noa 4.0 MB/s | 1.1 MB     00:00
    ezconf.tp1.efrei: (18/76): rpm-sign-libs-4.16.1.3-27.el9_3.x86_64 192 kB/s |  19 kB     00:00
    ezconf.tp1.efrei: (19/76): rpm-plugin-selinux-4.16.1.3-27.el9_3.x 191 kB/s |  15 kB     00:00
    ezconf.tp1.efrei: (20/76): rpm-plugin-audit-4.16.1.3-27.el9_3.x86 246 kB/s |  15 kB     00:00
    ezconf.tp1.efrei: (21/76): rpm-libs-4.16.1.3-27.el9_3.x86_64.rpm  2.7 MB/s | 307 kB     00:00
    ezconf.tp1.efrei: (22/76): rpm-build-libs-4.16.1.3-27.el9_3.x86_6 983 kB/s |  87 kB     00:00
    ezconf.tp1.efrei: (23/76): python3-rpm-4.16.1.3-27.el9_3.x86_64.r 951 kB/s |  64 kB     00:00
    ezconf.tp1.efrei: (24/76): rpm-4.16.1.3-27.el9_3.x86_64.rpm       2.8 MB/s | 485 kB     00:00
    ezconf.tp1.efrei: (25/76): binutils-2.35.2-42.el9_3.1.x86_64.rpm  5.0 MB/s | 4.5 MB     00:00
    ezconf.tp1.efrei: (26/76): openssl-libs-3.0.7-25.el9_3.x86_64.rpm 6.3 MB/s | 2.1 MB     00:00
    ezconf.tp1.efrei: (27/76): sqlite-libs-3.34.1-7.el9_3.x86_64.rpm  2.5 MB/s | 618 kB     00:00
    ezconf.tp1.efrei: (28/76): tzdata-2024a-1.el9.noarch.rpm          1.5 MB/s | 430 kB     00:00
    ezconf.tp1.efrei: (29/76): linux-firmware-whence-20230814-142.el9 948 kB/s |  77 kB     00:00
    ezconf.tp1.efrei: (30/76): openssl-3.0.7-25.el9_3.x86_64.rpm      1.6 MB/s | 1.2 MB     00:00
    ezconf.tp1.efrei: (31/76): python3-3.9.18-1.el9_3.1.x86_64.rpm    230 kB/s |  25 kB     00:00
    ezconf.tp1.efrei: (32/76): gnutls-3.7.6-23.el9_3.3.x86_64.rpm     2.4 MB/s | 1.0 MB     00:00
    ezconf.tp1.efrei: (33/76): openssh-server-8.7p1-34.el9_3.3.x86_64 1.8 MB/s | 458 kB     00:00
    ezconf.tp1.efrei: (34/76): python3-libs-3.9.18-1.el9_3.1.x86_64.r 5.9 MB/s | 7.3 MB     00:01
    ezconf.tp1.efrei: (35/76): openssh-clients-8.7p1-34.el9_3.3.x86_6 2.2 MB/s | 712 kB     00:00
    ezconf.tp1.efrei: (36/76): sssd-kcm-2.9.1-4.el9_3.5.x86_64.rpm    1.2 MB/s | 110 kB     00:00
    ezconf.tp1.efrei: (37/76): openssh-8.7p1-34.el9_3.3.x86_64.rpm    3.1 MB/s | 456 kB     00:00
    ezconf.tp1.efrei: (38/76): sssd-client-2.9.1-4.el9_3.5.x86_64.rpm 1.6 MB/s | 162 kB     00:00
    ezconf.tp1.efrei: (39/76): libsss_sudo-2.9.1-4.el9_3.5.x86_64.rpm 391 kB/s |  37 kB     00:00
    ezconf.tp1.efrei: (40/76): libsss_nss_idmap-2.9.1-4.el9_3.5.x86_6 585 kB/s |  47 kB     00:00
    ezconf.tp1.efrei: (41/76): libsss_idmap-2.9.1-4.el9_3.5.x86_64.rp 749 kB/s |  43 kB     00:00
    ezconf.tp1.efrei: (42/76): sssd-common-2.9.1-4.el9_3.5.x86_64.rpm 4.1 MB/s | 1.6 MB     00:00
    ezconf.tp1.efrei: (43/76): libsss_certmap-2.9.1-4.el9_3.5.x86_64. 1.2 MB/s |  92 kB     00:00
    ezconf.tp1.efrei: (44/76): systemd-rpm-macros-252-18.el9.0.1.rock 645 kB/s |  47 kB     00:00
    ezconf.tp1.efrei: (45/76): systemd-pam-252-18.el9.0.1.rocky.x86_6 2.0 MB/s | 259 kB     00:00
    ezconf.tp1.efrei: (46/76): systemd-udev-252-18.el9.0.1.rocky.x86_ 4.1 MB/s | 1.8 MB     00:00
    ezconf.tp1.efrei: (47/76): systemd-libs-252-18.el9.0.1.rocky.x86_ 3.5 MB/s | 651 kB     00:00
    ezconf.tp1.efrei: (48/76): libcurl-7.76.1-26.el9_3.3.x86_64.rpm   2.3 MB/s | 283 kB     00:00
    ezconf.tp1.efrei: (49/76): curl-7.76.1-26.el9_3.3.x86_64.rpm      1.7 MB/s | 293 kB     00:00
    ezconf.tp1.efrei: (50/76): libssh-config-0.10.4-12.el9_3.noarch.r 190 kB/s | 9.2 kB     00:00
    ezconf.tp1.efrei: (51/76): libssh-0.10.4-12.el9_3.x86_64.rpm      1.5 MB/s | 215 kB     00:00
    ezconf.tp1.efrei: (52/76): sudo-1.9.5p2-10.el9_3.x86_64.rpm       3.1 MB/s | 1.0 MB     00:00
    ezconf.tp1.efrei: (53/76): systemd-252-18.el9.0.1.rocky.x86_64.rp 4.4 MB/s | 3.9 MB     00:00
    ezconf.tp1.efrei: (54/76): kernel-tools-libs-5.14.0-362.24.1.el9_ 4.7 MB/s | 4.6 MB     00:00
    ezconf.tp1.efrei: (55/76): python3-perf-5.14.0-362.24.1.el9_3.x86 3.9 MB/s | 4.7 MB     00:01
    ezconf.tp1.efrei: (56/76): glibc-langpack-en-2.34-83.el9.12.x86_6 2.3 MB/s | 561 kB     00:00
    ezconf.tp1.efrei: (57/76): glibc-gconv-extra-2.34-83.el9.12.x86_6 3.7 MB/s | 1.5 MB     00:00
    ezconf.tp1.efrei: (58/76): glibc-common-2.34-83.el9.12.x86_64.rpm 2.3 MB/s | 303 kB     00:00
    ezconf.tp1.efrei: (59/76): glibc-2.34-83.el9.12.x86_64.rpm        4.7 MB/s | 1.9 MB     00:00
    ezconf.tp1.efrei: (60/76): kernel-tools-5.14.0-362.24.1.el9_3.x86 3.3 MB/s | 4.9 MB     00:01
    ezconf.tp1.efrei: (61/76): NetworkManager-tui-1.44.0-5.el9_3.x86_ 1.7 MB/s | 243 kB     00:00
    ezconf.tp1.efrei: (62/76): NetworkManager-team-1.44.0-5.el9_3.x86 510 kB/s |  38 kB     00:00
    ezconf.tp1.efrei: (63/76): NetworkManager-libnm-1.44.0-5.el9_3.x8 3.6 MB/s | 1.8 MB     00:00
    ezconf.tp1.efrei: (64/76): NetworkManager-1.44.0-5.el9_3.x86_64.r 3.5 MB/s | 2.2 MB     00:00
    ezconf.tp1.efrei: (65/76): grub2-tools-minimal-2.06-70.el9_3.2.ro 2.6 MB/s | 601 kB     00:00
    ezconf.tp1.efrei: (66/76): grub2-pc-2.06-70.el9_3.2.rocky.0.4.x86 212 kB/s |  13 kB     00:00
    ezconf.tp1.efrei: (67/76): grub2-tools-2.06-70.el9_3.2.rocky.0.4. 4.6 MB/s | 1.8 MB     00:00
    ezconf.tp1.efrei: (68/76): grub2-pc-modules-2.06-70.el9_3.2.rocky 2.6 MB/s | 909 kB     00:00
    ezconf.tp1.efrei: (69/76): grub2-common-2.06-70.el9_3.2.rocky.0.4 3.1 MB/s | 903 kB     00:00
    ezconf.tp1.efrei: (70/76): openssl-devel-3.0.7-25.el9_3.x86_64.rp 1.5 MB/s | 3.0 MB     00:01
    ezconf.tp1.efrei: (71/76): python-unversioned-command-3.9.18-1.el  71 kB/s | 8.6 kB     00:00
    ezconf.tp1.efrei: (72/76): rpm-plugin-systemd-inhibit-4.16.1.3-27 4.7 kB/s |  15 kB     00:03
    ezconf.tp1.efrei: (73/76): glibc-headers-2.34-83.el9.12.x86_64.rp 1.1 MB/s | 444 kB     00:00
    ezconf.tp1.efrei: (74/76): glibc-devel-2.34-83.el9.12.x86_64.rpm   27 kB/s |  43 kB     00:01
    ezconf.tp1.efrei: (75/76): kernel-headers-5.14.0-362.24.1.el9_3.x 2.1 MB/s | 6.1 MB     00:02
    ezconf.tp1.efrei: (76/76): linux-firmware-20230814-142.el9_3.noar  17 MB/s | 307 MB     00:18
    ezconf.tp1.efrei: --------------------------------------------------------------------------------
    ezconf.tp1.efrei: Total                                            19 MB/s | 497 MB     00:26
    ezconf.tp1.efrei: Running transaction check
    ezconf.tp1.efrei: Transaction check succeeded.
    ezconf.tp1.efrei: Running transaction test
    ezconf.tp1.efrei: Transaction test succeeded.
    ezconf.tp1.efrei: Running transaction
    ezconf.tp1.efrei:   Running scriptlet: selinux-policy-targeted-38.1.23-1.el9_3.2.noarch       1/1
    ezconf.tp1.efrei:   Preparing        :                                                        1/1
    ezconf.tp1.efrei:   Upgrading        : grub2-common-1:2.06-70.el9_3.2.rocky.0.4.noarch      1/141
    ezconf.tp1.efrei:   Upgrading        : tzdata-2024a-1.el9.noarch                            2/141
    ezconf.tp1.efrei:   Upgrading        : grub2-pc-modules-1:2.06-70.el9_3.2.rocky.0.4.noa     3/141
    ezconf.tp1.efrei:   Upgrading        : selinux-policy-38.1.23-1.el9_3.2.noarch              4/141
    ezconf.tp1.efrei:   Running scriptlet: selinux-policy-38.1.23-1.el9_3.2.noarch              4/141
    ezconf.tp1.efrei:   Running scriptlet: selinux-policy-targeted-38.1.23-1.el9_3.2.noarch     5/141
    ezconf.tp1.efrei:   Upgrading        : selinux-policy-targeted-38.1.23-1.el9_3.2.noarch     5/141
    ezconf.tp1.efrei:   Running scriptlet: selinux-policy-targeted-38.1.23-1.el9_3.2.noarch     5/141
    ezconf.tp1.efrei:   Upgrading        : kernel-headers-5.14.0-362.24.1.el9_3.x86_64          6/141
    ezconf.tp1.efrei:   Upgrading        : libssh-config-0.10.4-12.el9_3.noarch                 7/141
    ezconf.tp1.efrei:   Upgrading        : systemd-rpm-macros-252-18.el9.0.1.rocky.noarch       8/141
    ezconf.tp1.efrei:   Upgrading        : linux-firmware-whence-20230814-142.el9_3.noarch      9/141
    ezconf.tp1.efrei:   Upgrading        : linux-firmware-20230814-142.el9_3.noarch            10/141
    ezconf.tp1.efrei:   Upgrading        : python3-pip-wheel-21.2.3-7.el9_3.1.noarch           11/141
    ezconf.tp1.efrei:   Upgrading        : basesystem-11-13.el9.0.1.noarch                     12/141
    ezconf.tp1.efrei:   Upgrading        : glibc-langpack-en-2.34-83.el9.12.x86_64             13/141
    ezconf.tp1.efrei:   Upgrading        : glibc-gconv-extra-2.34-83.el9.12.x86_64             14/141
    ezconf.tp1.efrei:   Running scriptlet: glibc-gconv-extra-2.34-83.el9.12.x86_64             14/141
    ezconf.tp1.efrei:   Upgrading        : glibc-common-2.34-83.el9.12.x86_64                  15/141
    ezconf.tp1.efrei:   Running scriptlet: glibc-2.34-83.el9.12.x86_64                         16/141
    ezconf.tp1.efrei:   Upgrading        : glibc-2.34-83.el9.12.x86_64                         16/141
    ezconf.tp1.efrei:   Running scriptlet: glibc-2.34-83.el9.12.x86_64                         16/141
    ezconf.tp1.efrei:   Upgrading        : openssl-libs-1:3.0.7-25.el9_3.x86_64                17/141
    ezconf.tp1.efrei:   Upgrading        : sqlite-libs-3.34.1-7.el9_3.x86_64                   18/141
    ezconf.tp1.efrei:   Upgrading        : systemd-libs-252-18.el9.0.1.rocky.x86_64            19/141
    ezconf.tp1.efrei:   Running scriptlet: systemd-libs-252-18.el9.0.1.rocky.x86_64            19/141
    ezconf.tp1.efrei:   Upgrading        : systemd-pam-252-18.el9.0.1.rocky.x86_64             20/141
    ezconf.tp1.efrei:   Running scriptlet: systemd-252-18.el9.0.1.rocky.x86_64                 21/141
    ezconf.tp1.efrei:   Upgrading        : systemd-252-18.el9.0.1.rocky.x86_64                 21/141
    ezconf.tp1.efrei:   Running scriptlet: systemd-252-18.el9.0.1.rocky.x86_64                 21/141
    ezconf.tp1.efrei:   Upgrading        : systemd-udev-252-18.el9.0.1.rocky.x86_64            22/141
    ezconf.tp1.efrei:   Running scriptlet: systemd-udev-252-18.el9.0.1.rocky.x86_64            22/141
    ezconf.tp1.efrei:   Installing       : kernel-modules-core-5.14.0-362.24.1.el9_3.x86_64    23/141
    ezconf.tp1.efrei:   Installing       : kernel-core-5.14.0-362.24.1.el9_3.x86_64            24/141
    ezconf.tp1.efrei:   Running scriptlet: kernel-core-5.14.0-362.24.1.el9_3.x86_64            24/141
    ezconf.tp1.efrei:   Running scriptlet: openssh-8.7p1-34.el9_3.3.x86_64                     25/141
    ezconf.tp1.efrei:   Upgrading        : openssh-8.7p1-34.el9_3.3.x86_64                     25/141
    ezconf.tp1.efrei:   Upgrading        : gnutls-3.7.6-23.el9_3.3.x86_64                      26/141
    ezconf.tp1.efrei:   Upgrading        : NetworkManager-libnm-1:1.44.0-5.el9_3.x86_64        27/141
    ezconf.tp1.efrei:   Running scriptlet: NetworkManager-libnm-1:1.44.0-5.el9_3.x86_64        27/141
    ezconf.tp1.efrei:   Upgrading        : libsss_idmap-2.9.1-4.el9_3.5.x86_64                 28/141
    ezconf.tp1.efrei:   Upgrading        : grub2-tools-minimal-1:2.06-70.el9_3.2.rocky.0.4.    29/141
    ezconf.tp1.efrei:   Installing       : kernel-modules-5.14.0-362.24.1.el9_3.x86_64         30/141
    ezconf.tp1.efrei:   Running scriptlet: kernel-modules-5.14.0-362.24.1.el9_3.x86_64         30/141
    ezconf.tp1.efrei:   Upgrading        : libsss_certmap-2.9.1-4.el9_3.5.x86_64               31/141
    ezconf.tp1.efrei:   Upgrading        : libssh-0.10.4-12.el9_3.x86_64                       32/141
    ezconf.tp1.efrei:   Upgrading        : libcurl-7.76.1-26.el9_3.3.x86_64                    33/141
    ezconf.tp1.efrei:   Running scriptlet: NetworkManager-1:1.44.0-5.el9_3.x86_64              34/141
    ezconf.tp1.efrei:   Upgrading        : NetworkManager-1:1.44.0-5.el9_3.x86_64              34/141
    ezconf.tp1.efrei:   Running scriptlet: NetworkManager-1:1.44.0-5.el9_3.x86_64              34/141
    ezconf.tp1.efrei:   Upgrading        : curl-7.76.1-26.el9_3.3.x86_64                       35/141
    ezconf.tp1.efrei:   Upgrading        : rpm-libs-4.16.1.3-27.el9_3.x86_64                   36/141
    ezconf.tp1.efrei:   Upgrading        : rpm-4.16.1.3-27.el9_3.x86_64                        37/141
    ezconf.tp1.efrei:   Upgrading        : rpm-sign-libs-4.16.1.3-27.el9_3.x86_64              38/141
    ezconf.tp1.efrei:   Upgrading        : rpm-build-libs-4.16.1.3-27.el9_3.x86_64             39/141
    ezconf.tp1.efrei:   Upgrading        : openssl-devel-1:3.0.7-25.el9_3.x86_64               40/141
    ezconf.tp1.efrei:   Installing       : graphite2-1.3.14-9.el9.x86_64                       41/141
    ezconf.tp1.efrei:   Installing       : libpng-2:1.6.37-12.el9.x86_64                       42/141
    ezconf.tp1.efrei:   Installing       : freetype-2.10.4-9.el9.x86_64                        43/141
    ezconf.tp1.efrei:   Installing       : harfbuzz-2.7.4-8.el9.x86_64                         44/141
    ezconf.tp1.efrei:   Upgrading        : expat-2.5.0-1.el9_3.1.x86_64                        45/141
    ezconf.tp1.efrei:   Upgrading        : python3-libs-3.9.18-1.el9_3.1.x86_64                46/141
    ezconf.tp1.efrei:   Upgrading        : python-unversioned-command-3.9.18-1.el9_3.1.noar    47/141
    ezconf.tp1.efrei:   Upgrading        : python3-3.9.18-1.el9_3.1.x86_64                     48/141
    ezconf.tp1.efrei:   Upgrading        : binutils-gold-2.35.2-42.el9_3.1.x86_64              49/141
    ezconf.tp1.efrei:   Upgrading        : binutils-2.35.2-42.el9_3.1.x86_64                   50/141
    ezconf.tp1.efrei:   Running scriptlet: binutils-2.35.2-42.el9_3.1.x86_64                   50/141
    ezconf.tp1.efrei:   Upgrading        : libsss_sudo-2.9.1-4.el9_3.5.x86_64                  51/141
    ezconf.tp1.efrei:   Upgrading        : libsss_nss_idmap-2.9.1-4.el9_3.5.x86_64             52/141
    ezconf.tp1.efrei:   Upgrading        : sssd-client-2.9.1-4.el9_3.5.x86_64                  53/141
    ezconf.tp1.efrei:   Running scriptlet: sssd-client-2.9.1-4.el9_3.5.x86_64                  53/141
    ezconf.tp1.efrei:   Running scriptlet: sssd-common-2.9.1-4.el9_3.5.x86_64                  54/141
    ezconf.tp1.efrei:   Upgrading        : sssd-common-2.9.1-4.el9_3.5.x86_64                  54/141
    ezconf.tp1.efrei:   Running scriptlet: sssd-common-2.9.1-4.el9_3.5.x86_64                  54/141
    ezconf.tp1.efrei:   Upgrading        : kernel-tools-libs-5.14.0-362.24.1.el9_3.x86_64      55/141
    ezconf.tp1.efrei:   Running scriptlet: kernel-tools-libs-5.14.0-362.24.1.el9_3.x86_64      55/141
    ezconf.tp1.efrei:   Running scriptlet: grub2-tools-1:2.06-70.el9_3.2.rocky.0.4.x86_64      56/141
    ezconf.tp1.efrei:   Upgrading        : grub2-tools-1:2.06-70.el9_3.2.rocky.0.4.x86_64      56/141
    ezconf.tp1.efrei:   Upgrading        : glibc-headers-2.34-83.el9.12.x86_64                 57/141
    ezconf.tp1.efrei:   Upgrading        : glibc-devel-2.34-83.el9.12.x86_64                   58/141
    ezconf.tp1.efrei:   Upgrading        : grub2-pc-1:2.06-70.el9_3.2.rocky.0.4.x86_64         59/141
    ezconf.tp1.efrei:   Upgrading        : kernel-tools-5.14.0-362.24.1.el9_3.x86_64           60/141
    ezconf.tp1.efrei:   Running scriptlet: kernel-tools-5.14.0-362.24.1.el9_3.x86_64           60/141
    ezconf.tp1.efrei:   Upgrading        : sssd-kcm-2.9.1-4.el9_3.5.x86_64                     61/141
    ezconf.tp1.efrei:   Running scriptlet: sssd-kcm-2.9.1-4.el9_3.5.x86_64                     61/141
    ezconf.tp1.efrei:   Upgrading        : python3-rpm-4.16.1.3-27.el9_3.x86_64                62/141
    ezconf.tp1.efrei:   Upgrading        : python3-perf-5.14.0-362.24.1.el9_3.x86_64           63/141
    ezconf.tp1.efrei:   Installing       : grub2-tools-extra-1:2.06-70.el9_3.2.rocky.0.4.x8    64/141
    ezconf.tp1.efrei:   Installing       : kernel-devel-5.14.0-362.24.1.el9_3.x86_64           65/141
    ezconf.tp1.efrei:   Running scriptlet: kernel-devel-5.14.0-362.24.1.el9_3.x86_64           65/141
    ezconf.tp1.efrei:   Upgrading        : rpm-plugin-selinux-4.16.1.3-27.el9_3.x86_64         66/141
    ezconf.tp1.efrei:   Upgrading        : rpm-plugin-audit-4.16.1.3-27.el9_3.x86_64           67/141
    ezconf.tp1.efrei:   Upgrading        : rpm-plugin-systemd-inhibit-4.16.1.3-27.el9_3.x86    68/141
    ezconf.tp1.efrei:   Upgrading        : NetworkManager-tui-1:1.44.0-5.el9_3.x86_64          69/141
    ezconf.tp1.efrei:   Upgrading        : NetworkManager-team-1:1.44.0-5.el9_3.x86_64         70/141
    ezconf.tp1.efrei:   Installing       : kernel-5.14.0-362.24.1.el9_3.x86_64                 71/141
    ezconf.tp1.efrei:   Running scriptlet: openssh-server-8.7p1-34.el9_3.3.x86_64              72/141
    ezconf.tp1.efrei:   Upgrading        : openssh-server-8.7p1-34.el9_3.3.x86_64              72/141
    ezconf.tp1.efrei:   Running scriptlet: openssh-server-8.7p1-34.el9_3.3.x86_64              72/141
    ezconf.tp1.efrei:   Upgrading        : openssh-clients-8.7p1-34.el9_3.3.x86_64             73/141
    ezconf.tp1.efrei:   Running scriptlet: openssh-clients-8.7p1-34.el9_3.3.x86_64             73/141
    ezconf.tp1.efrei:   Upgrading        : openssl-1:3.0.7-25.el9_3.x86_64                     74/141
    ezconf.tp1.efrei:   Installing       : grub2-tools-efi-1:2.06-70.el9_3.2.rocky.0.4.x86_    75/141
    ezconf.tp1.efrei:   Upgrading        : sudo-1.9.5p2-10.el9_3.x86_64                        76/141
    ezconf.tp1.efrei:   Running scriptlet: sudo-1.9.5p2-10.el9_3.x86_64                        76/141
    ezconf.tp1.efrei:   Cleanup          : glibc-devel-2.34-83.el9.7.x86_64                    77/141
    ezconf.tp1.efrei:   Cleanup          : grub2-pc-1:2.06-70.el9_3.1.rocky.0.2.x86_64         78/141
    ezconf.tp1.efrei:   Cleanup          : openssl-devel-1:3.0.7-24.el9.x86_64                 79/141
    ezconf.tp1.efrei:   Cleanup          : selinux-policy-targeted-38.1.23-1.el9.noarch        80/141
    ezconf.tp1.efrei:   Running scriptlet: selinux-policy-targeted-38.1.23-1.el9.noarch        80/141
    ezconf.tp1.efrei:   Cleanup          : NetworkManager-tui-1:1.44.0-3.el9.x86_64            81/141
    ezconf.tp1.efrei:   Cleanup          : python3-libs-3.9.18-1.el9_3.x86_64                  82/141
    ezconf.tp1.efrei:   Cleanup          : systemd-pam-252-18.el9.x86_64                       83/141
    ezconf.tp1.efrei:   Running scriptlet: openssh-server-8.7p1-34.el9.x86_64                  84/141
    ezconf.tp1.efrei:   Cleanup          : openssh-server-8.7p1-34.el9.x86_64                  84/141
    ezconf.tp1.efrei:   Running scriptlet: openssh-server-8.7p1-34.el9.x86_64                  84/141
    ezconf.tp1.efrei:   Running scriptlet: sssd-kcm-2.9.1-4.el9_3.1.x86_64                     85/141
    ezconf.tp1.efrei:   Cleanup          : sssd-kcm-2.9.1-4.el9_3.1.x86_64                     85/141
    ezconf.tp1.efrei:   Running scriptlet: sssd-kcm-2.9.1-4.el9_3.1.x86_64                     85/141
    ezconf.tp1.efrei:   Running scriptlet: sssd-common-2.9.1-4.el9_3.1.x86_64                  86/141
    ezconf.tp1.efrei:   Cleanup          : sssd-common-2.9.1-4.el9_3.1.x86_64                  86/141
    ezconf.tp1.efrei:   Running scriptlet: sssd-common-2.9.1-4.el9_3.1.x86_64                  86/141
    ezconf.tp1.efrei:   Running scriptlet: sssd-client-2.9.1-4.el9_3.1.x86_64                  87/141
    ezconf.tp1.efrei:   Cleanup          : sssd-client-2.9.1-4.el9_3.1.x86_64                  87/141
    ezconf.tp1.efrei:   Cleanup          : sudo-1.9.5p2-9.el9.x86_64                           88/141
    ezconf.tp1.efrei:   Running scriptlet: kernel-tools-5.14.0-362.13.1.el9_3.x86_64           89/141
    ezconf.tp1.efrei:   Cleanup          : kernel-tools-5.14.0-362.13.1.el9_3.x86_64           89/141
    ezconf.tp1.efrei:   Running scriptlet: kernel-tools-5.14.0-362.13.1.el9_3.x86_64           89/141
    ezconf.tp1.efrei:   Running scriptlet: openssh-clients-8.7p1-34.el9.x86_64                 90/141
    ezconf.tp1.efrei:   Cleanup          : openssh-clients-8.7p1-34.el9.x86_64                 90/141
    ezconf.tp1.efrei:   Cleanup          : openssl-1:3.0.7-24.el9.x86_64                       91/141
    ezconf.tp1.efrei:   Cleanup          : openssh-8.7p1-34.el9.x86_64                         92/141
    ezconf.tp1.efrei:   Cleanup          : python3-perf-5.14.0-362.13.1.el9_3.x86_64           93/141
    ezconf.tp1.efrei:   Cleanup          : grub2-tools-1:2.06-70.el9_3.1.rocky.0.2.x86_64      94/141
    ezconf.tp1.efrei:   Cleanup          : grub2-tools-minimal-1:2.06-70.el9_3.1.rocky.0.2.    95/141
    ezconf.tp1.efrei:   Cleanup          : binutils-gold-2.35.2-42.el9.x86_64                  96/141
    ezconf.tp1.efrei:   Running scriptlet: binutils-2.35.2-42.el9.x86_64                       97/141
    ezconf.tp1.efrei:   Cleanup          : binutils-2.35.2-42.el9.x86_64                       97/141
    ezconf.tp1.efrei:   Running scriptlet: binutils-2.35.2-42.el9.x86_64                       97/141
    ezconf.tp1.efrei:   Cleanup          : python3-rpm-4.16.1.3-25.el9.x86_64                  98/141
    ezconf.tp1.efrei:   Cleanup          : rpm-build-libs-4.16.1.3-25.el9.x86_64               99/141
    ezconf.tp1.efrei:   Cleanup          : rpm-sign-libs-4.16.1.3-25.el9.x86_64               100/141
    ezconf.tp1.efrei:   Cleanup          : libsss_nss_idmap-2.9.1-4.el9_3.1.x86_64            101/141
    ezconf.tp1.efrei:   Cleanup          : rpm-plugin-systemd-inhibit-4.16.1.3-25.el9.x86_6   102/141
    ezconf.tp1.efrei:   Cleanup          : rpm-plugin-selinux-4.16.1.3-25.el9.x86_64          103/141
    ezconf.tp1.efrei:   Cleanup          : expat-2.5.0-1.el9.x86_64                           104/141
    ezconf.tp1.efrei:   Cleanup          : rpm-plugin-audit-4.16.1.3-25.el9.x86_64            105/141
    ezconf.tp1.efrei:   Cleanup          : grub2-pc-modules-1:2.06-70.el9_3.1.rocky.0.2.noa   106/141
    ezconf.tp1.efrei:   Cleanup          : glibc-headers-2.34-83.el9.7.x86_64                 107/141
    ezconf.tp1.efrei:   Cleanup          : linux-firmware-20230814-140.el9_3.noarch           108/141
    ezconf.tp1.efrei:   Cleanup          : rpm-libs-4.16.1.3-25.el9.x86_64                    109/141
    ezconf.tp1.efrei:   Cleanup          : sqlite-libs-3.34.1-6.el9_1.x86_64                  110/141
    ezconf.tp1.efrei:   Cleanup          : libsss_certmap-2.9.1-4.el9_3.1.x86_64              111/141
    ezconf.tp1.efrei:   Cleanup          : libsss_sudo-2.9.1-4.el9_3.1.x86_64                 112/141
    ezconf.tp1.efrei:   Cleanup          : rpm-4.16.1.3-25.el9.x86_64                         113/141
    ezconf.tp1.efrei:   Cleanup          : curl-7.76.1-26.el9_3.2.0.1.x86_64                  114/141
    ezconf.tp1.efrei:   Cleanup          : kernel-tools-libs-5.14.0-362.13.1.el9_3.x86_64     115/141
    ezconf.tp1.efrei:   Running scriptlet: kernel-tools-libs-5.14.0-362.13.1.el9_3.x86_64     115/141
    ezconf.tp1.efrei:   Cleanup          : libsss_idmap-2.9.1-4.el9_3.1.x86_64                116/141
    ezconf.tp1.efrei:   Cleanup          : python3-3.9.18-1.el9_3.x86_64                      117/141
    ezconf.tp1.efrei:   Cleanup          : NetworkManager-team-1:1.44.0-3.el9.x86_64          118/141
    ezconf.tp1.efrei:   Cleanup          : python-unversioned-command-3.9.18-1.el9_3.noarch   119/141
    ezconf.tp1.efrei:   Cleanup          : linux-firmware-whence-20230814-140.el9_3.noarch    120/141
    ezconf.tp1.efrei:   Cleanup          : grub2-common-1:2.06-70.el9_3.1.rocky.0.2.noarch    121/141
    ezconf.tp1.efrei:   Running scriptlet: selinux-policy-38.1.23-1.el9.noarch                122/141
    ezconf.tp1.efrei:   Cleanup          : selinux-policy-38.1.23-1.el9.noarch                122/141
    ezconf.tp1.efrei:   Running scriptlet: selinux-policy-38.1.23-1.el9.noarch                122/141
    ezconf.tp1.efrei:   Cleanup          : python3-pip-wheel-21.2.3-7.el9.noarch              123/141
    ezconf.tp1.efrei:   Cleanup          : kernel-headers-5.14.0-362.13.1.el9_3.x86_64        124/141
    ezconf.tp1.efrei:   Running scriptlet: NetworkManager-1:1.44.0-3.el9.x86_64               125/141
    ezconf.tp1.efrei:   Cleanup          : NetworkManager-1:1.44.0-3.el9.x86_64               125/141
    ezconf.tp1.efrei:   Running scriptlet: NetworkManager-1:1.44.0-3.el9.x86_64               125/141
    ezconf.tp1.efrei:   Running scriptlet: systemd-udev-252-18.el9.x86_64                     126/141
    ezconf.tp1.efrei:   Cleanup          : systemd-udev-252-18.el9.x86_64                     126/141
    ezconf.tp1.efrei:   Running scriptlet: systemd-udev-252-18.el9.x86_64                     126/141
    ezconf.tp1.efrei:   Cleanup          : systemd-252-18.el9.x86_64                          127/141
    ezconf.tp1.efrei:   Running scriptlet: systemd-252-18.el9.x86_64                          127/141
    ezconf.tp1.efrei:   Cleanup          : libcurl-7.76.1-26.el9_3.2.0.1.x86_64               128/141
    ezconf.tp1.efrei:   Cleanup          : NetworkManager-libnm-1:1.44.0-3.el9.x86_64         129/141
    ezconf.tp1.efrei:   Running scriptlet: NetworkManager-libnm-1:1.44.0-3.el9.x86_64         129/141
    ezconf.tp1.efrei:   Cleanup          : systemd-rpm-macros-252-18.el9.noarch               130/141
    ezconf.tp1.efrei:   Cleanup          : systemd-libs-252-18.el9.x86_64                     131/141
    ezconf.tp1.efrei:   Cleanup          : libssh-0.10.4-11.el9.x86_64                        132/141
    ezconf.tp1.efrei:   Cleanup          : gnutls-3.7.6-23.el9.x86_64                         133/141
    ezconf.tp1.efrei:   Cleanup          : libssh-config-0.10.4-11.el9.noarch                 134/141
    ezconf.tp1.efrei:   Cleanup          : openssl-libs-1:3.0.7-24.el9.x86_64                 135/141
    ezconf.tp1.efrei:   Cleanup          : glibc-langpack-en-2.34-83.el9.7.x86_64             136/141
    ezconf.tp1.efrei:   Cleanup          : glibc-common-2.34-83.el9.7.x86_64                  137/141
    ezconf.tp1.efrei:   Cleanup          : glibc-gconv-extra-2.34-83.el9.7.x86_64             138/141
    ezconf.tp1.efrei:   Running scriptlet: glibc-gconv-extra-2.34-83.el9.7.x86_64             138/141
    ezconf.tp1.efrei:   Cleanup          : glibc-2.34-83.el9.7.x86_64                         139/141
    ezconf.tp1.efrei:   Cleanup          : basesystem-11-13.el9.noarch                        140/141
    ezconf.tp1.efrei:   Cleanup          : tzdata-2023d-1.el9.noarch                          141/141
    ezconf.tp1.efrei:   Running scriptlet: grub2-common-1:2.06-70.el9_3.2.rocky.0.4.noarch    141/141
    ezconf.tp1.efrei:   Running scriptlet: selinux-policy-targeted-38.1.23-1.el9_3.2.noarch   141/141
    ezconf.tp1.efrei:   Running scriptlet: kernel-modules-core-5.14.0-362.24.1.el9_3.x86_64   141/141
    ezconf.tp1.efrei:   Running scriptlet: kernel-core-5.14.0-362.24.1.el9_3.x86_64           141/141
    ezconf.tp1.efrei:   Devices file sys_wwid t10.ATA_VBOX_HARDDISK_VBa64d3d26-8dd3a5f6 PVID bbyAlJIN9bj4rcDjcYVVqMBz72umwe05 last seen on /dev/sda2 not found.
    ezconf.tp1.efrei:   Devices file sys_wwid t10.ATA_VBOX_HARDDISK_VBa64d3d26-8dd3a5f6 PVID bbyAlJIN9bj4rcDjcYVVqMBz72umwe05 last seen on /dev/sda2 not found.
    ezconf.tp1.efrei: VirtualBox Guest Additions: Building the modules for kernel
    ezconf.tp1.efrei: 5.14.0-362.24.1.el9_3.x86_64.
    ezconf.tp1.efrei:
    ezconf.tp1.efrei:   Running scriptlet: kernel-modules-5.14.0-362.24.1.el9_3.x86_64        141/141
    ezconf.tp1.efrei:   Running scriptlet: rpm-4.16.1.3-27.el9_3.x86_64                       141/141
    ezconf.tp1.efrei:   Running scriptlet: sssd-common-2.9.1-4.el9_3.5.x86_64                 141/141
    ezconf.tp1.efrei:   Running scriptlet: tzdata-2023d-1.el9.noarch                          141/141
    ezconf.tp1.efrei: Couldn't write '64' to 'kernel/random/read_wakeup_threshold', ignoring: No such file or directory
    ezconf.tp1.efrei:
    ezconf.tp1.efrei:   Verifying        : harfbuzz-2.7.4-8.el9.x86_64                          1/141
    ezconf.tp1.efrei:   Verifying        : graphite2-1.3.14-9.el9.x86_64                        2/141
    ezconf.tp1.efrei:   Verifying        : freetype-2.10.4-9.el9.x86_64                         3/141
    ezconf.tp1.efrei:   Verifying        : libpng-2:1.6.37-12.el9.x86_64                        4/141
    ezconf.tp1.efrei:   Verifying        : kernel-modules-5.14.0-362.24.1.el9_3.x86_64          5/141
    ezconf.tp1.efrei:   Verifying        : kernel-core-5.14.0-362.24.1.el9_3.x86_64             6/141
    ezconf.tp1.efrei:   Verifying        : kernel-5.14.0-362.24.1.el9_3.x86_64                  7/141
    ezconf.tp1.efrei:   Verifying        : kernel-modules-core-5.14.0-362.24.1.el9_3.x86_64     8/141
    ezconf.tp1.efrei:   Verifying        : grub2-tools-extra-1:2.06-70.el9_3.2.rocky.0.4.x8     9/141
    ezconf.tp1.efrei:   Verifying        : grub2-tools-efi-1:2.06-70.el9_3.2.rocky.0.4.x86_    10/141
    ezconf.tp1.efrei:   Verifying        : kernel-devel-5.14.0-362.24.1.el9_3.x86_64           11/141
    ezconf.tp1.efrei:   Verifying        : basesystem-11-13.el9.0.1.noarch                     12/141
    ezconf.tp1.efrei:   Verifying        : basesystem-11-13.el9.noarch                         13/141
    ezconf.tp1.efrei:   Verifying        : selinux-policy-targeted-38.1.23-1.el9_3.2.noarch    14/141
    ezconf.tp1.efrei:   Verifying        : selinux-policy-targeted-38.1.23-1.el9.noarch        15/141
    ezconf.tp1.efrei:   Verifying        : selinux-policy-38.1.23-1.el9_3.2.noarch             16/141
    ezconf.tp1.efrei:   Verifying        : selinux-policy-38.1.23-1.el9.noarch                 17/141
    ezconf.tp1.efrei:   Verifying        : expat-2.5.0-1.el9_3.1.x86_64                        18/141
    ezconf.tp1.efrei:   Verifying        : expat-2.5.0-1.el9.x86_64                            19/141
    ezconf.tp1.efrei:   Verifying        : binutils-gold-2.35.2-42.el9_3.1.x86_64              20/141
    ezconf.tp1.efrei:   Verifying        : binutils-gold-2.35.2-42.el9.x86_64                  21/141
    ezconf.tp1.efrei:   Verifying        : binutils-2.35.2-42.el9_3.1.x86_64                   22/141
    ezconf.tp1.efrei:   Verifying        : binutils-2.35.2-42.el9.x86_64                       23/141
    ezconf.tp1.efrei:   Verifying        : python3-pip-wheel-21.2.3-7.el9_3.1.noarch           24/141
    ezconf.tp1.efrei:   Verifying        : python3-pip-wheel-21.2.3-7.el9.noarch               25/141
    ezconf.tp1.efrei:   Verifying        : rpm-sign-libs-4.16.1.3-27.el9_3.x86_64              26/141
    ezconf.tp1.efrei:   Verifying        : rpm-sign-libs-4.16.1.3-25.el9.x86_64                27/141
    ezconf.tp1.efrei:   Verifying        : rpm-plugin-selinux-4.16.1.3-27.el9_3.x86_64         28/141
    ezconf.tp1.efrei:   Verifying        : rpm-plugin-selinux-4.16.1.3-25.el9.x86_64           29/141
    ezconf.tp1.efrei:   Verifying        : rpm-plugin-audit-4.16.1.3-27.el9_3.x86_64           30/141
    ezconf.tp1.efrei:   Verifying        : rpm-plugin-audit-4.16.1.3-25.el9.x86_64             31/141
    ezconf.tp1.efrei:   Verifying        : rpm-libs-4.16.1.3-27.el9_3.x86_64                   32/141
    ezconf.tp1.efrei:   Verifying        : rpm-libs-4.16.1.3-25.el9.x86_64                     33/141
    ezconf.tp1.efrei:   Verifying        : rpm-build-libs-4.16.1.3-27.el9_3.x86_64             34/141
    ezconf.tp1.efrei:   Verifying        : rpm-build-libs-4.16.1.3-25.el9.x86_64               35/141
    ezconf.tp1.efrei:   Verifying        : rpm-4.16.1.3-27.el9_3.x86_64                        36/141
    ezconf.tp1.efrei:   Verifying        : rpm-4.16.1.3-25.el9.x86_64                          37/141
    ezconf.tp1.efrei:   Verifying        : python3-rpm-4.16.1.3-27.el9_3.x86_64                38/141
    ezconf.tp1.efrei:   Verifying        : python3-rpm-4.16.1.3-25.el9.x86_64                  39/141
    ezconf.tp1.efrei:   Verifying        : openssl-libs-1:3.0.7-25.el9_3.x86_64                40/141
    ezconf.tp1.efrei:   Verifying        : openssl-libs-1:3.0.7-24.el9.x86_64                  41/141
    ezconf.tp1.efrei:   Verifying        : openssl-1:3.0.7-25.el9_3.x86_64                     42/141
    ezconf.tp1.efrei:   Verifying        : openssl-1:3.0.7-24.el9.x86_64                       43/141
    ezconf.tp1.efrei:   Verifying        : tzdata-2024a-1.el9.noarch                           44/141
    ezconf.tp1.efrei:   Verifying        : tzdata-2023d-1.el9.noarch                           45/141
    ezconf.tp1.efrei:   Verifying        : sqlite-libs-3.34.1-7.el9_3.x86_64                   46/141
    ezconf.tp1.efrei:   Verifying        : sqlite-libs-3.34.1-6.el9_1.x86_64                   47/141
    ezconf.tp1.efrei:   Verifying        : linux-firmware-whence-20230814-142.el9_3.noarch     48/141
    ezconf.tp1.efrei:   Verifying        : linux-firmware-whence-20230814-140.el9_3.noarch     49/141
    ezconf.tp1.efrei:   Verifying        : linux-firmware-20230814-142.el9_3.noarch            50/141
    ezconf.tp1.efrei:   Verifying        : linux-firmware-20230814-140.el9_3.noarch            51/141
    ezconf.tp1.efrei:   Verifying        : python3-libs-3.9.18-1.el9_3.1.x86_64                52/141
    ezconf.tp1.efrei:   Verifying        : python3-libs-3.9.18-1.el9_3.x86_64                  53/141
    ezconf.tp1.efrei:   Verifying        : python3-3.9.18-1.el9_3.1.x86_64                     54/141
    ezconf.tp1.efrei:   Verifying        : python3-3.9.18-1.el9_3.x86_64                       55/141
    ezconf.tp1.efrei:   Verifying        : gnutls-3.7.6-23.el9_3.3.x86_64                      56/141
    ezconf.tp1.efrei:   Verifying        : gnutls-3.7.6-23.el9.x86_64                          57/141
    ezconf.tp1.efrei:   Verifying        : openssh-server-8.7p1-34.el9_3.3.x86_64              58/141
    ezconf.tp1.efrei:   Verifying        : openssh-server-8.7p1-34.el9.x86_64                  59/141
    ezconf.tp1.efrei:   Verifying        : openssh-clients-8.7p1-34.el9_3.3.x86_64             60/141
    ezconf.tp1.efrei:   Verifying        : openssh-clients-8.7p1-34.el9.x86_64                 61/141
    ezconf.tp1.efrei:   Verifying        : openssh-8.7p1-34.el9_3.3.x86_64                     62/141
    ezconf.tp1.efrei:   Verifying        : openssh-8.7p1-34.el9.x86_64                         63/141
    ezconf.tp1.efrei:   Verifying        : sssd-kcm-2.9.1-4.el9_3.5.x86_64                     64/141
    ezconf.tp1.efrei:   Verifying        : sssd-kcm-2.9.1-4.el9_3.1.x86_64                     65/141
    ezconf.tp1.efrei:   Verifying        : sssd-common-2.9.1-4.el9_3.5.x86_64                  66/141
    ezconf.tp1.efrei:   Verifying        : sssd-common-2.9.1-4.el9_3.1.x86_64                  67/141
    ezconf.tp1.efrei:   Verifying        : sssd-client-2.9.1-4.el9_3.5.x86_64                  68/141
    ezconf.tp1.efrei:   Verifying        : sssd-client-2.9.1-4.el9_3.1.x86_64                  69/141
    ezconf.tp1.efrei:   Verifying        : libsss_sudo-2.9.1-4.el9_3.5.x86_64                  70/141
    ezconf.tp1.efrei:   Verifying        : libsss_sudo-2.9.1-4.el9_3.1.x86_64                  71/141
    ezconf.tp1.efrei:   Verifying        : libsss_nss_idmap-2.9.1-4.el9_3.5.x86_64             72/141
    ezconf.tp1.efrei:   Verifying        : libsss_nss_idmap-2.9.1-4.el9_3.1.x86_64             73/141
    ezconf.tp1.efrei:   Verifying        : libsss_idmap-2.9.1-4.el9_3.5.x86_64                 74/141
    ezconf.tp1.efrei:   Verifying        : libsss_idmap-2.9.1-4.el9_3.1.x86_64                 75/141
    ezconf.tp1.efrei:   Verifying        : libsss_certmap-2.9.1-4.el9_3.5.x86_64               76/141
    ezconf.tp1.efrei:   Verifying        : libsss_certmap-2.9.1-4.el9_3.1.x86_64               77/141
    ezconf.tp1.efrei:   Verifying        : systemd-udev-252-18.el9.0.1.rocky.x86_64            78/141
    ezconf.tp1.efrei:   Verifying        : systemd-udev-252-18.el9.x86_64                      79/141
    ezconf.tp1.efrei:   Verifying        : systemd-rpm-macros-252-18.el9.0.1.rocky.noarch      80/141
    ezconf.tp1.efrei:   Verifying        : systemd-rpm-macros-252-18.el9.noarch                81/141
    ezconf.tp1.efrei:   Verifying        : systemd-pam-252-18.el9.0.1.rocky.x86_64             82/141
    ezconf.tp1.efrei:   Verifying        : systemd-pam-252-18.el9.x86_64                       83/141
    ezconf.tp1.efrei:   Verifying        : systemd-libs-252-18.el9.0.1.rocky.x86_64            84/141
    ezconf.tp1.efrei:   Verifying        : systemd-libs-252-18.el9.x86_64                      85/141
    ezconf.tp1.efrei:   Verifying        : systemd-252-18.el9.0.1.rocky.x86_64                 86/141
    ezconf.tp1.efrei:   Verifying        : systemd-252-18.el9.x86_64                           87/141
    ezconf.tp1.efrei:   Verifying        : libcurl-7.76.1-26.el9_3.3.x86_64                    88/141
    ezconf.tp1.efrei:   Verifying        : libcurl-7.76.1-26.el9_3.2.0.1.x86_64                89/141
    ezconf.tp1.efrei:   Verifying        : curl-7.76.1-26.el9_3.3.x86_64                       90/141
    ezconf.tp1.efrei:   Verifying        : curl-7.76.1-26.el9_3.2.0.1.x86_64                   91/141
    ezconf.tp1.efrei:   Verifying        : libssh-config-0.10.4-12.el9_3.noarch                92/141
    ezconf.tp1.efrei:   Verifying        : libssh-config-0.10.4-11.el9.noarch                  93/141
    ezconf.tp1.efrei:   Verifying        : libssh-0.10.4-12.el9_3.x86_64                       94/141
    ezconf.tp1.efrei:   Verifying        : libssh-0.10.4-11.el9.x86_64                         95/141
    ezconf.tp1.efrei:   Verifying        : sudo-1.9.5p2-10.el9_3.x86_64                        96/141
    ezconf.tp1.efrei:   Verifying        : sudo-1.9.5p2-9.el9.x86_64                           97/141
    ezconf.tp1.efrei:   Verifying        : python3-perf-5.14.0-362.24.1.el9_3.x86_64           98/141
    ezconf.tp1.efrei:   Verifying        : python3-perf-5.14.0-362.13.1.el9_3.x86_64           99/141
    ezconf.tp1.efrei:   Verifying        : kernel-tools-libs-5.14.0-362.24.1.el9_3.x86_64     100/141
    ezconf.tp1.efrei:   Verifying        : kernel-tools-libs-5.14.0-362.13.1.el9_3.x86_64     101/141
    ezconf.tp1.efrei:   Verifying        : kernel-tools-5.14.0-362.24.1.el9_3.x86_64          102/141
    ezconf.tp1.efrei:   Verifying        : kernel-tools-5.14.0-362.13.1.el9_3.x86_64          103/141
    ezconf.tp1.efrei:   Verifying        : glibc-langpack-en-2.34-83.el9.12.x86_64            104/141
    ezconf.tp1.efrei:   Verifying        : glibc-langpack-en-2.34-83.el9.7.x86_64             105/141
    ezconf.tp1.efrei:   Verifying        : glibc-gconv-extra-2.34-83.el9.12.x86_64            106/141
    ezconf.tp1.efrei:   Verifying        : glibc-gconv-extra-2.34-83.el9.7.x86_64             107/141
    ezconf.tp1.efrei:   Verifying        : glibc-common-2.34-83.el9.12.x86_64                 108/141
    ezconf.tp1.efrei:   Verifying        : glibc-common-2.34-83.el9.7.x86_64                  109/141
    ezconf.tp1.efrei:   Verifying        : glibc-2.34-83.el9.12.x86_64                        110/141
    ezconf.tp1.efrei:   Verifying        : glibc-2.34-83.el9.7.x86_64                         111/141
    ezconf.tp1.efrei:   Verifying        : NetworkManager-tui-1:1.44.0-5.el9_3.x86_64         112/141
    ezconf.tp1.efrei:   Verifying        : NetworkManager-tui-1:1.44.0-3.el9.x86_64           113/141
    ezconf.tp1.efrei:   Verifying        : NetworkManager-team-1:1.44.0-5.el9_3.x86_64        114/141
    ezconf.tp1.efrei:   Verifying        : NetworkManager-team-1:1.44.0-3.el9.x86_64          115/141
    ezconf.tp1.efrei:   Verifying        : NetworkManager-libnm-1:1.44.0-5.el9_3.x86_64       116/141
    ezconf.tp1.efrei:   Verifying        : NetworkManager-libnm-1:1.44.0-3.el9.x86_64         117/141
    ezconf.tp1.efrei:   Verifying        : NetworkManager-1:1.44.0-5.el9_3.x86_64             118/141
    ezconf.tp1.efrei:   Verifying        : NetworkManager-1:1.44.0-3.el9.x86_64               119/141
    ezconf.tp1.efrei:   Verifying        : grub2-tools-minimal-1:2.06-70.el9_3.2.rocky.0.4.   120/141
    ezconf.tp1.efrei:   Verifying        : grub2-tools-minimal-1:2.06-70.el9_3.1.rocky.0.2.   121/141
    ezconf.tp1.efrei:   Verifying        : grub2-tools-1:2.06-70.el9_3.2.rocky.0.4.x86_64     122/141
    ezconf.tp1.efrei:   Verifying        : grub2-tools-1:2.06-70.el9_3.1.rocky.0.2.x86_64     123/141
    ezconf.tp1.efrei:   Verifying        : grub2-pc-1:2.06-70.el9_3.2.rocky.0.4.x86_64        124/141
    ezconf.tp1.efrei:   Verifying        : grub2-pc-1:2.06-70.el9_3.1.rocky.0.2.x86_64        125/141
    ezconf.tp1.efrei:   Verifying        : grub2-pc-modules-1:2.06-70.el9_3.2.rocky.0.4.noa   126/141
    ezconf.tp1.efrei:   Verifying        : grub2-pc-modules-1:2.06-70.el9_3.1.rocky.0.2.noa   127/141
    ezconf.tp1.efrei:   Verifying        : grub2-common-1:2.06-70.el9_3.2.rocky.0.4.noarch    128/141
    ezconf.tp1.efrei:   Verifying        : grub2-common-1:2.06-70.el9_3.1.rocky.0.2.noarch    129/141
    ezconf.tp1.efrei:   Verifying        : rpm-plugin-systemd-inhibit-4.16.1.3-27.el9_3.x86   130/141
    ezconf.tp1.efrei:   Verifying        : rpm-plugin-systemd-inhibit-4.16.1.3-25.el9.x86_6   131/141
    ezconf.tp1.efrei:   Verifying        : openssl-devel-1:3.0.7-25.el9_3.x86_64              132/141
    ezconf.tp1.efrei:   Verifying        : openssl-devel-1:3.0.7-24.el9.x86_64                133/141
    ezconf.tp1.efrei:   Verifying        : python-unversioned-command-3.9.18-1.el9_3.1.noar   134/141
    ezconf.tp1.efrei:   Verifying        : python-unversioned-command-3.9.18-1.el9_3.noarch   135/141
    ezconf.tp1.efrei:   Verifying        : kernel-headers-5.14.0-362.24.1.el9_3.x86_64        136/141
    ezconf.tp1.efrei:   Verifying        : kernel-headers-5.14.0-362.13.1.el9_3.x86_64        137/141
    ezconf.tp1.efrei:   Verifying        : glibc-headers-2.34-83.el9.12.x86_64                138/141
    ezconf.tp1.efrei:   Verifying        : glibc-headers-2.34-83.el9.7.x86_64                 139/141
    ezconf.tp1.efrei:   Verifying        : glibc-devel-2.34-83.el9.12.x86_64                  140/141
    ezconf.tp1.efrei:   Verifying        : glibc-devel-2.34-83.el9.7.x86_64                   141/141
    ezconf.tp1.efrei:
    ezconf.tp1.efrei: Upgraded:
    ezconf.tp1.efrei:   NetworkManager-1:1.44.0-5.el9_3.x86_64
    ezconf.tp1.efrei:   NetworkManager-libnm-1:1.44.0-5.el9_3.x86_64
    ezconf.tp1.efrei:   NetworkManager-team-1:1.44.0-5.el9_3.x86_64
    ezconf.tp1.efrei:   NetworkManager-tui-1:1.44.0-5.el9_3.x86_64
    ezconf.tp1.efrei:   basesystem-11-13.el9.0.1.noarch
    ezconf.tp1.efrei:   binutils-2.35.2-42.el9_3.1.x86_64
    ezconf.tp1.efrei:   binutils-gold-2.35.2-42.el9_3.1.x86_64
    ezconf.tp1.efrei:   curl-7.76.1-26.el9_3.3.x86_64
    ezconf.tp1.efrei:   expat-2.5.0-1.el9_3.1.x86_64
    ezconf.tp1.efrei:   glibc-2.34-83.el9.12.x86_64
    ezconf.tp1.efrei:   glibc-common-2.34-83.el9.12.x86_64
    ezconf.tp1.efrei:   glibc-devel-2.34-83.el9.12.x86_64
    ezconf.tp1.efrei:   glibc-gconv-extra-2.34-83.el9.12.x86_64
    ezconf.tp1.efrei:   glibc-headers-2.34-83.el9.12.x86_64
    ezconf.tp1.efrei:   glibc-langpack-en-2.34-83.el9.12.x86_64
    ezconf.tp1.efrei:   gnutls-3.7.6-23.el9_3.3.x86_64
    ezconf.tp1.efrei:   grub2-common-1:2.06-70.el9_3.2.rocky.0.4.noarch
    ezconf.tp1.efrei:   grub2-pc-1:2.06-70.el9_3.2.rocky.0.4.x86_64
    ezconf.tp1.efrei:   grub2-pc-modules-1:2.06-70.el9_3.2.rocky.0.4.noarch
    ezconf.tp1.efrei:   grub2-tools-1:2.06-70.el9_3.2.rocky.0.4.x86_64
    ezconf.tp1.efrei:   grub2-tools-minimal-1:2.06-70.el9_3.2.rocky.0.4.x86_64
    ezconf.tp1.efrei:   kernel-headers-5.14.0-362.24.1.el9_3.x86_64
    ezconf.tp1.efrei:   kernel-tools-5.14.0-362.24.1.el9_3.x86_64
    ezconf.tp1.efrei:   kernel-tools-libs-5.14.0-362.24.1.el9_3.x86_64
    ezconf.tp1.efrei:   libcurl-7.76.1-26.el9_3.3.x86_64
    ezconf.tp1.efrei:   libssh-0.10.4-12.el9_3.x86_64
    ezconf.tp1.efrei:   libssh-config-0.10.4-12.el9_3.noarch
    ezconf.tp1.efrei:   libsss_certmap-2.9.1-4.el9_3.5.x86_64
    ezconf.tp1.efrei:   libsss_idmap-2.9.1-4.el9_3.5.x86_64
    ezconf.tp1.efrei:   libsss_nss_idmap-2.9.1-4.el9_3.5.x86_64
    ezconf.tp1.efrei:   libsss_sudo-2.9.1-4.el9_3.5.x86_64
    ezconf.tp1.efrei:   linux-firmware-20230814-142.el9_3.noarch
    ezconf.tp1.efrei:   linux-firmware-whence-20230814-142.el9_3.noarch
    ezconf.tp1.efrei:   openssh-8.7p1-34.el9_3.3.x86_64
    ezconf.tp1.efrei:   openssh-clients-8.7p1-34.el9_3.3.x86_64
    ezconf.tp1.efrei:   openssh-server-8.7p1-34.el9_3.3.x86_64
    ezconf.tp1.efrei:   openssl-1:3.0.7-25.el9_3.x86_64
    ezconf.tp1.efrei:   openssl-devel-1:3.0.7-25.el9_3.x86_64
    ezconf.tp1.efrei:   openssl-libs-1:3.0.7-25.el9_3.x86_64
    ezconf.tp1.efrei:   python-unversioned-command-3.9.18-1.el9_3.1.noarch
    ezconf.tp1.efrei:   python3-3.9.18-1.el9_3.1.x86_64
    ezconf.tp1.efrei:   python3-libs-3.9.18-1.el9_3.1.x86_64
    ezconf.tp1.efrei:   python3-perf-5.14.0-362.24.1.el9_3.x86_64
    ezconf.tp1.efrei:   python3-pip-wheel-21.2.3-7.el9_3.1.noarch
    ezconf.tp1.efrei:   python3-rpm-4.16.1.3-27.el9_3.x86_64
    ezconf.tp1.efrei:   rpm-4.16.1.3-27.el9_3.x86_64
    ezconf.tp1.efrei:   rpm-build-libs-4.16.1.3-27.el9_3.x86_64
    ezconf.tp1.efrei:   rpm-libs-4.16.1.3-27.el9_3.x86_64
    ezconf.tp1.efrei:   rpm-plugin-audit-4.16.1.3-27.el9_3.x86_64
    ezconf.tp1.efrei:   rpm-plugin-selinux-4.16.1.3-27.el9_3.x86_64
    ezconf.tp1.efrei:   rpm-plugin-systemd-inhibit-4.16.1.3-27.el9_3.x86_64
    ezconf.tp1.efrei:   rpm-sign-libs-4.16.1.3-27.el9_3.x86_64
    ezconf.tp1.efrei:   selinux-policy-38.1.23-1.el9_3.2.noarch
    ezconf.tp1.efrei:   selinux-policy-targeted-38.1.23-1.el9_3.2.noarch
    ezconf.tp1.efrei:   sqlite-libs-3.34.1-7.el9_3.x86_64
    ezconf.tp1.efrei:   sssd-client-2.9.1-4.el9_3.5.x86_64
    ezconf.tp1.efrei:   sssd-common-2.9.1-4.el9_3.5.x86_64
    ezconf.tp1.efrei:   sssd-kcm-2.9.1-4.el9_3.5.x86_64
    ezconf.tp1.efrei:   sudo-1.9.5p2-10.el9_3.x86_64
    ezconf.tp1.efrei:   systemd-252-18.el9.0.1.rocky.x86_64
    ezconf.tp1.efrei:   systemd-libs-252-18.el9.0.1.rocky.x86_64
    ezconf.tp1.efrei:   systemd-pam-252-18.el9.0.1.rocky.x86_64
    ezconf.tp1.efrei:   systemd-rpm-macros-252-18.el9.0.1.rocky.noarch
    ezconf.tp1.efrei:   systemd-udev-252-18.el9.0.1.rocky.x86_64
    ezconf.tp1.efrei:   tzdata-2024a-1.el9.noarch
    ezconf.tp1.efrei: Installed:
    ezconf.tp1.efrei:   freetype-2.10.4-9.el9.x86_64
    ezconf.tp1.efrei:   graphite2-1.3.14-9.el9.x86_64
    ezconf.tp1.efrei:   grub2-tools-efi-1:2.06-70.el9_3.2.rocky.0.4.x86_64
    ezconf.tp1.efrei:   grub2-tools-extra-1:2.06-70.el9_3.2.rocky.0.4.x86_64
    ezconf.tp1.efrei:   harfbuzz-2.7.4-8.el9.x86_64
    ezconf.tp1.efrei:   kernel-5.14.0-362.24.1.el9_3.x86_64
    ezconf.tp1.efrei:   kernel-core-5.14.0-362.24.1.el9_3.x86_64
    ezconf.tp1.efrei:   kernel-devel-5.14.0-362.24.1.el9_3.x86_64
    ezconf.tp1.efrei:   kernel-modules-5.14.0-362.24.1.el9_3.x86_64
    ezconf.tp1.efrei:   kernel-modules-core-5.14.0-362.24.1.el9_3.x86_64
    ezconf.tp1.efrei:   libpng-2:1.6.37-12.el9.x86_64
    ezconf.tp1.efrei:
    ezconf.tp1.efrei: Complete!
    ezconf.tp1.efrei: Last metadata expiration check: 0:03:42 ago on Tue 02 Apr 2024 02:10:02 PM UTC.
    ezconf.tp1.efrei: Package vim-enhanced-2:8.2.2637-20.el9_1.x86_64 is already installed.
    ezconf.tp1.efrei: Package python3-3.9.18-1.el9_3.1.x86_64 is already installed.
    ezconf.tp1.efrei: Dependencies resolved.
    ezconf.tp1.efrei: Nothing to do.
    ezconf.tp1.efrei: Complete!
    ezconf.tp1.efrei: Last metadata expiration check: 0:03:43 ago on Tue 02 Apr 2024 02:10:02 PM UTC.
    ezconf.tp1.efrei: Dependencies resolved.
    ezconf.tp1.efrei: Nothing to do.
    ezconf.tp1.efrei: Complete!

tomto@MSI MINGW64 /c/cour/Cloud2
$ vagrant package --output rocky-efrei.box
==> ezconf.tp1.efrei: Attempting graceful shutdown of VM...
==> ezconf.tp1.efrei: Clearing any previously set forwarded ports...
==> ezconf.tp1.efrei: Exporting VM...
==> ezconf.tp1.efrei: Compressing package to: C:/cour/Cloud2/rocky-efrei.box

tomto@MSI MINGW64 /c/cour/Cloud2
$ vagrant box add rocky-efrei.box rocky-efrei.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'rocky-efrei.box' (v0) for provider:
    box: Unpacking necessary files from: file://C:/cour/Cloud2/rocky-efrei.box
    box:
==> box: Successfully added box 'rocky-efrei.box' (v0) for ''!

tomto@MSI MINGW64 /c/cour/Cloud2
$ vagrant box list
generic/rocky9  (virtualbox, 4.3.12, (amd64))
rocky-efrei.box (virtualbox, 0)


```

**Multi VM**

Ping entre machine

```
tomto@MSI MINGW64 /c/cour/Cloud2
$ vagrant ssh node1.tp1.efrei
[vagrant@node1 ~]$ ping 10.1.1.102
PING 10.1.1.102 (10.1.1.102) 56(84) bytes of data.
64 bytes from 10.1.1.102: icmp_seq=1 ttl=64 time=2.24 ms
64 bytes from 10.1.1.102: icmp_seq=2 ttl=64 time=0.488 ms
64 bytes from 10.1.1.102: icmp_seq=3 ttl=64 time=0.474 ms
64 bytes from 10.1.1.102: icmp_seq=4 ttl=64 time=0.544 ms
64 bytes from 10.1.1.102: icmp_seq=5 ttl=64 time=0.443 ms

^C
--- 10.1.1.102 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4216ms
rtt min/avg/max/mdev = 0.443/0.837/2.239/0.701 ms
[vagrant@node1 ~]$

```

# V cloud init 
**on créer une box avec cloud init**

**fichier Vagrantfile**

```
Vagrant.configure("2") do |config|
  config.vm.box = "rocky-efrei.box"
  config.vm.network "private_network", ip: "192.168.50.4", netmask: "24"
  config.vm.hostname = "ezconf.tp1.efrei"
  config.vm.provision "shell", path: "bootstrap.sh"
  config.vm.define "ezconf.tp1.efrei" do |h|
    h.vm.disk :disk, size: "100GB", primary: true
  end
  config.vm.provider "virtualbox" do |v|
    v.memory = 2048
  end
end

```
**scirpt d'instalation de could-init pour la nouvelle vm**

```
#!/bin/bash

sudo dnf install -y cloud-init
sudo systemctl enable cloud-init

```

**ensuite on repackage la box**

```
tomto@MSI MINGW64 /c/cour/Cloud2
$ vagrant package --output rocky-efreicloudinit.box
==> ezconf.tp1.efrei: Attempting graceful shutdown of VM...
==> ezconf.tp1.efrei: Clearing any previously set forwarded ports...
==> ezconf.tp1.efrei: Exporting VM...
==> ezconf.tp1.efrei: Compressing package to: C:/cour/Cloud2/rocky-efreicloudinit.box

tomto@MSI MINGW64 /c/cour/Cloud2
$ vagrant box add rocky-efreicloudinit.box rocky-efreicloudinit.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'rocky-efreicloudinit.box' (v0) for provider:
    box: Unpacking necessary files from: file://C:/cour/Cloud2/rocky-efreicloudinit.box
    box:
==> box: Successfully added box 'rocky-efreicloudinit.box' (v0) for ''!

tomto@MSI MINGW64 /c/cour/Cloud2
$ vagrant box list
generic/rocky9           (virtualbox, 4.3.12, (amd64))
rocky-efrei.box          (virtualbox, 0)
rocky-efreicloudinit.box (virtualbox, 0)


```


**maintenant on peut tester**

**fichier config user-data.yml**

```
#cloud-config
users:
  - name: tom
    gecos: Super adminsys
    primary_group: tom
    groups: wheel
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    lock_passwd: false
    passwd: $6$bV62paDqH/ZQSVFb$jiBgcgpkuzmmoZSvvLPwpd4gjwvnKQEWTE119tMNTnICtMcJ6dyPcDCVaTur8j5UQFuxAAM6eTimGdr97Nagh1
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3Nz34RFRU51NTE5AAAAIMO/JQ3AtA3k8iXJWlkdUKSHDh215OKyLR0vauzD7BgA
```

**Vagrantfile**

```
Vagrant.configure("2") do |config|
  config.vm.box = "rocky-efreicloudinit.box"
  config.vm.network "private_network", ip: "10.10.10.203"
  config.vm.hostname = "ezconf.tp1.efrei"
  config.vm.provision "shell", path: "bootstrap.sh"
  config.vm.cloud_init :user_data, content_type: "text/cloud-config", path: "user-data.yml"
  config.vm.define "ezconf.tp1.efrei" do |h|
    h.vm.disk :disk, size: "100GB", primary: true
  end
  config.vm.provider "virtualbox" do |v|
    v.memory = 2048
  end
end
```

On obtient finalement un user du nom de Tom Dans notre fichier /etc/passwd.









