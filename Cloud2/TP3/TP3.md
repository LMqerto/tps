# compte rendu TP3 Tom Bernard

**🌞 Installer un serveur MySQL**

```
[vagrant@frontend ~]$ wget https://dev.mysql.com/get/mysql80-community-release-el9-5.noarch.rpm
--2024-04-09 09:01:51--  https://dev.mysql.com/get/mysql80-community-release-el9-5.noarch.rpm
Resolving dev.mysql.com (dev.mysql.com)... 23.54.143.15, 2a02:26f0:2b00:3a2::2e31, 2a02:26f0:2b00:387::2e31
Connecting to dev.mysql.com (dev.mysql.com)|23.54.143.15|:443... connected.
HTTP request sent, awaiting response... 302 Moved Temporarily
Location: https://repo.mysql.com//mysql80-community-release-el9-5.noarch.rpm [following]
--2024-04-09 09:01:52--  https://repo.mysql.com//mysql80-community-release-el9-5.noarch.rpm
Resolving repo.mysql.com (repo.mysql.com)... 2.18.132.71, 2a02:26f0:5700:294::1d68, 2a02:26f0:5700:2af::1d68
Connecting to repo.mysql.com (repo.mysql.com)|2.18.132.71|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 13319 (13K) [application/x-redhat-package-manager]
Saving to: ‘mysql80-community-release-el9-5.noarch.rpm’

mysql80-community-r 100%[===================>]  13.01K  --.-KB/s    in 0s

2024-04-09 09:01:52 (177 MB/s) - ‘mysql80-community-release-el9-5.noarch.rpm’ saved [13319/13319]

[vagrant@frontend ~]$ rpm -Uvh mysql80-community-release-el9-5.noarch.rpm       warning: mysql80-community-release-el9-5.noarch.rpm: Header V4 RSA/SHA256 Signature, key ID 3a79bd29: NOKEY
error: can't create transaction lock on /var/lib/rpm/.rpm.lock (Permission denied)
[vagrant@frontend ~]$ sudo rpm -Uvh mysql80-community-release-el9-5.noarch.rpm
warning: mysql80-community-release-el9-5.noarch.rpm: Header V4 RSA/SHA256 Signature, key ID 3a79bd29: NOKEY
Verifying...                          ################################# [100%]
Preparing...                          ################################# [100%]
Updating / installing...
   1:mysql80-community-release-el9-5  ################################# [100%]
[vagrant@frontend ~]$ sudo dnf search mysql
MySQL 8.0 Community Server                      1.7 MB/s | 1.4 MB     00:00
MySQL Connectors Community                      143 kB/s |  45 kB     00:00
MySQL Tools Community                           896 kB/s | 536 kB     00:00
======================== Name & Summary Matched: mysql =========================
mysql.x86_64 : MySQL client programs and shared libraries
MySQL-zrm.noarch : MySQL backup manager
anope-mysql.x86_64 : MariaDB/MySQL modules for Anope IRC services
ansible-collection-community-mysql.noarch : MySQL collection for Ansible
apr-util-mysql.x86_64 : APR utility library MySQL DBD driver
asterisk-mysql.x86_64 : Applications for Asterisk that use MySQL
collectd-mysql.x86_64 : MySQL plugin for collectd
dovecot-mysql.x86_64 : MySQL back end for dovecot
exim-mysql.x86_64 : MySQL lookup support for Exim
gnokii-smsd-mysql.x86_64 : MySQL support for Gnokii SMS daemon
holland-mysql.noarch : MySQL library functionality for Holland Plugins
holland-mysqldump.noarch : Logical mysqldump backup plugin for Holland
holland-mysqllvm.noarch : Holland LVM snapshot backup plugin for MySQL
kdb-driver-mysql.x86_64 : Mysql driver for kdb
kf5-akonadi-server-mysql.x86_64 : Akonadi MySQL backend support
libnss-mysql.x86_64 : NSS library for MySQL
lighttpd-mod_vhostdb_mysql.x86_64 : Virtual host module for lighttpd that uses
                                  : MySQL
mysql-common.x86_64 : The shared files required for MySQL server and client
mysql-community-client.x86_64 : MySQL database client applications and tools
mysql-community-client-plugins.x86_64 : Shared plugins for MySQL client
                                      : applications
mysql-community-common.x86_64 : MySQL database common files for server and
                              : client libs
mysql-community-debugsource.x86_64 : Debug sources for package mysql-community
mysql-community-devel.x86_64 : Development header files and libraries for MySQL
                             : database client applications
mysql-community-icu-data-files.x86_64 : MySQL packaging of ICU data files
mysql-community-libs.x86_64 : Shared libraries for MySQL database client
                            : applications
mysql-community-server-debug.x86_64 : The debug version of MySQL server
mysql-community-test.x86_64 : Test suite for the MySQL database server
mysql-connector-c++.x86_64 : MySQL database connector for C++
mysql-connector-c++-debugsource.x86_64 : Debug sources for package
                                       : mysql-connector-c++
mysql-connector-c++-devel.x86_64 : Development header files and libraries for
                                 : MySQL C++ client applications
mysql-connector-c++-jdbc.x86_64 : MySQL Driver for C++ which mimics the JDBC 4.0
                                : API
mysql-connector-j.noarch : Standardized MySQL database driver for Java
mysql-connector-odbc.x86_64 : An ODBC 8.3 driver for MySQL - driver package
mysql-connector-odbc-debugsource.x86_64 : Debug sources for package
                                        : mysql-connector-odbc
mysql-connector-odbc-setup.x86_64 : An ODBC 8.3 driver for MySQL - setup library
mysql-connector-python3.x86_64 : Standardized MySQL database driver for Python 3
mysql-errmsg.x86_64 : The error messages files required by MySQL server
mysql-ref-manual-8.0-en-html-chapter.noarch : The MySQL Reference Manual (HTML,
                                            : English)
mysql-ref-manual-8.0-en-pdf.noarch : The MySQL Reference Manual (PDF, English)
mysql-router-community.x86_64 : MySQL Router
mysql-selinux.noarch : SELinux policy modules for MySQL and MariaDB packages
mysql-server.x86_64 : The MySQL server and related files
mysql-shell.x86_64 : Command line shell and scripting environment for MySQL
mysql-shell-debugsource.x86_64 : Debug sources for package mysql-shell
mysql-workbench-community.x86_64 : A MySQL visual database modeling,
     ...: administration, development and migration tool
mysql80-community-release.noarch : MySQL repository configuration for yum
mysqltuner.noarch : MySQL configuration assistant
mysqlx-connector-python3.x86_64 : Standardized MySQL database driver for Python
                                : 3
nagios-plugins-mysql.x86_64 : Nagios Plugin - check_mysql
opendbx-mysql.x86_64 : MySQL backend - provides mysql support in opendbx
pcp-pmda-mysql.x86_64 : Performance Co-Pilot (PCP) metrics for MySQL
pdns-backend-mysql.x86_64 : MySQL backend for pdns
perl-DBD-MySQL.x86_64 : A MySQL interface for Perl
perl-DateTime-Format-MySQL.noarch : Parse and format MySQL dates and times
perl-RDF-Trine-mysql.noarch : RDF::Trine store in MySQL
perl-Test-mysqld.noarch : Mysqld runner for tests
perl-Time-Piece-MySQL.noarch : MySQL-specific methods for Time::Piece
php-mysqlnd.x86_64 : A module for PHP applications that use MySQL databases
poco-mysql.x86_64 : The Data/MySQL POCO component
postfix-mysql.x86_64 : Postfix MySQL map support
preludedb-mysql.x86_64 : Plugin to use prelude with a MySQL database
proftpd-mysql.x86_64 : Module to add MySQL support to the ProFTPD FTP server
python-mysqlclient-doc.x86_64 : Documentation for python-mysqlclient
python3-PyMySQL.noarch : Pure-Python MySQL client library
python3-mysqlclient.x86_64 : MySQL/mariaDB database connector for Python
python3.11-PyMySQL.noarch : Pure-Python MySQL client library
python3.11-PyMySQL+rsa.noarch : Metapackage for python3.11-PyMySQL: rsa extras
qt5-qtbase-mysql.x86_64 : MySQL driver for Qt5's SQL classes
qt5-qtbase-mysql.i686 : MySQL driver for Qt5's SQL classes
qt6-qtbase-mysql.x86_64 : MySQL driver for Qt6's SQL classes
root-sql-mysql.x86_64 : MySQL client plugin for ROOT
rsyslog-mysql.x86_64 : MySQL support for rsyslog
rubygem-mysql2.x86_64 : A simple, fast Mysql library for Ruby, binding to
                      : libmysql
soci-mysql.x86_64 : MySQL back-end for soci
soci-mysql-devel.x86_64 : MySQL back-end for soci
voms-mysql-plugin.x86_64 : VOMS server plugin for MySQL
zabbix-proxy-mysql.x86_64 : Zabbix proxy compiled to use MySQL
zabbix-server-mysql.x86_64 : Zabbix server compiled to use MySQL
zabbix-web-mysql.noarch : Zabbix web frontend for MySQL
============================= Name Matched: mysql ==============================
mysql-community-server.x86_64 : A very fast and reliable SQL database server
zabbix-dbfiles-mysql.noarch : Zabbix database schemas, images, data and patches
============================ Summary Matched: mysql ============================
mariadb-java-client.noarch : Connects applications developed in Java to MariaDB
                           : and MySQL databases
mariadb-server-utils.x86_64 : Non-essential server utilities for MariaDB/MySQL
                            : applications
mycli.noarch : Interactive CLI for MySQL Database with auto-completion and
             : syntax highlighting
mytop.noarch : A top clone for MySQL
perl-DBD-MariaDB.x86_64 : MariaDB and MySQL driver for the Perl5 Database
                        : Interface (DBI)
phpMyAdmin.noarch : A web interface for MySQL and MariaDB
proxysql.x86_64 : A high-performance MySQL proxy
python3-asyncmy.x86_64 : A fast asyncio MySQL/MariaDB driver
[vagrant@frontend ~]$ sudo dnf -y install mysql-community-server
Last metadata expiration check: 0:00:49 ago on Tue 09 Apr 2024 09:03:50 AM UTC.
Dependencies resolved.
================================================================================
 Package                         Arch    Version       Repository          Size
================================================================================
Installing:
 mysql-community-server          x86_64  8.0.36-1.el9  mysql80-community   49 M
Installing dependencies:
 mysql-community-client          x86_64  8.0.36-1.el9  mysql80-community  3.4 M
 mysql-community-client-plugins  x86_64  8.0.36-1.el9  mysql80-community  1.4 M
 mysql-community-common          x86_64  8.0.36-1.el9  mysql80-community  556 k
 mysql-community-icu-data-files  x86_64  8.0.36-1.el9  mysql80-community  2.3 M
 mysql-community-libs            x86_64  8.0.36-1.el9  mysql80-community  1.4 M

Transaction Summary
================================================================================
Install  6 Packages

Total download size: 58 M
Installed size: 334 M
Downloading Packages:
(1/6): mysql-community-common-8.0.36-1.el9.x86_ 1.0 MB/s | 556 kB     00:00
(2/6): mysql-community-icu-data-files-8.0.36-1. 5.6 MB/s | 2.3 MB     00:00
(3/6): mysql-community-client-plugins-8.0.36-1. 675 kB/s | 1.4 MB     00:02
(4/6): mysql-community-libs-8.0.36-1.el9.x86_64 850 kB/s | 1.4 MB     00:01
(5/6): mysql-community-client-8.0.36-1.el9.x86_ 1.0 MB/s | 3.4 MB     00:03
(6/6): mysql-community-server-8.0.36-1.el9.x86_  20 MB/s |  49 MB     00:02
--------------------------------------------------------------------------------
Total                                            13 MB/s |  58 MB     00:04
MySQL 8.0 Community Server                      3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0xA8D3785C:
 Userid     : "MySQL Release Engineering <mysql-build@oss.oracle.com>"
 Fingerprint: BCA4 3417 C3B4 85DD 128E C6D4 B7B3 B788 A8D3 785C
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-mysql-2023
Key imported successfully
MySQL 8.0 Community Server                      3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x3A79BD29:
 Userid     : "MySQL Release Engineering <mysql-build@oss.oracle.com>"
 Fingerprint: 859B E8D7 C586 F538 430B 19C2 467B 942D 3A79 BD29
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-mysql-2022
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1
  Installing       : mysql-community-common-8.0.36-1.el9.x86_64             1/6
  Installing       : mysql-community-client-plugins-8.0.36-1.el9.x86_64     2/6
  Installing       : mysql-community-libs-8.0.36-1.el9.x86_64               3/6
  Running scriptlet: mysql-community-libs-8.0.36-1.el9.x86_64               3/6
  Installing       : mysql-community-client-8.0.36-1.el9.x86_64             4/6
  Installing       : mysql-community-icu-data-files-8.0.36-1.el9.x86_64     5/6
  Running scriptlet: mysql-community-server-8.0.36-1.el9.x86_64             6/6
  Installing       : mysql-community-server-8.0.36-1.el9.x86_64             6/6
  Running scriptlet: mysql-community-server-8.0.36-1.el9.x86_64             6/6
  Verifying        : mysql-community-client-8.0.36-1.el9.x86_64             1/6
  Verifying        : mysql-community-client-plugins-8.0.36-1.el9.x86_64     2/6
  Verifying        : mysql-community-common-8.0.36-1.el9.x86_64             3/6
  Verifying        : mysql-community-icu-data-files-8.0.36-1.el9.x86_64     4/6
  Verifying        : mysql-community-libs-8.0.36-1.el9.x86_64               5/6
  Verifying        : mysql-community-server-8.0.36-1.el9.x86_64             6/6

Installed:
  mysql-community-client-8.0.36-1.el9.x86_64
  mysql-community-client-plugins-8.0.36-1.el9.x86_64
  mysql-community-common-8.0.36-1.el9.x86_64
  mysql-community-icu-data-files-8.0.36-1.el9.x86_64
  mysql-community-libs-8.0.36-1.el9.x86_64
  mysql-community-server-8.0.36-1.el9.x86_64

Complete!
[vagrant@frontend ~]$


```


**🌞 Démarrer le serveur MySQL**

```
[vagrant@frontend ~]$ sudo systemctl start mysqld
[vagrant@frontend ~]$ sudo systemctl enable mysqld
[vagrant@frontend ~]$ ^C
[vagrant@frontend ~]$

tomto@MSI MINGW64 /c/cour/Cloud2
$ vagrant ssh frontend.one
Last login: Tue Apr  9 08:55:33 2024 from 10.0.2.2
[vagrant@frontend ~]$ cat /var/lo
local/ lock/  log/
[vagrant@frontend ~]$ cat /var/log/mysqld.log
cat: /var/log/mysqld.log: Permission denied
[vagrant@frontend ~]$ sudo cat /var/log/mysqld.log
2024-04-09T09:06:24.816872Z 0 [System] [MY-013169] [Server] /usr/sbin/mysqld (mysqld 8.0.36) initializing of server in progress as process 4517
2024-04-09T09:06:24.823906Z 1 [System] [MY-013576] [InnoDB] InnoDB initialization has started.
2024-04-09T09:06:26.227595Z 1 [System] [MY-013577] [InnoDB] InnoDB initialization has ended.
2024-04-09T09:06:28.623780Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: wuBpPRkTd2/*
2024-04-09T09:06:35.069306Z 0 [System] [MY-010116] [Server] /usr/sbin/mysqld (mysqld 8.0.36) starting as process 4558
2024-04-09T09:06:35.092506Z 1 [System] [MY-013576] [InnoDB] InnoDB initialization has started.
2024-04-09T09:06:35.538920Z 1 [System] [MY-013577] [InnoDB] InnoDB initialization has ended.
2024-04-09T09:06:35.929855Z 0 [Warning] [MY-010068] [Server] CA certificate ca.pem is self signed.
2024-04-09T09:06:35.929878Z 0 [System] [MY-013602] [Server] Channel mysql_main configured to support TLS. Encrypted connections are now supported for this channel.
2024-04-09T09:06:35.943611Z 0 [System] [MY-011323] [Server] X Plugin ready for connections. Bind-address: '::' port: 33060, socket: /var/run/mysqld/mysqlx.sock
2024-04-09T09:06:35.943616Z 0 [System] [MY-010931] [Server] /usr/sbin/mysqld: ready for connections. Version: '8.0.36'  socket: '/var/lib/mysql/mysql.sock'  port: 3306  MySQL Community Server - GPL.
[vagrant@frontend ~]$ sudo systemctl status mysqld
● mysqld.service - MySQL Server
     Loaded: loaded (/usr/lib/systemd/system/mysqld.service; enabled; preset: d>
     Active: active (running) since Tue 2024-04-09 09:06:35 UTC; 2min 27s ago
       Docs: man:mysqld(8)
             http://dev.mysql.com/doc/refman/en/using-systemd.html
   Main PID: 4558 (mysqld)
     Status: "Server is operational"
      Tasks: 37 (limit: 11112)
     Memory: 454.4M
        CPU: 3.630s
     CGroup: /system.slice/mysqld.service
             └─4558 /usr/sbin/mysqld

Apr 09 09:06:24 frontend.one systemd[1]: Starting MySQL Server...
Apr 09 09:06:35 frontend.one systemd[1]: Started MySQL Server.

[vagrant@frontend ~]$




```



**🌞 Setup MySQL**

```


[vagrant@frontend ~]$ sudo cat /var/log/mysqld.log
2024-04-09T09:06:24.816872Z 0 [System] [MY-013169] [Server] /usr/sbin/mysqld (mysqld 8.0.36) initializing of server in progress as process 4517
2024-04-09T09:06:24.823906Z 1 [System] [MY-013576] [InnoDB] InnoDB initialization has started.
2024-04-09T09:06:26.227595Z 1 [System] [MY-013577] [InnoDB] InnoDB initialization has ended.
2024-04-09T09:06:28.623780Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: wuBpPRkTd2/*
2024-04-09T09:06:35.069306Z 0 [System] [MY-010116] [Server] /usr/sbin/mysqld (mysqld 8.0.36) starting as process 4558
2024-04-09T09:06:35.092506Z 1 [System] [MY-013576] [InnoDB] InnoDB initialization has started.
2024-04-09T09:06:35.538920Z 1 [System] [MY-013577] [InnoDB] InnoDB initialization has ended.
2024-04-09T09:06:35.929855Z 0 [Warning] [MY-010068] [Server] CA certificate ca.pem is self signed.
2024-04-09T09:06:35.929878Z 0 [System] [MY-013602] [Server] Channel mysql_main configured to support TLS. Encrypted connections are now supported for this channel.
2024-04-09T09:06:35.943611Z 0 [System] [MY-011323] [Server] X Plugin ready for connections. Bind-address: '::' port: 33060, socket: /var/run/mysqld/mysqlx.sock
2024-04-09T09:06:35.943616Z 0 [System] [MY-010931] [Server] /usr/sbin/mysqld: ready for connections. Version: '8.0.36'  socket: '/var/lib/mysql/mysql.sock'  port: 3306  MySQL Community Server - GPL.

[vagrant@frontend ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 12
Server version: 8.0.36

Copyright (c) 2000, 2024, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY 'hey_boi_define_a_strong_password';
ERROR 1819 (HY000): Your password does not satisfy the current policy requirements
mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY '130504Tb';
Query OK, 0 rows affected (0.01 sec)

mysql> CREATE USER 'oneadmin' IDENTIFIED BY '130504Tb';
Query OK, 0 rows affected (0.01 sec)

mysql> CREATE DATABASE opennebula;
Query OK, 1 row affected (0.01 sec)

mysql> GRANT ALL PRIVILEGES ON opennebula.* TO 'oneadmin';
Query OK, 0 rows affected (0.01 sec)

mysql> SET GLOBAL TRANSACTION ISOLATION LEVEL READ COMMITTED;
Query OK, 0 rows affected (0.00 sec)

mysql>

```
**🌞 Ajouter les dépôts Open Nebula**


```
[vagrant@frontend ~]$ sudo nano /etc/yum.repos.d/opennebula.repo
[vagrant@frontend ~]$ sudo dnf makecache -y
Extra Packages for Enterprise Linux 9 - x86_64   17 kB/s |  11 kB     00:00
Extra Packages for Enterprise Linux 9 openh264  4.8 kB/s | 993  B     00:00
MySQL 8.0 Community Server                       16 kB/s | 2.6 kB     00:00
MySQL Connectors Community                       24 kB/s | 2.6 kB     00:00
MySQL Tools Community                            24 kB/s | 2.6 kB     00:00
OpenNebula Community Edition                    2.1 kB/s | 833  B     00:00
OpenNebula Community Edition                     21 kB/s | 3.1 kB     00:00
Importing GPG key 0x906DC27C:
 Userid     : "OpenNebula Repository <contact@opennebula.io>"
 Fingerprint: 0B2D 385C 7C93 04B1 1A03 67B9 05A0 5927 906D C27C
 From       : https://downloads.opennebula.io/repo/repo2.key
OpenNebula Community Edition                    830 kB/s | 675 kB     00:00
Rocky Linux 9 - BaseOS                          8.7 kB/s | 4.1 kB     00:00
Rocky Linux 9 - AppStream                        12 kB/s | 4.5 kB     00:00
Rocky Linux 9 - Extras                          9.0 kB/s | 2.9 kB     00:00
Metadata cache created.
[vagrant@frontend ~]$

```

**🌞 Installer OpenNebula**

```
[vagrant@frontend ~]$ sudo dnf -y install opennebula
Last metadata expiration check: 0:01:01 ago on Tue 09 Apr 2024 09:25:02 AM UTC.
Dependencies resolved.
================================================================================
 Package                  Arch   Version                       Repository  Size
================================================================================
Installing:
 opennebula               x86_64 6.8.0-1.el9                   opennebula 3.0 M
Installing dependencies:
 augeas-libs              x86_64 1.13.0-5.el9                  appstream  405 k
 cairo                    x86_64 1.17.4-7.el9                  appstream  659 k
 cups-libs                x86_64 1:2.3.3op2-21.el9             baseos     261 k
 fontconfig               x86_64 2.14.0-2.el9_1                appstream  274 k
 fribidi                  x86_64 1.0.10-6.el9.2                appstream   84 k
 gd                       x86_64 2.3.2-3.el9                   appstream  131 k
 genisoimage              x86_64 1.1.11-48.el9                 epel       324 k
 git                      x86_64 2.39.3-1.el9_2                appstream   61 k
 git-core                 x86_64 2.39.3-1.el9_2                appstream  4.2 M
 git-core-doc             noarch 2.39.3-1.el9_2                appstream  2.6 M
 glx-utils                x86_64 8.4.0-12.20210504git0f9e7d9.el9.0.1
                                                               appstream   40 k
 gnuplot-common           x86_64 5.4.3-2.el9                   epel       776 k
 jbigkit-libs             x86_64 2.1-23.el9                    appstream   52 k
 jq                       x86_64 1.6-15.el9                    appstream  186 k
 keyutils-libs-devel      x86_64 1.6.3-1.el9                   appstream   54 k
 krb5-devel               x86_64 1.21.1-1.el9                  appstream  133 k
 libX11-xcb               x86_64 1.7.0-8.el9                   appstream  9.9 k
 libXfixes                x86_64 5.0.3-16.el9                  appstream   19 k
 libXft                   x86_64 2.3.3-8.el9                   appstream   61 k
 libXpm                   x86_64 3.5.13-8.el9_1                appstream   57 k
 libXrender               x86_64 0.9.10-16.el9                 appstream   27 k
 libXxf86vm               x86_64 1.1.4-18.el9                  appstream   18 k
 libcerf                  x86_64 1.17-2.el9                    epel        38 k
 libcom_err-devel         x86_64 1.46.5-3.el9                  appstream   16 k
 libdrm                   x86_64 2.4.115-1.el9                 appstream  156 k
 libevdev                 x86_64 1.11.0-3.el9                  appstream   45 k
 libglvnd                 x86_64 1:1.3.4-1.el9                 appstream  133 k
 libglvnd-egl             x86_64 1:1.3.4-1.el9                 appstream   36 k
 libglvnd-glx             x86_64 1:1.3.4-1.el9                 appstream  140 k
 libgudev                 x86_64 237-1.el9                     baseos      35 k
 libicu                   x86_64 67.1-9.el9                    baseos     9.6 M
 libinput                 x86_64 1.19.3-4.el9_2                appstream  194 k
 libjpeg-turbo            x86_64 2.0.90-6.el9_1                appstream  175 k
 libkadm5                 x86_64 1.21.1-1.el9                  baseos      77 k
 libpciaccess             x86_64 0.16-6.el9                    baseos      27 k
 libpq                    x86_64 13.11-1.el9                   appstream  201 k
 libproxy                 x86_64 0.4.15-35.el9                 baseos      73 k
 libselinux-devel         x86_64 3.5-1.el9                     appstream  114 k
 libsepol-devel           x86_64 3.5-1.el9                     appstream   40 k
 libsodium                x86_64 1.0.18-8.el9                  epel       161 k
 libsodium-devel          x86_64 1.0.18-8.el9                  epel       1.0 M
 libtiff                  x86_64 4.4.0-10.el9                  appstream  196 k
 libunwind                x86_64 1.6.2-1.el9                   epel        67 k
 libunwind-devel          x86_64 1.6.2-1.el9                   epel        80 k
 liburing                 x86_64 2.3-2.el9                     appstream   26 k
 libusal                  x86_64 1.1.11-48.el9                 epel       137 k
 libverto-devel           x86_64 0.3.2-3.el9                   appstream   14 k
 libwacom                 x86_64 1.12.1-2.el9                  appstream   45 k
 libwacom-data            noarch 1.12.1-2.el9                  appstream  104 k
 libwayland-client        x86_64 1.21.0-1.el9                  appstream   33 k
 libwayland-server        x86_64 1.21.0-1.el9                  appstream   41 k
 libwebp                  x86_64 1.2.0-8.el9                   appstream  276 k
 libxkbcommon             x86_64 1.0.3-4.el9                   appstream  132 k
 libxkbcommon-x11         x86_64 1.0.3-4.el9                   appstream   21 k
 libxshmfence             x86_64 1.3-10.el9                    appstream   12 k
 libxslt                  x86_64 1.1.34-9.el9                  appstream  240 k
 mesa-filesystem          x86_64 23.1.4-1.el9                  appstream  9.9 k
 mesa-libEGL              x86_64 23.1.4-1.el9                  appstream  124 k
 mesa-libGL               x86_64 23.1.4-1.el9                  appstream  165 k
 mesa-libgbm              x86_64 23.1.4-1.el9                  appstream   37 k
 mesa-libglapi            x86_64 23.1.4-1.el9                  appstream   46 k
 mtdev                    x86_64 1.1.5-22.el9                  appstream   21 k
 oniguruma                x86_64 6.9.6-1.el9.5                 appstream  217 k
 opennebula-common        noarch 6.8.0-1.el9                   opennebula  22 k
 opennebula-common-onecfg noarch 6.8.0-1.el9                   opennebula 9.1 k
 opennebula-libs          noarch 6.8.0-1.el9                   opennebula 202 k
 opennebula-rubygems      x86_64 6.8.0-1.el9                   opennebula  16 M
 opennebula-tools         noarch 6.8.0-1.el9                   opennebula 188 k
 openpgm                  x86_64 5.2.122-28.el9                epel       176 k
 openpgm-devel            x86_64 5.2.122-28.el9                epel        58 k
 pango                    x86_64 1.48.7-3.el9                  appstream  297 k
 pcre2-devel              x86_64 10.40-2.el9                   appstream  474 k
 pcre2-utf16              x86_64 10.40-2.el9                   appstream  216 k
 pcre2-utf32              x86_64 10.40-2.el9                   appstream  205 k
 perl-Error               noarch 1:0.17029-7.el9               appstream   41 k
 perl-Git                 noarch 2.39.3-1.el9_2                appstream   37 k
 pixman                   x86_64 0.40.0-6.el9_3                appstream  269 k
 qemu-img                 x86_64 17:8.0.0-16.el9_3.3           appstream  2.4 M
 qt5-qtbase               x86_64 5.15.9-7.el9                  appstream  3.5 M
 qt5-qtbase-common        noarch 5.15.9-7.el9                  appstream  9.0 k
 qt5-qtbase-gui           x86_64 5.15.9-7.el9                  appstream  6.3 M
 qt5-qtsvg                x86_64 5.15.9-2.el9                  appstream  184 k
 ruby                     x86_64 3.0.4-160.el9_0               appstream   41 k
 ruby-libs                x86_64 3.0.4-160.el9_0               appstream  3.2 M
 rubygem-bigdecimal       x86_64 3.0.0-160.el9_0               appstream   54 k
 rubygem-io-console       x86_64 0.5.7-160.el9_0               appstream   25 k
 rubygem-json             x86_64 2.5.1-160.el9_0               appstream   54 k
 rubygem-psych            x86_64 3.3.2-160.el9_0               appstream   51 k
 rubygem-rexml            noarch 3.2.5-160.el9_0               appstream   95 k
 rubygems                 noarch 3.2.33-160.el9_0              appstream  256 k
 sqlite                   x86_64 3.34.1-7.el9_3                appstream  747 k
 xcb-util                 x86_64 0.4.0-19.el9                  appstream   18 k
 xcb-util-image           x86_64 0.4.0-19.el9                  appstream   19 k
 xcb-util-keysyms         x86_64 0.4.0-17.el9                  appstream   14 k
 xcb-util-renderutil      x86_64 0.3.9-20.el9                  appstream   17 k
 xcb-util-wm              x86_64 0.4.1-22.el9                  appstream   31 k
 xkeyboard-config         noarch 2.33-2.el9                    appstream  779 k
 xml-common               noarch 0.6.3-58.el9                  appstream   31 k
 xmlrpc-c                 x86_64 1.51.0-16.el9                 appstream  140 k
 zeromq                   x86_64 4.3.4-2.el9                   epel       431 k
 zeromq-devel             x86_64 4.3.4-2.el9                   epel        16 k
Installing weak dependencies:
 gnuplot                  x86_64 5.4.3-2.el9                   epel       820 k
 mesa-dri-drivers         x86_64 23.1.4-1.el9                  appstream   10 M
 ruby-default-gems        noarch 3.0.4-160.el9_0               appstream   32 k
 rubygem-bundler          noarch 2.2.33-160.el9_0              appstream  372 k
 rubygem-rdoc             noarch 6.3.3-160.el9_0               appstream  400 k

Transaction Summary
================================================================================
Install  107 Packages

Total download size: 76 M
Installed size: 415 M
Downloading Packages:
(1/107): gnuplot-5.4.3-2.el9.x86_64.rpm         2.2 MB/s | 820 kB     00:00
(2/107): gnuplot-common-5.4.3-2.el9.x86_64.rpm  2.1 MB/s | 776 kB     00:00
(3/107): libcerf-1.17-2.el9.x86_64.rpm          1.1 MB/s |  38 kB     00:00
(4/107): libsodium-1.0.18-8.el9.x86_64.rpm      2.6 MB/s | 161 kB     00:00
(5/107): libunwind-1.6.2-1.el9.x86_64.rpm       1.3 MB/s |  67 kB     00:00
(6/107): libsodium-devel-1.0.18-8.el9.x86_64.rp  11 MB/s | 1.0 MB     00:00
(7/107): libunwind-devel-1.6.2-1.el9.x86_64.rpm 1.1 MB/s |  80 kB     00:00
(8/107): libusal-1.1.11-48.el9.x86_64.rpm       1.8 MB/s | 137 kB     00:00
(9/107): genisoimage-1.1.11-48.el9.x86_64.rpm   544 kB/s | 324 kB     00:00
(10/107): openpgm-devel-5.2.122-28.el9.x86_64.r 973 kB/s |  58 kB     00:00
(11/107): openpgm-5.2.122-28.el9.x86_64.rpm     2.0 MB/s | 176 kB     00:00
(12/107): zeromq-devel-4.3.4-2.el9.x86_64.rpm   396 kB/s |  16 kB     00:00
(13/107): zeromq-4.3.4-2.el9.x86_64.rpm         3.1 MB/s | 431 kB     00:00
(14/107): opennebula-common-6.8.0-1.el9.noarch. 139 kB/s |  22 kB     00:00
(15/107): opennebula-common-onecfg-6.8.0-1.el9.  70 kB/s | 9.1 kB     00:00
(16/107): opennebula-libs-6.8.0-1.el9.noarch.rp 1.6 MB/s | 202 kB     00:00
(17/107): opennebula-tools-6.8.0-1.el9.noarch.r 1.7 MB/s | 188 kB     00:00
(18/107): opennebula-6.8.0-1.el9.x86_64.rpm     6.2 MB/s | 3.0 MB     00:00
(19/107): libpciaccess-0.16-6.el9.x86_64.rpm     36 kB/s |  27 kB     00:00
(20/107): libgudev-237-1.el9.x86_64.rpm          51 kB/s |  35 kB     00:00
(21/107): opennebula-rubygems-6.8.0-1.el9.x86_6 9.8 MB/s |  16 MB     00:01
(22/107): cups-libs-2.3.3op2-21.el9.x86_64.rpm  356 kB/s | 261 kB     00:00
(23/107): libproxy-0.4.15-35.el9.x86_64.rpm     144 kB/s |  73 kB     00:00
(24/107): libkadm5-1.21.1-1.el9.x86_64.rpm      132 kB/s |  77 kB     00:00
[MIRROR] gd-2.3.2-3.el9.x86_64.rpm: Status code: 403 for http://rocky.quelquesmots.fr/9.3/AppStream/x86_64/os/Packages/g/gd-2.3.2-3.el9.x86_64.rpm (IP: 137.74.202.137)
[MIRROR] augeas-libs-1.13.0-5.el9.x86_64.rpm: Status code: 403 for http://rocky.quelquesmots.fr/9.3/AppStream/x86_64/os/Packages/a/augeas-libs-1.13.0-5.el9.x86_64.rpm (IP: 137.74.202.137)
(25/107): gd-2.3.2-3.el9.x86_64.rpm             151 kB/s | 131 kB     00:00
(26/107): augeas-libs-1.13.0-5.el9.x86_64.rpm   432 kB/s | 405 kB     00:00
[MIRROR] fribidi-1.0.10-6.el9.2.x86_64.rpm: Status code: 403 for http://rocky.quelquesmots.fr/9.3/AppStream/x86_64/os/Packages/f/fribidi-1.0.10-6.el9.2.x86_64.rpm (IP: 137.74.202.137)
[MIRROR] fontconfig-2.14.0-2.el9_1.x86_64.rpm: Status code: 403 for http://rocky.quelquesmots.fr/9.3/AppStream/x86_64/os/Packages/f/fontconfig-2.14.0-2.el9_1.x86_64.rpm (IP: 137.74.202.137)
(27/107): fribidi-1.0.10-6.el9.2.x86_64.rpm     141 kB/s |  84 kB     00:00
(28/107): fontconfig-2.14.0-2.el9_1.x86_64.rpm  399 kB/s | 274 kB     00:00
(29/107): libxkbcommon-x11-1.0.3-4.el9.x86_64.r  42 kB/s |  21 kB     00:00
(30/107): libicu-67.1-9.el9.x86_64.rpm          2.8 MB/s | 9.6 MB     00:03
(31/107): libxkbcommon-1.0.3-4.el9.x86_64.rpm   252 kB/s | 132 kB     00:00
(32/107): mtdev-1.1.5-22.el9.x86_64.rpm          41 kB/s |  21 kB     00:00
(33/107): libxshmfence-1.3-10.el9.x86_64.rpm     24 kB/s |  12 kB     00:00
(34/107): libpq-13.11-1.el9.x86_64.rpm          408 kB/s | 201 kB     00:00
(35/107): libverto-devel-0.3.2-3.el9.x86_64.rpm 142 kB/s |  14 kB     00:00
(36/107): perl-Error-0.17029-7.el9.noarch.rpm   355 kB/s |  41 kB     00:00
(37/107): libXpm-3.5.13-8.el9_1.x86_64.rpm      117 kB/s |  57 kB     00:00
(38/107): libXfixes-5.0.3-16.el9.x86_64.rpm      33 kB/s |  19 kB     00:00
(39/107): pcre2-utf32-10.40-2.el9.x86_64.rpm    336 kB/s | 205 kB     00:00
(40/107): pcre2-devel-10.40-2.el9.x86_64.rpm    1.4 MB/s | 474 kB     00:00
(41/107): xkeyboard-config-2.33-2.el9.noarch.rp 2.9 MB/s | 779 kB     00:00
(42/107): pcre2-utf16-10.40-2.el9.x86_64.rpm    241 kB/s | 216 kB     00:00
(43/107): xcb-util-keysyms-0.4.0-17.el9.x86_64.  27 kB/s |  14 kB     00:00
(44/107): xcb-util-0.4.0-19.el9.x86_64.rpm       38 kB/s |  18 kB     00:00
(45/107): xcb-util-wm-0.4.1-22.el9.x86_64.rpm    65 kB/s |  31 kB     00:00
(46/107): xcb-util-renderutil-0.3.9-20.el9.x86_  35 kB/s |  17 kB     00:00
(47/107): sqlite-3.34.1-7.el9_3.x86_64.rpm      1.1 MB/s | 747 kB     00:00
(48/107): xcb-util-image-0.4.0-19.el9.x86_64.rp  33 kB/s |  19 kB     00:00
(49/107): xmlrpc-c-1.51.0-16.el9.x86_64.rpm     232 kB/s | 140 kB     00:00
(50/107): rubygem-rexml-3.2.5-160.el9_0.noarch. 690 kB/s |  95 kB     00:00
(51/107): rubygems-3.2.33-160.el9_0.noarch.rpm  1.7 MB/s | 256 kB     00:00
(52/107): ruby-default-gems-3.0.4-160.el9_0.noa 265 kB/s |  32 kB     00:00
(53/107): rubygem-bundler-2.2.33-160.el9_0.noar 1.5 MB/s | 372 kB     00:00
(54/107): rubygem-rdoc-6.3.3-160.el9_0.noarch.r 1.3 MB/s | 400 kB     00:00
(55/107): libevdev-1.11.0-3.el9.x86_64.rpm       92 kB/s |  45 kB     00:00
(56/107): mesa-libglapi-23.1.4-1.el9.x86_64.rpm  93 kB/s |  46 kB     00:00
(57/107): mesa-libgbm-23.1.4-1.el9.x86_64.rpm    71 kB/s |  37 kB     00:00
(58/107): mesa-filesystem-23.1.4-1.el9.x86_64.r  65 kB/s | 9.9 kB     00:00
(59/107): mesa-libGL-23.1.4-1.el9.x86_64.rpm    313 kB/s | 165 kB     00:00
(60/107): mesa-libEGL-23.1.4-1.el9.x86_64.rpm   223 kB/s | 124 kB     00:00
(61/107): libXrender-0.9.10-16.el9.x86_64.rpm    45 kB/s |  27 kB     00:00
(62/107): libdrm-2.4.115-1.el9.x86_64.rpm       261 kB/s | 156 kB     00:00
(63/107): mesa-dri-drivers-23.1.4-1.el9.x86_64. 8.8 MB/s |  10 MB     00:01
(64/107): jq-1.6-15.el9.x86_64.rpm              315 kB/s | 186 kB     00:00
(65/107): libjpeg-turbo-2.0.90-6.el9_1.x86_64.r 338 kB/s | 175 kB     00:00
(66/107): libsepol-devel-3.5-1.el9.x86_64.rpm   352 kB/s |  40 kB     00:00
(67/107): libXft-2.3.3-8.el9.x86_64.rpm         127 kB/s |  61 kB     00:00
(68/107): jbigkit-libs-2.1-23.el9.x86_64.rpm    102 kB/s |  52 kB     00:00
(69/107): liburing-2.3-2.el9.x86_64.rpm          51 kB/s |  26 kB     00:00
(70/107): libwacom-data-1.12.1-2.el9.noarch.rpm 842 kB/s | 104 kB     00:00
(71/107): libcom_err-devel-1.46.5-3.el9.x86_64. 169 kB/s |  16 kB     00:00
(72/107): libwacom-1.12.1-2.el9.x86_64.rpm       89 kB/s |  45 kB     00:00
(73/107): libtiff-4.4.0-10.el9.x86_64.rpm       321 kB/s | 196 kB     00:00
(74/107): libxslt-1.1.34-9.el9.x86_64.rpm       389 kB/s | 240 kB     00:00
(75/107): glx-utils-8.4.0-12.20210504git0f9e7d9  68 kB/s |  40 kB     00:00
(76/107): libglvnd-glx-1.3.4-1.el9.x86_64.rpm   261 kB/s | 140 kB     00:00
(77/107): libglvnd-egl-1.3.4-1.el9.x86_64.rpm    67 kB/s |  36 kB     00:00
(78/107): libglvnd-1.3.4-1.el9.x86_64.rpm       254 kB/s | 133 kB     00:00
(79/107): libselinux-devel-3.5-1.el9.x86_64.rpm 1.3 MB/s | 114 kB     00:00
(80/107): libXxf86vm-1.1.4-18.el9.x86_64.rpm     34 kB/s |  18 kB     00:00
(81/107): libwebp-1.2.0-8.el9.x86_64.rpm        452 kB/s | 276 kB     00:00
(82/107): qt5-qtsvg-5.15.9-2.el9.x86_64.rpm     288 kB/s | 184 kB     00:00
(83/107): qt5-qtbase-common-5.15.9-7.el9.noarch  60 kB/s | 9.0 kB     00:00
(84/107): oniguruma-6.9.6-1.el9.5.x86_64.rpm    339 kB/s | 217 kB     00:00
(85/107): qt5-qtbase-5.15.9-7.el9.x86_64.rpm    2.5 MB/s | 3.5 MB     00:01
(86/107): libwayland-server-1.21.0-1.el9.x86_64  74 kB/s |  41 kB     00:00
(87/107): libwayland-client-1.21.0-1.el9.x86_64  66 kB/s |  33 kB     00:00
(88/107): qt5-qtbase-gui-5.15.9-7.el9.x86_64.rp 3.1 MB/s | 6.3 MB     00:02
(89/107): xml-common-0.6.3-58.el9.noarch.rpm    272 kB/s |  31 kB     00:00
(90/107): pixman-0.40.0-6.el9_3.x86_64.rpm      425 kB/s | 269 kB     00:00
(91/107): perl-Git-2.39.3-1.el9_2.noarch.rpm    227 kB/s |  37 kB     00:00
(92/107): krb5-devel-1.21.1-1.el9.x86_64.rpm    1.2 MB/s | 133 kB     00:00
(93/107): libinput-1.19.3-4.el9_2.x86_64.rpm    332 kB/s | 194 kB     00:00
(94/107): pango-1.48.7-3.el9.x86_64.rpm         555 kB/s | 297 kB     00:00
(95/107): git-core-doc-2.39.3-1.el9_2.noarch.rp 1.9 MB/s | 2.6 MB     00:01
(96/107): rubygem-psych-3.3.2-160.el9_0.x86_64.  86 kB/s |  51 kB     00:00
(97/107): qemu-img-8.0.0-16.el9_3.3.x86_64.rpm  1.2 MB/s | 2.4 MB     00:02
(98/107): rubygem-json-2.5.1-160.el9_0.x86_64.r  58 kB/s |  54 kB     00:00
(99/107): rubygem-io-console-0.5.7-160.el9_0.x8  27 kB/s |  25 kB     00:00
(100/107): rubygem-bigdecimal-3.0.0-160.el9_0.x  56 kB/s |  54 kB     00:00
(101/107): ruby-3.0.4-160.el9_0.x86_64.rpm       42 kB/s |  41 kB     00:00
(102/107): keyutils-libs-devel-1.6.3-1.el9.x86_ 496 kB/s |  54 kB     00:00
(103/107): cairo-1.17.4-7.el9.x86_64.rpm        1.1 MB/s | 659 kB     00:00
(104/107): ruby-libs-3.0.4-160.el9_0.x86_64.rpm 1.9 MB/s | 3.2 MB     00:01
(105/107): libX11-xcb-1.7.0-8.el9.x86_64.rpm     20 kB/s | 9.9 kB     00:00
(106/107): git-2.39.3-1.el9_2.x86_64.rpm        416 kB/s |  61 kB     00:00
(107/107): git-core-2.39.3-1.el9_2.x86_64.rpm   3.5 MB/s | 4.2 MB     00:01
--------------------------------------------------------------------------------
Total                                           3.6 MB/s |  76 MB     00:21
OpenNebula Community Edition                     20 kB/s | 3.1 kB     00:00
Importing GPG key 0x906DC27C:
 Userid     : "OpenNebula Repository <contact@opennebula.io>"
 Fingerprint: 0B2D 385C 7C93 04B1 1A03 67B9 05A0 5927 906D C27C
 From       : https://downloads.opennebula.io/repo/repo2.key
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1
  Installing       : ruby-libs-3.0.4-160.el9_0.x86_64                     1/107
  Installing       : rubygem-psych-3.3.2-160.el9_0.x86_64                 2/107
  Installing       : rubygem-json-2.5.1-160.el9_0.x86_64                  3/107
  Installing       : rubygem-io-console-0.5.7-160.el9_0.x86_64            4/107
  Installing       : rubygem-bigdecimal-3.0.0-160.el9_0.x86_64            5/107
  Installing       : rubygem-rdoc-6.3.3-160.el9_0.noarch                  6/107
  Installing       : rubygem-bundler-2.2.33-160.el9_0.noarch              7/107
  Installing       : ruby-default-gems-3.0.4-160.el9_0.noarch             8/107
  Installing       : ruby-3.0.4-160.el9_0.x86_64                          9/107
  Installing       : rubygems-3.2.33-160.el9_0.noarch                    10/107
  Installing       : libX11-xcb-1.7.0-8.el9.x86_64                       11/107
  Installing       : libjpeg-turbo-2.0.90-6.el9_1.x86_64                 12/107
  Installing       : libXrender-0.9.10-16.el9.x86_64                     13/107
  Installing       : mesa-libglapi-23.1.4-1.el9.x86_64                   14/107
  Installing       : libxshmfence-1.3-10.el9.x86_64                      15/107
  Running scriptlet: opennebula-common-onecfg-6.8.0-1.el9.noarch         16/107
  Installing       : opennebula-common-onecfg-6.8.0-1.el9.noarch         16/107
  Installing       : git-core-2.39.3-1.el9_2.x86_64                      17/107
  Installing       : libwayland-server-1.21.0-1.el9.x86_64               18/107
  Installing       : libwebp-1.2.0-8.el9.x86_64                          19/107
  Installing       : libglvnd-1:1.3.4-1.el9.x86_64                       20/107
  Installing       : pcre2-utf16-10.40-2.el9.x86_64                      21/107
  Installing       : libpq-13.11-1.el9.x86_64                            22/107
  Installing       : augeas-libs-1.13.0-5.el9.x86_64                     23/107
  Installing       : openpgm-5.2.122-28.el9.x86_64                       24/107
  Installing       : libunwind-1.6.2-1.el9.x86_64                        25/107
  Installing       : libsodium-1.0.18-8.el9.x86_64                       26/107
  Installing       : zeromq-4.3.4-2.el9.x86_64                           27/107
  Installing       : libsodium-devel-1.0.18-8.el9.x86_64                 28/107
  Installing       : libunwind-devel-1.6.2-1.el9.x86_64                  29/107
  Installing       : openpgm-devel-5.2.122-28.el9.x86_64                 30/107
  Installing       : git-core-doc-2.39.3-1.el9_2.noarch                  31/107
  Installing       : rubygem-rexml-3.2.5-160.el9_0.noarch                32/107
  Installing       : keyutils-libs-devel-1.6.3-1.el9.x86_64              33/107
  Running scriptlet: xml-common-0.6.3-58.el9.noarch                      34/107
  Installing       : xml-common-0.6.3-58.el9.noarch                      34/107
  Installing       : fontconfig-2.14.0-2.el9_1.x86_64                    35/107
  Running scriptlet: fontconfig-2.14.0-2.el9_1.x86_64                    35/107
  Installing       : libXft-2.3.3-8.el9.x86_64                           36/107
  Installing       : pixman-0.40.0-6.el9_3.x86_64                        37/107
  Installing       : cairo-1.17.4-7.el9.x86_64                           38/107
  Installing       : libwayland-client-1.21.0-1.el9.x86_64               39/107
  Installing       : oniguruma-6.9.6-1.el9.5.x86_64                      40/107
  Installing       : jq-1.6-15.el9.x86_64                                41/107
  Running scriptlet: opennebula-common-6.8.0-1.el9.noarch                42/107
  Installing       : opennebula-common-6.8.0-1.el9.noarch                42/107
  Running scriptlet: opennebula-common-6.8.0-1.el9.noarch                42/107
  Installing       : libXxf86vm-1.1.4-18.el9.x86_64                      43/107
  Installing       : libxslt-1.1.34-9.el9.x86_64                         44/107
  Installing       : opennebula-rubygems-6.8.0-1.el9.x86_64              45/107
  Running scriptlet: opennebula-rubygems-6.8.0-1.el9.x86_64              45/107
  Installing       : libcom_err-devel-1.46.5-3.el9.x86_64                46/107
  Installing       : libwacom-data-1.12.1-2.el9.noarch                   47/107
  Installing       : liburing-2.3-2.el9.x86_64                           48/107
  Installing       : qemu-img-17:8.0.0-16.el9_3.3.x86_64                 49/107
  Installing       : jbigkit-libs-2.1-23.el9.x86_64                      50/107
  Installing       : libtiff-4.4.0-10.el9.x86_64                         51/107
  Installing       : libsepol-devel-3.5-1.el9.x86_64                     52/107
  Installing       : mesa-filesystem-23.1.4-1.el9.x86_64                 53/107
  Installing       : libevdev-1.11.0-3.el9.x86_64                        54/107
  Installing       : xmlrpc-c-1.51.0-16.el9.x86_64                       55/107
  Installing       : sqlite-3.34.1-7.el9_3.x86_64                        56/107
  Installing       : xcb-util-renderutil-0.3.9-20.el9.x86_64             57/107
  Installing       : xcb-util-wm-0.4.1-22.el9.x86_64                     58/107
  Installing       : xcb-util-0.4.0-19.el9.x86_64                        59/107
  Installing       : xcb-util-image-0.4.0-19.el9.x86_64                  60/107
  Installing       : xkeyboard-config-2.33-2.el9.noarch                  61/107
  Installing       : libxkbcommon-1.0.3-4.el9.x86_64                     62/107
  Installing       : libxkbcommon-x11-1.0.3-4.el9.x86_64                 63/107
  Installing       : xcb-util-keysyms-0.4.0-17.el9.x86_64                64/107
  Installing       : pcre2-utf32-10.40-2.el9.x86_64                      65/107
  Installing       : pcre2-devel-10.40-2.el9.x86_64                      66/107
  Installing       : libselinux-devel-3.5-1.el9.x86_64                   67/107
  Installing       : perl-Error-1:0.17029-7.el9.noarch                   68/107
  Installing       : git-2.39.3-1.el9_2.x86_64                           69/107
  Installing       : perl-Git-2.39.3-1.el9_2.noarch                      70/107
  Installing       : libverto-devel-0.3.2-3.el9.x86_64                   71/107
  Installing       : libXfixes-5.0.3-16.el9.x86_64                       72/107
  Installing       : libXpm-3.5.13-8.el9_1.x86_64                        73/107
  Installing       : gd-2.3.2-3.el9.x86_64                               74/107
  Installing       : mtdev-1.1.5-22.el9.x86_64                           75/107
  Installing       : fribidi-1.0.10-6.el9.2.x86_64                       76/107
  Installing       : pango-1.48.7-3.el9.x86_64                           77/107
  Installing       : libproxy-0.4.15-35.el9.x86_64                       78/107
  Installing       : libkadm5-1.21.1-1.el9.x86_64                        79/107
  Installing       : krb5-devel-1.21.1-1.el9.x86_64                      80/107
  Installing       : zeromq-devel-4.3.4-2.el9.x86_64                     81/107
  Installing       : opennebula-libs-6.8.0-1.el9.noarch                  82/107
  Running scriptlet: opennebula-libs-6.8.0-1.el9.noarch                  82/107
  Installing       : libicu-67.1-9.el9.x86_64                            83/107
  Installing       : qt5-qtbase-common-5.15.9-7.el9.noarch               84/107
  Running scriptlet: qt5-qtbase-5.15.9-7.el9.x86_64                      85/107
  Installing       : qt5-qtbase-5.15.9-7.el9.x86_64                      85/107
  Running scriptlet: qt5-qtbase-5.15.9-7.el9.x86_64                      85/107
  Installing       : cups-libs-1:2.3.3op2-21.el9.x86_64                  86/107
  Installing       : libgudev-237-1.el9.x86_64                           87/107
  Installing       : libwacom-1.12.1-2.el9.x86_64                        88/107
  Installing       : libinput-1.19.3-4.el9_2.x86_64                      89/107
  Running scriptlet: libinput-1.19.3-4.el9_2.x86_64                      89/107
  Installing       : libpciaccess-0.16-6.el9.x86_64                      90/107
  Installing       : libdrm-2.4.115-1.el9.x86_64                         91/107
  Installing       : mesa-libgbm-23.1.4-1.el9.x86_64                     92/107
  Installing       : libglvnd-egl-1:1.3.4-1.el9.x86_64                   93/107
  Installing       : mesa-libEGL-23.1.4-1.el9.x86_64                     94/107
  Installing       : libglvnd-glx-1:1.3.4-1.el9.x86_64                   95/107
  Installing       : mesa-libGL-23.1.4-1.el9.x86_64                      96/107
  Installing       : glx-utils-8.4.0-12.20210504git0f9e7d9.el9.0.1.x8    97/107
  Installing       : mesa-dri-drivers-23.1.4-1.el9.x86_64                98/107
  Installing       : qt5-qtbase-gui-5.15.9-7.el9.x86_64                  99/107
  Installing       : qt5-qtsvg-5.15.9-2.el9.x86_64                      100/107
  Installing       : libusal-1.1.11-48.el9.x86_64                       101/107
  Installing       : genisoimage-1.1.11-48.el9.x86_64                   102/107
  Running scriptlet: genisoimage-1.1.11-48.el9.x86_64                   102/107
  Installing       : libcerf-1.17-2.el9.x86_64                          103/107
  Installing       : gnuplot-common-5.4.3-2.el9.x86_64                  104/107
  Installing       : gnuplot-5.4.3-2.el9.x86_64                         105/107
  Installing       : opennebula-tools-6.8.0-1.el9.noarch                106/107
  Running scriptlet: opennebula-6.8.0-1.el9.x86_64                      107/107
  Installing       : opennebula-6.8.0-1.el9.x86_64                      107/107
  Running scriptlet: opennebula-6.8.0-1.el9.x86_64                      107/107
Generating public/private rsa key pair.
Your identification has been saved in /var/lib/one/.ssh/id_rsa
Your public key has been saved in /var/lib/one/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:uWZONu6281GTsJGUHlH/gXSgqK5e3x72UQcfapCBkwY oneadmin@frontend.one
The key's randomart image is:
+---[RSA 3072]----+
|       E. ==.o.. |
|         *+.=.o  |
|        .o== .oo |
|        ...+...+o|
|       .S . +o .+|
|      .  . .... .|
|       oB .o .   |
|      oB+.o.o .  |
|    .o o==oo .   |
+----[SHA256]-----+

  Running scriptlet: fontconfig-2.14.0-2.el9_1.x86_64                   107/107
  Running scriptlet: gnuplot-5.4.3-2.el9.x86_64                         107/107
  Running scriptlet: opennebula-6.8.0-1.el9.x86_64                      107/107
  Verifying        : genisoimage-1.1.11-48.el9.x86_64                     1/107
  Verifying        : gnuplot-5.4.3-2.el9.x86_64                           2/107
  Verifying        : gnuplot-common-5.4.3-2.el9.x86_64                    3/107
  Verifying        : libcerf-1.17-2.el9.x86_64                            4/107
  Verifying        : libsodium-1.0.18-8.el9.x86_64                        5/107
  Verifying        : libsodium-devel-1.0.18-8.el9.x86_64                  6/107
  Verifying        : libunwind-1.6.2-1.el9.x86_64                         7/107
  Verifying        : libunwind-devel-1.6.2-1.el9.x86_64                   8/107
  Verifying        : libusal-1.1.11-48.el9.x86_64                         9/107
  Verifying        : openpgm-5.2.122-28.el9.x86_64                       10/107
  Verifying        : openpgm-devel-5.2.122-28.el9.x86_64                 11/107
  Verifying        : zeromq-4.3.4-2.el9.x86_64                           12/107
  Verifying        : zeromq-devel-4.3.4-2.el9.x86_64                     13/107
  Verifying        : opennebula-6.8.0-1.el9.x86_64                       14/107
  Verifying        : opennebula-common-6.8.0-1.el9.noarch                15/107
  Verifying        : opennebula-common-onecfg-6.8.0-1.el9.noarch         16/107
  Verifying        : opennebula-libs-6.8.0-1.el9.noarch                  17/107
  Verifying        : opennebula-rubygems-6.8.0-1.el9.x86_64              18/107
  Verifying        : opennebula-tools-6.8.0-1.el9.noarch                 19/107
  Verifying        : libpciaccess-0.16-6.el9.x86_64                      20/107
  Verifying        : libgudev-237-1.el9.x86_64                           21/107
  Verifying        : cups-libs-1:2.3.3op2-21.el9.x86_64                  22/107
  Verifying        : libicu-67.1-9.el9.x86_64                            23/107
  Verifying        : libkadm5-1.21.1-1.el9.x86_64                        24/107
  Verifying        : libproxy-0.4.15-35.el9.x86_64                       25/107
  Verifying        : augeas-libs-1.13.0-5.el9.x86_64                     26/107
  Verifying        : gd-2.3.2-3.el9.x86_64                               27/107
  Verifying        : fribidi-1.0.10-6.el9.2.x86_64                       28/107
  Verifying        : fontconfig-2.14.0-2.el9_1.x86_64                    29/107
  Verifying        : libxkbcommon-x11-1.0.3-4.el9.x86_64                 30/107
  Verifying        : libxkbcommon-1.0.3-4.el9.x86_64                     31/107
  Verifying        : mtdev-1.1.5-22.el9.x86_64                           32/107
  Verifying        : libxshmfence-1.3-10.el9.x86_64                      33/107
  Verifying        : libpq-13.11-1.el9.x86_64                            34/107
  Verifying        : libXpm-3.5.13-8.el9_1.x86_64                        35/107
  Verifying        : libXfixes-5.0.3-16.el9.x86_64                       36/107
  Verifying        : libverto-devel-0.3.2-3.el9.x86_64                   37/107
  Verifying        : perl-Error-1:0.17029-7.el9.noarch                   38/107
  Verifying        : pcre2-utf32-10.40-2.el9.x86_64                      39/107
  Verifying        : pcre2-utf16-10.40-2.el9.x86_64                      40/107
  Verifying        : pcre2-devel-10.40-2.el9.x86_64                      41/107
  Verifying        : xcb-util-keysyms-0.4.0-17.el9.x86_64                42/107
  Verifying        : xkeyboard-config-2.33-2.el9.noarch                  43/107
  Verifying        : xcb-util-0.4.0-19.el9.x86_64                        44/107
  Verifying        : xcb-util-wm-0.4.1-22.el9.x86_64                     45/107
  Verifying        : xcb-util-renderutil-0.3.9-20.el9.x86_64             46/107
  Verifying        : sqlite-3.34.1-7.el9_3.x86_64                        47/107
  Verifying        : xcb-util-image-0.4.0-19.el9.x86_64                  48/107
  Verifying        : xmlrpc-c-1.51.0-16.el9.x86_64                       49/107
  Verifying        : rubygems-3.2.33-160.el9_0.noarch                    50/107
  Verifying        : rubygem-rexml-3.2.5-160.el9_0.noarch                51/107
  Verifying        : rubygem-rdoc-6.3.3-160.el9_0.noarch                 52/107
  Verifying        : rubygem-bundler-2.2.33-160.el9_0.noarch             53/107
  Verifying        : ruby-default-gems-3.0.4-160.el9_0.noarch            54/107
  Verifying        : libevdev-1.11.0-3.el9.x86_64                        55/107
  Verifying        : mesa-libglapi-23.1.4-1.el9.x86_64                   56/107
  Verifying        : mesa-libgbm-23.1.4-1.el9.x86_64                     57/107
  Verifying        : mesa-libGL-23.1.4-1.el9.x86_64                      58/107
  Verifying        : mesa-libEGL-23.1.4-1.el9.x86_64                     59/107
  Verifying        : mesa-filesystem-23.1.4-1.el9.x86_64                 60/107
  Verifying        : mesa-dri-drivers-23.1.4-1.el9.x86_64                61/107
  Verifying        : libXrender-0.9.10-16.el9.x86_64                     62/107
  Verifying        : libdrm-2.4.115-1.el9.x86_64                         63/107
  Verifying        : jq-1.6-15.el9.x86_64                                64/107
  Verifying        : libjpeg-turbo-2.0.90-6.el9_1.x86_64                 65/107
  Verifying        : libXft-2.3.3-8.el9.x86_64                           66/107
  Verifying        : libsepol-devel-3.5-1.el9.x86_64                     67/107
  Verifying        : jbigkit-libs-2.1-23.el9.x86_64                      68/107
  Verifying        : liburing-2.3-2.el9.x86_64                           69/107
  Verifying        : libwacom-1.12.1-2.el9.x86_64                        70/107
  Verifying        : libwacom-data-1.12.1-2.el9.noarch                   71/107
  Verifying        : libcom_err-devel-1.46.5-3.el9.x86_64                72/107
  Verifying        : libtiff-4.4.0-10.el9.x86_64                         73/107
  Verifying        : libxslt-1.1.34-9.el9.x86_64                         74/107
  Verifying        : glx-utils-8.4.0-12.20210504git0f9e7d9.el9.0.1.x8    75/107
  Verifying        : libglvnd-glx-1:1.3.4-1.el9.x86_64                   76/107
  Verifying        : libglvnd-egl-1:1.3.4-1.el9.x86_64                   77/107
  Verifying        : libglvnd-1:1.3.4-1.el9.x86_64                       78/107
  Verifying        : libselinux-devel-3.5-1.el9.x86_64                   79/107
  Verifying        : libwebp-1.2.0-8.el9.x86_64                          80/107
  Verifying        : libXxf86vm-1.1.4-18.el9.x86_64                      81/107
  Verifying        : qt5-qtsvg-5.15.9-2.el9.x86_64                       82/107
  Verifying        : qt5-qtbase-gui-5.15.9-7.el9.x86_64                  83/107
  Verifying        : qt5-qtbase-5.15.9-7.el9.x86_64                      84/107
  Verifying        : qt5-qtbase-common-5.15.9-7.el9.noarch               85/107
  Verifying        : oniguruma-6.9.6-1.el9.5.x86_64                      86/107
  Verifying        : libwayland-server-1.21.0-1.el9.x86_64               87/107
  Verifying        : libwayland-client-1.21.0-1.el9.x86_64               88/107
  Verifying        : pixman-0.40.0-6.el9_3.x86_64                        89/107
  Verifying        : xml-common-0.6.3-58.el9.noarch                      90/107
  Verifying        : libinput-1.19.3-4.el9_2.x86_64                      91/107
  Verifying        : perl-Git-2.39.3-1.el9_2.noarch                      92/107
  Verifying        : git-core-doc-2.39.3-1.el9_2.noarch                  93/107
  Verifying        : krb5-devel-1.21.1-1.el9.x86_64                      94/107
  Verifying        : qemu-img-17:8.0.0-16.el9_3.3.x86_64                 95/107
  Verifying        : pango-1.48.7-3.el9.x86_64                           96/107
  Verifying        : rubygem-psych-3.3.2-160.el9_0.x86_64                97/107
  Verifying        : rubygem-json-2.5.1-160.el9_0.x86_64                 98/107
  Verifying        : rubygem-io-console-0.5.7-160.el9_0.x86_64           99/107
  Verifying        : rubygem-bigdecimal-3.0.0-160.el9_0.x86_64          100/107
  Verifying        : ruby-libs-3.0.4-160.el9_0.x86_64                   101/107
  Verifying        : ruby-3.0.4-160.el9_0.x86_64                        102/107
  Verifying        : cairo-1.17.4-7.el9.x86_64                          103/107
  Verifying        : keyutils-libs-devel-1.6.3-1.el9.x86_64             104/107
  Verifying        : libX11-xcb-1.7.0-8.el9.x86_64                      105/107
  Verifying        : git-core-2.39.3-1.el9_2.x86_64                     106/107
  Verifying        : git-2.39.3-1.el9_2.x86_64                          107/107

Installed:
  augeas-libs-1.13.0-5.el9.x86_64
  cairo-1.17.4-7.el9.x86_64
  cups-libs-1:2.3.3op2-21.el9.x86_64
  fontconfig-2.14.0-2.el9_1.x86_64
  fribidi-1.0.10-6.el9.2.x86_64
  gd-2.3.2-3.el9.x86_64
  genisoimage-1.1.11-48.el9.x86_64
  git-2.39.3-1.el9_2.x86_64
  git-core-2.39.3-1.el9_2.x86_64
  git-core-doc-2.39.3-1.el9_2.noarch
  glx-utils-8.4.0-12.20210504git0f9e7d9.el9.0.1.x86_64
  gnuplot-5.4.3-2.el9.x86_64
  gnuplot-common-5.4.3-2.el9.x86_64
  jbigkit-libs-2.1-23.el9.x86_64
  jq-1.6-15.el9.x86_64
  keyutils-libs-devel-1.6.3-1.el9.x86_64
  krb5-devel-1.21.1-1.el9.x86_64
  libX11-xcb-1.7.0-8.el9.x86_64
  libXfixes-5.0.3-16.el9.x86_64
  libXft-2.3.3-8.el9.x86_64
  libXpm-3.5.13-8.el9_1.x86_64
  libXrender-0.9.10-16.el9.x86_64
  libXxf86vm-1.1.4-18.el9.x86_64
  libcerf-1.17-2.el9.x86_64
  libcom_err-devel-1.46.5-3.el9.x86_64
  libdrm-2.4.115-1.el9.x86_64
  libevdev-1.11.0-3.el9.x86_64
  libglvnd-1:1.3.4-1.el9.x86_64
  libglvnd-egl-1:1.3.4-1.el9.x86_64
  libglvnd-glx-1:1.3.4-1.el9.x86_64
  libgudev-237-1.el9.x86_64
  libicu-67.1-9.el9.x86_64
  libinput-1.19.3-4.el9_2.x86_64
  libjpeg-turbo-2.0.90-6.el9_1.x86_64
  libkadm5-1.21.1-1.el9.x86_64
  libpciaccess-0.16-6.el9.x86_64
  libpq-13.11-1.el9.x86_64
  libproxy-0.4.15-35.el9.x86_64
  libselinux-devel-3.5-1.el9.x86_64
  libsepol-devel-3.5-1.el9.x86_64
  libsodium-1.0.18-8.el9.x86_64
  libsodium-devel-1.0.18-8.el9.x86_64
  libtiff-4.4.0-10.el9.x86_64
  libunwind-1.6.2-1.el9.x86_64
  libunwind-devel-1.6.2-1.el9.x86_64
  liburing-2.3-2.el9.x86_64
  libusal-1.1.11-48.el9.x86_64
  libverto-devel-0.3.2-3.el9.x86_64
  libwacom-1.12.1-2.el9.x86_64
  libwacom-data-1.12.1-2.el9.noarch
  libwayland-client-1.21.0-1.el9.x86_64
  libwayland-server-1.21.0-1.el9.x86_64
  libwebp-1.2.0-8.el9.x86_64
  libxkbcommon-1.0.3-4.el9.x86_64
  libxkbcommon-x11-1.0.3-4.el9.x86_64
  libxshmfence-1.3-10.el9.x86_64
  libxslt-1.1.34-9.el9.x86_64
  mesa-dri-drivers-23.1.4-1.el9.x86_64
  mesa-filesystem-23.1.4-1.el9.x86_64
  mesa-libEGL-23.1.4-1.el9.x86_64
  mesa-libGL-23.1.4-1.el9.x86_64
  mesa-libgbm-23.1.4-1.el9.x86_64
  mesa-libglapi-23.1.4-1.el9.x86_64
  mtdev-1.1.5-22.el9.x86_64
  oniguruma-6.9.6-1.el9.5.x86_64
  opennebula-6.8.0-1.el9.x86_64
  opennebula-common-6.8.0-1.el9.noarch
  opennebula-common-onecfg-6.8.0-1.el9.noarch
  opennebula-libs-6.8.0-1.el9.noarch
  opennebula-rubygems-6.8.0-1.el9.x86_64
  opennebula-tools-6.8.0-1.el9.noarch
  openpgm-5.2.122-28.el9.x86_64
  openpgm-devel-5.2.122-28.el9.x86_64
  pango-1.48.7-3.el9.x86_64
  pcre2-devel-10.40-2.el9.x86_64
  pcre2-utf16-10.40-2.el9.x86_64
  pcre2-utf32-10.40-2.el9.x86_64
  perl-Error-1:0.17029-7.el9.noarch
  perl-Git-2.39.3-1.el9_2.noarch
  pixman-0.40.0-6.el9_3.x86_64
  qemu-img-17:8.0.0-16.el9_3.3.x86_64
  qt5-qtbase-5.15.9-7.el9.x86_64
  qt5-qtbase-common-5.15.9-7.el9.noarch
  qt5-qtbase-gui-5.15.9-7.el9.x86_64
  qt5-qtsvg-5.15.9-2.el9.x86_64
  ruby-3.0.4-160.el9_0.x86_64
  ruby-default-gems-3.0.4-160.el9_0.noarch
  ruby-libs-3.0.4-160.el9_0.x86_64
  rubygem-bigdecimal-3.0.0-160.el9_0.x86_64
  rubygem-bundler-2.2.33-160.el9_0.noarch
  rubygem-io-console-0.5.7-160.el9_0.x86_64
  rubygem-json-2.5.1-160.el9_0.x86_64
  rubygem-psych-3.3.2-160.el9_0.x86_64
  rubygem-rdoc-6.3.3-160.el9_0.noarch
  rubygem-rexml-3.2.5-160.el9_0.noarch
  rubygems-3.2.33-160.el9_0.noarch
  sqlite-3.34.1-7.el9_3.x86_64
  xcb-util-0.4.0-19.el9.x86_64
  xcb-util-image-0.4.0-19.el9.x86_64
  xcb-util-keysyms-0.4.0-17.el9.x86_64
  xcb-util-renderutil-0.3.9-20.el9.x86_64
  xcb-util-wm-0.4.1-22.el9.x86_64
  xkeyboard-config-2.33-2.el9.noarch
  xml-common-0.6.3-58.el9.noarch
  xmlrpc-c-1.51.0-16.el9.x86_64
  zeromq-4.3.4-2.el9.x86_64
  zeromq-devel-4.3.4-2.el9.x86_64

Complete!
[vagrant@frontend ~]$ sudo dnf -y install opennebula-sunstone
Last metadata expiration check: 0:02:04 ago on Tue 09 Apr 2024 09:25:02 AM UTC.
Dependencies resolved.
================================================================================
 Package                      Arch      Version             Repository     Size
================================================================================
Installing:
 opennebula-sunstone          noarch    6.8.0-1.el9         opennebula     28 M
Installing dependencies:
 flexiblas                    x86_64    3.0.4-8.el9         appstream      32 k
 flexiblas-netlib             x86_64    3.0.4-8.el9         appstream     3.0 M
 flexiblas-openblas-openmp    x86_64    3.0.4-8.el9         appstream      17 k
 libgfortran                  x86_64    11.4.1-2.1.el9      baseos        807 k
 libquadmath                  x86_64    11.4.1-2.1.el9      baseos        197 k
 openblas                     x86_64    0.3.21-2.el9        appstream      34 k
 openblas-openmp              x86_64    0.3.21-2.el9        appstream     4.7 M
 python3-numpy                x86_64    1:1.20.1-5.el9      appstream     4.8 M
 python3-setuptools           noarch    53.0.0-12.el9       baseos        839 k

Transaction Summary
================================================================================
Install  10 Packages

Total download size: 43 M
Installed size: 267 M
Downloading Packages:
(1/10): python3-setuptools-53.0.0-12.el9.noarch 969 kB/s | 839 kB     00:00
(2/10): opennebula-sunstone-6.8.0-1.el9.noarch.  16 MB/s |  28 MB     00:01
(3/10): libquadmath-11.4.1-2.1.el9.x86_64.rpm   108 kB/s | 197 kB     00:01
(4/10): openblas-0.3.21-2.el9.x86_64.rpm        163 kB/s |  34 kB     00:00
(5/10): python3-numpy-1.20.1-5.el9.x86_64.rpm    11 MB/s | 4.8 MB     00:00
(6/10): openblas-openmp-0.3.21-2.el9.x86_64.rpm 5.4 MB/s | 4.7 MB     00:00
(7/10): flexiblas-openblas-openmp-3.0.4-8.el9.x  71 kB/s |  17 kB     00:00
(8/10): flexiblas-3.0.4-8.el9.x86_64.rpm        838 kB/s |  32 kB     00:00
(9/10): flexiblas-netlib-3.0.4-8.el9.x86_64.rpm  17 MB/s | 3.0 MB     00:00
(10/10): libgfortran-11.4.1-2.1.el9.x86_64.rpm  400 kB/s | 807 kB     00:02
--------------------------------------------------------------------------------
Total                                            13 MB/s |  43 MB     00:03
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1
  Installing       : flexiblas-3.0.4-8.el9.x86_64                          1/10
  Installing       : libquadmath-11.4.1-2.1.el9.x86_64                     2/10
  Installing       : libgfortran-11.4.1-2.1.el9.x86_64                     3/10
  Installing       : openblas-0.3.21-2.el9.x86_64                          4/10
  Installing       : openblas-openmp-0.3.21-2.el9.x86_64                   5/10
  Installing       : flexiblas-openblas-openmp-3.0.4-8.el9.x86_64          6/10
  Installing       : flexiblas-netlib-3.0.4-8.el9.x86_64                   7/10
  Installing       : python3-setuptools-53.0.0-12.el9.noarch               8/10
  Installing       : python3-numpy-1:1.20.1-5.el9.x86_64                   9/10
  Running scriptlet: opennebula-sunstone-6.8.0-1.el9.noarch               10/10
  Installing       : opennebula-sunstone-6.8.0-1.el9.noarch               10/10
  Running scriptlet: opennebula-sunstone-6.8.0-1.el9.noarch               10/10
  Verifying        : opennebula-sunstone-6.8.0-1.el9.noarch                1/10
  Verifying        : python3-setuptools-53.0.0-12.el9.noarch               2/10
  Verifying        : libquadmath-11.4.1-2.1.el9.x86_64                     3/10
  Verifying        : libgfortran-11.4.1-2.1.el9.x86_64                     4/10
  Verifying        : openblas-openmp-0.3.21-2.el9.x86_64                   5/10
  Verifying        : openblas-0.3.21-2.el9.x86_64                          6/10
  Verifying        : python3-numpy-1:1.20.1-5.el9.x86_64                   7/10
  Verifying        : flexiblas-openblas-openmp-3.0.4-8.el9.x86_64          8/10
  Verifying        : flexiblas-netlib-3.0.4-8.el9.x86_64                   9/10
  Verifying        : flexiblas-3.0.4-8.el9.x86_64                         10/10

Installed:
  flexiblas-3.0.4-8.el9.x86_64
  flexiblas-netlib-3.0.4-8.el9.x86_64
  flexiblas-openblas-openmp-3.0.4-8.el9.x86_64
  libgfortran-11.4.1-2.1.el9.x86_64
  libquadmath-11.4.1-2.1.el9.x86_64
  openblas-0.3.21-2.el9.x86_64
  openblas-openmp-0.3.21-2.el9.x86_64
  opennebula-sunstone-6.8.0-1.el9.noarch
  python3-numpy-1:1.20.1-5.el9.x86_64
  python3-setuptools-53.0.0-12.el9.noarch

Complete!
[vagrant@frontend ~]$ sudo dnf -y install opennebula-fireedge
Last metadata expiration check: 0:02:35 ago on Tue 09 Apr 2024 09:25:02 AM UTC.
Dependencies resolved.
================================================================================
 Package                   Arch   Version                      Repository  Size
================================================================================
Installing:
 opennebula-fireedge       x86_64 6.8.0-1.el9                  opennebula  32 M
Installing dependencies:
 alsa-lib                  x86_64 1.2.9-1.el9                  appstream  499 k
 flac-libs                 x86_64 1.3.3-10.el9_2.1             appstream  217 k
 freerdp-libs              x86_64 2:2.4.1-5.el9                appstream  873 k
 gsm                       x86_64 1.0.19-6.el9                 appstream   33 k
 lame-libs                 x86_64 3.100-12.el9                 appstream  332 k
 libasyncns                x86_64 0.8-22.el9                   appstream   29 k
 libogg                    x86_64 2:1.3.4-6.el9                appstream   32 k
 libsndfile                x86_64 1.0.31-7.el9                 appstream  206 k
 libssh2                   x86_64 1.11.0-1.el9                 epel       132 k
 libusbx                   x86_64 1.0.26-1.el9                 baseos      75 k
 libvncserver              x86_64 0.9.13-11.el9                epel       296 k
 libvorbis                 x86_64 1:1.3.7-5.el9                appstream  192 k
 libwayland-cursor         x86_64 1.21.0-1.el9                 appstream   18 k
 libwinpr                  x86_64 2:2.4.1-5.el9                appstream  344 k
 libxkbfile                x86_64 1.1.0-8.el9                  appstream   88 k
 nodejs                    x86_64 1:16.20.2-4.el9_3            appstream  111 k
 nodejs-libs               x86_64 1:16.20.2-4.el9_3            appstream   14 M
 opennebula-provision-data noarch 6.8.0-1.el9                  opennebula  58 k
 opus                      x86_64 1.3.1-10.el9                 appstream  199 k
 pulseaudio-libs           x86_64 15.0-2.el9                   appstream  666 k
 uuid                      x86_64 1.6.2-55.el9                 appstream   56 k
Installing weak dependencies:
 nodejs-docs               noarch 1:16.20.2-4.el9_3            appstream  7.1 M
 nodejs-full-i18n          x86_64 1:16.20.2-4.el9_3            appstream  8.2 M
 npm                       x86_64 1:8.19.4-1.16.20.2.4.el9_3   appstream  1.7 M
 opennebula-guacd          x86_64 6.8.0-1.2.0+1.el9            opennebula 220 k

Transaction Summary
================================================================================
Install  26 Packages

Total download size: 68 M
Installed size: 402 M
Downloading Packages:
(1/26): libssh2-1.11.0-1.el9.x86_64.rpm         524 kB/s | 132 kB     00:00
(2/26): libvncserver-0.9.13-11.el9.x86_64.rpm   942 kB/s | 296 kB     00:00
(3/26): opennebula-guacd-6.8.0-1.2.0+1.el9.x86_ 923 kB/s | 220 kB     00:00
(4/26): opennebula-provision-data-6.8.0-1.el9.n 311 kB/s |  58 kB     00:00
(5/26): libusbx-1.0.26-1.el9.x86_64.rpm          44 kB/s |  75 kB     00:01
(6/26): libasyncns-0.8-22.el9.x86_64.rpm         17 kB/s |  29 kB     00:01
(7/26): libogg-1.3.4-6.el9.x86_64.rpm            22 kB/s |  32 kB     00:01
(8/26): gsm-1.0.19-6.el9.x86_64.rpm              22 kB/s |  33 kB     00:01
(9/26): opennebula-fireedge-6.8.0-1.el9.x86_64. 5.6 MB/s |  32 MB     00:05
(10/26): libwinpr-2.4.1-5.el9.x86_64.rpm        117 kB/s | 344 kB     00:02
(11/26): freerdp-libs-2.4.1-5.el9.x86_64.rpm    279 kB/s | 873 kB     00:03
(12/26): flac-libs-1.3.3-10.el9_2.1.x86_64.rpm  127 kB/s | 217 kB     00:01
(13/26): libsndfile-1.0.31-7.el9.x86_64.rpm     140 kB/s | 206 kB     00:01
(14/26): libxkbfile-1.1.0-8.el9.x86_64.rpm       62 kB/s |  88 kB     00:01
(15/26): libvorbis-1.3.7-5.el9.x86_64.rpm       129 kB/s | 192 kB     00:01
(16/26): alsa-lib-1.2.9-1.el9.x86_64.rpm        304 kB/s | 499 kB     00:01
(17/26): lame-libs-3.100-12.el9.x86_64.rpm      184 kB/s | 332 kB     00:01
(18/26): opus-1.3.1-10.el9.x86_64.rpm           131 kB/s | 199 kB     00:01
(19/26): npm-8.19.4-1.16.20.2.4.el9_3.x86_64.rp 2.4 MB/s | 1.7 MB     00:00
(20/26): libwayland-cursor-1.21.0-1.el9.x86_64.  12 kB/s |  18 kB     00:01
(21/26): nodejs-full-i18n-16.20.2-4.el9_3.x86_6 7.6 MB/s | 8.2 MB     00:01
(22/26): nodejs-libs-16.20.2-4.el9_3.x86_64.rpm 5.4 MB/s |  14 MB     00:02
(23/26): nodejs-16.20.2-4.el9_3.x86_64.rpm       45 kB/s | 111 kB     00:02
(24/26): nodejs-docs-16.20.2-4.el9_3.noarch.rpm 3.9 MB/s | 7.1 MB     00:01
(25/26): pulseaudio-libs-15.0-2.el9.x86_64.rpm  909 kB/s | 666 kB     00:00
(26/26): uuid-1.6.2-55.el9.x86_64.rpm            66 kB/s |  56 kB     00:00
--------------------------------------------------------------------------------
Total                                           4.4 MB/s |  68 MB     00:15
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Running scriptlet: npm-1:8.19.4-1.16.20.2.4.el9_3.x86_64                  1/1
  Preparing        :                                                        1/1
  Installing       : libogg-2:1.3.4-6.el9.x86_64                           1/26
  Installing       : libvorbis-1:1.3.7-5.el9.x86_64                        2/26
  Installing       : libwinpr-2:2.4.1-5.el9.x86_64                         3/26
  Installing       : gsm-1.0.19-6.el9.x86_64                               4/26
  Installing       : flac-libs-1.3.3-10.el9_2.1.x86_64                     5/26
  Installing       : uuid-1.6.2-55.el9.x86_64                              6/26
  Installing       : nodejs-docs-1:16.20.2-4.el9_3.noarch                  7/26
  Installing       : nodejs-libs-1:16.20.2-4.el9_3.x86_64                  8/26
  Installing       : nodejs-full-i18n-1:16.20.2-4.el9_3.x86_64             9/26
  Installing       : nodejs-1:16.20.2-4.el9_3.x86_64                      10/26
  Installing       : npm-1:8.19.4-1.16.20.2.4.el9_3.x86_64                11/26
  Installing       : libwayland-cursor-1.21.0-1.el9.x86_64                12/26
  Installing       : opus-1.3.1-10.el9.x86_64                             13/26
  Installing       : libsndfile-1.0.31-7.el9.x86_64                       14/26
  Installing       : lame-libs-3.100-12.el9.x86_64                        15/26
  Installing       : alsa-lib-1.2.9-1.el9.x86_64                          16/26
  Installing       : libxkbfile-1.1.0-8.el9.x86_64                        17/26
  Installing       : libasyncns-0.8-22.el9.x86_64                         18/26
  Installing       : pulseaudio-libs-15.0-2.el9.x86_64                    19/26
  Installing       : libusbx-1.0.26-1.el9.x86_64                          20/26
  Installing       : freerdp-libs-2:2.4.1-5.el9.x86_64                    21/26
  Installing       : opennebula-provision-data-6.8.0-1.el9.noarch         22/26
  Installing       : libvncserver-0.9.13-11.el9.x86_64                    23/26
  Installing       : libssh2-1.11.0-1.el9.x86_64                          24/26
  Running scriptlet: opennebula-guacd-6.8.0-1.2.0+1.el9.x86_64            25/26
  Installing       : opennebula-guacd-6.8.0-1.2.0+1.el9.x86_64            25/26
  Running scriptlet: opennebula-guacd-6.8.0-1.2.0+1.el9.x86_64            25/26
  Running scriptlet: opennebula-fireedge-6.8.0-1.el9.x86_64               26/26
  Installing       : opennebula-fireedge-6.8.0-1.el9.x86_64               26/26
  Running scriptlet: opennebula-fireedge-6.8.0-1.el9.x86_64               26/26
  Verifying        : libssh2-1.11.0-1.el9.x86_64                           1/26
  Verifying        : libvncserver-0.9.13-11.el9.x86_64                     2/26
  Verifying        : opennebula-fireedge-6.8.0-1.el9.x86_64                3/26
  Verifying        : opennebula-guacd-6.8.0-1.2.0+1.el9.x86_64             4/26
  Verifying        : opennebula-provision-data-6.8.0-1.el9.noarch          5/26
  Verifying        : libusbx-1.0.26-1.el9.x86_64                           6/26
  Verifying        : libasyncns-0.8-22.el9.x86_64                          7/26
  Verifying        : libogg-2:1.3.4-6.el9.x86_64                           8/26
  Verifying        : gsm-1.0.19-6.el9.x86_64                               9/26
  Verifying        : libwinpr-2:2.4.1-5.el9.x86_64                        10/26
  Verifying        : freerdp-libs-2:2.4.1-5.el9.x86_64                    11/26
  Verifying        : flac-libs-1.3.3-10.el9_2.1.x86_64                    12/26
  Verifying        : libsndfile-1.0.31-7.el9.x86_64                       13/26
  Verifying        : libxkbfile-1.1.0-8.el9.x86_64                        14/26
  Verifying        : libvorbis-1:1.3.7-5.el9.x86_64                       15/26
  Verifying        : alsa-lib-1.2.9-1.el9.x86_64                          16/26
  Verifying        : lame-libs-3.100-12.el9.x86_64                        17/26
  Verifying        : opus-1.3.1-10.el9.x86_64                             18/26
  Verifying        : libwayland-cursor-1.21.0-1.el9.x86_64                19/26
  Verifying        : npm-1:8.19.4-1.16.20.2.4.el9_3.x86_64                20/26
  Verifying        : nodejs-libs-1:16.20.2-4.el9_3.x86_64                 21/26
  Verifying        : nodejs-full-i18n-1:16.20.2-4.el9_3.x86_64            22/26
  Verifying        : nodejs-1:16.20.2-4.el9_3.x86_64                      23/26
  Verifying        : nodejs-docs-1:16.20.2-4.el9_3.noarch                 24/26
  Verifying        : pulseaudio-libs-15.0-2.el9.x86_64                    25/26
  Verifying        : uuid-1.6.2-55.el9.x86_64                             26/26

Installed:
  alsa-lib-1.2.9-1.el9.x86_64
  flac-libs-1.3.3-10.el9_2.1.x86_64
  freerdp-libs-2:2.4.1-5.el9.x86_64
  gsm-1.0.19-6.el9.x86_64
  lame-libs-3.100-12.el9.x86_64
  libasyncns-0.8-22.el9.x86_64
  libogg-2:1.3.4-6.el9.x86_64
  libsndfile-1.0.31-7.el9.x86_64
  libssh2-1.11.0-1.el9.x86_64
  libusbx-1.0.26-1.el9.x86_64
  libvncserver-0.9.13-11.el9.x86_64
  libvorbis-1:1.3.7-5.el9.x86_64
  libwayland-cursor-1.21.0-1.el9.x86_64
  libwinpr-2:2.4.1-5.el9.x86_64
  libxkbfile-1.1.0-8.el9.x86_64
  nodejs-1:16.20.2-4.el9_3.x86_64
  nodejs-docs-1:16.20.2-4.el9_3.noarch
  nodejs-full-i18n-1:16.20.2-4.el9_3.x86_64
  nodejs-libs-1:16.20.2-4.el9_3.x86_64
  npm-1:8.19.4-1.16.20.2.4.el9_3.x86_64
  opennebula-fireedge-6.8.0-1.el9.x86_64
  opennebula-guacd-6.8.0-1.2.0+1.el9.x86_64
  opennebula-provision-data-6.8.0-1.el9.noarch
  opus-1.3.1-10.el9.x86_64
  pulseaudio-libs-15.0-2.el9.x86_64
  uuid-1.6.2-55.el9.x86_64

Complete!
[vagrant@frontend ~]$

```
**🌞 Configuration OpenNebula**

```
[vagrant@frontend ~]$ sudo nano /etc/one/oned.conf
DB = [ BACKEND = "mysql",
       SERVER  = "localhost",
       PORT    = 0,
       USER    = "oneadmin",
       PASSWD  = "130504Tb",
       DB_NAME = "opennebula",
       CONNECTIONS = 25,
       COMPARE_BINARY = "no" ]

```
**🌞 Créer un user pour se log sur la WebUI OpenNebula**



```
[vagrant@frontend ~]$ sudo su - oneadmin
Last login: Tue Apr  9 09:40:30 UTC 2024 on pts/0
[oneadmin@frontend ~]$ nano /var/lib/one/.one/one_auth


  GNU nano 5.6.1              /var/lib/one/.one/one_auth
oneadmin:e65a260e49a68d85018d35cd674e15a2
tom:130504Tb


```


**🌞 Démarrer les services OpenNebula**

```
[vagrant@frontend ~]$ sudo systemctl start opennebula
[vagrant@frontend ~]$ sudo systemctl start opennebula-sunstone
[vagrant@frontend ~]$ sudo systemctl enable opennebula
Created symlink /etc/systemd/system/multi-user.target.wants/opennebula.service → /usr/lib/systemd/system/opennebula.service.
[vagrant@frontend ~]$ sudo systemctl enable opennebula-sunstone
Created symlink /etc/systemd/system/multi-user.target.wants/opennebula-sunstone.service → /usr/lib/systemd/system/opennebula-sunstone.service.
[vagrant@frontend ~]$

```
**🌞 Ouverture firewall**

```
[vagrant@frontend ~]$ sudo firewall-cmd --zone=public --add-port=9869/tcp --permanent
success
[vagrant@frontend ~]$ sudo firewall-cmd --zone=public --add-port=22/tcp --permanent
success
[vagrant@frontend ~]$ sudo firewall-cmd --zone=public --add-port=2633/tcp --permanent
success
[vagrant@frontend ~]$ sudo firewall-cmd --zone=public --add-port=4124/tcp --permanent
success
[vagrant@frontend ~]$ sudo firewall-cmd --zone=public --add-port=4124/udp --permanent
success
[vagrant@frontend ~]$ sudo firewall-cmd --zone=public --add-port=29876/tcp --permanent
success
[vagrant@frontend ~]$ sudo firewall-cmd --reload
success

```

# A kvm

**🌞 Ajouter des dépôts supplémentaires**

```
[vagrant@kvm1 ~]$ sudo nano /etc/yum.repos.d/opennebula.repo
[vagrant@kvm1 ~]$ sudo dnf install -y epel-release
Extra Packages for Enterprise Linux 9 - x86_64                                                                                                                                     18 kB/s |  12 kB     00:00
OpenNebula Community Edition                                                                                                                                                      1.1 kB/s | 833  B     00:00
OpenNebula Community Edition                                                                                                                                                      6.7 kB/s | 3.1 kB     00:00
Importing GPG key 0x906DC27C:
 Userid     : "OpenNebula Repository <contact@opennebula.io>"
 Fingerprint: 0B2D 385C 7C93 04B1 1A03 67B9 05A0 5927 906D C27C
 From       : https://downloads.opennebula.io/repo/repo2.key
OpenNebula Community Edition                                                                                                                                                      904 kB/s | 675 kB     00:00
Package epel-release-9-7.el9.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!

```

**🌞 Installer KVM**

```
[vagrant@kvm1 ~]$ sudo dnf -y install opennebula-node-kvm
Last metadata expiration check: 0:02:04 ago on Tue 09 Apr 2024 11:44:21 AM UTC.
Dependencies resolved.
==================================================================================================================================================================================================================
 Package                                                           Architecture                      Version                                                          Repository                             Size
==================================================================================================================================================================================================================
Installing:
 opennebula-node-kvm                                               noarch                            6.8.0-1.el9                                                      opennebula                             17 k
Installing dependencies:
 augeas                                                            x86_64                            1.13.0-5.el9                                                     appstream                              49 k
 augeas-libs                                                       x86_64                            1.13.0-5.el9                                                     appstream                             405 k
 boost-iostreams                                                   x86_64                            1.75.0-8.el9                                                     appstream                              38 k
 boost-system                                                      x86_64                            1.75.0-8.el9                                                     appstream                              13 k
 boost-thread                                                      x86_64                            1.75.0-8.el9                                                     appstream                              55 k
 capstone                                                          x86_64                            4.0.2-10.el9                                                     appstream                             766 k
 checkpolicy                                                       x86_64                            3.5-1.el9                                                        appstream                             345 k
 cyrus-sasl                                                        x86_64                            2.1.27-21.el9                                                    baseos                                 71 k
 cyrus-sasl-gssapi                                                 x86_64                            2.1.27-21.el9                                                    baseos                                 26 k
 daxctl-libs                                                       x86_64                            71.1-8.el9                                                       baseos                                 41 k
 device-mapper-multipath-libs                                      x86_64                            0.8.7-22.el9                                                     baseos                                267 k
 dnsmasq                                                           x86_64                            2.85-14.el9_3.1                                                  appstream                             325 k
 edk2-ovmf                                                         noarch                            20230524-4.el9_3.2                                               appstream                             5.4 M
 flac-libs                                                         x86_64                            1.3.3-10.el9_2.1                                                 appstream                             217 k
 gnutls-dane                                                       x86_64                            3.7.6-23.el9_3.3                                                 appstream                              22 k
 gnutls-utils                                                      x86_64                            3.7.6-23.el9_3.3                                                 appstream                             265 k
 gsm                                                               x86_64                            1.0.19-6.el9                                                     appstream                              33 k
 gssproxy                                                          x86_64                            0.8.4-6.el9                                                      baseos                                108 k
 ipxe-roms-qemu                                                    noarch                            20200823-9.git4bd064de.el9                                       appstream                             673 k
 iscsi-initiator-utils                                             x86_64                            6.2.1.4-3.git2a8f9d8.el9                                         baseos                                378 k
 iscsi-initiator-utils-iscsiuio                                    x86_64                            6.2.1.4-3.git2a8f9d8.el9                                         baseos                                 94 k
 isns-utils-libs                                                   x86_64                            0.101-4.el9                                                      baseos                                100 k
 jq                                                                x86_64                            1.6-15.el9                                                       appstream                             186 k
 json-glib                                                         x86_64                            1.6.6-1.el9                                                      baseos                                151 k
 keyutils                                                          x86_64                            1.6.3-1.el9                                                      baseos                                 72 k
 libX11-xcb                                                        x86_64                            1.7.0-8.el9                                                      appstream                             9.9 k
 libXfixes                                                         x86_64                            5.0.3-16.el9                                                     appstream                              19 k
 libXxf86vm                                                        x86_64                            1.1.4-18.el9                                                     appstream                              18 k
 libasyncns                                                        x86_64                            0.8-22.el9                                                       appstream                              29 k
 libblkio                                                          x86_64                            1.3.0-1.el9                                                      appstream                             512 k
 libbsd                                                            x86_64                            0.11.7-2.el9                                                     epel                                  111 k
 libdrm                                                            x86_64                            2.4.115-1.el9                                                    appstream                             156 k
 libepoxy                                                          x86_64                            1.5.5-4.el9                                                      appstream                             244 k
 libev                                                             x86_64                            4.33-5.el9                                                       baseos                                 52 k
 libfdt                                                            x86_64                            1.6.0-7.el9                                                      appstream                              33 k
 libglvnd                                                          x86_64                            1:1.3.4-1.el9                                                    appstream                             133 k
 libglvnd-egl                                                      x86_64                            1:1.3.4-1.el9                                                    appstream                              36 k
 libglvnd-glx                                                      x86_64                            1:1.3.4-1.el9                                                    appstream                             140 k
 libmd                                                             x86_64                            1.1.0-1.el9                                                      epel                                   46 k
 libnbd                                                            x86_64                            1.16.0-1.el9                                                     appstream                             160 k
 libnfsidmap                                                       x86_64                            1:2.5.4-20.el9                                                   baseos                                 60 k
 libogg                                                            x86_64                            2:1.3.4-6.el9                                                    appstream                              32 k
 libpciaccess                                                      x86_64                            0.16-6.el9                                                       baseos                                 27 k
 libpmem                                                           x86_64                            1.12.1-1.el9                                                     appstream                             111 k
 librados2                                                         x86_64                            2:16.2.4-5.el9                                                   appstream                             3.4 M
 librbd1                                                           x86_64                            2:16.2.4-5.el9                                                   appstream                             3.0 M
 librdmacm                                                         x86_64                            46.0-1.el9                                                       baseos                                 70 k
 libretls                                                          x86_64                            3.8.1-1.el9                                                      epel                                   41 k
 libslirp                                                          x86_64                            4.4.0-7.el9                                                      appstream                              68 k
 libsndfile                                                        x86_64                            1.0.31-7.el9                                                     appstream                             206 k
 libtpms                                                           x86_64                            0.9.1-3.20211126git1ff6fe1f43.el9_2                              appstream                             184 k
 liburing                                                          x86_64                            2.3-2.el9                                                        appstream                              26 k
 libusbx                                                           x86_64                            1.0.26-1.el9                                                     baseos                                 75 k
 libverto-libev                                                    x86_64                            0.3.2-3.el9                                                      baseos                                 13 k
 libvirt                                                           x86_64                            9.5.0-7.2.el9_3                                                  appstream                              22 k
 libvirt-client                                                    x86_64                            9.5.0-7.2.el9_3                                                  appstream                             426 k
 libvirt-daemon                                                    x86_64                            9.5.0-7.2.el9_3                                                  appstream                             167 k
 libvirt-daemon-common                                             x86_64                            9.5.0-7.2.el9_3                                                  appstream                             128 k
 libvirt-daemon-config-network                                     x86_64                            9.5.0-7.2.el9_3                                                  appstream                              24 k
 libvirt-daemon-config-nwfilter                                    x86_64                            9.5.0-7.2.el9_3                                                  appstream                              30 k
 libvirt-daemon-driver-interface                                   x86_64                            9.5.0-7.2.el9_3                                                  appstream                             173 k
 libvirt-daemon-driver-network                                     x86_64                            9.5.0-7.2.el9_3                                                  appstream                             211 k
 libvirt-daemon-driver-nodedev                                     x86_64                            9.5.0-7.2.el9_3                                                  appstream                             194 k
 libvirt-daemon-driver-nwfilter                                    x86_64                            9.5.0-7.2.el9_3                                                  appstream                             209 k
 libvirt-daemon-driver-qemu                                        x86_64                            9.5.0-7.2.el9_3                                                  appstream                             909 k
 libvirt-daemon-driver-secret                                      x86_64                            9.5.0-7.2.el9_3                                                  appstream                             170 k
 libvirt-daemon-driver-storage                                     x86_64                            9.5.0-7.2.el9_3                                                  appstream                              22 k
 libvirt-daemon-driver-storage-core                                x86_64                            9.5.0-7.2.el9_3                                                  appstream                             228 k
 libvirt-daemon-driver-storage-disk                                x86_64                            9.5.0-7.2.el9_3                                                  appstream                              33 k
 libvirt-daemon-driver-storage-iscsi                               x86_64                            9.5.0-7.2.el9_3                                                  appstream                              30 k
 libvirt-daemon-driver-storage-logical                             x86_64                            9.5.0-7.2.el9_3                                                  appstream                              34 k
 libvirt-daemon-driver-storage-mpath                               x86_64                            9.5.0-7.2.el9_3                                                  appstream                              27 k
 libvirt-daemon-driver-storage-rbd                                 x86_64                            9.5.0-7.2.el9_3                                                  appstream                              38 k
 libvirt-daemon-driver-storage-scsi                                x86_64                            9.5.0-7.2.el9_3                                                  appstream                              30 k
 libvirt-daemon-lock                                               x86_64                            9.5.0-7.2.el9_3                                                  appstream                              58 k
 libvirt-daemon-log                                                x86_64                            9.5.0-7.2.el9_3                                                  appstream                              61 k
 libvirt-daemon-plugin-lockd                                       x86_64                            9.5.0-7.2.el9_3                                                  appstream                              33 k
 libvirt-daemon-proxy                                              x86_64                            9.5.0-7.2.el9_3                                                  appstream                             165 k
 libvirt-libs                                                      x86_64                            9.5.0-7.2.el9_3                                                  appstream                             4.8 M
 libvorbis                                                         x86_64                            1:1.3.7-5.el9                                                    appstream                             192 k
 libwayland-client                                                 x86_64                            1.21.0-1.el9                                                     appstream                              33 k
 libwayland-server                                                 x86_64                            1.21.0-1.el9                                                     appstream                              41 k
 libxkbcommon                                                      x86_64                            1.0.3-4.el9                                                      appstream                             132 k
 libxshmfence                                                      x86_64                            1.3-10.el9                                                       appstream                              12 k
 lzop                                                              x86_64                            1.04-8.el9                                                       baseos                                 55 k
 mdevctl                                                           x86_64                            1.1.0-4.el9                                                      appstream                             758 k
 mesa-dri-drivers                                                  x86_64                            23.1.4-1.el9                                                     appstream                              10 M
 mesa-filesystem                                                   x86_64                            23.1.4-1.el9                                                     appstream                             9.9 k
 mesa-libEGL                                                       x86_64                            23.1.4-1.el9                                                     appstream                             124 k
 mesa-libGL                                                        x86_64                            23.1.4-1.el9                                                     appstream                             165 k
 mesa-libgbm                                                       x86_64                            23.1.4-1.el9                                                     appstream                              37 k
 mesa-libglapi                                                     x86_64                            23.1.4-1.el9                                                     appstream                              46 k
 ndctl-libs                                                        x86_64                            71.1-8.el9                                                       baseos                                 88 k
 nfs-utils                                                         x86_64                            1:2.5.4-20.el9                                                   baseos                                425 k
 numad                                                             x86_64                            0.5-36.20150602git.el9                                           baseos                                 36 k
 oniguruma                                                         x86_64                            6.9.6-1.el9.5                                                    appstream                             217 k
 opennebula-common                                                 noarch                            6.8.0-1.el9                                                      opennebula                             22 k
 opennebula-common-onecfg                                          noarch                            6.8.0-1.el9                                                      opennebula                            9.1 k
 opus                                                              x86_64                            1.3.1-10.el9                                                     appstream                             199 k
 passt-selinux                                                     noarch                            0^20230818.g0af928e-4.el9                                        appstream                              29 k
 pciutils                                                          x86_64                            3.7.0-5.el9                                                      baseos                                 92 k
 pixman                                                            x86_64                            0.40.0-6.el9_3                                                   appstream                             269 k
 policycoreutils-python-utils                                      noarch                            3.5-3.el9_3                                                      appstream                              71 k
 pulseaudio-libs                                                   x86_64                            15.0-2.el9                                                       appstream                             666 k
 python3-audit                                                     x86_64                            3.0.7-104.el9                                                    appstream                              82 k
 python3-distro                                                    noarch                            1.5.0-7.el9                                                      appstream                              36 k
 python3-libsemanage                                               x86_64                            3.5-2.el9                                                        appstream                              79 k
 python3-policycoreutils                                           noarch                            3.5-3.el9_3                                                      appstream                             2.0 M
 python3-pyyaml                                                    x86_64                            5.4.1-6.el9                                                      baseos                                191 k
 python3-setools                                                   x86_64                            4.4.3-1.el9                                                      baseos                                551 k
 python3-setuptools                                                noarch                            53.0.0-12.el9                                                    baseos                                839 k
 qemu-img                                                          x86_64                            17:8.0.0-16.el9_3.3                                              appstream                             2.4 M
 qemu-kvm                                                          x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              62 k
 qemu-kvm-audio-pa                                                 x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              71 k
 qemu-kvm-block-blkio                                              x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              74 k
 qemu-kvm-block-rbd                                                x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              76 k
 qemu-kvm-common                                                   x86_64                            17:8.0.0-16.el9_3.3                                              appstream                             642 k
 qemu-kvm-core                                                     x86_64                            17:8.0.0-16.el9_3.3                                              appstream                             4.1 M
 qemu-kvm-device-display-virtio-gpu                                x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              81 k
 qemu-kvm-device-display-virtio-gpu-pci                            x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              66 k
 qemu-kvm-device-display-virtio-vga                                x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              67 k
 qemu-kvm-device-usb-host                                          x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              80 k
 qemu-kvm-device-usb-redirect                                      x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              85 k
 qemu-kvm-docs                                                     x86_64                            17:8.0.0-16.el9_3.3                                              appstream                             1.1 M
 qemu-kvm-tools                                                    x86_64                            17:8.0.0-16.el9_3.3                                              appstream                             559 k
 qemu-kvm-ui-egl-headless                                          x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              67 k
 qemu-kvm-ui-opengl                                                x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              73 k
 qemu-pr-helper                                                    x86_64                            17:8.0.0-16.el9_3.3                                              appstream                             486 k
 quota                                                             x86_64                            1:4.06-6.el9                                                     baseos                                190 k
 quota-nls                                                         noarch                            1:4.06-6.el9                                                     baseos                                 78 k
 rpcbind                                                           x86_64                            1.2.6-5.el9                                                      baseos                                 56 k
 ruby                                                              x86_64                            3.0.4-160.el9_0                                                  appstream                              41 k
 ruby-libs                                                         x86_64                            3.0.4-160.el9_0                                                  appstream                             3.2 M
 rubygem-io-console                                                x86_64                            0.5.7-160.el9_0                                                  appstream                              25 k
 rubygem-json                                                      x86_64                            2.5.1-160.el9_0                                                  appstream                              54 k
 rubygem-psych                                                     x86_64                            3.3.2-160.el9_0                                                  appstream                              51 k
 rubygem-rexml                                                     noarch                            3.2.5-160.el9_0                                                  appstream                              95 k
 rubygem-sqlite3                                                   x86_64                            1.4.2-8.el9                                                      epel                                   44 k
 rubygems                                                          noarch                            3.2.33-160.el9_0                                                 appstream                             256 k
 seabios-bin                                                       noarch                            1.16.1-1.el9                                                     appstream                             101 k
 seavgabios-bin                                                    noarch                            1.16.1-1.el9                                                     appstream                              36 k
 sssd-nfs-idmap                                                    x86_64                            2.9.1-4.el9_3.5                                                  baseos                                 42 k
 swtpm                                                             x86_64                            0.8.0-1.el9                                                      appstream                              42 k
 swtpm-libs                                                        x86_64                            0.8.0-1.el9                                                      appstream                              50 k
 swtpm-tools                                                       x86_64                            0.8.0-1.el9                                                      appstream                             117 k
 systemd-container                                                 x86_64                            252-18.el9.0.1.rocky                                             baseos                                556 k
 unbound-libs                                                      x86_64                            1.16.2-3.el9_3.1                                                 appstream                             548 k
 usbredir                                                          x86_64                            0.13.0-2.el9                                                     appstream                              50 k
 virtiofsd                                                         x86_64                            1.7.2-1.el9                                                      appstream                             864 k
 xkeyboard-config                                                  noarch                            2.33-2.el9                                                       appstream                             779 k
 yajl                                                              x86_64                            2.1.0-22.el9                                                     appstream                              37 k
Installing weak dependencies:
 netcat                                                            x86_64                            1.226-1.el9                                                      epel                                   34 k
 passt                                                             x86_64                            0^20230818.g0af928e-4.el9                                        appstream                             177 k
 ruby-default-gems                                                 noarch                            3.0.4-160.el9_0                                                  appstream                              32 k
 rubygem-bigdecimal                                                x86_64                            3.0.0-160.el9_0                                                  appstream                              54 k
 rubygem-bundler                                                   noarch                            2.2.33-160.el9_0                                                 appstream                             372 k
 rubygem-rdoc                                                      noarch                            6.3.3-160.el9_0                                                  appstream                             400 k

Transaction Summary
==================================================================================================================================================================================================================
Install  158 Packages

Total download size: 63 M
Installed size: 256 M
Downloading Packages:
(1/158): libmd-1.1.0-1.el9.x86_64.rpm                                                                                                                                             101 kB/s |  46 kB     00:00
(2/158): libretls-3.8.1-1.el9.x86_64.rpm                                                                                                                                           81 kB/s |  41 kB     00:00
(3/158): netcat-1.226-1.el9.x86_64.rpm                                                                                                                                            388 kB/s |  34 kB     00:00
(4/158): rubygem-sqlite3-1.4.2-8.el9.x86_64.rpm                                                                                                                                   206 kB/s |  44 kB     00:00
(5/158): libbsd-0.11.7-2.el9.x86_64.rpm                                                                                                                                           138 kB/s | 111 kB     00:00
(6/158): opennebula-common-6.8.0-1.el9.noarch.rpm                                                                                                                                  76 kB/s |  22 kB     00:00
(7/158): opennebula-common-onecfg-6.8.0-1.el9.noarch.rpm                                                                                                                           46 kB/s | 9.1 kB     00:00
(8/158): opennebula-node-kvm-6.8.0-1.el9.noarch.rpm                                                                                                                                89 kB/s |  17 kB     00:00
(9/158): json-glib-1.6.6-1.el9.x86_64.rpm                                                                                                                                          87 kB/s | 151 kB     00:01
(10/158): librdmacm-46.0-1.el9.x86_64.rpm                                                                                                                                          24 kB/s |  70 kB     00:02
(11/158): quota-nls-4.06-6.el9.noarch.rpm                                                                                                                                         378 kB/s |  78 kB     00:00
(12/158): libverto-libev-0.3.2-3.el9.x86_64.rpm                                                                                                                                   3.8 kB/s |  13 kB     00:03
(13/158): quota-4.06-6.el9.x86_64.rpm                                                                                                                                             100 kB/s | 190 kB     00:01
(14/158): python3-pyyaml-5.4.1-6.el9.x86_64.rpm                                                                                                                                    92 kB/s | 191 kB     00:02
(15/158): numad-0.5-36.20150602git.el9.x86_64.rpm                                                                                                                                  20 kB/s |  36 kB     00:01
(16/158): rpcbind-1.2.6-5.el9.x86_64.rpm                                                                                                                                           33 kB/s |  56 kB     00:01
(17/158): isns-utils-libs-0.101-4.el9.x86_64.rpm                                                                                                                                   59 kB/s | 100 kB     00:01
(18/158): libpciaccess-0.16-6.el9.x86_64.rpm                                                                                                                                       16 kB/s |  27 kB     00:01
(19/158): libusbx-1.0.26-1.el9.x86_64.rpm                                                                                                                                          42 kB/s |  75 kB     00:01
(20/158): libev-4.33-5.el9.x86_64.rpm                                                                                                                                              12 kB/s |  52 kB     00:04
(21/158): iscsi-initiator-utils-iscsiuio-6.2.1.4-3.git2a8f9d8.el9.x86_64.rpm                                                                                                       49 kB/s |  94 kB     00:01
(22/158): iscsi-initiator-utils-6.2.1.4-3.git2a8f9d8.el9.x86_64.rpm                                                                                                               142 kB/s | 378 kB     00:02
(23/158): pciutils-3.7.0-5.el9.x86_64.rpm                                                                                                                                          47 kB/s |  92 kB     00:01
(24/158): ndctl-libs-71.1-8.el9.x86_64.rpm                                                                                                                                         52 kB/s |  88 kB     00:01
(25/158): daxctl-libs-71.1-8.el9.x86_64.rpm                                                                                                                                        15 kB/s |  41 kB     00:02
(26/158): python3-setools-4.4.3-1.el9.x86_64.rpm                                                                                                                                  296 kB/s | 551 kB     00:01
(27/158): sssd-nfs-idmap-2.9.1-4.el9_3.5.x86_64.rpm                                                                                                                                27 kB/s |  42 kB     00:01
(28/158): systemd-container-252-18.el9.0.1.rocky.x86_64.rpm                                                                                                                       315 kB/s | 556 kB     00:01
(29/158): python3-setuptools-53.0.0-12.el9.noarch.rpm                                                                                                                              50 kB/s | 839 kB     00:16
(30/158): lzop-1.04-8.el9.x86_64.rpm                                                                                                                                               35 kB/s |  55 kB     00:01
(31/158): gssproxy-0.8.4-6.el9.x86_64.rpm                                                                                                                                          37 kB/s | 108 kB     00:02
(32/158): nfs-utils-2.5.4-20.el9.x86_64.rpm                                                                                                                                       222 kB/s | 425 kB     00:01
(33/158): cyrus-sasl-gssapi-2.1.27-21.el9.x86_64.rpm                                                                                                                               17 kB/s |  26 kB     00:01
(34/158): libnfsidmap-2.5.4-20.el9.x86_64.rpm                                                                                                                                      20 kB/s |  60 kB     00:02
(35/158): keyutils-1.6.3-1.el9.x86_64.rpm                                                                                                                                          68 kB/s |  72 kB     00:01
(36/158): python3-distro-1.5.0-7.el9.noarch.rpm                                                                                                                                   173 kB/s |  36 kB     00:00
(37/158): device-mapper-multipath-libs-0.8.7-22.el9.x86_64.rpm                                                                                                                    390 kB/s | 267 kB     00:00
(38/158): augeas-libs-1.13.0-5.el9.x86_64.rpm                                                                                                                                     3.1 MB/s | 405 kB     00:00
(39/158): checkpolicy-3.5-1.el9.x86_64.rpm                                                                                                                                        5.0 MB/s | 345 kB     00:00
(40/158): libfdt-1.6.0-7.el9.x86_64.rpm                                                                                                                                           623 kB/s |  33 kB     00:00
(41/158): python3-audit-3.0.7-104.el9.x86_64.rpm                                                                                                                                  2.2 MB/s |  82 kB     00:00
(42/158): augeas-1.13.0-5.el9.x86_64.rpm                                                                                                                                          240 kB/s |  49 kB     00:00
(43/158): libasyncns-0.8-22.el9.x86_64.rpm                                                                                                                                        917 kB/s |  29 kB     00:00
(44/158): libxshmfence-1.3-10.el9.x86_64.rpm                                                                                                                                      349 kB/s |  12 kB     00:00
(45/158): libxkbcommon-1.0.3-4.el9.x86_64.rpm                                                                                                                                     1.7 MB/s | 132 kB     00:00
(46/158): libepoxy-1.5.5-4.el9.x86_64.rpm                                                                                                                                         5.4 MB/s | 244 kB     00:00
(47/158): libXfixes-5.0.3-16.el9.x86_64.rpm                                                                                                                                       659 kB/s |  19 kB     00:00
(48/158): libogg-1.3.4-6.el9.x86_64.rpm                                                                                                                                           1.0 MB/s |  32 kB     00:00
(49/158): policycoreutils-python-utils-3.5-3.el9_3.noarch.rpm                                                                                                                     1.8 MB/s |  71 kB     00:00
(50/158): usbredir-0.13.0-2.el9.x86_64.rpm                                                                                                                                        1.1 MB/s |  50 kB     00:00
(51/158): python3-policycoreutils-3.5-3.el9_3.noarch.rpm                                                                                                                          9.8 MB/s | 2.0 MB     00:00
(52/158): xkeyboard-config-2.33-2.el9.noarch.rpm                                                                                                                                  6.1 MB/s | 779 kB     00:00
(53/158): yajl-2.1.0-22.el9.x86_64.rpm                                                                                                                                            1.0 MB/s |  37 kB     00:00
(54/158): seavgabios-bin-1.16.1-1.el9.noarch.rpm                                                                                                                                  896 kB/s |  36 kB     00:00
(55/158): seabios-bin-1.16.1-1.el9.noarch.rpm                                                                                                                                     2.0 MB/s | 101 kB     00:00
(56/158): libpmem-1.12.1-1.el9.x86_64.rpm                                                                                                                                         2.5 MB/s | 111 kB     00:00
(57/158): virtiofsd-1.7.2-1.el9.x86_64.rpm                                                                                                                                        6.8 MB/s | 864 kB     00:00
(58/158): unbound-libs-1.16.2-3.el9_3.1.x86_64.rpm                                                                                                                                3.4 MB/s | 548 kB     00:00
(59/158): gnutls-utils-3.7.6-23.el9_3.3.x86_64.rpm                                                                                                                                4.6 MB/s | 265 kB     00:00
(60/158): rubygems-3.2.33-160.el9_0.noarch.rpm                                                                                                                                    5.1 MB/s | 256 kB     00:00
(61/158): gnutls-dane-3.7.6-23.el9_3.3.x86_64.rpm                                                                                                                                 258 kB/s |  22 kB     00:00
(62/158): rubygem-rexml-3.2.5-160.el9_0.noarch.rpm                                                                                                                                2.2 MB/s |  95 kB     00:00
(63/158): rubygem-bundler-2.2.33-160.el9_0.noarch.rpm                                                                                                                             6.1 MB/s | 372 kB     00:00
(64/158): ruby-default-gems-3.0.4-160.el9_0.noarch.rpm                                                                                                                            859 kB/s |  32 kB     00:00
(65/158): ipxe-roms-qemu-20200823-9.git4bd064de.el9.noarch.rpm                                                                                                                    9.3 MB/s | 673 kB     00:00
(66/158): gsm-1.0.19-6.el9.x86_64.rpm                                                                                                                                             973 kB/s |  33 kB     00:00
(67/158): mesa-libglapi-23.1.4-1.el9.x86_64.rpm                                                                                                                                   1.1 MB/s |  46 kB     00:00
(68/158): mesa-libgbm-23.1.4-1.el9.x86_64.rpm                                                                                                                                     1.1 MB/s |  37 kB     00:00
(69/158): mesa-libGL-23.1.4-1.el9.x86_64.rpm                                                                                                                                      4.4 MB/s | 165 kB     00:00
(70/158): mesa-libEGL-23.1.4-1.el9.x86_64.rpm                                                                                                                                     3.2 MB/s | 124 kB     00:00
(71/158): mesa-filesystem-23.1.4-1.el9.x86_64.rpm                                                                                                                                 218 kB/s | 9.9 kB     00:00
(72/158): rubygem-rdoc-6.3.3-160.el9_0.noarch.rpm                                                                                                                                 425 kB/s | 400 kB     00:00
(73/158): mesa-dri-drivers-23.1.4-1.el9.x86_64.rpm                                                                                                                                 13 MB/s |  10 MB     00:00
(74/158): libdrm-2.4.115-1.el9.x86_64.rpm                                                                                                                                         4.6 MB/s | 156 kB     00:00
(75/158): libsndfile-1.0.31-7.el9.x86_64.rpm                                                                                                                                      4.6 MB/s | 206 kB     00:00
(76/158): flac-libs-1.3.3-10.el9_2.1.x86_64.rpm                                                                                                                                   631 kB/s | 217 kB     00:00
(77/158): jq-1.6-15.el9.x86_64.rpm                                                                                                                                                4.6 MB/s | 186 kB     00:00
(78/158): liburing-2.3-2.el9.x86_64.rpm                                                                                                                                           685 kB/s |  26 kB     00:00
(79/158): libvorbis-1.3.7-5.el9.x86_64.rpm                                                                                                                                        4.6 MB/s | 192 kB     00:00
(80/158): libslirp-4.4.0-7.el9.x86_64.rpm                                                                                                                                         615 kB/s |  68 kB     00:00
(81/158): libglvnd-glx-1.3.4-1.el9.x86_64.rpm                                                                                                                                     4.0 MB/s | 140 kB     00:00
(82/158): libglvnd-1.3.4-1.el9.x86_64.rpm                                                                                                                                         3.2 MB/s | 133 kB     00:00
(83/158): libglvnd-egl-1.3.4-1.el9.x86_64.rpm                                                                                                                                     505 kB/s |  36 kB     00:00
(84/158): libtpms-0.9.1-3.20211126git1ff6fe1f43.el9_2.x86_64.rpm                                                                                                                  1.9 MB/s | 184 kB     00:00
(85/158): libXxf86vm-1.1.4-18.el9.x86_64.rpm                                                                                                                                      617 kB/s |  18 kB     00:00
(86/158): python3-libsemanage-3.5-2.el9.x86_64.rpm                                                                                                                                659 kB/s |  79 kB     00:00
(87/158): opus-1.3.1-10.el9.x86_64.rpm                                                                                                                                            4.8 MB/s | 199 kB     00:00
(88/158): libwayland-server-1.21.0-1.el9.x86_64.rpm                                                                                                                               1.2 MB/s |  41 kB     00:00
(89/158): libwayland-client-1.21.0-1.el9.x86_64.rpm                                                                                                                               841 kB/s |  33 kB     00:00
(90/158): mdevctl-1.1.0-4.el9.x86_64.rpm                                                                                                                                          5.2 MB/s | 758 kB     00:00
(91/158): oniguruma-6.9.6-1.el9.5.x86_64.rpm                                                                                                                                      861 kB/s | 217 kB     00:00
(92/158): pixman-0.40.0-6.el9_3.x86_64.rpm                                                                                                                                        4.0 MB/s | 269 kB     00:00
(93/158): cyrus-sasl-2.1.27-21.el9.x86_64.rpm                                                                                                                                      13 kB/s |  71 kB     00:05
(94/158): pulseaudio-libs-15.0-2.el9.x86_64.rpm                                                                                                                                   1.2 MB/s | 666 kB     00:00
(95/158): libvirt-libs-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                                 9.7 MB/s | 4.8 MB     00:00
(96/158): libvirt-daemon-driver-storage-scsi-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                           165 kB/s |  30 kB     00:00
(97/158): libvirt-daemon-driver-storage-rbd-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                            627 kB/s |  38 kB     00:00
(98/158): libvirt-daemon-driver-storage-mpath-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                          669 kB/s |  27 kB     00:00
(99/158): libvirt-daemon-driver-storage-logical-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                        661 kB/s |  34 kB     00:00
(100/158): libvirt-daemon-driver-storage-disk-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                          863 kB/s |  33 kB     00:00
(101/158): libvirt-daemon-driver-storage-iscsi-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                         615 kB/s |  30 kB     00:00
(102/158): libvirt-daemon-driver-storage-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                               559 kB/s |  22 kB     00:00
(103/158): libvirt-daemon-driver-storage-core-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                          1.6 MB/s | 228 kB     00:00
(104/158): libvirt-daemon-driver-secret-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                1.2 MB/s | 170 kB     00:00
(105/158): libvirt-daemon-driver-qemu-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                  7.7 MB/s | 909 kB     00:00
(106/158): libvirt-daemon-driver-nwfilter-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                              3.3 MB/s | 209 kB     00:00
(107/158): libvirt-daemon-driver-network-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                               3.9 MB/s | 211 kB     00:00
(108/158): libvirt-daemon-driver-interface-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                             3.7 MB/s | 173 kB     00:00
(109/158): libvirt-daemon-config-network-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                               585 kB/s |  24 kB     00:00
(110/158): libvirt-daemon-config-nwfilter-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                              404 kB/s |  30 kB     00:00
(111/158): libvirt-daemon-driver-nodedev-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                               1.3 MB/s | 194 kB     00:00
(112/158): libvirt-daemon-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                              3.5 MB/s | 167 kB     00:00
(113/158): libvirt-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                                     402 kB/s |  22 kB     00:00
(114/158): boost-thread-1.75.0-8.el9.x86_64.rpm                                                                                                                                   1.6 MB/s |  55 kB     00:00
(115/158): libvirt-client-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                              4.7 MB/s | 426 kB     00:00
(116/158): boost-system-1.75.0-8.el9.x86_64.rpm                                                                                                                                   280 kB/s |  13 kB     00:00
(117/158): boost-iostreams-1.75.0-8.el9.x86_64.rpm                                                                                                                                849 kB/s |  38 kB     00:00
(118/158): swtpm-tools-0.8.0-1.el9.x86_64.rpm                                                                                                                                     1.8 MB/s | 117 kB     00:00
(119/158): swtpm-0.8.0-1.el9.x86_64.rpm                                                                                                                                           934 kB/s |  42 kB     00:00
(120/158): swtpm-libs-0.8.0-1.el9.x86_64.rpm                                                                                                                                      919 kB/s |  50 kB     00:00
(121/158): qemu-pr-helper-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                             1.7 MB/s | 486 kB     00:00
(122/158): librbd1-16.2.4-5.el9.x86_64.rpm                                                                                                                                        9.2 MB/s | 3.0 MB     00:00
(123/158): qemu-kvm-ui-egl-headless-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                   826 kB/s |  67 kB     00:00
(124/158): qemu-kvm-ui-opengl-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                         707 kB/s |  73 kB     00:00
(125/158): qemu-kvm-tools-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                             3.9 MB/s | 559 kB     00:00
(126/158): qemu-kvm-device-usb-redirect-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                               1.3 MB/s |  85 kB     00:00
(127/158): librados2-16.2.4-5.el9.x86_64.rpm                                                                                                                                      5.5 MB/s | 3.4 MB     00:00
(128/158): qemu-kvm-device-usb-host-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                   1.2 MB/s |  80 kB     00:00
(129/158): qemu-kvm-device-display-virtio-vga-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                         1.1 MB/s |  67 kB     00:00
(130/158): qemu-kvm-device-display-virtio-gpu-pci-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                     1.1 MB/s |  66 kB     00:00
(131/158): qemu-kvm-device-display-virtio-gpu-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                         1.6 MB/s |  81 kB     00:00
(132/158): qemu-kvm-common-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                            4.7 MB/s | 642 kB     00:00
(133/158): qemu-kvm-block-rbd-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                         987 kB/s |  76 kB     00:00
(134/158): qemu-kvm-audio-pa-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                          901 kB/s |  71 kB     00:00
(135/158): qemu-kvm-docs-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                              1.7 MB/s | 1.1 MB     00:00
(136/158): qemu-kvm-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                                   1.2 MB/s |  62 kB     00:00
(137/158): qemu-kvm-core-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                               11 MB/s | 4.1 MB     00:00
(138/158): rubygem-psych-3.3.2-160.el9_0.x86_64.rpm                                                                                                                               980 kB/s |  51 kB     00:00
(139/158): rubygem-json-2.5.1-160.el9_0.x86_64.rpm                                                                                                                                1.1 MB/s |  54 kB     00:00
(140/158): rubygem-io-console-0.5.7-160.el9_0.x86_64.rpm                                                                                                                          549 kB/s |  25 kB     00:00
(141/158): rubygem-bigdecimal-3.0.0-160.el9_0.x86_64.rpm                                                                                                                          1.3 MB/s |  54 kB     00:00
(142/158): ruby-3.0.4-160.el9_0.x86_64.rpm                                                                                                                                        788 kB/s |  41 kB     00:00
(143/158): libnbd-1.16.0-1.el9.x86_64.rpm                                                                                                                                         1.7 MB/s | 160 kB     00:00
(144/158): ruby-libs-3.0.4-160.el9_0.x86_64.rpm                                                                                                                                   8.1 MB/s | 3.2 MB     00:00
(145/158): libX11-xcb-1.7.0-8.el9.x86_64.rpm                                                                                                                                      188 kB/s | 9.9 kB     00:00
(146/158): dnsmasq-2.85-14.el9_3.1.x86_64.rpm                                                                                                                                     4.2 MB/s | 325 kB     00:00
(147/158): edk2-ovmf-20230524-4.el9_3.2.noarch.rpm                                                                                                                                 12 MB/s | 5.4 MB     00:00
(148/158): capstone-4.0.2-10.el9.x86_64.rpm                                                                                                                                       5.5 MB/s | 766 kB     00:00
(149/158): passt-0^20230818.g0af928e-4.el9.x86_64.rpm                                                                                                                             3.5 MB/s | 177 kB     00:00
(150/158): passt-selinux-0^20230818.g0af928e-4.el9.noarch.rpm                                                                                                                     727 kB/s |  29 kB     00:00
(151/158): qemu-img-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                                   2.8 MB/s | 2.4 MB     00:00
(152/158): qemu-kvm-block-blkio-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                       1.3 MB/s |  74 kB     00:00
(153/158): libblkio-1.3.0-1.el9.x86_64.rpm                                                                                                                                        6.5 MB/s | 512 kB     00:00
(154/158): libvirt-daemon-plugin-lockd-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                 811 kB/s |  33 kB     00:00
(155/158): libvirt-daemon-lock-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                         1.3 MB/s |  58 kB     00:00
(156/158): libvirt-daemon-proxy-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                        1.3 MB/s | 165 kB     00:00
(157/158): libvirt-daemon-common-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                       2.7 MB/s | 128 kB     00:00
(158/158): libvirt-daemon-log-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                          172 kB/s |  61 kB     00:00
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                                                                                             1.9 MB/s |  63 MB     00:33
OpenNebula Community Edition                                                                                                                                                       17 kB/s | 3.1 kB     00:00
Importing GPG key 0x906DC27C:
 Userid     : "OpenNebula Repository <contact@opennebula.io>"
 Fingerprint: 0B2D 385C 7C93 04B1 1A03 67B9 05A0 5927 906D C27C
 From       : https://downloads.opennebula.io/repo/repo2.key
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                                                                                                          1/1
  Installing       : ruby-libs-3.0.4-160.el9_0.x86_64                                                                                                                                                       1/158
  Installing       : rubygem-psych-3.3.2-160.el9_0.x86_64                                                                                                                                                   2/158
  Installing       : rubygem-json-2.5.1-160.el9_0.x86_64                                                                                                                                                    3/158
  Installing       : rubygem-io-console-0.5.7-160.el9_0.x86_64                                                                                                                                              4/158
  Installing       : rubygem-bigdecimal-3.0.0-160.el9_0.x86_64                                                                                                                                              5/158
  Installing       : rubygem-rdoc-6.3.3-160.el9_0.noarch                                                                                                                                                    6/158
  Installing       : rubygem-bundler-2.2.33-160.el9_0.noarch                                                                                                                                                7/158
  Installing       : ruby-default-gems-3.0.4-160.el9_0.noarch                                                                                                                                               8/158
  Installing       : ruby-3.0.4-160.el9_0.x86_64                                                                                                                                                            9/158
  Installing       : rubygems-3.2.33-160.el9_0.noarch                                                                                                                                                      10/158
  Installing       : libX11-xcb-1.7.0-8.el9.x86_64                                                                                                                                                         11/158
  Installing       : pixman-0.40.0-6.el9_3.x86_64                                                                                                                                                          12/158
  Installing       : liburing-2.3-2.el9.x86_64                                                                                                                                                             13/158
  Installing       : qemu-img-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                                   14/158
  Installing       : mesa-libglapi-23.1.4-1.el9.x86_64                                                                                                                                                     15/158
  Installing       : libogg-2:1.3.4-6.el9.x86_64                                                                                                                                                           16/158
  Installing       : libxshmfence-1.3-10.el9.x86_64                                                                                                                                                        17/158
  Installing       : libusbx-1.0.26-1.el9.x86_64                                                                                                                                                           18/158
  Installing       : passt-0^20230818.g0af928e-4.el9.x86_64                                                                                                                                                19/158
  Running scriptlet: passt-selinux-0^20230818.g0af928e-4.el9.noarch                                                                                                                                        20/158
  Installing       : passt-selinux-0^20230818.g0af928e-4.el9.noarch                                                                                                                                        20/158
  Running scriptlet: passt-selinux-0^20230818.g0af928e-4.el9.noarch                                                                                                                                        20/158
  Installing       : libwayland-server-1.21.0-1.el9.x86_64                                                                                                                                                 21/158
  Installing       : libtpms-0.9.1-3.20211126git1ff6fe1f43.el9_2.x86_64                                                                                                                                    22/158
  Installing       : libglvnd-1:1.3.4-1.el9.x86_64                                                                                                                                                         23/158
  Installing       : libepoxy-1.5.5-4.el9.x86_64                                                                                                                                                           24/158
  Installing       : libnfsidmap-1:2.5.4-20.el9.x86_64                                                                                                                                                     25/158
  Installing       : daxctl-libs-71.1-8.el9.x86_64                                                                                                                                                         26/158
  Installing       : libpciaccess-0.16-6.el9.x86_64                                                                                                                                                        27/158
  Installing       : libdrm-2.4.115-1.el9.x86_64                                                                                                                                                           28/158
  Installing       : mesa-libgbm-23.1.4-1.el9.x86_64                                                                                                                                                       29/158
  Installing       : python3-setuptools-53.0.0-12.el9.noarch                                                                                                                                               30/158
  Installing       : librdmacm-46.0-1.el9.x86_64                                                                                                                                                           31/158
  Installing       : python3-setools-4.4.3-1.el9.x86_64                                                                                                                                                    32/158
  Installing       : python3-distro-1.5.0-7.el9.noarch                                                                                                                                                     33/158
  Installing       : ndctl-libs-71.1-8.el9.x86_64                                                                                                                                                          34/158
  Installing       : libpmem-1.12.1-1.el9.x86_64                                                                                                                                                           35/158
  Installing       : swtpm-libs-0.8.0-1.el9.x86_64                                                                                                                                                         36/158
  Installing       : usbredir-0.13.0-2.el9.x86_64                                                                                                                                                          37/158
  Installing       : flac-libs-1.3.3-10.el9_2.1.x86_64                                                                                                                                                     38/158
  Installing       : libvorbis-1:1.3.7-5.el9.x86_64                                                                                                                                                        39/158
  Installing       : rubygem-sqlite3-1.4.2-8.el9.x86_64                                                                                                                                                    40/158
  Installing       : rubygem-rexml-3.2.5-160.el9_0.noarch                                                                                                                                                  41/158
  Installing       : libblkio-1.3.0-1.el9.x86_64                                                                                                                                                           42/158
  Installing       : capstone-4.0.2-10.el9.x86_64                                                                                                                                                          43/158
  Running scriptlet: dnsmasq-2.85-14.el9_3.1.x86_64                                                                                                                                                        44/158
  Installing       : dnsmasq-2.85-14.el9_3.1.x86_64                                                                                                                                                        44/158
  Running scriptlet: dnsmasq-2.85-14.el9_3.1.x86_64                                                                                                                                                        44/158
  Installing       : edk2-ovmf-20230524-4.el9_3.2.noarch                                                                                                                                                   45/158
  Installing       : libnbd-1.16.0-1.el9.x86_64                                                                                                                                                            46/158
  Installing       : qemu-kvm-docs-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                              47/158
  Installing       : boost-iostreams-1.75.0-8.el9.x86_64                                                                                                                                                   48/158
  Installing       : boost-system-1.75.0-8.el9.x86_64                                                                                                                                                      49/158
  Installing       : boost-thread-1.75.0-8.el9.x86_64                                                                                                                                                      50/158
  Installing       : librados2-2:16.2.4-5.el9.x86_64                                                                                                                                                       51/158
  Running scriptlet: librados2-2:16.2.4-5.el9.x86_64                                                                                                                                                       51/158
  Installing       : librbd1-2:16.2.4-5.el9.x86_64                                                                                                                                                         52/158
  Running scriptlet: librbd1-2:16.2.4-5.el9.x86_64                                                                                                                                                         52/158
  Installing       : mdevctl-1.1.0-4.el9.x86_64                                                                                                                                                            53/158
  Installing       : libwayland-client-1.21.0-1.el9.x86_64                                                                                                                                                 54/158
  Installing       : libglvnd-egl-1:1.3.4-1.el9.x86_64                                                                                                                                                     55/158
  Installing       : mesa-libEGL-23.1.4-1.el9.x86_64                                                                                                                                                       56/158
  Installing       : oniguruma-6.9.6-1.el9.5.x86_64                                                                                                                                                        57/158
  Installing       : jq-1.6-15.el9.x86_64                                                                                                                                                                  58/158
  Installing       : opus-1.3.1-10.el9.x86_64                                                                                                                                                              59/158
  Installing       : libXxf86vm-1.1.4-18.el9.x86_64                                                                                                                                                        60/158
  Installing       : python3-libsemanage-3.5-2.el9.x86_64                                                                                                                                                  61/158
  Installing       : libslirp-4.4.0-7.el9.x86_64                                                                                                                                                           62/158
  Installing       : mesa-filesystem-23.1.4-1.el9.x86_64                                                                                                                                                   63/158
  Installing       : mesa-dri-drivers-23.1.4-1.el9.x86_64                                                                                                                                                  64/158
  Installing       : gsm-1.0.19-6.el9.x86_64                                                                                                                                                               65/158
  Installing       : libsndfile-1.0.31-7.el9.x86_64                                                                                                                                                        66/158
  Installing       : ipxe-roms-qemu-20200823-9.git4bd064de.el9.noarch                                                                                                                                      67/158
  Running scriptlet: unbound-libs-1.16.2-3.el9_3.1.x86_64                                                                                                                                                  68/158
  Installing       : unbound-libs-1.16.2-3.el9_3.1.x86_64                                                                                                                                                  68/158
  Running scriptlet: unbound-libs-1.16.2-3.el9_3.1.x86_64                                                                                                                                                  68/158
Created symlink /etc/systemd/system/timers.target.wants/unbound-anchor.timer → /usr/lib/systemd/system/unbound-anchor.timer.

  Installing       : gnutls-dane-3.7.6-23.el9_3.3.x86_64                                                                                                                                                   69/158
  Installing       : gnutls-utils-3.7.6-23.el9_3.3.x86_64                                                                                                                                                  70/158
  Installing       : virtiofsd-1.7.2-1.el9.x86_64                                                                                                                                                          71/158
  Installing       : seabios-bin-1.16.1-1.el9.noarch                                                                                                                                                       72/158
  Installing       : seavgabios-bin-1.16.1-1.el9.noarch                                                                                                                                                    73/158
  Installing       : qemu-kvm-common-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                            74/158
  Running scriptlet: qemu-kvm-common-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                            74/158
  Installing       : qemu-kvm-device-display-virtio-gpu-17:8.0.0-16.el9_3.3.x86_64                                                                                                                         75/158
  Installing       : qemu-kvm-device-display-virtio-gpu-pci-17:8.0.0-16.el9_3.3.x86_64                                                                                                                     76/158
  Installing       : qemu-kvm-device-usb-redirect-17:8.0.0-16.el9_3.3.x86_64                                                                                                                               77/158
  Installing       : qemu-kvm-device-usb-host-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                   78/158
  Installing       : qemu-kvm-device-display-virtio-vga-17:8.0.0-16.el9_3.3.x86_64                                                                                                                         79/158
  Installing       : qemu-kvm-block-rbd-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                         80/158
  Installing       : qemu-kvm-block-blkio-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                       81/158
  Installing       : yajl-2.1.0-22.el9.x86_64                                                                                                                                                              82/158
  Installing       : xkeyboard-config-2.33-2.el9.noarch                                                                                                                                                    83/158
  Installing       : libxkbcommon-1.0.3-4.el9.x86_64                                                                                                                                                       84/158
  Installing       : qemu-kvm-tools-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                             85/158
  Installing       : libXfixes-5.0.3-16.el9.x86_64                                                                                                                                                         86/158
  Installing       : libglvnd-glx-1:1.3.4-1.el9.x86_64                                                                                                                                                     87/158
  Installing       : mesa-libGL-23.1.4-1.el9.x86_64                                                                                                                                                        88/158
  Installing       : qemu-kvm-ui-opengl-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                         89/158
  Installing       : qemu-kvm-ui-egl-headless-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                   90/158
  Installing       : libasyncns-0.8-22.el9.x86_64                                                                                                                                                          91/158
  Installing       : pulseaudio-libs-15.0-2.el9.x86_64                                                                                                                                                     92/158
  Installing       : qemu-kvm-audio-pa-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                          93/158
  Installing       : python3-audit-3.0.7-104.el9.x86_64                                                                                                                                                    94/158
  Installing       : libfdt-1.6.0-7.el9.x86_64                                                                                                                                                             95/158
  Installing       : qemu-kvm-core-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                              96/158
  Installing       : checkpolicy-3.5-1.el9.x86_64                                                                                                                                                          97/158
  Installing       : python3-policycoreutils-3.5-3.el9_3.noarch                                                                                                                                            98/158
  Installing       : policycoreutils-python-utils-3.5-3.el9_3.noarch                                                                                                                                       99/158
  Installing       : swtpm-0.8.0-1.el9.x86_64                                                                                                                                                             100/158
  Running scriptlet: swtpm-0.8.0-1.el9.x86_64                                                                                                                                                             100/158
  Installing       : augeas-libs-1.13.0-5.el9.x86_64                                                                                                                                                      101/158
  Installing       : augeas-1.13.0-5.el9.x86_64                                                                                                                                                           102/158
  Installing       : device-mapper-multipath-libs-0.8.7-22.el9.x86_64                                                                                                                                     103/158
  Installing       : qemu-pr-helper-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                            104/158
  Installing       : qemu-kvm-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                                  105/158
  Installing       : keyutils-1.6.3-1.el9.x86_64                                                                                                                                                          106/158
  Running scriptlet: cyrus-sasl-2.1.27-21.el9.x86_64                                                                                                                                                      107/158
  Installing       : cyrus-sasl-2.1.27-21.el9.x86_64                                                                                                                                                      107/158
  Running scriptlet: cyrus-sasl-2.1.27-21.el9.x86_64                                                                                                                                                      107/158
  Installing       : cyrus-sasl-gssapi-2.1.27-21.el9.x86_64                                                                                                                                               108/158
  Installing       : libvirt-libs-9.5.0-7.2.el9_3.x86_64                                                                                                                                                  109/158
  Installing       : libvirt-client-9.5.0-7.2.el9_3.x86_64                                                                                                                                                110/158
  Running scriptlet: libvirt-daemon-common-9.5.0-7.2.el9_3.x86_64                                                                                                                                         111/158
  Installing       : libvirt-daemon-common-9.5.0-7.2.el9_3.x86_64                                                                                                                                         111/158
  Running scriptlet: libvirt-daemon-driver-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                112/158
  Installing       : libvirt-daemon-driver-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                112/158
  Running scriptlet: libvirt-daemon-driver-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 113/158
  Installing       : libvirt-daemon-driver-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 113/158
  Running scriptlet: libvirt-daemon-driver-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 113/158
  Running scriptlet: libvirt-daemon-log-9.5.0-7.2.el9_3.x86_64                                                                                                                                            114/158
  Installing       : libvirt-daemon-log-9.5.0-7.2.el9_3.x86_64                                                                                                                                            114/158
  Running scriptlet: libvirt-daemon-lock-9.5.0-7.2.el9_3.x86_64                                                                                                                                           115/158
  Installing       : libvirt-daemon-lock-9.5.0-7.2.el9_3.x86_64                                                                                                                                           115/158
  Installing       : libvirt-daemon-plugin-lockd-9.5.0-7.2.el9_3.x86_64                                                                                                                                   116/158
  Running scriptlet: libvirt-daemon-config-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 117/158
  Installing       : libvirt-daemon-config-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 117/158
  Running scriptlet: libvirt-daemon-config-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 117/158
  Running scriptlet: libvirt-daemon-config-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                118/158
  Installing       : libvirt-daemon-config-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                118/158
  Running scriptlet: libvirt-daemon-config-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                118/158
  Running scriptlet: libvirt-daemon-driver-secret-9.5.0-7.2.el9_3.x86_64                                                                                                                                  119/158
  Installing       : libvirt-daemon-driver-secret-9.5.0-7.2.el9_3.x86_64                                                                                                                                  119/158
  Running scriptlet: libvirt-daemon-driver-nodedev-9.5.0-7.2.el9_3.x86_64                                                                                                                                 120/158
  Installing       : libvirt-daemon-driver-nodedev-9.5.0-7.2.el9_3.x86_64                                                                                                                                 120/158
  Running scriptlet: libvirt-daemon-driver-interface-9.5.0-7.2.el9_3.x86_64                                                                                                                               121/158
  Installing       : libvirt-daemon-driver-interface-9.5.0-7.2.el9_3.x86_64                                                                                                                               121/158
  Installing       : lzop-1.04-8.el9.x86_64                                                                                                                                                               122/158
  Installing       : systemd-container-252-18.el9.0.1.rocky.x86_64                                                                                                                                        123/158
  Installing       : pciutils-3.7.0-5.el9.x86_64                                                                                                                                                          124/158
  Installing       : libev-4.33-5.el9.x86_64                                                                                                                                                              125/158
  Installing       : libverto-libev-0.3.2-3.el9.x86_64                                                                                                                                                    126/158
  Installing       : gssproxy-0.8.4-6.el9.x86_64                                                                                                                                                          127/158
  Running scriptlet: gssproxy-0.8.4-6.el9.x86_64                                                                                                                                                          127/158
  Installing       : isns-utils-libs-0.101-4.el9.x86_64                                                                                                                                                   128/158
  Installing       : iscsi-initiator-utils-iscsiuio-6.2.1.4-3.git2a8f9d8.el9.x86_64                                                                                                                       129/158
  Running scriptlet: iscsi-initiator-utils-iscsiuio-6.2.1.4-3.git2a8f9d8.el9.x86_64                                                                                                                       129/158
Created symlink /etc/systemd/system/sockets.target.wants/iscsiuio.socket → /usr/lib/systemd/system/iscsiuio.socket.

  Installing       : iscsi-initiator-utils-6.2.1.4-3.git2a8f9d8.el9.x86_64                                                                                                                                130/158
  Running scriptlet: iscsi-initiator-utils-6.2.1.4-3.git2a8f9d8.el9.x86_64                                                                                                                                130/158
Created symlink /etc/systemd/system/remote-fs.target.wants/iscsi.service → /usr/lib/systemd/system/iscsi.service.
Created symlink /etc/systemd/system/sockets.target.wants/iscsid.socket → /usr/lib/systemd/system/iscsid.socket.
Created symlink /etc/systemd/system/sysinit.target.wants/iscsi-onboot.service → /usr/lib/systemd/system/iscsi-onboot.service.

  Running scriptlet: rpcbind-1.2.6-5.el9.x86_64                                                                                                                                                           131/158
  Installing       : rpcbind-1.2.6-5.el9.x86_64                                                                                                                                                           131/158
  Running scriptlet: rpcbind-1.2.6-5.el9.x86_64                                                                                                                                                           131/158
Created symlink /etc/systemd/system/multi-user.target.wants/rpcbind.service → /usr/lib/systemd/system/rpcbind.service.
Created symlink /etc/systemd/system/sockets.target.wants/rpcbind.socket → /usr/lib/systemd/system/rpcbind.socket.

  Installing       : numad-0.5-36.20150602git.el9.x86_64                                                                                                                                                  132/158
  Running scriptlet: numad-0.5-36.20150602git.el9.x86_64                                                                                                                                                  132/158
  Installing       : python3-pyyaml-5.4.1-6.el9.x86_64                                                                                                                                                    133/158
  Installing       : quota-nls-1:4.06-6.el9.noarch                                                                                                                                                        134/158
  Installing       : quota-1:4.06-6.el9.x86_64                                                                                                                                                            135/158
  Running scriptlet: nfs-utils-1:2.5.4-20.el9.x86_64                                                                                                                                                      136/158
  Installing       : nfs-utils-1:2.5.4-20.el9.x86_64                                                                                                                                                      136/158
  Running scriptlet: nfs-utils-1:2.5.4-20.el9.x86_64                                                                                                                                                      136/158
  Running scriptlet: libvirt-daemon-driver-storage-core-9.5.0-7.2.el9_3.x86_64                                                                                                                            137/158
  Installing       : libvirt-daemon-driver-storage-core-9.5.0-7.2.el9_3.x86_64                                                                                                                            137/158
  Installing       : libvirt-daemon-driver-storage-scsi-9.5.0-7.2.el9_3.x86_64                                                                                                                            138/158
  Installing       : libvirt-daemon-driver-storage-rbd-9.5.0-7.2.el9_3.x86_64                                                                                                                             139/158
  Installing       : libvirt-daemon-driver-storage-mpath-9.5.0-7.2.el9_3.x86_64                                                                                                                           140/158
  Installing       : libvirt-daemon-driver-storage-logical-9.5.0-7.2.el9_3.x86_64                                                                                                                         141/158
  Installing       : libvirt-daemon-driver-storage-iscsi-9.5.0-7.2.el9_3.x86_64                                                                                                                           142/158
  Installing       : libvirt-daemon-driver-storage-disk-9.5.0-7.2.el9_3.x86_64                                                                                                                            143/158
  Installing       : libvirt-daemon-driver-storage-9.5.0-7.2.el9_3.x86_64                                                                                                                                 144/158
  Installing       : json-glib-1.6.6-1.el9.x86_64                                                                                                                                                         145/158
  Installing       : swtpm-tools-0.8.0-1.el9.x86_64                                                                                                                                                       146/158
  Running scriptlet: libvirt-daemon-driver-qemu-9.5.0-7.2.el9_3.x86_64                                                                                                                                    147/158
  Installing       : libvirt-daemon-driver-qemu-9.5.0-7.2.el9_3.x86_64                                                                                                                                    147/158
  Running scriptlet: opennebula-common-onecfg-6.8.0-1.el9.noarch                                                                                                                                          148/158
  Installing       : opennebula-common-onecfg-6.8.0-1.el9.noarch                                                                                                                                          148/158
  Running scriptlet: opennebula-common-6.8.0-1.el9.noarch                                                                                                                                                 149/158
  Installing       : opennebula-common-6.8.0-1.el9.noarch                                                                                                                                                 149/158
  Running scriptlet: opennebula-common-6.8.0-1.el9.noarch                                                                                                                                                 149/158
  Installing       : libretls-3.8.1-1.el9.x86_64                                                                                                                                                          150/158
  Installing       : libmd-1.1.0-1.el9.x86_64                                                                                                                                                             151/158
  Installing       : libbsd-0.11.7-2.el9.x86_64                                                                                                                                                           152/158
  Installing       : netcat-1.226-1.el9.x86_64                                                                                                                                                            153/158
  Running scriptlet: netcat-1.226-1.el9.x86_64                                                                                                                                                            153/158
  Running scriptlet: libvirt-daemon-proxy-9.5.0-7.2.el9_3.x86_64                                                                                                                                          154/158
  Installing       : libvirt-daemon-proxy-9.5.0-7.2.el9_3.x86_64                                                                                                                                          154/158
  Running scriptlet: libvirt-daemon-9.5.0-7.2.el9_3.x86_64                                                                                                                                                155/158
  Installing       : libvirt-daemon-9.5.0-7.2.el9_3.x86_64                                                                                                                                                155/158
  Installing       : libvirt-9.5.0-7.2.el9_3.x86_64                                                                                                                                                       156/158
  Installing       : opennebula-node-kvm-6.8.0-1.el9.noarch                                                                                                                                               157/158
  Running scriptlet: opennebula-node-kvm-6.8.0-1.el9.noarch                                                                                                                                               157/158
  Installing       : sssd-nfs-idmap-2.9.1-4.el9_3.5.x86_64                                                                                                                                                158/158
  Running scriptlet: passt-selinux-0^20230818.g0af928e-4.el9.noarch                                                                                                                                       158/158
  Running scriptlet: swtpm-0.8.0-1.el9.x86_64                                                                                                                                                             158/158
  Running scriptlet: libvirt-daemon-common-9.5.0-7.2.el9_3.x86_64                                                                                                                                         158/158
  Running scriptlet: libvirt-daemon-driver-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtnwfilterd.socket → /usr/lib/systemd/system/virtnwfilterd.socket.

  Running scriptlet: libvirt-daemon-driver-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtnetworkd.socket → /usr/lib/systemd/system/virtnetworkd.socket.

  Running scriptlet: libvirt-daemon-log-9.5.0-7.2.el9_3.x86_64                                                                                                                                            158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtlogd.socket → /usr/lib/systemd/system/virtlogd.socket.

  Running scriptlet: libvirt-daemon-lock-9.5.0-7.2.el9_3.x86_64                                                                                                                                           158/158
  Running scriptlet: libvirt-daemon-config-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 158/158
  Running scriptlet: libvirt-daemon-config-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                158/158
  Running scriptlet: libvirt-daemon-driver-secret-9.5.0-7.2.el9_3.x86_64                                                                                                                                  158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtsecretd.socket → /usr/lib/systemd/system/virtsecretd.socket.

  Running scriptlet: libvirt-daemon-driver-nodedev-9.5.0-7.2.el9_3.x86_64                                                                                                                                 158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtnodedevd.socket → /usr/lib/systemd/system/virtnodedevd.socket.

  Running scriptlet: libvirt-daemon-driver-interface-9.5.0-7.2.el9_3.x86_64                                                                                                                               158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtinterfaced.socket → /usr/lib/systemd/system/virtinterfaced.socket.

  Running scriptlet: libvirt-daemon-driver-storage-core-9.5.0-7.2.el9_3.x86_64                                                                                                                            158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtstoraged.socket → /usr/lib/systemd/system/virtstoraged.socket.

  Running scriptlet: libvirt-daemon-driver-qemu-9.5.0-7.2.el9_3.x86_64                                                                                                                                    158/158
Created symlink /etc/systemd/system/multi-user.target.wants/virtqemud.service → /usr/lib/systemd/system/virtqemud.service.
Created symlink /etc/systemd/system/sockets.target.wants/virtlockd.socket → /usr/lib/systemd/system/virtlockd.socket.
Created symlink /etc/systemd/system/sockets.target.wants/virtqemud.socket → /usr/lib/systemd/system/virtqemud.socket.
Created symlink /etc/systemd/system/sockets.target.wants/virtqemud-ro.socket → /usr/lib/systemd/system/virtqemud-ro.socket.
Created symlink /etc/systemd/system/sockets.target.wants/virtqemud-admin.socket → /usr/lib/systemd/system/virtqemud-admin.socket.

  Running scriptlet: libvirt-daemon-proxy-9.5.0-7.2.el9_3.x86_64                                                                                                                                          158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtproxyd.socket → /usr/lib/systemd/system/virtproxyd.socket.

  Running scriptlet: libvirt-daemon-9.5.0-7.2.el9_3.x86_64                                                                                                                                                158/158
  Running scriptlet: sssd-nfs-idmap-2.9.1-4.el9_3.5.x86_64                                                                                                                                                158/158
Couldn't write '64' to 'kernel/random/read_wakeup_threshold', ignoring: No such file or directory
Couldn't write '1' to 'net/bridge/bridge-nf-call-arptables', ignoring: No such file or directory
Couldn't write '1' to 'net/bridge/bridge-nf-call-ip6tables', ignoring: No such file or directory
Couldn't write '1' to 'net/bridge/bridge-nf-call-iptables', ignoring: No such file or directory

  Verifying        : libbsd-0.11.7-2.el9.x86_64                                                                                                                                                             1/158
  Verifying        : libmd-1.1.0-1.el9.x86_64                                                                                                                                                               2/158
  Verifying        : libretls-3.8.1-1.el9.x86_64                                                                                                                                                            3/158
  Verifying        : netcat-1.226-1.el9.x86_64                                                                                                                                                              4/158
  Verifying        : rubygem-sqlite3-1.4.2-8.el9.x86_64                                                                                                                                                     5/158
  Verifying        : opennebula-common-6.8.0-1.el9.noarch                                                                                                                                                   6/158
  Verifying        : opennebula-common-onecfg-6.8.0-1.el9.noarch                                                                                                                                            7/158
  Verifying        : opennebula-node-kvm-6.8.0-1.el9.noarch                                                                                                                                                 8/158
  Verifying        : libverto-libev-0.3.2-3.el9.x86_64                                                                                                                                                      9/158
  Verifying        : json-glib-1.6.6-1.el9.x86_64                                                                                                                                                          10/158
  Verifying        : librdmacm-46.0-1.el9.x86_64                                                                                                                                                           11/158
  Verifying        : quota-1:4.06-6.el9.x86_64                                                                                                                                                             12/158
  Verifying        : quota-nls-1:4.06-6.el9.noarch                                                                                                                                                         13/158
  Verifying        : python3-pyyaml-5.4.1-6.el9.x86_64                                                                                                                                                     14/158
  Verifying        : python3-setuptools-53.0.0-12.el9.noarch                                                                                                                                               15/158
  Verifying        : numad-0.5-36.20150602git.el9.x86_64                                                                                                                                                   16/158
  Verifying        : rpcbind-1.2.6-5.el9.x86_64                                                                                                                                                            17/158
  Verifying        : isns-utils-libs-0.101-4.el9.x86_64                                                                                                                                                    18/158
  Verifying        : libev-4.33-5.el9.x86_64                                                                                                                                                               19/158
  Verifying        : libpciaccess-0.16-6.el9.x86_64                                                                                                                                                        20/158
  Verifying        : libusbx-1.0.26-1.el9.x86_64                                                                                                                                                           21/158
  Verifying        : iscsi-initiator-utils-iscsiuio-6.2.1.4-3.git2a8f9d8.el9.x86_64                                                                                                                        22/158
  Verifying        : iscsi-initiator-utils-6.2.1.4-3.git2a8f9d8.el9.x86_64                                                                                                                                 23/158
  Verifying        : pciutils-3.7.0-5.el9.x86_64                                                                                                                                                           24/158
  Verifying        : ndctl-libs-71.1-8.el9.x86_64                                                                                                                                                          25/158
  Verifying        : daxctl-libs-71.1-8.el9.x86_64                                                                                                                                                         26/158
  Verifying        : python3-setools-4.4.3-1.el9.x86_64                                                                                                                                                    27/158
  Verifying        : sssd-nfs-idmap-2.9.1-4.el9_3.5.x86_64                                                                                                                                                 28/158
  Verifying        : systemd-container-252-18.el9.0.1.rocky.x86_64                                                                                                                                         29/158
  Verifying        : gssproxy-0.8.4-6.el9.x86_64                                                                                                                                                           30/158
  Verifying        : lzop-1.04-8.el9.x86_64                                                                                                                                                                31/158
  Verifying        : nfs-utils-1:2.5.4-20.el9.x86_64                                                                                                                                                       32/158
  Verifying        : libnfsidmap-1:2.5.4-20.el9.x86_64                                                                                                                                                     33/158
  Verifying        : cyrus-sasl-gssapi-2.1.27-21.el9.x86_64                                                                                                                                                34/158
  Verifying        : cyrus-sasl-2.1.27-21.el9.x86_64                                                                                                                                                       35/158
  Verifying        : keyutils-1.6.3-1.el9.x86_64                                                                                                                                                           36/158
  Verifying        : device-mapper-multipath-libs-0.8.7-22.el9.x86_64                                                                                                                                      37/158
  Verifying        : python3-distro-1.5.0-7.el9.noarch                                                                                                                                                     38/158
  Verifying        : augeas-libs-1.13.0-5.el9.x86_64                                                                                                                                                       39/158
  Verifying        : augeas-1.13.0-5.el9.x86_64                                                                                                                                                            40/158
  Verifying        : checkpolicy-3.5-1.el9.x86_64                                                                                                                                                          41/158
  Verifying        : libfdt-1.6.0-7.el9.x86_64                                                                                                                                                             42/158
  Verifying        : python3-audit-3.0.7-104.el9.x86_64                                                                                                                                                    43/158
  Verifying        : libasyncns-0.8-22.el9.x86_64                                                                                                                                                          44/158
  Verifying        : libxkbcommon-1.0.3-4.el9.x86_64                                                                                                                                                       45/158
  Verifying        : libxshmfence-1.3-10.el9.x86_64                                                                                                                                                        46/158
  Verifying        : libepoxy-1.5.5-4.el9.x86_64                                                                                                                                                           47/158
  Verifying        : libXfixes-5.0.3-16.el9.x86_64                                                                                                                                                         48/158
  Verifying        : libogg-2:1.3.4-6.el9.x86_64                                                                                                                                                           49/158
  Verifying        : python3-policycoreutils-3.5-3.el9_3.noarch                                                                                                                                            50/158
  Verifying        : policycoreutils-python-utils-3.5-3.el9_3.noarch                                                                                                                                       51/158
  Verifying        : usbredir-0.13.0-2.el9.x86_64                                                                                                                                                          52/158
  Verifying        : xkeyboard-config-2.33-2.el9.noarch                                                                                                                                                    53/158
  Verifying        : yajl-2.1.0-22.el9.x86_64                                                                                                                                                              54/158
  Verifying        : seavgabios-bin-1.16.1-1.el9.noarch                                                                                                                                                    55/158
  Verifying        : seabios-bin-1.16.1-1.el9.noarch                                                                                                                                                       56/158
  Verifying        : libpmem-1.12.1-1.el9.x86_64                                                                                                                                                           57/158
  Verifying        : virtiofsd-1.7.2-1.el9.x86_64                                                                                                                                                          58/158
  Verifying        : unbound-libs-1.16.2-3.el9_3.1.x86_64                                                                                                                                                  59/158
  Verifying        : gnutls-utils-3.7.6-23.el9_3.3.x86_64                                                                                                                                                  60/158
  Verifying        : gnutls-dane-3.7.6-23.el9_3.3.x86_64                                                                                                                                                   61/158
  Verifying        : rubygems-3.2.33-160.el9_0.noarch                                                                                                                                                      62/158
  Verifying        : rubygem-rexml-3.2.5-160.el9_0.noarch                                                                                                                                                  63/158
  Verifying        : rubygem-rdoc-6.3.3-160.el9_0.noarch                                                                                                                                                   64/158
  Verifying        : rubygem-bundler-2.2.33-160.el9_0.noarch                                                                                                                                               65/158
  Verifying        : ruby-default-gems-3.0.4-160.el9_0.noarch                                                                                                                                              66/158
  Verifying        : ipxe-roms-qemu-20200823-9.git4bd064de.el9.noarch                                                                                                                                      67/158
  Verifying        : gsm-1.0.19-6.el9.x86_64                                                                                                                                                               68/158
  Verifying        : mesa-libglapi-23.1.4-1.el9.x86_64                                                                                                                                                     69/158
  Verifying        : mesa-libgbm-23.1.4-1.el9.x86_64                                                                                                                                                       70/158
  Verifying        : mesa-libGL-23.1.4-1.el9.x86_64                                                                                                                                                        71/158
  Verifying        : mesa-libEGL-23.1.4-1.el9.x86_64                                                                                                                                                       72/158
  Verifying        : mesa-filesystem-23.1.4-1.el9.x86_64                                                                                                                                                   73/158
  Verifying        : mesa-dri-drivers-23.1.4-1.el9.x86_64                                                                                                                                                  74/158
  Verifying        : flac-libs-1.3.3-10.el9_2.1.x86_64                                                                                                                                                     75/158
  Verifying        : libdrm-2.4.115-1.el9.x86_64                                                                                                                                                           76/158
  Verifying        : libsndfile-1.0.31-7.el9.x86_64                                                                                                                                                        77/158
  Verifying        : jq-1.6-15.el9.x86_64                                                                                                                                                                  78/158
  Verifying        : libslirp-4.4.0-7.el9.x86_64                                                                                                                                                           79/158
  Verifying        : liburing-2.3-2.el9.x86_64                                                                                                                                                             80/158
  Verifying        : libvorbis-1:1.3.7-5.el9.x86_64                                                                                                                                                        81/158
  Verifying        : libglvnd-glx-1:1.3.4-1.el9.x86_64                                                                                                                                                     82/158
  Verifying        : libglvnd-egl-1:1.3.4-1.el9.x86_64                                                                                                                                                     83/158
  Verifying        : libglvnd-1:1.3.4-1.el9.x86_64                                                                                                                                                         84/158
  Verifying        : libtpms-0.9.1-3.20211126git1ff6fe1f43.el9_2.x86_64                                                                                                                                    85/158
  Verifying        : python3-libsemanage-3.5-2.el9.x86_64                                                                                                                                                  86/158
  Verifying        : libXxf86vm-1.1.4-18.el9.x86_64                                                                                                                                                        87/158
  Verifying        : opus-1.3.1-10.el9.x86_64                                                                                                                                                              88/158
  Verifying        : oniguruma-6.9.6-1.el9.5.x86_64                                                                                                                                                        89/158
  Verifying        : libwayland-server-1.21.0-1.el9.x86_64                                                                                                                                                 90/158
  Verifying        : libwayland-client-1.21.0-1.el9.x86_64                                                                                                                                                 91/158
  Verifying        : mdevctl-1.1.0-4.el9.x86_64                                                                                                                                                            92/158
  Verifying        : pixman-0.40.0-6.el9_3.x86_64                                                                                                                                                          93/158
  Verifying        : pulseaudio-libs-15.0-2.el9.x86_64                                                                                                                                                     94/158
  Verifying        : libvirt-libs-9.5.0-7.2.el9_3.x86_64                                                                                                                                                   95/158
  Verifying        : libvirt-daemon-driver-storage-scsi-9.5.0-7.2.el9_3.x86_64                                                                                                                             96/158
  Verifying        : libvirt-daemon-driver-storage-rbd-9.5.0-7.2.el9_3.x86_64                                                                                                                              97/158
  Verifying        : libvirt-daemon-driver-storage-mpath-9.5.0-7.2.el9_3.x86_64                                                                                                                            98/158
  Verifying        : libvirt-daemon-driver-storage-logical-9.5.0-7.2.el9_3.x86_64                                                                                                                          99/158
  Verifying        : libvirt-daemon-driver-storage-iscsi-9.5.0-7.2.el9_3.x86_64                                                                                                                           100/158
  Verifying        : libvirt-daemon-driver-storage-disk-9.5.0-7.2.el9_3.x86_64                                                                                                                            101/158
  Verifying        : libvirt-daemon-driver-storage-core-9.5.0-7.2.el9_3.x86_64                                                                                                                            102/158
  Verifying        : libvirt-daemon-driver-storage-9.5.0-7.2.el9_3.x86_64                                                                                                                                 103/158
  Verifying        : libvirt-daemon-driver-secret-9.5.0-7.2.el9_3.x86_64                                                                                                                                  104/158
  Verifying        : libvirt-daemon-driver-qemu-9.5.0-7.2.el9_3.x86_64                                                                                                                                    105/158
  Verifying        : libvirt-daemon-driver-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                106/158
  Verifying        : libvirt-daemon-driver-nodedev-9.5.0-7.2.el9_3.x86_64                                                                                                                                 107/158
  Verifying        : libvirt-daemon-driver-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 108/158
  Verifying        : libvirt-daemon-driver-interface-9.5.0-7.2.el9_3.x86_64                                                                                                                               109/158
  Verifying        : libvirt-daemon-config-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                110/158
  Verifying        : libvirt-daemon-config-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 111/158
  Verifying        : libvirt-daemon-9.5.0-7.2.el9_3.x86_64                                                                                                                                                112/158
  Verifying        : libvirt-client-9.5.0-7.2.el9_3.x86_64                                                                                                                                                113/158
  Verifying        : libvirt-9.5.0-7.2.el9_3.x86_64                                                                                                                                                       114/158
  Verifying        : boost-thread-1.75.0-8.el9.x86_64                                                                                                                                                     115/158
  Verifying        : boost-system-1.75.0-8.el9.x86_64                                                                                                                                                     116/158
  Verifying        : boost-iostreams-1.75.0-8.el9.x86_64                                                                                                                                                  117/158
  Verifying        : swtpm-tools-0.8.0-1.el9.x86_64                                                                                                                                                       118/158
  Verifying        : swtpm-libs-0.8.0-1.el9.x86_64                                                                                                                                                        119/158
  Verifying        : swtpm-0.8.0-1.el9.x86_64                                                                                                                                                             120/158
  Verifying        : librbd1-2:16.2.4-5.el9.x86_64                                                                                                                                                        121/158
  Verifying        : librados2-2:16.2.4-5.el9.x86_64                                                                                                                                                      122/158
  Verifying        : qemu-pr-helper-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                            123/158
  Verifying        : qemu-kvm-ui-opengl-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                        124/158
  Verifying        : qemu-kvm-ui-egl-headless-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                  125/158
  Verifying        : qemu-kvm-tools-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                            126/158
  Verifying        : qemu-kvm-docs-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                             127/158
  Verifying        : qemu-kvm-device-usb-redirect-17:8.0.0-16.el9_3.3.x86_64                                                                                                                              128/158
  Verifying        : qemu-kvm-device-usb-host-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                  129/158
  Verifying        : qemu-kvm-device-display-virtio-vga-17:8.0.0-16.el9_3.3.x86_64                                                                                                                        130/158
  Verifying        : qemu-kvm-device-display-virtio-gpu-pci-17:8.0.0-16.el9_3.3.x86_64                                                                                                                    131/158
  Verifying        : qemu-kvm-device-display-virtio-gpu-17:8.0.0-16.el9_3.3.x86_64                                                                                                                        132/158
  Verifying        : qemu-kvm-core-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                             133/158
  Verifying        : qemu-kvm-common-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                           134/158
  Verifying        : qemu-kvm-block-rbd-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                        135/158
  Verifying        : qemu-kvm-audio-pa-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                         136/158
  Verifying        : qemu-kvm-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                                  137/158
  Verifying        : qemu-img-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                                  138/158
  Verifying        : rubygem-psych-3.3.2-160.el9_0.x86_64                                                                                                                                                 139/158
  Verifying        : rubygem-json-2.5.1-160.el9_0.x86_64                                                                                                                                                  140/158
  Verifying        : rubygem-io-console-0.5.7-160.el9_0.x86_64                                                                                                                                            141/158
  Verifying        : rubygem-bigdecimal-3.0.0-160.el9_0.x86_64                                                                                                                                            142/158
  Verifying        : ruby-libs-3.0.4-160.el9_0.x86_64                                                                                                                                                     143/158
  Verifying        : ruby-3.0.4-160.el9_0.x86_64                                                                                                                                                          144/158
  Verifying        : libnbd-1.16.0-1.el9.x86_64                                                                                                                                                           145/158
  Verifying        : edk2-ovmf-20230524-4.el9_3.2.noarch                                                                                                                                                  146/158
  Verifying        : libX11-xcb-1.7.0-8.el9.x86_64                                                                                                                                                        147/158
  Verifying        : dnsmasq-2.85-14.el9_3.1.x86_64                                                                                                                                                       148/158
  Verifying        : capstone-4.0.2-10.el9.x86_64                                                                                                                                                         149/158
  Verifying        : passt-0^20230818.g0af928e-4.el9.x86_64                                                                                                                                               150/158
  Verifying        : passt-selinux-0^20230818.g0af928e-4.el9.noarch                                                                                                                                       151/158
  Verifying        : libblkio-1.3.0-1.el9.x86_64                                                                                                                                                          152/158
  Verifying        : qemu-kvm-block-blkio-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                      153/158
  Verifying        : libvirt-daemon-proxy-9.5.0-7.2.el9_3.x86_64                                                                                                                                          154/158
  Verifying        : libvirt-daemon-plugin-lockd-9.5.0-7.2.el9_3.x86_64                                                                                                                                   155/158
  Verifying        : libvirt-daemon-log-9.5.0-7.2.el9_3.x86_64                                                                                                                                            156/158
  Verifying        : libvirt-daemon-lock-9.5.0-7.2.el9_3.x86_64                                                                                                                                           157/158
  Verifying        : libvirt-daemon-common-9.5.0-7.2.el9_3.x86_64                                                                                                                                         158/158

Installed:
  augeas-1.13.0-5.el9.x86_64                                          augeas-libs-1.13.0-5.el9.x86_64                                     boost-iostreams-1.75.0-8.el9.x86_64
  boost-system-1.75.0-8.el9.x86_64                                    boost-thread-1.75.0-8.el9.x86_64                                    capstone-4.0.2-10.el9.x86_64
  checkpolicy-3.5-1.el9.x86_64                                        cyrus-sasl-2.1.27-21.el9.x86_64                                     cyrus-sasl-gssapi-2.1.27-21.el9.x86_64
  daxctl-libs-71.1-8.el9.x86_64                                       device-mapper-multipath-libs-0.8.7-22.el9.x86_64                    dnsmasq-2.85-14.el9_3.1.x86_64
  edk2-ovmf-20230524-4.el9_3.2.noarch                                 flac-libs-1.3.3-10.el9_2.1.x86_64                                   gnutls-dane-3.7.6-23.el9_3.3.x86_64
  gnutls-utils-3.7.6-23.el9_3.3.x86_64                                gsm-1.0.19-6.el9.x86_64                                             gssproxy-0.8.4-6.el9.x86_64
  ipxe-roms-qemu-20200823-9.git4bd064de.el9.noarch                    iscsi-initiator-utils-6.2.1.4-3.git2a8f9d8.el9.x86_64               iscsi-initiator-utils-iscsiuio-6.2.1.4-3.git2a8f9d8.el9.x86_64
  isns-utils-libs-0.101-4.el9.x86_64                                  jq-1.6-15.el9.x86_64                                                json-glib-1.6.6-1.el9.x86_64
  keyutils-1.6.3-1.el9.x86_64                                         libX11-xcb-1.7.0-8.el9.x86_64                                       libXfixes-5.0.3-16.el9.x86_64
  libXxf86vm-1.1.4-18.el9.x86_64                                      libasyncns-0.8-22.el9.x86_64                                        libblkio-1.3.0-1.el9.x86_64
  libbsd-0.11.7-2.el9.x86_64                                          libdrm-2.4.115-1.el9.x86_64                                         libepoxy-1.5.5-4.el9.x86_64
  libev-4.33-5.el9.x86_64                                             libfdt-1.6.0-7.el9.x86_64                                           libglvnd-1:1.3.4-1.el9.x86_64
  libglvnd-egl-1:1.3.4-1.el9.x86_64                                   libglvnd-glx-1:1.3.4-1.el9.x86_64                                   libmd-1.1.0-1.el9.x86_64
  libnbd-1.16.0-1.el9.x86_64                                          libnfsidmap-1:2.5.4-20.el9.x86_64                                   libogg-2:1.3.4-6.el9.x86_64
  libpciaccess-0.16-6.el9.x86_64                                      libpmem-1.12.1-1.el9.x86_64                                         librados2-2:16.2.4-5.el9.x86_64
  librbd1-2:16.2.4-5.el9.x86_64                                       librdmacm-46.0-1.el9.x86_64                                         libretls-3.8.1-1.el9.x86_64
  libslirp-4.4.0-7.el9.x86_64                                         libsndfile-1.0.31-7.el9.x86_64                                      libtpms-0.9.1-3.20211126git1ff6fe1f43.el9_2.x86_64
  liburing-2.3-2.el9.x86_64                                           libusbx-1.0.26-1.el9.x86_64                                         libverto-libev-0.3.2-3.el9.x86_64
  libvirt-9.5.0-7.2.el9_3.x86_64                                      libvirt-client-9.5.0-7.2.el9_3.x86_64                               libvirt-daemon-9.5.0-7.2.el9_3.x86_64
  libvirt-daemon-common-9.5.0-7.2.el9_3.x86_64                        libvirt-daemon-config-network-9.5.0-7.2.el9_3.x86_64                libvirt-daemon-config-nwfilter-9.5.0-7.2.el9_3.x86_64
  libvirt-daemon-driver-interface-9.5.0-7.2.el9_3.x86_64              libvirt-daemon-driver-network-9.5.0-7.2.el9_3.x86_64                libvirt-daemon-driver-nodedev-9.5.0-7.2.el9_3.x86_64
  libvirt-daemon-driver-nwfilter-9.5.0-7.2.el9_3.x86_64               libvirt-daemon-driver-qemu-9.5.0-7.2.el9_3.x86_64                   libvirt-daemon-driver-secret-9.5.0-7.2.el9_3.x86_64
  libvirt-daemon-driver-storage-9.5.0-7.2.el9_3.x86_64                libvirt-daemon-driver-storage-core-9.5.0-7.2.el9_3.x86_64           libvirt-daemon-driver-storage-disk-9.5.0-7.2.el9_3.x86_64
  libvirt-daemon-driver-storage-iscsi-9.5.0-7.2.el9_3.x86_64          libvirt-daemon-driver-storage-logical-9.5.0-7.2.el9_3.x86_64        libvirt-daemon-driver-storage-mpath-9.5.0-7.2.el9_3.x86_64
  libvirt-daemon-driver-storage-rbd-9.5.0-7.2.el9_3.x86_64            libvirt-daemon-driver-storage-scsi-9.5.0-7.2.el9_3.x86_64           libvirt-daemon-lock-9.5.0-7.2.el9_3.x86_64
  libvirt-daemon-log-9.5.0-7.2.el9_3.x86_64                           libvirt-daemon-plugin-lockd-9.5.0-7.2.el9_3.x86_64                  libvirt-daemon-proxy-9.5.0-7.2.el9_3.x86_64
  libvirt-libs-9.5.0-7.2.el9_3.x86_64                                 libvorbis-1:1.3.7-5.el9.x86_64                                      libwayland-client-1.21.0-1.el9.x86_64
  libwayland-server-1.21.0-1.el9.x86_64                               libxkbcommon-1.0.3-4.el9.x86_64                                     libxshmfence-1.3-10.el9.x86_64
  lzop-1.04-8.el9.x86_64                                              mdevctl-1.1.0-4.el9.x86_64                                          mesa-dri-drivers-23.1.4-1.el9.x86_64
  mesa-filesystem-23.1.4-1.el9.x86_64                                 mesa-libEGL-23.1.4-1.el9.x86_64                                     mesa-libGL-23.1.4-1.el9.x86_64
  mesa-libgbm-23.1.4-1.el9.x86_64                                     mesa-libglapi-23.1.4-1.el9.x86_64                                   ndctl-libs-71.1-8.el9.x86_64
  netcat-1.226-1.el9.x86_64                                           nfs-utils-1:2.5.4-20.el9.x86_64                                     numad-0.5-36.20150602git.el9.x86_64
  oniguruma-6.9.6-1.el9.5.x86_64                                      opennebula-common-6.8.0-1.el9.noarch                                opennebula-common-onecfg-6.8.0-1.el9.noarch
  opennebula-node-kvm-6.8.0-1.el9.noarch                              opus-1.3.1-10.el9.x86_64                                            passt-0^20230818.g0af928e-4.el9.x86_64
  passt-selinux-0^20230818.g0af928e-4.el9.noarch                      pciutils-3.7.0-5.el9.x86_64                                         pixman-0.40.0-6.el9_3.x86_64
  policycoreutils-python-utils-3.5-3.el9_3.noarch                     pulseaudio-libs-15.0-2.el9.x86_64                                   python3-audit-3.0.7-104.el9.x86_64
  python3-distro-1.5.0-7.el9.noarch                                   python3-libsemanage-3.5-2.el9.x86_64                                python3-policycoreutils-3.5-3.el9_3.noarch
  python3-pyyaml-5.4.1-6.el9.x86_64                                   python3-setools-4.4.3-1.el9.x86_64                                  python3-setuptools-53.0.0-12.el9.noarch
  qemu-img-17:8.0.0-16.el9_3.3.x86_64                                 qemu-kvm-17:8.0.0-16.el9_3.3.x86_64                                 qemu-kvm-audio-pa-17:8.0.0-16.el9_3.3.x86_64
  qemu-kvm-block-blkio-17:8.0.0-16.el9_3.3.x86_64                     qemu-kvm-block-rbd-17:8.0.0-16.el9_3.3.x86_64                       qemu-kvm-common-17:8.0.0-16.el9_3.3.x86_64
  qemu-kvm-core-17:8.0.0-16.el9_3.3.x86_64                            qemu-kvm-device-display-virtio-gpu-17:8.0.0-16.el9_3.3.x86_64       qemu-kvm-device-display-virtio-gpu-pci-17:8.0.0-16.el9_3.3.x86_64
  qemu-kvm-device-display-virtio-vga-17:8.0.0-16.el9_3.3.x86_64       qemu-kvm-device-usb-host-17:8.0.0-16.el9_3.3.x86_64                 qemu-kvm-device-usb-redirect-17:8.0.0-16.el9_3.3.x86_64
  qemu-kvm-docs-17:8.0.0-16.el9_3.3.x86_64                            qemu-kvm-tools-17:8.0.0-16.el9_3.3.x86_64                           qemu-kvm-ui-egl-headless-17:8.0.0-16.el9_3.3.x86_64
  qemu-kvm-ui-opengl-17:8.0.0-16.el9_3.3.x86_64                       qemu-pr-helper-17:8.0.0-16.el9_3.3.x86_64                           quota-1:4.06-6.el9.x86_64
  quota-nls-1:4.06-6.el9.noarch                                       rpcbind-1.2.6-5.el9.x86_64                                          ruby-3.0.4-160.el9_0.x86_64
  ruby-default-gems-3.0.4-160.el9_0.noarch                            ruby-libs-3.0.4-160.el9_0.x86_64                                    rubygem-bigdecimal-3.0.0-160.el9_0.x86_64
  rubygem-bundler-2.2.33-160.el9_0.noarch                             rubygem-io-console-0.5.7-160.el9_0.x86_64                           rubygem-json-2.5.1-160.el9_0.x86_64
  rubygem-psych-3.3.2-160.el9_0.x86_64                                rubygem-rdoc-6.3.3-160.el9_0.noarch                                 rubygem-rexml-3.2.5-160.el9_0.noarch
  rubygem-sqlite3-1.4.2-8.el9.x86_64                                  rubygems-3.2.33-160.el9_0.noarch                                    seabios-bin-1.16.1-1.el9.noarch
  seavgabios-bin-1.16.1-1.el9.noarch                                  sssd-nfs-idmap-2.9.1-4.el9_3.5.x86_64                               swtpm-0.8.0-1.el9.x86_64
  swtpm-libs-0.8.0-1.el9.x86_64                                       swtpm-tools-0.8.0-1.el9.x86_64                                      systemd-container-252-18.el9.0.1.rocky.x86_64
  unbound-libs-1.16.2-3.el9_3.1.x86_64                                usbredir-0.13.0-2.el9.x86_64                                        virtiofsd-1.7.2-1.el9.x86_64
  xkeyboard-config-2.33-2.el9.noarch                                  yajl-2.1.0-22.el9.x86_64

Complete!

```

**🌞 Démarrer le service libvirtd**

```
[vagrant@kvm1 ~]$ sudo systemctl status libvirtd
● libvirtd.service - Virtualization daemon
     Loaded: loaded (/usr/lib/systemd/system/libvirtd.service; enabled; preset: disabled)
     Active: active (running) since Tue 2024-04-09 11:50:08 UTC; 16s ago
TriggeredBy: ● libvirtd.socket
             ○ libvirtd-tcp.socket
             ○ libvirtd-tls.socket
             ● libvirtd-ro.socket
             ● libvirtd-admin.socket
       Docs: man:libvirtd(8)
             https://libvirt.org
   Main PID: 9795 (libvirtd)
      Tasks: 21 (limit: 32768)
     Memory: 19.3M
        CPU: 355ms
     CGroup: /system.slice/libvirtd.service
             ├─9795 /usr/sbin/libvirtd --timeout 120
             ├─9897 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/default.conf --leasefile-ro --dhcp-script=/usr/libexec/libvirt_leaseshelper
             └─9898 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/default.conf --leasefile-ro --dhcp-script=/usr/libexec/libvirt_leaseshelper


```
**🌞 Ouverture firewall**
```
[vagrant@kvm1 ~]$ sudo firewall-cmd --zone=public --add-port=22/tcp --permanent
success
[vagrant@kvm1 ~]$ sudo firewall-cmd --zone=public --add-port=8472/udp --permanent
success
[vagrant@kvm1 ~]$ sudo firewall-cmd --reload
success
[vagrant@kvm1 ~]$

```
**🌞 Handle SSH**
on copy id_rsa.pub de la machine frontend dans un fhicher se situant dans  .ssh de la machine km1 se nommant authorized_keys

voici ma clé : 

```
[oneadmin@frontend .ssh]$ cat id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwVDhFRk/K6ntAQ0lYt1NEPw0K4jbT+W5KNxV6fY7I4XwTnP2wn4QxMF/vptakwhHmQdfXs29fPR2rGBSnpNRjVxzFBFoLbnnYedWM0STZurSpCTySH1iI433C6O5o0WEcR6liV8ZL6bbrMphY4T1efUGiJi60EQ7n9mGg7o8Nknry4Afp1IreTq84uo3cDI2Z5RGimnEnK7Rhxk/CI+OU9c/VghumZlVFdwf0LTwoovI+idr3wWXzHZ5nQThv39J45E/p7z0AaHCEw5sZanPKAa9vAwgJWdzhlcxdE8nZYVFKQFGs9cI52R5v4cZRuvKm71NUYAsE1Oh0SFvmYtqBNpqhIDd4ATP1gSG/iDPm0PjqOdn4LOggdfVE84zN2Lns/p7HMDUDTRFCwl8NxSl+s0nL4zl/9U+gZMWqeahxph6cYYACx6mcaUrmZC3z0walVg78pttzFmU2KHcVQtNaXmviVdgJ96ewLNThnaaEJ/VEyo6ZTLwpNH0dQU9Ocas= oneadmin@frontend.one

```

# Réseau

**🌞 Créer et configurer le bridge Linux, j'vous file tout, suivez le guide :**

```
[vagrant@kvm1 .ssh]$ sudo ip link add name vxlan_bridge type bridge
[vagrant@kvm1 .ssh]$ sudo ip link set dev vxlan_bridge up
[vagrant@kvm1 .ssh]$ sudo ip addr add 10.220.220.201/24 dev vxlan_bridge
[vagrant@kvm1 .ssh]$ sudo firewall-cmd --add-interface=vxlan_bridge --zone=public --permanent
success
[vagrant@kvm1 .ssh]$ sudo firewall-cmd --add-masquerade --permanent
success
[vagrant@kvm1 .ssh]$ sudo firewall-cmd --reload
success
[vagrant@kvm1 .ssh]$

```

**🌞 Setup de kvm2.one, à l'identique de kvm1.one excepté :**

```
[vagrant@kvm2 ~]$ sudo nano /etc/yum.repos.d/opennebula.repo
[vagrant@kvm2 ~]$ dnf makecache -y
Extra Packages for Enterprise Linux 9 - x86_64                                                                                                                                     11 MB/s |  21 MB     00:01
Extra Packages for Enterprise Linux 9 openh264 (From Cisco) - x86_64                                                                                                              2.0 kB/s | 2.5 kB     00:01
OpenNebula Community Edition                                                                                                                                                      1.3 kB/s | 833  B     00:00
OpenNebula Community Edition                                                                                                                                                       19 kB/s | 3.1 kB     00:00
Importing GPG key 0x906DC27C:
 Userid     : "OpenNebula Repository <contact@opennebula.io>"
 Fingerprint: 0B2D 385C 7C93 04B1 1A03 67B9 05A0 5927 906D C27C
 From       : https://downloads.opennebula.io/repo/repo2.key
OpenNebula Community Edition                                                                                                                                                      511 kB/s | 675 kB     00:01
Rocky Linux 9 - BaseOS                                                                                                                                                            1.4 MB/s | 2.2 MB     00:01
Rocky Linux 9 - AppStream                                                                                                                                                         3.9 MB/s | 7.4 MB     00:01
Rocky Linux 9 - Extras                                                                                                                                                             25 kB/s |  14 kB     00:00
Metadata cache created.
[vagrant@kvm2 ~]$ sudo dnf -y install epel-release
Extra Packages for Enterprise Linux 9 - x86_64                                                                                                                                     15 kB/s |  11 kB     00:00
OpenNebula Community Edition                                                                                                                                                      1.9 kB/s | 833  B     00:00
OpenNebula Community Edition                                                                                                                                                       16 kB/s | 3.1 kB     00:00
Importing GPG key 0x906DC27C:
 Userid     : "OpenNebula Repository <contact@opennebula.io>"
 Fingerprint: 0B2D 385C 7C93 04B1 1A03 67B9 05A0 5927 906D C27C
 From       : https://downloads.opennebula.io/repo/repo2.key
OpenNebula Community Edition                                                                                                                                                      771 kB/s | 675 kB     00:00
Rocky Linux 9 - AppStream                                                                                                                                                          12 kB/s | 4.5 kB     00:00
Rocky Linux 9 - AppStream                                                                                                                                                         1.5 MB/s | 7.4 MB     00:04
Rocky Linux 9 - Extras                                                                                                                                                            4.5 kB/s | 2.9 kB     00:00
Package epel-release-9-7.el9.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!
[vagrant@kvm2 ~]$ sudo dnf -y install opennebula-node-kvm
Last metadata expiration check: 0:00:41 ago on Wed 10 Apr 2024 12:22:00 PM UTC.
Dependencies resolved.
==================================================================================================================================================================================================================
 Package                                                           Architecture                      Version                                                          Repository                             Size
==================================================================================================================================================================================================================
Installing:
 opennebula-node-kvm                                               noarch                            6.8.0-1.el9                                                      opennebula                             17 k
Installing dependencies:
 augeas                                                            x86_64                            1.13.0-5.el9                                                     appstream                              49 k
 augeas-libs                                                       x86_64                            1.13.0-5.el9                                                     appstream                             405 k
 boost-iostreams                                                   x86_64                            1.75.0-8.el9                                                     appstream                              38 k
 boost-system                                                      x86_64                            1.75.0-8.el9                                                     appstream                              13 k
 boost-thread                                                      x86_64                            1.75.0-8.el9                                                     appstream                              55 k
 capstone                                                          x86_64                            4.0.2-10.el9                                                     appstream                             766 k
 checkpolicy                                                       x86_64                            3.5-1.el9                                                        appstream                             345 k
 cyrus-sasl                                                        x86_64                            2.1.27-21.el9                                                    baseos                                 71 k
 cyrus-sasl-gssapi                                                 x86_64                            2.1.27-21.el9                                                    baseos                                 26 k
 daxctl-libs                                                       x86_64                            71.1-8.el9                                                       baseos                                 41 k
 device-mapper-multipath-libs                                      x86_64                            0.8.7-22.el9                                                     baseos                                267 k
 dnsmasq                                                           x86_64                            2.85-14.el9_3.1                                                  appstream                             325 k
 edk2-ovmf                                                         noarch                            20230524-4.el9_3.2                                               appstream                             5.4 M
 flac-libs                                                         x86_64                            1.3.3-10.el9_2.1                                                 appstream                             217 k
 gnutls-dane                                                       x86_64                            3.7.6-23.el9_3.3                                                 appstream                              22 k
 gnutls-utils                                                      x86_64                            3.7.6-23.el9_3.3                                                 appstream                             265 k
 gsm                                                               x86_64                            1.0.19-6.el9                                                     appstream                              33 k
 gssproxy                                                          x86_64                            0.8.4-6.el9                                                      baseos                                108 k
 ipxe-roms-qemu                                                    noarch                            20200823-9.git4bd064de.el9                                       appstream                             673 k
 iscsi-initiator-utils                                             x86_64                            6.2.1.4-3.git2a8f9d8.el9                                         baseos                                378 k
 iscsi-initiator-utils-iscsiuio                                    x86_64                            6.2.1.4-3.git2a8f9d8.el9                                         baseos                                 94 k
 isns-utils-libs                                                   x86_64                            0.101-4.el9                                                      baseos                                100 k
 jq                                                                x86_64                            1.6-15.el9                                                       appstream                             186 k
 json-glib                                                         x86_64                            1.6.6-1.el9                                                      baseos                                151 k
 keyutils                                                          x86_64                            1.6.3-1.el9                                                      baseos                                 72 k
 libX11-xcb                                                        x86_64                            1.7.0-8.el9                                                      appstream                             9.9 k
 libXfixes                                                         x86_64                            5.0.3-16.el9                                                     appstream                              19 k
 libXxf86vm                                                        x86_64                            1.1.4-18.el9                                                     appstream                              18 k
 libasyncns                                                        x86_64                            0.8-22.el9                                                       appstream                              29 k
 libblkio                                                          x86_64                            1.3.0-1.el9                                                      appstream                             512 k
 libbsd                                                            x86_64                            0.12.2-1.el9                                                     epel                                  120 k
 libdrm                                                            x86_64                            2.4.115-1.el9                                                    appstream                             156 k
 libepoxy                                                          x86_64                            1.5.5-4.el9                                                      appstream                             244 k
 libev                                                             x86_64                            4.33-5.el9                                                       baseos                                 52 k
 libfdt                                                            x86_64                            1.6.0-7.el9                                                      appstream                              33 k
 libglvnd                                                          x86_64                            1:1.3.4-1.el9                                                    appstream                             133 k
 libglvnd-egl                                                      x86_64                            1:1.3.4-1.el9                                                    appstream                              36 k
 libglvnd-glx                                                      x86_64                            1:1.3.4-1.el9                                                    appstream                             140 k
 libmd                                                             x86_64                            1.1.0-1.el9                                                      epel                                   46 k
 libnbd                                                            x86_64                            1.16.0-1.el9                                                     appstream                             160 k
 libnfsidmap                                                       x86_64                            1:2.5.4-20.el9                                                   baseos                                 60 k
 libogg                                                            x86_64                            2:1.3.4-6.el9                                                    appstream                              32 k
 libpciaccess                                                      x86_64                            0.16-6.el9                                                       baseos                                 27 k
 libpmem                                                           x86_64                            1.12.1-1.el9                                                     appstream                             111 k
 librados2                                                         x86_64                            2:16.2.4-5.el9                                                   appstream                             3.4 M
 librbd1                                                           x86_64                            2:16.2.4-5.el9                                                   appstream                             3.0 M
 librdmacm                                                         x86_64                            46.0-1.el9                                                       baseos                                 70 k
 libretls                                                          x86_64                            3.8.1-1.el9                                                      epel                                   41 k
 libslirp                                                          x86_64                            4.4.0-7.el9                                                      appstream                              68 k
 libsndfile                                                        x86_64                            1.0.31-7.el9                                                     appstream                             206 k
 libtpms                                                           x86_64                            0.9.1-3.20211126git1ff6fe1f43.el9_2                              appstream                             184 k
 liburing                                                          x86_64                            2.3-2.el9                                                        appstream                              26 k
 libusbx                                                           x86_64                            1.0.26-1.el9                                                     baseos                                 75 k
 libverto-libev                                                    x86_64                            0.3.2-3.el9                                                      baseos                                 13 k
 libvirt                                                           x86_64                            9.5.0-7.2.el9_3                                                  appstream                              22 k
 libvirt-client                                                    x86_64                            9.5.0-7.2.el9_3                                                  appstream                             426 k
 libvirt-daemon                                                    x86_64                            9.5.0-7.2.el9_3                                                  appstream                             167 k
 libvirt-daemon-common                                             x86_64                            9.5.0-7.2.el9_3                                                  appstream                             128 k
 libvirt-daemon-config-network                                     x86_64                            9.5.0-7.2.el9_3                                                  appstream                              24 k
 libvirt-daemon-config-nwfilter                                    x86_64                            9.5.0-7.2.el9_3                                                  appstream                              30 k
 libvirt-daemon-driver-interface                                   x86_64                            9.5.0-7.2.el9_3                                                  appstream                             173 k
 libvirt-daemon-driver-network                                     x86_64                            9.5.0-7.2.el9_3                                                  appstream                             211 k
 libvirt-daemon-driver-nodedev                                     x86_64                            9.5.0-7.2.el9_3                                                  appstream                             194 k
 libvirt-daemon-driver-nwfilter                                    x86_64                            9.5.0-7.2.el9_3                                                  appstream                             209 k
 libvirt-daemon-driver-qemu                                        x86_64                            9.5.0-7.2.el9_3                                                  appstream                             909 k
 libvirt-daemon-driver-secret                                      x86_64                            9.5.0-7.2.el9_3                                                  appstream                             170 k
 libvirt-daemon-driver-storage                                     x86_64                            9.5.0-7.2.el9_3                                                  appstream                              22 k
 libvirt-daemon-driver-storage-core                                x86_64                            9.5.0-7.2.el9_3                                                  appstream                             228 k
 libvirt-daemon-driver-storage-disk                                x86_64                            9.5.0-7.2.el9_3                                                  appstream                              33 k
 libvirt-daemon-driver-storage-iscsi                               x86_64                            9.5.0-7.2.el9_3                                                  appstream                              30 k
 libvirt-daemon-driver-storage-logical                             x86_64                            9.5.0-7.2.el9_3                                                  appstream                              34 k
 libvirt-daemon-driver-storage-mpath                               x86_64                            9.5.0-7.2.el9_3                                                  appstream                              27 k
 libvirt-daemon-driver-storage-rbd                                 x86_64                            9.5.0-7.2.el9_3                                                  appstream                              38 k
 libvirt-daemon-driver-storage-scsi                                x86_64                            9.5.0-7.2.el9_3                                                  appstream                              30 k
 libvirt-daemon-lock                                               x86_64                            9.5.0-7.2.el9_3                                                  appstream                              58 k
 libvirt-daemon-log                                                x86_64                            9.5.0-7.2.el9_3                                                  appstream                              61 k
 libvirt-daemon-plugin-lockd                                       x86_64                            9.5.0-7.2.el9_3                                                  appstream                              33 k
 libvirt-daemon-proxy                                              x86_64                            9.5.0-7.2.el9_3                                                  appstream                             165 k
 libvirt-libs                                                      x86_64                            9.5.0-7.2.el9_3                                                  appstream                             4.8 M
 libvorbis                                                         x86_64                            1:1.3.7-5.el9                                                    appstream                             192 k
 libwayland-client                                                 x86_64                            1.21.0-1.el9                                                     appstream                              33 k
 libwayland-server                                                 x86_64                            1.21.0-1.el9                                                     appstream                              41 k
 libxkbcommon                                                      x86_64                            1.0.3-4.el9                                                      appstream                             132 k
 libxshmfence                                                      x86_64                            1.3-10.el9                                                       appstream                              12 k
 lzop                                                              x86_64                            1.04-8.el9                                                       baseos                                 55 k
 mdevctl                                                           x86_64                            1.1.0-4.el9                                                      appstream                             758 k
 mesa-dri-drivers                                                  x86_64                            23.1.4-1.el9                                                     appstream                              10 M
 mesa-filesystem                                                   x86_64                            23.1.4-1.el9                                                     appstream                             9.9 k
 mesa-libEGL                                                       x86_64                            23.1.4-1.el9                                                     appstream                             124 k
 mesa-libGL                                                        x86_64                            23.1.4-1.el9                                                     appstream                             165 k
 mesa-libgbm                                                       x86_64                            23.1.4-1.el9                                                     appstream                              37 k
 mesa-libglapi                                                     x86_64                            23.1.4-1.el9                                                     appstream                              46 k
 ndctl-libs                                                        x86_64                            71.1-8.el9                                                       baseos                                 88 k
 nfs-utils                                                         x86_64                            1:2.5.4-20.el9                                                   baseos                                425 k
 numad                                                             x86_64                            0.5-36.20150602git.el9                                           baseos                                 36 k
 oniguruma                                                         x86_64                            6.9.6-1.el9.5                                                    appstream                             217 k
 opennebula-common                                                 noarch                            6.8.0-1.el9                                                      opennebula                             22 k
 opennebula-common-onecfg                                          noarch                            6.8.0-1.el9                                                      opennebula                            9.1 k
 opus                                                              x86_64                            1.3.1-10.el9                                                     appstream                             199 k
 passt-selinux                                                     noarch                            0^20230818.g0af928e-4.el9                                        appstream                              29 k
 pciutils                                                          x86_64                            3.7.0-5.el9                                                      baseos                                 92 k
 pixman                                                            x86_64                            0.40.0-6.el9_3                                                   appstream                             269 k
 policycoreutils-python-utils                                      noarch                            3.5-3.el9_3                                                      appstream                              71 k
 pulseaudio-libs                                                   x86_64                            15.0-2.el9                                                       appstream                             666 k
 python3-audit                                                     x86_64                            3.0.7-104.el9                                                    appstream                              82 k
 python3-distro                                                    noarch                            1.5.0-7.el9                                                      appstream                              36 k
 python3-libsemanage                                               x86_64                            3.5-2.el9                                                        appstream                              79 k
 python3-policycoreutils                                           noarch                            3.5-3.el9_3                                                      appstream                             2.0 M
 python3-pyyaml                                                    x86_64                            5.4.1-6.el9                                                      baseos                                191 k
 python3-setools                                                   x86_64                            4.4.3-1.el9                                                      baseos                                551 k
 python3-setuptools                                                noarch                            53.0.0-12.el9                                                    baseos                                839 k
 qemu-img                                                          x86_64                            17:8.0.0-16.el9_3.3                                              appstream                             2.4 M
 qemu-kvm                                                          x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              62 k
 qemu-kvm-audio-pa                                                 x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              71 k
 qemu-kvm-block-blkio                                              x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              74 k
 qemu-kvm-block-rbd                                                x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              76 k
 qemu-kvm-common                                                   x86_64                            17:8.0.0-16.el9_3.3                                              appstream                             642 k
 qemu-kvm-core                                                     x86_64                            17:8.0.0-16.el9_3.3                                              appstream                             4.1 M
 qemu-kvm-device-display-virtio-gpu                                x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              81 k
 qemu-kvm-device-display-virtio-gpu-pci                            x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              66 k
 qemu-kvm-device-display-virtio-vga                                x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              67 k
 qemu-kvm-device-usb-host                                          x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              80 k
 qemu-kvm-device-usb-redirect                                      x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              85 k
 qemu-kvm-docs                                                     x86_64                            17:8.0.0-16.el9_3.3                                              appstream                             1.1 M
 qemu-kvm-tools                                                    x86_64                            17:8.0.0-16.el9_3.3                                              appstream                             559 k
 qemu-kvm-ui-egl-headless                                          x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              67 k
 qemu-kvm-ui-opengl                                                x86_64                            17:8.0.0-16.el9_3.3                                              appstream                              73 k
 qemu-pr-helper                                                    x86_64                            17:8.0.0-16.el9_3.3                                              appstream                             486 k
 quota                                                             x86_64                            1:4.06-6.el9                                                     baseos                                190 k
 quota-nls                                                         noarch                            1:4.06-6.el9                                                     baseos                                 78 k
 rpcbind                                                           x86_64                            1.2.6-5.el9                                                      baseos                                 56 k
 ruby                                                              x86_64                            3.0.4-160.el9_0                                                  appstream                              41 k
 ruby-libs                                                         x86_64                            3.0.4-160.el9_0                                                  appstream                             3.2 M
 rubygem-io-console                                                x86_64                            0.5.7-160.el9_0                                                  appstream                              25 k
 rubygem-json                                                      x86_64                            2.5.1-160.el9_0                                                  appstream                              54 k
 rubygem-psych                                                     x86_64                            3.3.2-160.el9_0                                                  appstream                              51 k
 rubygem-rexml                                                     noarch                            3.2.5-160.el9_0                                                  appstream                              95 k
 rubygem-sqlite3                                                   x86_64                            1.4.2-8.el9                                                      epel                                   44 k
 rubygems                                                          noarch                            3.2.33-160.el9_0                                                 appstream                             256 k
 seabios-bin                                                       noarch                            1.16.1-1.el9                                                     appstream                             101 k
 seavgabios-bin                                                    noarch                            1.16.1-1.el9                                                     appstream                              36 k
 sssd-nfs-idmap                                                    x86_64                            2.9.1-4.el9_3.5                                                  baseos                                 42 k
 swtpm                                                             x86_64                            0.8.0-1.el9                                                      appstream                              42 k
 swtpm-libs                                                        x86_64                            0.8.0-1.el9                                                      appstream                              50 k
 swtpm-tools                                                       x86_64                            0.8.0-1.el9                                                      appstream                             117 k
 systemd-container                                                 x86_64                            252-18.el9.0.1.rocky                                             baseos                                556 k
 unbound-libs                                                      x86_64                            1.16.2-3.el9_3.1                                                 appstream                             548 k
 usbredir                                                          x86_64                            0.13.0-2.el9                                                     appstream                              50 k
 virtiofsd                                                         x86_64                            1.7.2-1.el9                                                      appstream                             864 k
 xkeyboard-config                                                  noarch                            2.33-2.el9                                                       appstream                             779 k
 yajl                                                              x86_64                            2.1.0-22.el9                                                     appstream                              37 k
Installing weak dependencies:
 netcat                                                            x86_64                            1.226-1.el9                                                      epel                                   34 k
 passt                                                             x86_64                            0^20230818.g0af928e-4.el9                                        appstream                             177 k
 ruby-default-gems                                                 noarch                            3.0.4-160.el9_0                                                  appstream                              32 k
 rubygem-bigdecimal                                                x86_64                            3.0.0-160.el9_0                                                  appstream                              54 k
 rubygem-bundler                                                   noarch                            2.2.33-160.el9_0                                                 appstream                             372 k
 rubygem-rdoc                                                      noarch                            6.3.3-160.el9_0                                                  appstream                             400 k

Transaction Summary
==================================================================================================================================================================================================================
Install  158 Packages

Total download size: 63 M
Installed size: 256 M
Downloading Packages:
(1/158): libmd-1.1.0-1.el9.x86_64.rpm                                                                                                                                             138 kB/s |  46 kB     00:00
(2/158): libbsd-0.12.2-1.el9.x86_64.rpm                                                                                                                                           347 kB/s | 120 kB     00:00
(3/158): libretls-3.8.1-1.el9.x86_64.rpm                                                                                                                                          117 kB/s |  41 kB     00:00
(4/158): rubygem-sqlite3-1.4.2-8.el9.x86_64.rpm                                                                                                                                   1.0 MB/s |  44 kB     00:00
(5/158): netcat-1.226-1.el9.x86_64.rpm                                                                                                                                            540 kB/s |  34 kB     00:00
(6/158): opennebula-common-6.8.0-1.el9.noarch.rpm                                                                                                                                 105 kB/s |  22 kB     00:00
(7/158): opennebula-common-onecfg-6.8.0-1.el9.noarch.rpm                                                                                                                           51 kB/s | 9.1 kB     00:00
(8/158): opennebula-node-kvm-6.8.0-1.el9.noarch.rpm                                                                                                                                87 kB/s |  17 kB     00:00
(9/158): json-glib-1.6.6-1.el9.x86_64.rpm                                                                                                                                         623 kB/s | 151 kB     00:00
(10/158): quota-4.06-6.el9.x86_64.rpm                                                                                                                                             3.2 MB/s | 190 kB     00:00
(11/158): quota-nls-4.06-6.el9.noarch.rpm                                                                                                                                         1.9 MB/s |  78 kB     00:00
(12/158): libverto-libev-0.3.2-3.el9.x86_64.rpm                                                                                                                                    35 kB/s |  13 kB     00:00
(13/158): python3-pyyaml-5.4.1-6.el9.x86_64.rpm                                                                                                                                   1.8 MB/s | 191 kB     00:00
(14/158): librdmacm-46.0-1.el9.x86_64.rpm                                                                                                                                         141 kB/s |  70 kB     00:00
(15/158): python3-setuptools-53.0.0-12.el9.noarch.rpm                                                                                                                             5.2 MB/s | 839 kB     00:00
(16/158): numad-0.5-36.20150602git.el9.x86_64.rpm                                                                                                                                 436 kB/s |  36 kB     00:00
(17/158): rpcbind-1.2.6-5.el9.x86_64.rpm                                                                                                                                          1.3 MB/s |  56 kB     00:00
(18/158): isns-utils-libs-0.101-4.el9.x86_64.rpm                                                                                                                                  2.1 MB/s | 100 kB     00:00
(19/158): libev-4.33-5.el9.x86_64.rpm                                                                                                                                             1.0 MB/s |  52 kB     00:00
(20/158): libpciaccess-0.16-6.el9.x86_64.rpm                                                                                                                                      785 kB/s |  27 kB     00:00
(21/158): libusbx-1.0.26-1.el9.x86_64.rpm                                                                                                                                         2.3 MB/s |  75 kB     00:00
(22/158): iscsi-initiator-utils-iscsiuio-6.2.1.4-3.git2a8f9d8.el9.x86_64.rpm                                                                                                      2.2 MB/s |  94 kB     00:00
(23/158): iscsi-initiator-utils-6.2.1.4-3.git2a8f9d8.el9.x86_64.rpm                                                                                                               8.5 MB/s | 378 kB     00:00
(24/158): pciutils-3.7.0-5.el9.x86_64.rpm                                                                                                                                         2.2 MB/s |  92 kB     00:00
(25/158): ndctl-libs-71.1-8.el9.x86_64.rpm                                                                                                                                        2.4 MB/s |  88 kB     00:00
(26/158): daxctl-libs-71.1-8.el9.x86_64.rpm                                                                                                                                       922 kB/s |  41 kB     00:00
(27/158): python3-setools-4.4.3-1.el9.x86_64.rpm                                                                                                                                  8.6 MB/s | 551 kB     00:00
(28/158): sssd-nfs-idmap-2.9.1-4.el9_3.5.x86_64.rpm                                                                                                                               661 kB/s |  42 kB     00:00
(29/158): gssproxy-0.8.4-6.el9.x86_64.rpm                                                                                                                                         2.5 MB/s | 108 kB     00:00
(30/158): lzop-1.04-8.el9.x86_64.rpm                                                                                                                                              1.1 MB/s |  55 kB     00:00
(31/158): systemd-container-252-18.el9.0.1.rocky.x86_64.rpm                                                                                                                       6.6 MB/s | 556 kB     00:00
(32/158): libnfsidmap-2.5.4-20.el9.x86_64.rpm                                                                                                                                     1.7 MB/s |  60 kB     00:00
(33/158): cyrus-sasl-gssapi-2.1.27-21.el9.x86_64.rpm                                                                                                                              837 kB/s |  26 kB     00:00
(34/158): nfs-utils-2.5.4-20.el9.x86_64.rpm                                                                                                                                       5.1 MB/s | 425 kB     00:00
(35/158): keyutils-1.6.3-1.el9.x86_64.rpm                                                                                                                                         1.5 MB/s |  72 kB     00:00
(36/158): cyrus-sasl-2.1.27-21.el9.x86_64.rpm                                                                                                                                     1.2 MB/s |  71 kB     00:00
(37/158): device-mapper-multipath-libs-0.8.7-22.el9.x86_64.rpm                                                                                                                    6.0 MB/s | 267 kB     00:00
(38/158): python3-distro-1.5.0-7.el9.noarch.rpm                                                                                                                                   989 kB/s |  36 kB     00:00
(39/158): augeas-libs-1.13.0-5.el9.x86_64.rpm                                                                                                                                     5.6 MB/s | 405 kB     00:00
(40/158): augeas-1.13.0-5.el9.x86_64.rpm                                                                                                                                          1.1 MB/s |  49 kB     00:00
(41/158): checkpolicy-3.5-1.el9.x86_64.rpm                                                                                                                                        6.9 MB/s | 345 kB     00:00
(42/158): libasyncns-0.8-22.el9.x86_64.rpm                                                                                                                                        615 kB/s |  29 kB     00:00
(43/158): libfdt-1.6.0-7.el9.x86_64.rpm                                                                                                                                           581 kB/s |  33 kB     00:00
(44/158): python3-audit-3.0.7-104.el9.x86_64.rpm                                                                                                                                  1.3 MB/s |  82 kB     00:00
(45/158): libxshmfence-1.3-10.el9.x86_64.rpm                                                                                                                                      339 kB/s |  12 kB     00:00
(46/158): libxkbcommon-1.0.3-4.el9.x86_64.rpm                                                                                                                                     2.7 MB/s | 132 kB     00:00
(47/158): libepoxy-1.5.5-4.el9.x86_64.rpm                                                                                                                                         5.2 MB/s | 244 kB     00:00
(48/158): libXfixes-5.0.3-16.el9.x86_64.rpm                                                                                                                                       507 kB/s |  19 kB     00:00
(49/158): libogg-1.3.4-6.el9.x86_64.rpm                                                                                                                                           713 kB/s |  32 kB     00:00
(50/158): policycoreutils-python-utils-3.5-3.el9_3.noarch.rpm                                                                                                                     876 kB/s |  71 kB     00:00
(51/158): python3-policycoreutils-3.5-3.el9_3.noarch.rpm                                                                                                                           17 MB/s | 2.0 MB     00:00
(52/158): usbredir-0.13.0-2.el9.x86_64.rpm                                                                                                                                        617 kB/s |  50 kB     00:00
(53/158): yajl-2.1.0-22.el9.x86_64.rpm                                                                                                                                            992 kB/s |  37 kB     00:00
(54/158): seavgabios-bin-1.16.1-1.el9.noarch.rpm                                                                                                                                  882 kB/s |  36 kB     00:00
(55/158): xkeyboard-config-2.33-2.el9.noarch.rpm                                                                                                                                  7.4 MB/s | 779 kB     00:00
(56/158): seabios-bin-1.16.1-1.el9.noarch.rpm                                                                                                                                     1.7 MB/s | 101 kB     00:00
(57/158): libpmem-1.12.1-1.el9.x86_64.rpm                                                                                                                                         1.9 MB/s | 111 kB     00:00
(58/158): virtiofsd-1.7.2-1.el9.x86_64.rpm                                                                                                                                        7.6 MB/s | 864 kB     00:00
(59/158): unbound-libs-1.16.2-3.el9_3.1.x86_64.rpm                                                                                                                                4.7 MB/s | 548 kB     00:00
(60/158): gnutls-utils-3.7.6-23.el9_3.3.x86_64.rpm                                                                                                                                2.2 MB/s | 265 kB     00:00
(61/158): gnutls-dane-3.7.6-23.el9_3.3.x86_64.rpm                                                                                                                                 670 kB/s |  22 kB     00:00
(62/158): rubygem-rexml-3.2.5-160.el9_0.noarch.rpm                                                                                                                                2.2 MB/s |  95 kB     00:00
(63/158): rubygems-3.2.33-160.el9_0.noarch.rpm                                                                                                                                    4.4 MB/s | 256 kB     00:00
(64/158): rubygem-rdoc-6.3.3-160.el9_0.noarch.rpm                                                                                                                                 6.2 MB/s | 400 kB     00:00
(65/158): ruby-default-gems-3.0.4-160.el9_0.noarch.rpm                                                                                                                            846 kB/s |  32 kB     00:00
(66/158): rubygem-bundler-2.2.33-160.el9_0.noarch.rpm                                                                                                                             5.3 MB/s | 372 kB     00:00
(67/158): gsm-1.0.19-6.el9.x86_64.rpm                                                                                                                                             947 kB/s |  33 kB     00:00
(68/158): mesa-libglapi-23.1.4-1.el9.x86_64.rpm                                                                                                                                   1.3 MB/s |  46 kB     00:00
(69/158): mesa-libgbm-23.1.4-1.el9.x86_64.rpm                                                                                                                                     928 kB/s |  37 kB     00:00
(70/158): mesa-libGL-23.1.4-1.el9.x86_64.rpm                                                                                                                                      4.0 MB/s | 165 kB     00:00
(71/158): ipxe-roms-qemu-20200823-9.git4bd064de.el9.noarch.rpm                                                                                                                    6.3 MB/s | 673 kB     00:00
(72/158): mesa-libEGL-23.1.4-1.el9.x86_64.rpm                                                                                                                                     3.1 MB/s | 124 kB     00:00
(73/158): mesa-filesystem-23.1.4-1.el9.x86_64.rpm                                                                                                                                 294 kB/s | 9.9 kB     00:00
(74/158): flac-libs-1.3.3-10.el9_2.1.x86_64.rpm                                                                                                                                   3.5 MB/s | 217 kB     00:00
(75/158): libdrm-2.4.115-1.el9.x86_64.rpm                                                                                                                                         2.8 MB/s | 156 kB     00:00
(76/158): jq-1.6-15.el9.x86_64.rpm                                                                                                                                                3.7 MB/s | 186 kB     00:00
(77/158): libsndfile-1.0.31-7.el9.x86_64.rpm                                                                                                                                      2.7 MB/s | 206 kB     00:00
(78/158): libslirp-4.4.0-7.el9.x86_64.rpm                                                                                                                                         1.9 MB/s |  68 kB     00:00
(79/158): liburing-2.3-2.el9.x86_64.rpm                                                                                                                                           737 kB/s |  26 kB     00:00
(80/158): libvorbis-1.3.7-5.el9.x86_64.rpm                                                                                                                                        4.3 MB/s | 192 kB     00:00
(81/158): libglvnd-glx-1.3.4-1.el9.x86_64.rpm                                                                                                                                     2.6 MB/s | 140 kB     00:00
(82/158): libglvnd-egl-1.3.4-1.el9.x86_64.rpm                                                                                                                                     1.0 MB/s |  36 kB     00:00
(83/158): libglvnd-1.3.4-1.el9.x86_64.rpm                                                                                                                                         2.9 MB/s | 133 kB     00:00
(84/158): libtpms-0.9.1-3.20211126git1ff6fe1f43.el9_2.x86_64.rpm                                                                                                                  4.2 MB/s | 184 kB     00:00
(85/158): python3-libsemanage-3.5-2.el9.x86_64.rpm                                                                                                                                2.1 MB/s |  79 kB     00:00
(86/158): libXxf86vm-1.1.4-18.el9.x86_64.rpm                                                                                                                                      403 kB/s |  18 kB     00:00
(87/158): oniguruma-6.9.6-1.el9.5.x86_64.rpm                                                                                                                                      4.1 MB/s | 217 kB     00:00
(88/158): opus-1.3.1-10.el9.x86_64.rpm                                                                                                                                            2.7 MB/s | 199 kB     00:00
(89/158): libwayland-server-1.21.0-1.el9.x86_64.rpm                                                                                                                               907 kB/s |  41 kB     00:00
(90/158): libwayland-client-1.21.0-1.el9.x86_64.rpm                                                                                                                               524 kB/s |  33 kB     00:00
(91/158): mdevctl-1.1.0-4.el9.x86_64.rpm                                                                                                                                           10 MB/s | 758 kB     00:00
(92/158): pixman-0.40.0-6.el9_3.x86_64.rpm                                                                                                                                        4.2 MB/s | 269 kB     00:00
(93/158): pulseaudio-libs-15.0-2.el9.x86_64.rpm                                                                                                                                   4.4 MB/s | 666 kB     00:00
(94/158): libvirt-libs-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                                  18 MB/s | 4.8 MB     00:00
(95/158): libvirt-daemon-driver-storage-scsi-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                           185 kB/s |  30 kB     00:00
(96/158): mesa-dri-drivers-23.1.4-1.el9.x86_64.rpm                                                                                                                                 11 MB/s |  10 MB     00:00
(97/158): libvirt-daemon-driver-storage-rbd-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                            327 kB/s |  38 kB     00:00
(98/158): libvirt-daemon-driver-storage-mpath-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                          281 kB/s |  27 kB     00:00
(99/158): libvirt-daemon-driver-storage-logical-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                        957 kB/s |  34 kB     00:00
(100/158): libvirt-daemon-driver-storage-iscsi-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                         897 kB/s |  30 kB     00:00
(101/158): libvirt-daemon-driver-storage-disk-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                          896 kB/s |  33 kB     00:00
(102/158): libvirt-daemon-driver-storage-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                               543 kB/s |  22 kB     00:00
(103/158): libvirt-daemon-driver-storage-core-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                          4.3 MB/s | 228 kB     00:00
(104/158): libvirt-daemon-driver-secret-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                3.4 MB/s | 170 kB     00:00
(105/158): libvirt-daemon-driver-nwfilter-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                              3.5 MB/s | 209 kB     00:00
(106/158): libvirt-daemon-driver-qemu-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                   11 MB/s | 909 kB     00:00
(107/158): libvirt-daemon-driver-nodedev-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                               2.6 MB/s | 194 kB     00:00
(108/158): libvirt-daemon-driver-network-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                               4.0 MB/s | 211 kB     00:00
(109/158): libvirt-daemon-config-nwfilter-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                              609 kB/s |  30 kB     00:00
(110/158): libvirt-daemon-driver-interface-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                             3.0 MB/s | 173 kB     00:00
(111/158): libvirt-daemon-config-network-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                               819 kB/s |  24 kB     00:00
(112/158): libvirt-daemon-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                              3.7 MB/s | 167 kB     00:00
(113/158): libvirt-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                                     574 kB/s |  22 kB     00:00
(114/158): libvirt-client-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                              6.7 MB/s | 426 kB     00:00
(115/158): boost-thread-1.75.0-8.el9.x86_64.rpm                                                                                                                                   1.4 MB/s |  55 kB     00:00
(116/158): boost-system-1.75.0-8.el9.x86_64.rpm                                                                                                                                   356 kB/s |  13 kB     00:00
(117/158): boost-iostreams-1.75.0-8.el9.x86_64.rpm                                                                                                                                1.0 MB/s |  38 kB     00:00
(118/158): swtpm-tools-0.8.0-1.el9.x86_64.rpm                                                                                                                                     2.9 MB/s | 117 kB     00:00
(119/158): swtpm-libs-0.8.0-1.el9.x86_64.rpm                                                                                                                                      1.2 MB/s |  50 kB     00:00
(120/158): swtpm-0.8.0-1.el9.x86_64.rpm                                                                                                                                           1.1 MB/s |  42 kB     00:00
(121/158): librbd1-16.2.4-5.el9.x86_64.rpm                                                                                                                                         16 MB/s | 3.0 MB     00:00
(122/158): qemu-pr-helper-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                             2.3 MB/s | 486 kB     00:00
(123/158): qemu-kvm-ui-opengl-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                         631 kB/s |  73 kB     00:00
(124/158): qemu-kvm-ui-egl-headless-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                   590 kB/s |  67 kB     00:00
(125/158): librados2-16.2.4-5.el9.x86_64.rpm                                                                                                                                      9.8 MB/s | 3.4 MB     00:00
(126/158): qemu-kvm-tools-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                             6.4 MB/s | 559 kB     00:00
(127/158): qemu-kvm-device-usb-redirect-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                               1.7 MB/s |  85 kB     00:00
(128/158): qemu-kvm-device-usb-host-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                   1.8 MB/s |  80 kB     00:00
(129/158): qemu-kvm-device-display-virtio-vga-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                         1.8 MB/s |  67 kB     00:00
(130/158): qemu-kvm-docs-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                              9.2 MB/s | 1.1 MB     00:00
(131/158): qemu-kvm-device-display-virtio-gpu-pci-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                     1.8 MB/s |  66 kB     00:00
(132/158): qemu-kvm-device-display-virtio-gpu-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                         1.9 MB/s |  81 kB     00:00
(133/158): qemu-kvm-block-rbd-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                         570 kB/s |  76 kB     00:00
(134/158): qemu-kvm-core-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                               14 MB/s | 4.1 MB     00:00
(135/158): qemu-kvm-common-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                            2.1 MB/s | 642 kB     00:00
(136/158): qemu-kvm-audio-pa-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                          458 kB/s |  71 kB     00:00
(137/158): qemu-kvm-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                                   1.7 MB/s |  62 kB     00:00
(138/158): rubygem-psych-3.3.2-160.el9_0.x86_64.rpm                                                                                                                               925 kB/s |  51 kB     00:00
(139/158): qemu-img-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                                    15 MB/s | 2.4 MB     00:00
(140/158): rubygem-json-2.5.1-160.el9_0.x86_64.rpm                                                                                                                                394 kB/s |  54 kB     00:00
(141/158): rubygem-io-console-0.5.7-160.el9_0.x86_64.rpm                                                                                                                          226 kB/s |  25 kB     00:00
(142/158): rubygem-bigdecimal-3.0.0-160.el9_0.x86_64.rpm                                                                                                                          1.5 MB/s |  54 kB     00:00
(143/158): ruby-3.0.4-160.el9_0.x86_64.rpm                                                                                                                                        1.0 MB/s |  41 kB     00:00
(144/158): libnbd-1.16.0-1.el9.x86_64.rpm                                                                                                                                         3.1 MB/s | 160 kB     00:00
(145/158): libX11-xcb-1.7.0-8.el9.x86_64.rpm                                                                                                                                      224 kB/s | 9.9 kB     00:00
(146/158): dnsmasq-2.85-14.el9_3.1.x86_64.rpm                                                                                                                                     4.8 MB/s | 325 kB     00:00
(147/158): ruby-libs-3.0.4-160.el9_0.x86_64.rpm                                                                                                                                   9.8 MB/s | 3.2 MB     00:00
(148/158): capstone-4.0.2-10.el9.x86_64.rpm                                                                                                                                       4.4 MB/s | 766 kB     00:00
(149/158): passt-0^20230818.g0af928e-4.el9.x86_64.rpm                                                                                                                             1.6 MB/s | 177 kB     00:00
(150/158): passt-selinux-0^20230818.g0af928e-4.el9.noarch.rpm                                                                                                                     352 kB/s |  29 kB     00:00
(151/158): edk2-ovmf-20230524-4.el9_3.2.noarch.rpm                                                                                                                                 12 MB/s | 5.4 MB     00:00
(152/158): libblkio-1.3.0-1.el9.x86_64.rpm                                                                                                                                        6.1 MB/s | 512 kB     00:00
(153/158): qemu-kvm-block-blkio-8.0.0-16.el9_3.3.x86_64.rpm                                                                                                                       946 kB/s |  74 kB     00:00
(154/158): libvirt-daemon-plugin-lockd-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                 1.1 MB/s |  33 kB     00:00
(155/158): libvirt-daemon-log-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                          1.9 MB/s |  61 kB     00:00
(156/158): libvirt-daemon-proxy-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                        2.8 MB/s | 165 kB     00:00
(157/158): libvirt-daemon-lock-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                         1.5 MB/s |  58 kB     00:00
(158/158): libvirt-daemon-common-9.5.0-7.2.el9_3.x86_64.rpm                                                                                                                       2.2 MB/s | 128 kB     00:00
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                                                                                              11 MB/s |  63 MB     00:05
OpenNebula Community Edition                                                                                                                                                       18 kB/s | 3.1 kB     00:00
Importing GPG key 0x906DC27C:
 Userid     : "OpenNebula Repository <contact@opennebula.io>"
 Fingerprint: 0B2D 385C 7C93 04B1 1A03 67B9 05A0 5927 906D C27C
 From       : https://downloads.opennebula.io/repo/repo2.key
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                                                                                                          1/1
  Installing       : ruby-libs-3.0.4-160.el9_0.x86_64                                                                                                                                                       1/158
  Installing       : rubygem-psych-3.3.2-160.el9_0.x86_64                                                                                                                                                   2/158
  Installing       : rubygem-json-2.5.1-160.el9_0.x86_64                                                                                                                                                    3/158
  Installing       : rubygem-io-console-0.5.7-160.el9_0.x86_64                                                                                                                                              4/158
  Installing       : rubygem-bigdecimal-3.0.0-160.el9_0.x86_64                                                                                                                                              5/158
  Installing       : rubygem-rdoc-6.3.3-160.el9_0.noarch                                                                                                                                                    6/158
  Installing       : rubygem-bundler-2.2.33-160.el9_0.noarch                                                                                                                                                7/158
  Installing       : ruby-default-gems-3.0.4-160.el9_0.noarch                                                                                                                                               8/158
  Installing       : ruby-3.0.4-160.el9_0.x86_64                                                                                                                                                            9/158
  Installing       : rubygems-3.2.33-160.el9_0.noarch                                                                                                                                                      10/158
  Installing       : libX11-xcb-1.7.0-8.el9.x86_64                                                                                                                                                         11/158
  Installing       : pixman-0.40.0-6.el9_3.x86_64                                                                                                                                                          12/158
  Installing       : liburing-2.3-2.el9.x86_64                                                                                                                                                             13/158
  Installing       : qemu-img-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                                   14/158
  Installing       : mesa-libglapi-23.1.4-1.el9.x86_64                                                                                                                                                     15/158
  Installing       : libogg-2:1.3.4-6.el9.x86_64                                                                                                                                                           16/158
  Installing       : libxshmfence-1.3-10.el9.x86_64                                                                                                                                                        17/158
  Installing       : libusbx-1.0.26-1.el9.x86_64                                                                                                                                                           18/158
  Installing       : passt-0^20230818.g0af928e-4.el9.x86_64                                                                                                                                                19/158
  Running scriptlet: passt-selinux-0^20230818.g0af928e-4.el9.noarch                                                                                                                                        20/158
  Installing       : passt-selinux-0^20230818.g0af928e-4.el9.noarch                                                                                                                                        20/158
  Running scriptlet: passt-selinux-0^20230818.g0af928e-4.el9.noarch                                                                                                                                        20/158
  Installing       : libwayland-server-1.21.0-1.el9.x86_64                                                                                                                                                 21/158
  Installing       : libtpms-0.9.1-3.20211126git1ff6fe1f43.el9_2.x86_64                                                                                                                                    22/158
  Installing       : libglvnd-1:1.3.4-1.el9.x86_64                                                                                                                                                         23/158
  Installing       : libepoxy-1.5.5-4.el9.x86_64                                                                                                                                                           24/158
  Installing       : libnfsidmap-1:2.5.4-20.el9.x86_64                                                                                                                                                     25/158
  Installing       : daxctl-libs-71.1-8.el9.x86_64                                                                                                                                                         26/158
  Installing       : libpciaccess-0.16-6.el9.x86_64                                                                                                                                                        27/158
  Installing       : libdrm-2.4.115-1.el9.x86_64                                                                                                                                                           28/158
  Installing       : mesa-libgbm-23.1.4-1.el9.x86_64                                                                                                                                                       29/158
  Installing       : python3-setuptools-53.0.0-12.el9.noarch                                                                                                                                               30/158
  Installing       : librdmacm-46.0-1.el9.x86_64                                                                                                                                                           31/158
  Installing       : python3-setools-4.4.3-1.el9.x86_64                                                                                                                                                    32/158
  Installing       : python3-distro-1.5.0-7.el9.noarch                                                                                                                                                     33/158
  Installing       : ndctl-libs-71.1-8.el9.x86_64                                                                                                                                                          34/158
  Installing       : libpmem-1.12.1-1.el9.x86_64                                                                                                                                                           35/158
  Installing       : swtpm-libs-0.8.0-1.el9.x86_64                                                                                                                                                         36/158
  Installing       : usbredir-0.13.0-2.el9.x86_64                                                                                                                                                          37/158
  Installing       : flac-libs-1.3.3-10.el9_2.1.x86_64                                                                                                                                                     38/158
  Installing       : libvorbis-1:1.3.7-5.el9.x86_64                                                                                                                                                        39/158
  Installing       : rubygem-sqlite3-1.4.2-8.el9.x86_64                                                                                                                                                    40/158
  Installing       : rubygem-rexml-3.2.5-160.el9_0.noarch                                                                                                                                                  41/158
  Installing       : libblkio-1.3.0-1.el9.x86_64                                                                                                                                                           42/158
  Installing       : capstone-4.0.2-10.el9.x86_64                                                                                                                                                          43/158
  Running scriptlet: dnsmasq-2.85-14.el9_3.1.x86_64                                                                                                                                                        44/158
  Installing       : dnsmasq-2.85-14.el9_3.1.x86_64                                                                                                                                                        44/158
  Running scriptlet: dnsmasq-2.85-14.el9_3.1.x86_64                                                                                                                                                        44/158
  Installing       : edk2-ovmf-20230524-4.el9_3.2.noarch                                                                                                                                                   45/158
  Installing       : libnbd-1.16.0-1.el9.x86_64                                                                                                                                                            46/158
  Installing       : qemu-kvm-docs-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                              47/158
  Installing       : boost-iostreams-1.75.0-8.el9.x86_64                                                                                                                                                   48/158
  Installing       : boost-system-1.75.0-8.el9.x86_64                                                                                                                                                      49/158
  Installing       : boost-thread-1.75.0-8.el9.x86_64                                                                                                                                                      50/158
  Installing       : librados2-2:16.2.4-5.el9.x86_64                                                                                                                                                       51/158
  Running scriptlet: librados2-2:16.2.4-5.el9.x86_64                                                                                                                                                       51/158
  Installing       : librbd1-2:16.2.4-5.el9.x86_64                                                                                                                                                         52/158
  Running scriptlet: librbd1-2:16.2.4-5.el9.x86_64                                                                                                                                                         52/158
  Installing       : mdevctl-1.1.0-4.el9.x86_64                                                                                                                                                            53/158
  Installing       : libwayland-client-1.21.0-1.el9.x86_64                                                                                                                                                 54/158
  Installing       : libglvnd-egl-1:1.3.4-1.el9.x86_64                                                                                                                                                     55/158
  Installing       : mesa-libEGL-23.1.4-1.el9.x86_64                                                                                                                                                       56/158
  Installing       : oniguruma-6.9.6-1.el9.5.x86_64                                                                                                                                                        57/158
  Installing       : jq-1.6-15.el9.x86_64                                                                                                                                                                  58/158
  Installing       : opus-1.3.1-10.el9.x86_64                                                                                                                                                              59/158
  Installing       : libXxf86vm-1.1.4-18.el9.x86_64                                                                                                                                                        60/158
  Installing       : python3-libsemanage-3.5-2.el9.x86_64                                                                                                                                                  61/158
  Installing       : libslirp-4.4.0-7.el9.x86_64                                                                                                                                                           62/158
  Installing       : mesa-filesystem-23.1.4-1.el9.x86_64                                                                                                                                                   63/158
  Installing       : mesa-dri-drivers-23.1.4-1.el9.x86_64                                                                                                                                                  64/158
  Installing       : gsm-1.0.19-6.el9.x86_64                                                                                                                                                               65/158
  Installing       : libsndfile-1.0.31-7.el9.x86_64                                                                                                                                                        66/158
  Installing       : ipxe-roms-qemu-20200823-9.git4bd064de.el9.noarch                                                                                                                                      67/158
  Running scriptlet: unbound-libs-1.16.2-3.el9_3.1.x86_64                                                                                                                                                  68/158
  Installing       : unbound-libs-1.16.2-3.el9_3.1.x86_64                                                                                                                                                  68/158
  Running scriptlet: unbound-libs-1.16.2-3.el9_3.1.x86_64                                                                                                                                                  68/158
Created symlink /etc/systemd/system/timers.target.wants/unbound-anchor.timer → /usr/lib/systemd/system/unbound-anchor.timer.

  Installing       : gnutls-dane-3.7.6-23.el9_3.3.x86_64                                                                                                                                                   69/158
  Installing       : gnutls-utils-3.7.6-23.el9_3.3.x86_64                                                                                                                                                  70/158
  Installing       : virtiofsd-1.7.2-1.el9.x86_64                                                                                                                                                          71/158
  Installing       : seabios-bin-1.16.1-1.el9.noarch                                                                                                                                                       72/158
  Installing       : seavgabios-bin-1.16.1-1.el9.noarch                                                                                                                                                    73/158
  Installing       : qemu-kvm-common-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                            74/158
  Running scriptlet: qemu-kvm-common-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                            74/158
  Installing       : qemu-kvm-device-display-virtio-gpu-17:8.0.0-16.el9_3.3.x86_64                                                                                                                         75/158
  Installing       : qemu-kvm-device-display-virtio-gpu-pci-17:8.0.0-16.el9_3.3.x86_64                                                                                                                     76/158
  Installing       : qemu-kvm-device-usb-redirect-17:8.0.0-16.el9_3.3.x86_64                                                                                                                               77/158
  Installing       : qemu-kvm-device-usb-host-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                   78/158
  Installing       : qemu-kvm-device-display-virtio-vga-17:8.0.0-16.el9_3.3.x86_64                                                                                                                         79/158
  Installing       : qemu-kvm-block-rbd-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                         80/158
  Installing       : qemu-kvm-block-blkio-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                       81/158
  Installing       : yajl-2.1.0-22.el9.x86_64                                                                                                                                                              82/158
  Installing       : xkeyboard-config-2.33-2.el9.noarch                                                                                                                                                    83/158
  Installing       : libxkbcommon-1.0.3-4.el9.x86_64                                                                                                                                                       84/158
  Installing       : qemu-kvm-tools-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                             85/158
  Installing       : libXfixes-5.0.3-16.el9.x86_64                                                                                                                                                         86/158
  Installing       : libglvnd-glx-1:1.3.4-1.el9.x86_64                                                                                                                                                     87/158
  Installing       : mesa-libGL-23.1.4-1.el9.x86_64                                                                                                                                                        88/158
  Installing       : qemu-kvm-ui-opengl-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                         89/158
  Installing       : qemu-kvm-ui-egl-headless-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                   90/158
  Installing       : libasyncns-0.8-22.el9.x86_64                                                                                                                                                          91/158
  Installing       : pulseaudio-libs-15.0-2.el9.x86_64                                                                                                                                                     92/158
  Installing       : qemu-kvm-audio-pa-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                          93/158
  Installing       : python3-audit-3.0.7-104.el9.x86_64                                                                                                                                                    94/158
  Installing       : libfdt-1.6.0-7.el9.x86_64                                                                                                                                                             95/158
  Installing       : qemu-kvm-core-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                              96/158
  Installing       : checkpolicy-3.5-1.el9.x86_64                                                                                                                                                          97/158
  Installing       : python3-policycoreutils-3.5-3.el9_3.noarch                                                                                                                                            98/158
  Installing       : policycoreutils-python-utils-3.5-3.el9_3.noarch                                                                                                                                       99/158
  Installing       : swtpm-0.8.0-1.el9.x86_64                                                                                                                                                             100/158
  Running scriptlet: swtpm-0.8.0-1.el9.x86_64                                                                                                                                                             100/158
  Installing       : augeas-libs-1.13.0-5.el9.x86_64                                                                                                                                                      101/158
  Installing       : augeas-1.13.0-5.el9.x86_64                                                                                                                                                           102/158
  Installing       : device-mapper-multipath-libs-0.8.7-22.el9.x86_64                                                                                                                                     103/158
  Installing       : qemu-pr-helper-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                            104/158
  Installing       : qemu-kvm-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                                  105/158
  Installing       : keyutils-1.6.3-1.el9.x86_64                                                                                                                                                          106/158
  Running scriptlet: cyrus-sasl-2.1.27-21.el9.x86_64                                                                                                                                                      107/158
  Installing       : cyrus-sasl-2.1.27-21.el9.x86_64                                                                                                                                                      107/158
  Running scriptlet: cyrus-sasl-2.1.27-21.el9.x86_64                                                                                                                                                      107/158
  Installing       : cyrus-sasl-gssapi-2.1.27-21.el9.x86_64                                                                                                                                               108/158
  Installing       : libvirt-libs-9.5.0-7.2.el9_3.x86_64                                                                                                                                                  109/158
  Installing       : libvirt-client-9.5.0-7.2.el9_3.x86_64                                                                                                                                                110/158
  Running scriptlet: libvirt-daemon-common-9.5.0-7.2.el9_3.x86_64                                                                                                                                         111/158
  Installing       : libvirt-daemon-common-9.5.0-7.2.el9_3.x86_64                                                                                                                                         111/158
  Running scriptlet: libvirt-daemon-driver-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                112/158
  Installing       : libvirt-daemon-driver-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                112/158
  Running scriptlet: libvirt-daemon-driver-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 113/158
  Installing       : libvirt-daemon-driver-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 113/158
  Running scriptlet: libvirt-daemon-driver-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 113/158
  Running scriptlet: libvirt-daemon-log-9.5.0-7.2.el9_3.x86_64                                                                                                                                            114/158
  Installing       : libvirt-daemon-log-9.5.0-7.2.el9_3.x86_64                                                                                                                                            114/158
  Running scriptlet: libvirt-daemon-lock-9.5.0-7.2.el9_3.x86_64                                                                                                                                           115/158
  Installing       : libvirt-daemon-lock-9.5.0-7.2.el9_3.x86_64                                                                                                                                           115/158
  Installing       : libvirt-daemon-plugin-lockd-9.5.0-7.2.el9_3.x86_64                                                                                                                                   116/158
  Running scriptlet: libvirt-daemon-config-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 117/158
  Installing       : libvirt-daemon-config-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 117/158
  Running scriptlet: libvirt-daemon-config-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 117/158
  Running scriptlet: libvirt-daemon-config-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                118/158
  Installing       : libvirt-daemon-config-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                118/158
  Running scriptlet: libvirt-daemon-config-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                118/158
  Running scriptlet: libvirt-daemon-driver-secret-9.5.0-7.2.el9_3.x86_64                                                                                                                                  119/158
  Installing       : libvirt-daemon-driver-secret-9.5.0-7.2.el9_3.x86_64                                                                                                                                  119/158
  Running scriptlet: libvirt-daemon-driver-nodedev-9.5.0-7.2.el9_3.x86_64                                                                                                                                 120/158
  Installing       : libvirt-daemon-driver-nodedev-9.5.0-7.2.el9_3.x86_64                                                                                                                                 120/158
  Running scriptlet: libvirt-daemon-driver-interface-9.5.0-7.2.el9_3.x86_64                                                                                                                               121/158
  Installing       : libvirt-daemon-driver-interface-9.5.0-7.2.el9_3.x86_64                                                                                                                               121/158
  Installing       : lzop-1.04-8.el9.x86_64                                                                                                                                                               122/158
  Installing       : systemd-container-252-18.el9.0.1.rocky.x86_64                                                                                                                                        123/158
  Installing       : pciutils-3.7.0-5.el9.x86_64                                                                                                                                                          124/158
  Installing       : libev-4.33-5.el9.x86_64                                                                                                                                                              125/158
  Installing       : libverto-libev-0.3.2-3.el9.x86_64                                                                                                                                                    126/158
  Installing       : gssproxy-0.8.4-6.el9.x86_64                                                                                                                                                          127/158
  Running scriptlet: gssproxy-0.8.4-6.el9.x86_64                                                                                                                                                          127/158
  Installing       : isns-utils-libs-0.101-4.el9.x86_64                                                                                                                                                   128/158
  Installing       : iscsi-initiator-utils-iscsiuio-6.2.1.4-3.git2a8f9d8.el9.x86_64                                                                                                                       129/158
  Running scriptlet: iscsi-initiator-utils-iscsiuio-6.2.1.4-3.git2a8f9d8.el9.x86_64                                                                                                                       129/158
Created symlink /etc/systemd/system/sockets.target.wants/iscsiuio.socket → /usr/lib/systemd/system/iscsiuio.socket.

  Installing       : iscsi-initiator-utils-6.2.1.4-3.git2a8f9d8.el9.x86_64                                                                                                                                130/158
  Running scriptlet: iscsi-initiator-utils-6.2.1.4-3.git2a8f9d8.el9.x86_64                                                                                                                                130/158
Created symlink /etc/systemd/system/remote-fs.target.wants/iscsi.service → /usr/lib/systemd/system/iscsi.service.
Created symlink /etc/systemd/system/sockets.target.wants/iscsid.socket → /usr/lib/systemd/system/iscsid.socket.
Created symlink /etc/systemd/system/sysinit.target.wants/iscsi-onboot.service → /usr/lib/systemd/system/iscsi-onboot.service.

  Running scriptlet: rpcbind-1.2.6-5.el9.x86_64                                                                                                                                                           131/158
  Installing       : rpcbind-1.2.6-5.el9.x86_64                                                                                                                                                           131/158
  Running scriptlet: rpcbind-1.2.6-5.el9.x86_64                                                                                                                                                           131/158
Created symlink /etc/systemd/system/multi-user.target.wants/rpcbind.service → /usr/lib/systemd/system/rpcbind.service.
Created symlink /etc/systemd/system/sockets.target.wants/rpcbind.socket → /usr/lib/systemd/system/rpcbind.socket.

  Installing       : numad-0.5-36.20150602git.el9.x86_64                                                                                                                                                  132/158
  Running scriptlet: numad-0.5-36.20150602git.el9.x86_64                                                                                                                                                  132/158
  Installing       : python3-pyyaml-5.4.1-6.el9.x86_64                                                                                                                                                    133/158
  Installing       : quota-nls-1:4.06-6.el9.noarch                                                                                                                                                        134/158
  Installing       : quota-1:4.06-6.el9.x86_64                                                                                                                                                            135/158
  Running scriptlet: nfs-utils-1:2.5.4-20.el9.x86_64                                                                                                                                                      136/158
  Installing       : nfs-utils-1:2.5.4-20.el9.x86_64                                                                                                                                                      136/158
  Running scriptlet: nfs-utils-1:2.5.4-20.el9.x86_64                                                                                                                                                      136/158
  Running scriptlet: libvirt-daemon-driver-storage-core-9.5.0-7.2.el9_3.x86_64                                                                                                                            137/158
  Installing       : libvirt-daemon-driver-storage-core-9.5.0-7.2.el9_3.x86_64                                                                                                                            137/158
  Installing       : libvirt-daemon-driver-storage-scsi-9.5.0-7.2.el9_3.x86_64                                                                                                                            138/158
  Installing       : libvirt-daemon-driver-storage-rbd-9.5.0-7.2.el9_3.x86_64                                                                                                                             139/158
  Installing       : libvirt-daemon-driver-storage-mpath-9.5.0-7.2.el9_3.x86_64                                                                                                                           140/158
  Installing       : libvirt-daemon-driver-storage-logical-9.5.0-7.2.el9_3.x86_64                                                                                                                         141/158
  Installing       : libvirt-daemon-driver-storage-iscsi-9.5.0-7.2.el9_3.x86_64                                                                                                                           142/158
  Installing       : libvirt-daemon-driver-storage-disk-9.5.0-7.2.el9_3.x86_64                                                                                                                            143/158
  Installing       : libvirt-daemon-driver-storage-9.5.0-7.2.el9_3.x86_64                                                                                                                                 144/158
  Installing       : json-glib-1.6.6-1.el9.x86_64                                                                                                                                                         145/158
  Installing       : swtpm-tools-0.8.0-1.el9.x86_64                                                                                                                                                       146/158
  Running scriptlet: libvirt-daemon-driver-qemu-9.5.0-7.2.el9_3.x86_64                                                                                                                                    147/158
  Installing       : libvirt-daemon-driver-qemu-9.5.0-7.2.el9_3.x86_64                                                                                                                                    147/158
  Running scriptlet: opennebula-common-onecfg-6.8.0-1.el9.noarch                                                                                                                                          148/158
  Installing       : opennebula-common-onecfg-6.8.0-1.el9.noarch                                                                                                                                          148/158
  Running scriptlet: opennebula-common-6.8.0-1.el9.noarch                                                                                                                                                 149/158
  Installing       : opennebula-common-6.8.0-1.el9.noarch                                                                                                                                                 149/158
  Running scriptlet: opennebula-common-6.8.0-1.el9.noarch                                                                                                                                                 149/158
  Installing       : libretls-3.8.1-1.el9.x86_64                                                                                                                                                          150/158
  Installing       : libmd-1.1.0-1.el9.x86_64                                                                                                                                                             151/158
  Installing       : libbsd-0.12.2-1.el9.x86_64                                                                                                                                                           152/158
  Installing       : netcat-1.226-1.el9.x86_64                                                                                                                                                            153/158
  Running scriptlet: netcat-1.226-1.el9.x86_64                                                                                                                                                            153/158
  Running scriptlet: libvirt-daemon-proxy-9.5.0-7.2.el9_3.x86_64                                                                                                                                          154/158
  Installing       : libvirt-daemon-proxy-9.5.0-7.2.el9_3.x86_64                                                                                                                                          154/158
  Running scriptlet: libvirt-daemon-9.5.0-7.2.el9_3.x86_64                                                                                                                                                155/158
  Installing       : libvirt-daemon-9.5.0-7.2.el9_3.x86_64                                                                                                                                                155/158
  Installing       : libvirt-9.5.0-7.2.el9_3.x86_64                                                                                                                                                       156/158
  Installing       : opennebula-node-kvm-6.8.0-1.el9.noarch                                                                                                                                               157/158
  Running scriptlet: opennebula-node-kvm-6.8.0-1.el9.noarch                                                                                                                                               157/158
  Installing       : sssd-nfs-idmap-2.9.1-4.el9_3.5.x86_64                                                                                                                                                158/158
  Running scriptlet: passt-selinux-0^20230818.g0af928e-4.el9.noarch                                                                                                                                       158/158
  Running scriptlet: swtpm-0.8.0-1.el9.x86_64                                                                                                                                                             158/158
  Running scriptlet: libvirt-daemon-common-9.5.0-7.2.el9_3.x86_64                                                                                                                                         158/158
  Running scriptlet: libvirt-daemon-driver-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtnwfilterd.socket → /usr/lib/systemd/system/virtnwfilterd.socket.

  Running scriptlet: libvirt-daemon-driver-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtnetworkd.socket → /usr/lib/systemd/system/virtnetworkd.socket.

  Running scriptlet: libvirt-daemon-log-9.5.0-7.2.el9_3.x86_64                                                                                                                                            158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtlogd.socket → /usr/lib/systemd/system/virtlogd.socket.

  Running scriptlet: libvirt-daemon-lock-9.5.0-7.2.el9_3.x86_64                                                                                                                                           158/158
  Running scriptlet: libvirt-daemon-config-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 158/158
  Running scriptlet: libvirt-daemon-config-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                158/158
  Running scriptlet: libvirt-daemon-driver-secret-9.5.0-7.2.el9_3.x86_64                                                                                                                                  158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtsecretd.socket → /usr/lib/systemd/system/virtsecretd.socket.

  Running scriptlet: libvirt-daemon-driver-nodedev-9.5.0-7.2.el9_3.x86_64                                                                                                                                 158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtnodedevd.socket → /usr/lib/systemd/system/virtnodedevd.socket.

  Running scriptlet: libvirt-daemon-driver-interface-9.5.0-7.2.el9_3.x86_64                                                                                                                               158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtinterfaced.socket → /usr/lib/systemd/system/virtinterfaced.socket.

  Running scriptlet: libvirt-daemon-driver-storage-core-9.5.0-7.2.el9_3.x86_64                                                                                                                            158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtstoraged.socket → /usr/lib/systemd/system/virtstoraged.socket.

  Running scriptlet: libvirt-daemon-driver-qemu-9.5.0-7.2.el9_3.x86_64                                                                                                                                    158/158
Created symlink /etc/systemd/system/multi-user.target.wants/virtqemud.service → /usr/lib/systemd/system/virtqemud.service.
Created symlink /etc/systemd/system/sockets.target.wants/virtlockd.socket → /usr/lib/systemd/system/virtlockd.socket.
Created symlink /etc/systemd/system/sockets.target.wants/virtqemud.socket → /usr/lib/systemd/system/virtqemud.socket.
Created symlink /etc/systemd/system/sockets.target.wants/virtqemud-ro.socket → /usr/lib/systemd/system/virtqemud-ro.socket.
Created symlink /etc/systemd/system/sockets.target.wants/virtqemud-admin.socket → /usr/lib/systemd/system/virtqemud-admin.socket.

  Running scriptlet: libvirt-daemon-proxy-9.5.0-7.2.el9_3.x86_64                                                                                                                                          158/158
Created symlink /etc/systemd/system/sockets.target.wants/virtproxyd.socket → /usr/lib/systemd/system/virtproxyd.socket.

  Running scriptlet: libvirt-daemon-9.5.0-7.2.el9_3.x86_64                                                                                                                                                158/158
  Running scriptlet: sssd-nfs-idmap-2.9.1-4.el9_3.5.x86_64                                                                                                                                                158/158
Couldn't write '64' to 'kernel/random/read_wakeup_threshold', ignoring: No such file or directory
Couldn't write '1' to 'net/bridge/bridge-nf-call-arptables', ignoring: No such file or directory
Couldn't write '1' to 'net/bridge/bridge-nf-call-ip6tables', ignoring: No such file or directory
Couldn't write '1' to 'net/bridge/bridge-nf-call-iptables', ignoring: No such file or directory

  Verifying        : libbsd-0.12.2-1.el9.x86_64                                                                                                                                                             1/158
  Verifying        : libmd-1.1.0-1.el9.x86_64                                                                                                                                                               2/158
  Verifying        : libretls-3.8.1-1.el9.x86_64                                                                                                                                                            3/158
  Verifying        : netcat-1.226-1.el9.x86_64                                                                                                                                                              4/158
  Verifying        : rubygem-sqlite3-1.4.2-8.el9.x86_64                                                                                                                                                     5/158
  Verifying        : opennebula-common-6.8.0-1.el9.noarch                                                                                                                                                   6/158
  Verifying        : opennebula-common-onecfg-6.8.0-1.el9.noarch                                                                                                                                            7/158
  Verifying        : opennebula-node-kvm-6.8.0-1.el9.noarch                                                                                                                                                 8/158
  Verifying        : libverto-libev-0.3.2-3.el9.x86_64                                                                                                                                                      9/158
  Verifying        : json-glib-1.6.6-1.el9.x86_64                                                                                                                                                          10/158
  Verifying        : librdmacm-46.0-1.el9.x86_64                                                                                                                                                           11/158
  Verifying        : quota-1:4.06-6.el9.x86_64                                                                                                                                                             12/158
  Verifying        : quota-nls-1:4.06-6.el9.noarch                                                                                                                                                         13/158
  Verifying        : python3-pyyaml-5.4.1-6.el9.x86_64                                                                                                                                                     14/158
  Verifying        : python3-setuptools-53.0.0-12.el9.noarch                                                                                                                                               15/158
  Verifying        : numad-0.5-36.20150602git.el9.x86_64                                                                                                                                                   16/158
  Verifying        : rpcbind-1.2.6-5.el9.x86_64                                                                                                                                                            17/158
  Verifying        : isns-utils-libs-0.101-4.el9.x86_64                                                                                                                                                    18/158
  Verifying        : libev-4.33-5.el9.x86_64                                                                                                                                                               19/158
  Verifying        : libpciaccess-0.16-6.el9.x86_64                                                                                                                                                        20/158
  Verifying        : libusbx-1.0.26-1.el9.x86_64                                                                                                                                                           21/158
  Verifying        : iscsi-initiator-utils-iscsiuio-6.2.1.4-3.git2a8f9d8.el9.x86_64                                                                                                                        22/158
  Verifying        : iscsi-initiator-utils-6.2.1.4-3.git2a8f9d8.el9.x86_64                                                                                                                                 23/158
  Verifying        : pciutils-3.7.0-5.el9.x86_64                                                                                                                                                           24/158
  Verifying        : ndctl-libs-71.1-8.el9.x86_64                                                                                                                                                          25/158
  Verifying        : daxctl-libs-71.1-8.el9.x86_64                                                                                                                                                         26/158
  Verifying        : python3-setools-4.4.3-1.el9.x86_64                                                                                                                                                    27/158
  Verifying        : sssd-nfs-idmap-2.9.1-4.el9_3.5.x86_64                                                                                                                                                 28/158
  Verifying        : systemd-container-252-18.el9.0.1.rocky.x86_64                                                                                                                                         29/158
  Verifying        : gssproxy-0.8.4-6.el9.x86_64                                                                                                                                                           30/158
  Verifying        : lzop-1.04-8.el9.x86_64                                                                                                                                                                31/158
  Verifying        : nfs-utils-1:2.5.4-20.el9.x86_64                                                                                                                                                       32/158
  Verifying        : libnfsidmap-1:2.5.4-20.el9.x86_64                                                                                                                                                     33/158
  Verifying        : cyrus-sasl-gssapi-2.1.27-21.el9.x86_64                                                                                                                                                34/158
  Verifying        : cyrus-sasl-2.1.27-21.el9.x86_64                                                                                                                                                       35/158
  Verifying        : keyutils-1.6.3-1.el9.x86_64                                                                                                                                                           36/158
  Verifying        : device-mapper-multipath-libs-0.8.7-22.el9.x86_64                                                                                                                                      37/158
  Verifying        : python3-distro-1.5.0-7.el9.noarch                                                                                                                                                     38/158
  Verifying        : augeas-libs-1.13.0-5.el9.x86_64                                                                                                                                                       39/158
  Verifying        : augeas-1.13.0-5.el9.x86_64                                                                                                                                                            40/158
  Verifying        : checkpolicy-3.5-1.el9.x86_64                                                                                                                                                          41/158
  Verifying        : libfdt-1.6.0-7.el9.x86_64                                                                                                                                                             42/158
  Verifying        : python3-audit-3.0.7-104.el9.x86_64                                                                                                                                                    43/158
  Verifying        : libasyncns-0.8-22.el9.x86_64                                                                                                                                                          44/158
  Verifying        : libxkbcommon-1.0.3-4.el9.x86_64                                                                                                                                                       45/158
  Verifying        : libxshmfence-1.3-10.el9.x86_64                                                                                                                                                        46/158
  Verifying        : libepoxy-1.5.5-4.el9.x86_64                                                                                                                                                           47/158
  Verifying        : libXfixes-5.0.3-16.el9.x86_64                                                                                                                                                         48/158
  Verifying        : libogg-2:1.3.4-6.el9.x86_64                                                                                                                                                           49/158
  Verifying        : python3-policycoreutils-3.5-3.el9_3.noarch                                                                                                                                            50/158
  Verifying        : policycoreutils-python-utils-3.5-3.el9_3.noarch                                                                                                                                       51/158
  Verifying        : usbredir-0.13.0-2.el9.x86_64                                                                                                                                                          52/158
  Verifying        : xkeyboard-config-2.33-2.el9.noarch                                                                                                                                                    53/158
  Verifying        : yajl-2.1.0-22.el9.x86_64                                                                                                                                                              54/158
  Verifying        : seavgabios-bin-1.16.1-1.el9.noarch                                                                                                                                                    55/158
  Verifying        : seabios-bin-1.16.1-1.el9.noarch                                                                                                                                                       56/158
  Verifying        : libpmem-1.12.1-1.el9.x86_64                                                                                                                                                           57/158
  Verifying        : virtiofsd-1.7.2-1.el9.x86_64                                                                                                                                                          58/158
  Verifying        : unbound-libs-1.16.2-3.el9_3.1.x86_64                                                                                                                                                  59/158
  Verifying        : gnutls-utils-3.7.6-23.el9_3.3.x86_64                                                                                                                                                  60/158
  Verifying        : gnutls-dane-3.7.6-23.el9_3.3.x86_64                                                                                                                                                   61/158
  Verifying        : rubygems-3.2.33-160.el9_0.noarch                                                                                                                                                      62/158
  Verifying        : rubygem-rexml-3.2.5-160.el9_0.noarch                                                                                                                                                  63/158
  Verifying        : rubygem-rdoc-6.3.3-160.el9_0.noarch                                                                                                                                                   64/158
  Verifying        : rubygem-bundler-2.2.33-160.el9_0.noarch                                                                                                                                               65/158
  Verifying        : ruby-default-gems-3.0.4-160.el9_0.noarch                                                                                                                                              66/158
  Verifying        : ipxe-roms-qemu-20200823-9.git4bd064de.el9.noarch                                                                                                                                      67/158
  Verifying        : gsm-1.0.19-6.el9.x86_64                                                                                                                                                               68/158
  Verifying        : mesa-libglapi-23.1.4-1.el9.x86_64                                                                                                                                                     69/158
  Verifying        : mesa-libgbm-23.1.4-1.el9.x86_64                                                                                                                                                       70/158
  Verifying        : mesa-libGL-23.1.4-1.el9.x86_64                                                                                                                                                        71/158
  Verifying        : mesa-libEGL-23.1.4-1.el9.x86_64                                                                                                                                                       72/158
  Verifying        : mesa-filesystem-23.1.4-1.el9.x86_64                                                                                                                                                   73/158
  Verifying        : mesa-dri-drivers-23.1.4-1.el9.x86_64                                                                                                                                                  74/158
  Verifying        : flac-libs-1.3.3-10.el9_2.1.x86_64                                                                                                                                                     75/158
  Verifying        : libdrm-2.4.115-1.el9.x86_64                                                                                                                                                           76/158
  Verifying        : libsndfile-1.0.31-7.el9.x86_64                                                                                                                                                        77/158
  Verifying        : jq-1.6-15.el9.x86_64                                                                                                                                                                  78/158
  Verifying        : libslirp-4.4.0-7.el9.x86_64                                                                                                                                                           79/158
  Verifying        : liburing-2.3-2.el9.x86_64                                                                                                                                                             80/158
  Verifying        : libvorbis-1:1.3.7-5.el9.x86_64                                                                                                                                                        81/158
  Verifying        : libglvnd-glx-1:1.3.4-1.el9.x86_64                                                                                                                                                     82/158
  Verifying        : libglvnd-egl-1:1.3.4-1.el9.x86_64                                                                                                                                                     83/158
  Verifying        : libglvnd-1:1.3.4-1.el9.x86_64                                                                                                                                                         84/158
  Verifying        : libtpms-0.9.1-3.20211126git1ff6fe1f43.el9_2.x86_64                                                                                                                                    85/158
  Verifying        : python3-libsemanage-3.5-2.el9.x86_64                                                                                                                                                  86/158
  Verifying        : libXxf86vm-1.1.4-18.el9.x86_64                                                                                                                                                        87/158
  Verifying        : opus-1.3.1-10.el9.x86_64                                                                                                                                                              88/158
  Verifying        : oniguruma-6.9.6-1.el9.5.x86_64                                                                                                                                                        89/158
  Verifying        : libwayland-server-1.21.0-1.el9.x86_64                                                                                                                                                 90/158
  Verifying        : libwayland-client-1.21.0-1.el9.x86_64                                                                                                                                                 91/158
  Verifying        : mdevctl-1.1.0-4.el9.x86_64                                                                                                                                                            92/158
  Verifying        : pixman-0.40.0-6.el9_3.x86_64                                                                                                                                                          93/158
  Verifying        : pulseaudio-libs-15.0-2.el9.x86_64                                                                                                                                                     94/158
  Verifying        : libvirt-libs-9.5.0-7.2.el9_3.x86_64                                                                                                                                                   95/158
  Verifying        : libvirt-daemon-driver-storage-scsi-9.5.0-7.2.el9_3.x86_64                                                                                                                             96/158
  Verifying        : libvirt-daemon-driver-storage-rbd-9.5.0-7.2.el9_3.x86_64                                                                                                                              97/158
  Verifying        : libvirt-daemon-driver-storage-mpath-9.5.0-7.2.el9_3.x86_64                                                                                                                            98/158
  Verifying        : libvirt-daemon-driver-storage-logical-9.5.0-7.2.el9_3.x86_64                                                                                                                          99/158
  Verifying        : libvirt-daemon-driver-storage-iscsi-9.5.0-7.2.el9_3.x86_64                                                                                                                           100/158
  Verifying        : libvirt-daemon-driver-storage-disk-9.5.0-7.2.el9_3.x86_64                                                                                                                            101/158
  Verifying        : libvirt-daemon-driver-storage-core-9.5.0-7.2.el9_3.x86_64                                                                                                                            102/158
  Verifying        : libvirt-daemon-driver-storage-9.5.0-7.2.el9_3.x86_64                                                                                                                                 103/158
  Verifying        : libvirt-daemon-driver-secret-9.5.0-7.2.el9_3.x86_64                                                                                                                                  104/158
  Verifying        : libvirt-daemon-driver-qemu-9.5.0-7.2.el9_3.x86_64                                                                                                                                    105/158
  Verifying        : libvirt-daemon-driver-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                106/158
  Verifying        : libvirt-daemon-driver-nodedev-9.5.0-7.2.el9_3.x86_64                                                                                                                                 107/158
  Verifying        : libvirt-daemon-driver-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 108/158
  Verifying        : libvirt-daemon-driver-interface-9.5.0-7.2.el9_3.x86_64                                                                                                                               109/158
  Verifying        : libvirt-daemon-config-nwfilter-9.5.0-7.2.el9_3.x86_64                                                                                                                                110/158
  Verifying        : libvirt-daemon-config-network-9.5.0-7.2.el9_3.x86_64                                                                                                                                 111/158
  Verifying        : libvirt-daemon-9.5.0-7.2.el9_3.x86_64                                                                                                                                                112/158
  Verifying        : libvirt-client-9.5.0-7.2.el9_3.x86_64                                                                                                                                                113/158
  Verifying        : libvirt-9.5.0-7.2.el9_3.x86_64                                                                                                                                                       114/158
  Verifying        : boost-thread-1.75.0-8.el9.x86_64                                                                                                                                                     115/158
  Verifying        : boost-system-1.75.0-8.el9.x86_64                                                                                                                                                     116/158
  Verifying        : boost-iostreams-1.75.0-8.el9.x86_64                                                                                                                                                  117/158
  Verifying        : swtpm-tools-0.8.0-1.el9.x86_64                                                                                                                                                       118/158
  Verifying        : swtpm-libs-0.8.0-1.el9.x86_64                                                                                                                                                        119/158
  Verifying        : swtpm-0.8.0-1.el9.x86_64                                                                                                                                                             120/158
  Verifying        : librbd1-2:16.2.4-5.el9.x86_64                                                                                                                                                        121/158
  Verifying        : librados2-2:16.2.4-5.el9.x86_64                                                                                                                                                      122/158
  Verifying        : qemu-pr-helper-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                            123/158
  Verifying        : qemu-kvm-ui-opengl-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                        124/158
  Verifying        : qemu-kvm-ui-egl-headless-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                  125/158
  Verifying        : qemu-kvm-tools-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                            126/158
  Verifying        : qemu-kvm-docs-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                             127/158
  Verifying        : qemu-kvm-device-usb-redirect-17:8.0.0-16.el9_3.3.x86_64                                                                                                                              128/158
  Verifying        : qemu-kvm-device-usb-host-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                  129/158
  Verifying        : qemu-kvm-device-display-virtio-vga-17:8.0.0-16.el9_3.3.x86_64                                                                                                                        130/158
  Verifying        : qemu-kvm-device-display-virtio-gpu-pci-17:8.0.0-16.el9_3.3.x86_64                                                                                                                    131/158
  Verifying        : qemu-kvm-device-display-virtio-gpu-17:8.0.0-16.el9_3.3.x86_64                                                                                                                        132/158
  Verifying        : qemu-kvm-core-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                             133/158
  Verifying        : qemu-kvm-common-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                           134/158
  Verifying        : qemu-kvm-block-rbd-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                        135/158
  Verifying        : qemu-kvm-audio-pa-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                         136/158
  Verifying        : qemu-kvm-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                                  137/158
  Verifying        : qemu-img-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                                  138/158
  Verifying        : rubygem-psych-3.3.2-160.el9_0.x86_64                                                                                                                                                 139/158
  Verifying        : rubygem-json-2.5.1-160.el9_0.x86_64                                                                                                                                                  140/158
  Verifying        : rubygem-io-console-0.5.7-160.el9_0.x86_64                                                                                                                                            141/158
  Verifying        : rubygem-bigdecimal-3.0.0-160.el9_0.x86_64                                                                                                                                            142/158
  Verifying        : ruby-libs-3.0.4-160.el9_0.x86_64                                                                                                                                                     143/158
  Verifying        : ruby-3.0.4-160.el9_0.x86_64                                                                                                                                                          144/158
  Verifying        : libnbd-1.16.0-1.el9.x86_64                                                                                                                                                           145/158
  Verifying        : edk2-ovmf-20230524-4.el9_3.2.noarch                                                                                                                                                  146/158
  Verifying        : libX11-xcb-1.7.0-8.el9.x86_64                                                                                                                                                        147/158
  Verifying        : dnsmasq-2.85-14.el9_3.1.x86_64                                                                                                                                                       148/158
  Verifying        : capstone-4.0.2-10.el9.x86_64                                                                                                                                                         149/158
  Verifying        : passt-0^20230818.g0af928e-4.el9.x86_64                                                                                                                                               150/158
  Verifying        : passt-selinux-0^20230818.g0af928e-4.el9.noarch                                                                                                                                       151/158
  Verifying        : libblkio-1.3.0-1.el9.x86_64                                                                                                                                                          152/158
  Verifying        : qemu-kvm-block-blkio-17:8.0.0-16.el9_3.3.x86_64                                                                                                                                      153/158
  Verifying        : libvirt-daemon-proxy-9.5.0-7.2.el9_3.x86_64                                                                                                                                          154/158
  Verifying        : libvirt-daemon-plugin-lockd-9.5.0-7.2.el9_3.x86_64                                                                                                                                   155/158
  Verifying        : libvirt-daemon-log-9.5.0-7.2.el9_3.x86_64                                                                                                                                            156/158
  Verifying        : libvirt-daemon-lock-9.5.0-7.2.el9_3.x86_64                                                                                                                                           157/158
  Verifying        : libvirt-daemon-common-9.5.0-7.2.el9_3.x86_64                                                                                                                                         158/158

Installed:
  augeas-1.13.0-5.el9.x86_64                                          augeas-libs-1.13.0-5.el9.x86_64                                     boost-iostreams-1.75.0-8.el9.x86_64
  boost-system-1.75.0-8.el9.x86_64                                    boost-thread-1.75.0-8.el9.x86_64                                    capstone-4.0.2-10.el9.x86_64
  checkpolicy-3.5-1.el9.x86_64                                        cyrus-sasl-2.1.27-21.el9.x86_64                                     cyrus-sasl-gssapi-2.1.27-21.el9.x86_64
  daxctl-libs-71.1-8.el9.x86_64                                       device-mapper-multipath-libs-0.8.7-22.el9.x86_64                    dnsmasq-2.85-14.el9_3.1.x86_64
  edk2-ovmf-20230524-4.el9_3.2.noarch                                 flac-libs-1.3.3-10.el9_2.1.x86_64                                   gnutls-dane-3.7.6-23.el9_3.3.x86_64
  gnutls-utils-3.7.6-23.el9_3.3.x86_64                                gsm-1.0.19-6.el9.x86_64                                             gssproxy-0.8.4-6.el9.x86_64
  ipxe-roms-qemu-20200823-9.git4bd064de.el9.noarch                    iscsi-initiator-utils-6.2.1.4-3.git2a8f9d8.el9.x86_64               iscsi-initiator-utils-iscsiuio-6.2.1.4-3.git2a8f9d8.el9.x86_64
  isns-utils-libs-0.101-4.el9.x86_64                                  jq-1.6-15.el9.x86_64                                                json-glib-1.6.6-1.el9.x86_64
  keyutils-1.6.3-1.el9.x86_64                                         libX11-xcb-1.7.0-8.el9.x86_64                                       libXfixes-5.0.3-16.el9.x86_64
  libXxf86vm-1.1.4-18.el9.x86_64                                      libasyncns-0.8-22.el9.x86_64                                        libblkio-1.3.0-1.el9.x86_64
  libbsd-0.12.2-1.el9.x86_64                                          libdrm-2.4.115-1.el9.x86_64                                         libepoxy-1.5.5-4.el9.x86_64
  libev-4.33-5.el9.x86_64                                             libfdt-1.6.0-7.el9.x86_64                                           libglvnd-1:1.3.4-1.el9.x86_64
  libglvnd-egl-1:1.3.4-1.el9.x86_64                                   libglvnd-glx-1:1.3.4-1.el9.x86_64                                   libmd-1.1.0-1.el9.x86_64
  libnbd-1.16.0-1.el9.x86_64                                          libnfsidmap-1:2.5.4-20.el9.x86_64                                   libogg-2:1.3.4-6.el9.x86_64
  libpciaccess-0.16-6.el9.x86_64                                      libpmem-1.12.1-1.el9.x86_64                                         librados2-2:16.2.4-5.el9.x86_64
  librbd1-2:16.2.4-5.el9.x86_64                                       librdmacm-46.0-1.el9.x86_64                                         libretls-3.8.1-1.el9.x86_64
  libslirp-4.4.0-7.el9.x86_64                                         libsndfile-1.0.31-7.el9.x86_64                                      libtpms-0.9.1-3.20211126git1ff6fe1f43.el9_2.x86_64
  liburing-2.3-2.el9.x86_64                                           libusbx-1.0.26-1.el9.x86_64                                         libverto-libev-0.3.2-3.el9.x86_64
  libvirt-9.5.0-7.2.el9_3.x86_64                                      libvirt-client-9.5.0-7.2.el9_3.x86_64                               libvirt-daemon-9.5.0-7.2.el9_3.x86_64
  libvirt-daemon-common-9.5.0-7.2.el9_3.x86_64                        libvirt-daemon-config-network-9.5.0-7.2.el9_3.x86_64                libvirt-daemon-config-nwfilter-9.5.0-7.2.el9_3.x86_64
  libvirt-daemon-driver-interface-9.5.0-7.2.el9_3.x86_64              libvirt-daemon-driver-network-9.5.0-7.2.el9_3.x86_64                libvirt-daemon-driver-nodedev-9.5.0-7.2.el9_3.x86_64
  libvirt-daemon-driver-nwfilter-9.5.0-7.2.el9_3.x86_64               libvirt-daemon-driver-qemu-9.5.0-7.2.el9_3.x86_64                   libvirt-daemon-driver-secret-9.5.0-7.2.el9_3.x86_64
  libvirt-daemon-driver-storage-9.5.0-7.2.el9_3.x86_64                libvirt-daemon-driver-storage-core-9.5.0-7.2.el9_3.x86_64           libvirt-daemon-driver-storage-disk-9.5.0-7.2.el9_3.x86_64
  libvirt-daemon-driver-storage-iscsi-9.5.0-7.2.el9_3.x86_64          libvirt-daemon-driver-storage-logical-9.5.0-7.2.el9_3.x86_64        libvirt-daemon-driver-storage-mpath-9.5.0-7.2.el9_3.x86_64
  libvirt-daemon-driver-storage-rbd-9.5.0-7.2.el9_3.x86_64            libvirt-daemon-driver-storage-scsi-9.5.0-7.2.el9_3.x86_64           libvirt-daemon-lock-9.5.0-7.2.el9_3.x86_64
  libvirt-daemon-log-9.5.0-7.2.el9_3.x86_64                           libvirt-daemon-plugin-lockd-9.5.0-7.2.el9_3.x86_64                  libvirt-daemon-proxy-9.5.0-7.2.el9_3.x86_64
  libvirt-libs-9.5.0-7.2.el9_3.x86_64                                 libvorbis-1:1.3.7-5.el9.x86_64                                      libwayland-client-1.21.0-1.el9.x86_64
  libwayland-server-1.21.0-1.el9.x86_64                               libxkbcommon-1.0.3-4.el9.x86_64                                     libxshmfence-1.3-10.el9.x86_64
  lzop-1.04-8.el9.x86_64                                              mdevctl-1.1.0-4.el9.x86_64                                          mesa-dri-drivers-23.1.4-1.el9.x86_64
  mesa-filesystem-23.1.4-1.el9.x86_64                                 mesa-libEGL-23.1.4-1.el9.x86_64                                     mesa-libGL-23.1.4-1.el9.x86_64
  mesa-libgbm-23.1.4-1.el9.x86_64                                     mesa-libglapi-23.1.4-1.el9.x86_64                                   ndctl-libs-71.1-8.el9.x86_64
  netcat-1.226-1.el9.x86_64                                           nfs-utils-1:2.5.4-20.el9.x86_64                                     numad-0.5-36.20150602git.el9.x86_64
  oniguruma-6.9.6-1.el9.5.x86_64                                      opennebula-common-6.8.0-1.el9.noarch                                opennebula-common-onecfg-6.8.0-1.el9.noarch
  opennebula-node-kvm-6.8.0-1.el9.noarch                              opus-1.3.1-10.el9.x86_64                                            passt-0^20230818.g0af928e-4.el9.x86_64
  passt-selinux-0^20230818.g0af928e-4.el9.noarch                      pciutils-3.7.0-5.el9.x86_64                                         pixman-0.40.0-6.el9_3.x86_64
  policycoreutils-python-utils-3.5-3.el9_3.noarch                     pulseaudio-libs-15.0-2.el9.x86_64                                   python3-audit-3.0.7-104.el9.x86_64
  python3-distro-1.5.0-7.el9.noarch                                   python3-libsemanage-3.5-2.el9.x86_64                                python3-policycoreutils-3.5-3.el9_3.noarch
  python3-pyyaml-5.4.1-6.el9.x86_64                                   python3-setools-4.4.3-1.el9.x86_64                                  python3-setuptools-53.0.0-12.el9.noarch
  qemu-img-17:8.0.0-16.el9_3.3.x86_64                                 qemu-kvm-17:8.0.0-16.el9_3.3.x86_64                                 qemu-kvm-audio-pa-17:8.0.0-16.el9_3.3.x86_64
  qemu-kvm-block-blkio-17:8.0.0-16.el9_3.3.x86_64                     qemu-kvm-block-rbd-17:8.0.0-16.el9_3.3.x86_64                       qemu-kvm-common-17:8.0.0-16.el9_3.3.x86_64
  qemu-kvm-core-17:8.0.0-16.el9_3.3.x86_64                            qemu-kvm-device-display-virtio-gpu-17:8.0.0-16.el9_3.3.x86_64       qemu-kvm-device-display-virtio-gpu-pci-17:8.0.0-16.el9_3.3.x86_64
  qemu-kvm-device-display-virtio-vga-17:8.0.0-16.el9_3.3.x86_64       qemu-kvm-device-usb-host-17:8.0.0-16.el9_3.3.x86_64                 qemu-kvm-device-usb-redirect-17:8.0.0-16.el9_3.3.x86_64
  qemu-kvm-docs-17:8.0.0-16.el9_3.3.x86_64                            qemu-kvm-tools-17:8.0.0-16.el9_3.3.x86_64                           qemu-kvm-ui-egl-headless-17:8.0.0-16.el9_3.3.x86_64
  qemu-kvm-ui-opengl-17:8.0.0-16.el9_3.3.x86_64                       qemu-pr-helper-17:8.0.0-16.el9_3.3.x86_64                           quota-1:4.06-6.el9.x86_64
  quota-nls-1:4.06-6.el9.noarch                                       rpcbind-1.2.6-5.el9.x86_64                                          ruby-3.0.4-160.el9_0.x86_64
  ruby-default-gems-3.0.4-160.el9_0.noarch                            ruby-libs-3.0.4-160.el9_0.x86_64                                    rubygem-bigdecimal-3.0.0-160.el9_0.x86_64
  rubygem-bundler-2.2.33-160.el9_0.noarch                             rubygem-io-console-0.5.7-160.el9_0.x86_64                           rubygem-json-2.5.1-160.el9_0.x86_64
  rubygem-psych-3.3.2-160.el9_0.x86_64                                rubygem-rdoc-6.3.3-160.el9_0.noarch                                 rubygem-rexml-3.2.5-160.el9_0.noarch
  rubygem-sqlite3-1.4.2-8.el9.x86_64                                  rubygems-3.2.33-160.el9_0.noarch                                    seabios-bin-1.16.1-1.el9.noarch
  seavgabios-bin-1.16.1-1.el9.noarch                                  sssd-nfs-idmap-2.9.1-4.el9_3.5.x86_64                               swtpm-0.8.0-1.el9.x86_64
  swtpm-libs-0.8.0-1.el9.x86_64                                       swtpm-tools-0.8.0-1.el9.x86_64                                      systemd-container-252-18.el9.0.1.rocky.x86_64
  unbound-libs-1.16.2-3.el9_3.1.x86_64                                usbredir-0.13.0-2.el9.x86_64                                        virtiofsd-1.7.2-1.el9.x86_64
  xkeyboard-config-2.33-2.el9.noarch                                  yajl-2.1.0-22.el9.x86_64

Complete!
[vagrant@kvm2 ~]$ sudo systemctl start libvirtd
[vagrant@kvm2 ~]$ sudo systemctl enable libvirtd
Created symlink /etc/systemd/system/multi-user.target.wants/libvirtd.service → /usr/lib/systemd/system/libvirtd.service.
Created symlink /etc/systemd/system/sockets.target.wants/libvirtd.socket → /usr/lib/systemd/system/libvirtd.socket.
Created symlink /etc/systemd/system/sockets.target.wants/libvirtd-ro.socket → /usr/lib/systemd/system/libvirtd-ro.socket.
[vagrant@kvm2 ~]$ sudo firewall-cmd --zone=public --add-port=22/tcp --permanent
success
[vagrant@kvm2 ~]$ sudo firewall-cmd --zone=public --add-port=8472/udp --permanent
success
[vagrant@kvm2 ~]$ sudo firewall-cmd --reload
success
[vagrant@kvm2 ~]$

[vagrant@kvm2 ~]$ sudo su - oneadmin
[oneadmin@kvm2 ~]$ cd
[oneadmin@kvm2 ~]$ cd .ssh
[oneadmin@kvm2 .ssh]$ ls
config
[oneadmin@kvm2 .ssh]$ nano authorized_keys
[oneadmin@kvm2 .ssh]$ ls
authorized_keys  config
[oneadmin@kvm2 .ssh]$ ssh-keyscan 10.3.1.11 10.3.1.22 frontend frontend.one rocky9.localdomain > known_hosts
# 10.3.1.11:22 SSH-2.0-OpenSSH_8.7
# 10.3.1.11:22 SSH-2.0-OpenSSH_8.7
# 10.3.1.11:22 SSH-2.0-OpenSSH_8.7
# 10.3.1.11:22 SSH-2.0-OpenSSH_8.7
# 10.3.1.11:22 SSH-2.0-OpenSSH_8.7
# 10.3.1.22:22 SSH-2.0-OpenSSH_8.7
# 10.3.1.22:22 SSH-2.0-OpenSSH_8.7
# 10.3.1.22:22 SSH-2.0-OpenSSH_8.7
# 10.3.1.22:22 SSH-2.0-OpenSSH_8.7
# 10.3.1.22:22 SSH-2.0-OpenSSH_8.7
# rocky9.localdomain:22 SSH-2.0-OpenSSH_8.7
# rocky9.localdomain:22 SSH-2.0-OpenSSH_8.7
# rocky9.localdomain:22 SSH-2.0-OpenSSH_8.7
# rocky9.localdomain:22 SSH-2.0-OpenSSH_8.7
# rocky9.localdomain:22 SSH-2.0-OpenSSH_8.7

[oneadmin@kvm2 .ssh]$
[oneadmin@kvm2 .ssh]$ cd ..
[oneadmin@kvm2 ~]$
logout
[vagrant@kvm2 ~]$ sudo nano /etc/hosts
[vagrant@kvm

[vagrant@kvm2 ~]$ sudo nano /etc/hosts
[vagrant@kvm2 ~]$ sudo ip link add name vxlan_bridge type bridge
[vagrant@kvm2 ~]$ sudo ip link set dev vxlan_bridge up
[vagrant@kvm2 ~]$ sudo ip addr add 10.220.220.202/24 dev vxlan_bridge
[vagrant@kvm2 ~]$ sudo firewall-cmd --add-interface=vxlan_bridge --zone=public --permanent
success
[vagrant@kvm2 ~]$ sudo firewall-cmd --add-masquerade --permanent
success
[vagrant@kvm2 ~]$ sudo firewall-cmd --reload
success
[vagrant@kvm2 ~]$ sudo systemctl start libvirtd


```
**🌞 Lancer une deuxième VM**

les deux vm sont dans le meme réseau mais des hyperviseurs différents

```
[vagrant@kvm2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:cb:b3:b6 brd ff:ff:ff:ff:ff:ff
    altname enp0s3
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute eth0
       valid_lft 83126sec preferred_lft 83126sec
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:19:9a:f2 brd ff:ff:ff:ff:ff:ff
    altname enp0s8
    inet 10.3.1.22/24 brd 10.3.1.255 scope global noprefixroute eth1
       valid_lft forever preferred_lft forever
4: virbr0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
    link/ether 52:54:00:6b:88:c3 brd ff:ff:ff:ff:ff:ff
    inet 192.168.122.1/24 brd 192.168.122.255 scope global virbr0
       valid_lft forever preferred_lft forever
5: vxlan_bridge: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue state UP group default qlen 1000
    link/ether 7e:69:2a:f3:57:56 brd ff:ff:ff:ff:ff:ff
    inet 10.220.220.202/24 scope global vxlan_bridge
       valid_lft forever preferred_lft forever
6: eth1.4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master vxlan_bridge state UNKNOWN group default qlen 1000
    link/ether 7e:69:2a:f3:57:56 brd ff:ff:ff:ff:ff:ff
7: one-16-0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master vxlan_bridge state UNKNOWN group default qlen 1000
    link/ether fe:00:0a:dc:dc:0b brd ff:ff:ff:ff:ff:ff


[vagrant@kvm1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:cb:b3:b6 brd ff:ff:ff:ff:ff:ff
    altname enp0s3
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute eth0
       valid_lft 77767sec preferred_lft 77767sec
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:98:2c:dd brd ff:ff:ff:ff:ff:ff
    altname enp0s8
    inet 10.3.1.21/24 brd 10.3.1.255 scope global noprefixroute eth1
       valid_lft forever preferred_lft forever
4: vxlan_bridge: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue state UP group default qlen 1000
    link/ether 42:cf:df:63:0f:48 brd ff:ff:ff:ff:ff:ff
    inet 10.220.220.201/24 scope global vxlan_bridge
       valid_lft forever preferred_lft forever
5: eth1.4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master vxlan_bridge state UNKNOWN group default qlen 1000
    link/ether 42:cf:df:63:0f:48 brd ff:ff:ff:ff:ff:ff
7: eth1.5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master vxlan_bridge state UNKNOWN group default qlen 1000
    link/ether 96:50:97:da:e7:e2 brd ff:ff:ff:ff:ff:ff
11: one-15-0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master vxlan_bridge state UNKNOWN group default qlen 1000
    link/ether fe:00:0a:dc:dc:0a brd ff:ff:ff:ff:ff:ff
[vagrant@kvm1 ~]$



```

on peut se connecter en SSH a la deuxième vm

```
PS C:\cour\Cloud2> vagrant ssh frontend.one
Last login: Wed Apr 10 12:52:02 2024 from 10.0.2.2
[vagrant@frontend ~]$ sudo su - oneadmin
Last login: Wed Apr 10 12:52:27 UTC 2024 on pts/0
[oneadmin@frontend ~]$ ssh oneadmin@10.3.1.22
Last login: Wed Apr 10 12:52:40 2024 from 10.3.1.11
[oneadmin@kvm2 ~]$
```

**🌞 Les deux VMs doivent pouvoir se ping**

```
PS C:\cour\Cloud2> vagrant ssh frontend.one
Last login: Wed Apr 10 13:09:56 2024 from 10.0.2.2
[vagrant@frontend ~]$ sudo su - oneadmin
Last login: Wed Apr 10 13:11:36 UTC 2024 on pts/0
[oneadmin@frontend ~]$ eval $(ssh-agent)
Agent pid 15739
[oneadmin@frontend ~]$ ssh-add
Identity added: /var/lib/one/.ssh/id_rsa (oneadmin@frontend.one)
[oneadmin@frontend ~]$ ssh -A 10.3.1.22
Last login: Wed Apr 10 13:14:45 2024 from 10.3.1.11
[oneadmin@kvm2 ~]$ ssh root@10.220.220.11
[root@localhost ~]# ping 10.220.220.10
PING 10.220.220.10 (10.220.220.10) 56(84) bytes of data.
64 bytes from 10.220.220.10: icmp_seq=1 ttl=64 time=2.69 ms
64 bytes from 10.220.220.10: icmp_seq=2 ttl=64 time=1.50 ms
64 bytes from 10.220.220.10: icmp_seq=3 ttl=64 time=0.817 ms
64 bytes from 10.220.220.10: icmp_seq=4 ttl=64 time=1.24 ms
^C
--- 10.220.220.10 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 0.817/1.560/2.689/0.695 ms
[root@localhost ~]#
```


**🌞 Téléchargez tcpdump sur l'un des noeuds KVM**

```
[vagrant@kvm1 ~]$ sudo tcpdump -i eth1 -w eth1.pcap
dropped privs to tcpdump
tcpdump: listening on eth1, link-type EN10MB (Ethernet), snapshot length 262144 bytes
^C53 packets captured
53 packets received by filter
0 packets dropped by kernel
[vagrant@kvm1 ~]$ tcpdump -i vxlan-bridge -w vxlan.pcap
tcpdump: vxlan-bridge: You don't have permission to capture on that device
(socket: Operation not permitted)
[vagrant@kvm1 ~]$ sudo tcpdump -i vxlan-bridge -w vxlan.pcap
tcpdump: vxlan-bridge: No such device exists
(SIOCGIFHWADDR: No such device)
[vagrant@kvm1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:cb:b3:b6 brd ff:ff:ff:ff:ff:ff
    altname enp0s3
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute eth0
       valid_lft 76055sec preferred_lft 76055sec
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:98:2c:dd brd ff:ff:ff:ff:ff:ff
    altname enp0s8
    inet 10.3.1.21/24 brd 10.3.1.255 scope global noprefixroute eth1
       valid_lft forever preferred_lft forever
4: vxlan_bridge: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue state UP group default qlen 1000
    link/ether 42:cf:df:63:0f:48 brd ff:ff:ff:ff:ff:ff
    inet 10.220.220.201/24 scope global vxlan_bridge
       valid_lft forever preferred_lft forever
5: eth1.4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master vxlan_bridge state UNKNOWN group default qlen 1000
    link/ether 42:cf:df:63:0f:48 brd ff:ff:ff:ff:ff:ff
7: eth1.5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master vxlan_bridge state UNKNOWN group default qlen 1000
    link/ether 96:50:97:da:e7:e2 brd ff:ff:ff:ff:ff:ff
11: one-15-0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master vxlan_bridge state UNKNOWN group default qlen 1000
    link/ether fe:00:0a:dc:dc:0a brd ff:ff:ff:ff:ff:ff
[vagrant@kvm1 ~]$ sudo tcpdump -i vxlan_bridge -w vxlan.pcap
dropped privs to tcpdump
tcpdump: listening on vxlan_bridge, link-type EN10MB (Ethernet), snapshot length 262144 bytes
^C33 packets captured
33 packets received by filter
0 packets dropped by kernel
[vagrant@kvm1 Pendant les tcpdump on ping les machines entre elle

```
[root@localhost ~]# ping 10.220.220.10
PING 10.220.220.10 (10.220.220.10) 56(84) bytes of data.
64 bytes from 10.220.220.10: icmp_seq=1 ttl=64 time=2.69 ms
64 bytes from 10.220.220.10: icmp_seq=2 ttl=64 time=1.50 ms
64 bytes from 10.220.220.10: icmp_seq=3 ttl=64 time=0.817 ms
64 bytes from 10.220.220.10: icmp_seq=4 ttl=64 time=1.24 ms
^C
--- 10.220.220.10 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 0.817/1.560/2.689/0.695 ms
[root@localhost ~]# ping 10.220.220.10
PING 10.220.220.10 (10.220.220.10) 56(84) bytes of data.
64 bytes from 10.220.220.10: icmp_seq=1 ttl=64 time=1.09 ms
64 bytes from 10.220.220.10: icmp_seq=2 ttl=64 time=1.16 ms
64 bytes from 10.220.220.10: icmp_seq=3 ttl=64 time=1.26 ms
64 bytes from 10.220.220.10: icmp_seq=4 ttl=64 time=0.940 ms
64 bytes from 10.220.220.10: icmp_seq=5 ttl=64 time=0.775 ms
64 bytes from 10.220.220.10: icmp_seq=6 ttl=64 time=1.42 ms
64 bytes from 10.220.220.10: icmp_seq=7 ttl=64 time=2.66 ms
64 bytes from 10.220.220.10: icmp_seq=8 ttl=64 time=1.94 ms
64 bytes from 10.220.220.10: icmp_seq=9 ttl=64 time=2.29 ms
64 bytes from 10.220.220.10: icmp_seq=10 ttl=64 time=0.886 ms
64 bytes from 10.220.220.10: icmp_seq=11 ttl=64 time=1.34 ms
^C
--- 10.220.220.10 ping statistics ---
11 packets transmitted, 11 received, 0% packet loss, time 10091ms
rtt min/avg/max/mdev = 0.775/1.431/2.657/0.579 ms
[root@localhost ~]# ping 10.220.220.10
PING 10.220.220.10 (10.220.220.10) 56(84) bytes of data.
64 bytes from 10.220.220.10: icmp_seq=1 ttl=64 time=1.28 ms
64 bytes from 10.220.220.10: icmp_seq=1 ttl=63 time=1.28 ms (DUP!)
64 bytes from 10.220.220.10: icmp_seq=2 ttl=64 time=2.06 ms
64 bytes from 10.220.220.10: icmp_seq=2 ttl=63 time=2.06 ms (DUP!)
64 bytes from 10.220.220.10: icmp_seq=3 ttl=64 time=1.88 ms
64 bytes from 10.220.220.10: icmp_seq=3 ttl=63 time=1.88 ms (DUP!)
64 bytes from 10.220.220.10: icmp_seq=4 ttl=64 time=1.48 ms
64 bytes from 10.220.220.10: icmp_seq=4 ttl=63 time=1.48 ms (DUP!)
64 bytes from 10.220.220.10: icmp_seq=5 ttl=64 time=2.15 ms
64 bytes from 10.220.220.10: icmp_seq=5 ttl=63 time=2.15 ms (DUP!)
64 bytes from 10.220.220.10: icmp_seq=6 ttl=64 time=1.30 ms
64 bytes from 10.220.220.10: icmp_seq=6 ttl=63 time=1.30 ms (DUP!)
64 bytes from 10.220.220.10: icmp_seq=7 ttl=64 time=1.60 ms
64 bytes from 10.220.220.10: icmp_seq=7 ttl=63 time=1.60 ms (DUP!)
^C
--- 10.220.220.10 ping statistics ---
7 packets transmitted, 7 received, +7 duplicates, 0% packet loss, time 6020ms
rtt min/avg/max/mdev = 1.276/1.678/2.149/0.328 ms
[root@localhost ~]#
```

on finit avec deux fichier de paquet, que je joins au compte rendu







