  # TP 2

**install dhcp server**
```
[vagrant@TP2 ~]$ sudo dnf install dhcp-server
Last metadata expiration check: 0:00:17 ago on Wed 03 Apr 2024 04:04:50 PM UTC.
Dependencies resolved.
==================================================================================================================================================================================================================
 Package                                            Architecture                                  Version                                                     Repository                                     Size
==================================================================================================================================================================================================================
Installing:
 dhcp-server                                        x86_64                                        12:4.4.2-19.b1.el9                                          baseos                                        1.2 M
Installing dependencies:
 dhcp-common                                        noarch                                        12:4.4.2-19.b1.el9                                          baseos                                        128 k

Transaction Summary
==================================================================================================================================================================================================================
Install  2 Packages

Total download size: 1.3 M
Installed size: 4.2 M
Is this ok [y/N]: y
Downloading Packages:
(1/2): dhcp-common-4.4.2-19.b1.el9.noarch.rpm                                                                                                                                     333 kB/s | 128 kB     00:00
(2/2): dhcp-server-4.4.2-19.b1.el9.x86_64.rpm                                                                                                                                     382 kB/s | 1.2 MB     00:03
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                                                                                             394 kB/s | 1.3 MB     00:03
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                                                                                                          1/1
  Installing       : dhcp-common-12:4.4.2-19.b1.el9.noarch                                                                                                                                                    1/2
  Running scriptlet: dhcp-server-12:4.4.2-19.b1.el9.x86_64                                                                                                                                                    2/2
  Installing       : dhcp-server-12:4.4.2-19.b1.el9.x86_64                                                                                                                                                    2/2
  Running scriptlet: dhcp-server-12:4.4.2-19.b1.el9.x86_64                                                                                                                                                    2/2
  Verifying        : dhcp-server-12:4.4.2-19.b1.el9.x86_64                                                                                                                                                    1/2
  Verifying        : dhcp-common-12:4.4.2-19.b1.el9.noarch                                                                                                                                                    2/2

Installed:
  dhcp-common-12:4.4.2-19.b1.el9.noarch                                                                   dhcp-server-12:4.4.2-19.b1.el9.x86_64

Complete!
[vagrant@TP2 ~]$

```

**config server dhcp**

```






default-lease-time 600;
max-lease-time 7200;
authoritative;

# PXE specifics
option space pxelinux;
option pxelinux.magic code 208 = string;
option pxelinux.configfile code 209 = text;
option pxelinux.pathprefix code 210 = text;
option pxelinux.reboottime code 211 = unsigned integer 32;
option architecture-type code 93 = unsigned integer 16;

subnet 10.10.10.0 netmask 255.255.255.0 {
    # définition de la range pour que votre DHCP attribue des IP entre <FIRST_I>
    range dynamic-bootp 10.10.10.50 10.10.10.99;

    # add follows
    class "pxeclients" {
        match if substring (option vendor-class-identifier, 0, 9) = "PXEClient";
        next-server 10.10.10.203;

        if option architecture-type = 00:07 {
            filename "BOOTX64.EFI";
        }
        else {
            filename "pxelinux.0";
        }
    }
}
```


**demarage du server + ouvrir bon port firewall**

```
[vagrant@TP2 ~]$ sudo systemctl start dhcpd
[vagrant@TP2 ~]$ sudo systemctl enable dhcpd
[vagrant@TP2 ~]$ sudo nano /etc/dhcp/dhcpd.conf
[vagrant@TP2 ~]$ sudo firewall-cmd --add-service=dhcp --permanent
Warning: ALREADY_ENABLED: dhcp
success
[vagrant@TP2 ~]$ sudo fireswall-cmd --reload
sudo: fireswall-cmd: command not found
[vagrant@TP2 ~]$ sudo fireswall -cmd --reload
sudo: fireswall: command not found
[vagrant@TP2 ~]$ sudo firewall-cmd --reload
success
[vagrant@TP2 ~]$

```

# installation server TFTP

```
[vagrant@TP2 ~]$ sudo dnf install tftp-server
Extra Packages for Enterprise Linux 9 - x86_64   19 kB/s |  11 kB     00:00
Extra Packages for Enterprise Linux 9 - x86_64  2.9 MB/s |  21 MB     00:07
Rocky Linux 9 - BaseOS                          9.4 kB/s | 4.1 kB     00:00
Rocky Linux 9 - AppStream                       9.1 kB/s | 4.5 kB     00:00
Rocky Linux 9 - Extras                          6.2 kB/s | 2.9 kB     00:00
Dependencies resolved.
================================================================================
 Package             Architecture   Version             Repository         Size
================================================================================
Installing:
 tftp-server         x86_64         5.2-37.el9          appstream          40 k

Transaction Summary
================================================================================
Install  1 Package

Total download size: 40 k
Installed size: 64 k
Is this ok [y/N]: y
Downloading Packages:
tftp-server-5.2-37.el9.x86_64.rpm                12 kB/s |  40 kB     00:03
--------------------------------------------------------------------------------
Total                                            11 kB/s |  40 kB     00:03
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1
  Installing       : tftp-server-5.2-37.el9.x86_64                          1/1
  Running scriptlet: tftp-server-5.2-37.el9.x86_64                          1/1
  Verifying        : tftp-server-5.2-37.el9.x86_64                          1/1

Installed:
  tftp-server-5.2-37.el9.x86_64

Complete!
[vagrant@TP2 ~]$ sudo systemctl enable --now tftp.socket
Created symlink /etc/systemd/system/sockets.target.wants/tftp.socket → /usr/lib/systemd/system/tftp.socket.
[vagrant@TP2 ~]$ sudo firewall-cmd --add-service=tftp --permanent
success
[vagrant@TP2 ~]$ sudo firewall-cmd --reload
success

```

**un peu de conf**

```
[vagrant@TP2 ~]$ ls
iso
[vagrant@TP2 ~]$ cd iso/
[vagrant@TP2 iso]$ ls
Rocky-9.2-x86_64-minimal.iso
[vagrant@TP2 iso]$ cd ..
[vagrant@TP2 ~]$ sudo dnf -y install syslinux
Extra Packages for Enterprise Linux 9 - x86_64   22 kB/s |  11 kB     00:00
Dependencies resolved.
================================================================================
 Package                 Architecture Version                Repository    Size
================================================================================
Installing:
 syslinux                x86_64       6.04-0.20.el9          baseos       560 k
Installing dependencies:
 mtools                  x86_64       4.0.26-4.el9_0         baseos       209 k
 syslinux-nonlinux       noarch       6.04-0.20.el9          baseos       559 k

Transaction Summary
================================================================================
Install  3 Packages

Total download size: 1.3 M
Installed size: 3.0 M
Downloading Packages:
(1/3): mtools-4.0.26-4.el9_0.x86_64.rpm         122 kB/s | 209 kB     00:01
(2/3): syslinux-6.04-0.20.el9.x86_64.rpm        306 kB/s | 560 kB     00:01
(3/3): syslinux-nonlinux-6.04-0.20.el9.noarch.r 275 kB/s | 559 kB     00:02
--------------------------------------------------------------------------------
Total                                           576 kB/s | 1.3 MB     00:02
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1
  Installing       : mtools-4.0.26-4.el9_0.x86_64                           1/3
  Installing       : syslinux-nonlinux-6.04-0.20.el9.noarch                 2/3
  Installing       : syslinux-6.04-0.20.el9.x86_64                          3/3
  Running scriptlet: syslinux-6.04-0.20.el9.x86_64                          3/3
  Verifying        : syslinux-6.04-0.20.el9.x86_64                          1/3
  Verifying        : mtools-4.0.26-4.el9_0.x86_64                           2/3
  Verifying        : syslinux-nonlinux-6.04-0.20.el9.noarch                 3/3

Installed:
  mtools-4.0.26-4.el9_0.x86_64                syslinux-6.04-0.20.el9.x86_64
  syslinux-nonlinux-6.04-0.20.el9.noarch

Complete!
[vagrant@TP2 ~]$ cp /usr/share/syslinux/pxelinux.0 /var/lib/tftpboot/
cp: cannot create regular file '/var/lib/tftpboot/pxelinux.0': Permission denied
[vagrant@TP2 ~]$ sudo cp /usr/share/syslinux/pxelinux.0 /var/lib/tftpboot/
[vagrant@TP2 ~]$ mkdir -p /var/pxe/rocky9
mkdir /var/lib/tftpboot/rocky9
mkdir: cannot create directory ‘/var/pxe’: Permission denied
mkdir: cannot create directory ‘/var/lib/tftpboot/rocky9’: Permission denied
[vagrant@TP2 ~]$ sudo mkdir -p /var/pxe/rocky9
[vagrant@TP2 ~]$ sudo mkdir /var/lib/tftpboot/rocky9
[vagrant@TP2 ~]$ pwd
/home/vagrant
[vagrant@TP2 ~]$ sudo mount -t iso9660 -o loop,ro /home/vagrant/iso/Rocky-9.2-x86_64-minimal.iso /var/pxe/rocky9
[vagrant@TP2 ~]$ sudo cp /var/pxe/rocky9/images/pxeboot/{vmlinuz,initrd.img} /var/lib/tftpboot/rocky9/
[vagrant@TP2 ~]$ sudo cp /usr/share/syslinux/{menu.c32,vesamenu.c32,ldlinux.c32,libcom32.c32,libutil.c32} /var/lib/tftpboot/
[vagrant@TP2 ~]$ sudo mkdir /var/lib/tftpboot/pxelinux.cfg
[vagrant@TP2 ~]$sudo nano /var/lib/tftpboot/pxelinux.cfg/default




fichier conf:

default vesamenu.c32
prompt 1
timeout 60

display boot.msg

label linux
  menu label ^Install Rocky Linux 9 my big boiiiiiii
  menu default
  kernel rocky9/vmlinuz
  append initrd=rocky9/initrd.img ip=dhcp inst.repo=http://10.10.10.203/rocky9
label rescue
  menu label ^Rescue installed system
  kernel rocky9/vmlinuz
  append initrd=rocky9/initrd.img rescue
label local
  menu label Boot from ^local drive
  localboot 0xffff


```

**installation server apache**

```
[vagrant@TP2 ~]$ sudo dnf -y install httpd
Extra Packages for Enterprise Linux 9 - x86_64   22 kB/s |  11 kB     00:00
Dependencies resolved.
================================================================================
 Package                Arch        Version                Repository      Size
================================================================================
Installing:
 httpd                  x86_64      2.4.57-5.el9           appstream       46 k
Installing dependencies:
 apr                    x86_64      1.7.0-12.el9_3         appstream      122 k
 apr-util               x86_64      1.6.1-23.el9           appstream       94 k
 apr-util-bdb           x86_64      1.6.1-23.el9           appstream       12 k
 httpd-core             x86_64      2.4.57-5.el9           appstream      1.4 M
 httpd-filesystem       noarch      2.4.57-5.el9           appstream       13 k
 httpd-tools            x86_64      2.4.57-5.el9           appstream       80 k
 mailcap                noarch      2.1.49-5.el9           baseos          32 k
 rocky-logos-httpd      noarch      90.15-2.el9            appstream       24 k
Installing weak dependencies:
 apr-util-openssl       x86_64      1.6.1-23.el9           appstream       14 k
 mod_http2              x86_64      1.15.19-5.el9          appstream      148 k
 mod_lua                x86_64      2.4.57-5.el9           appstream       60 k

Transaction Summary
================================================================================
Install  12 Packages

Total download size: 2.0 M
Installed size: 6.0 M
Downloading Packages:
(1/12): mailcap-2.1.49-5.el9.noarch.rpm         109 kB/s |  32 kB     00:00
(2/12): rocky-logos-httpd-90.15-2.el9.noarch.rp  81 kB/s |  24 kB     00:00
(3/12): mod_lua-2.4.57-5.el9.x86_64.rpm          32 kB/s |  60 kB     00:01
(4/12): httpd-2.4.57-5.el9.x86_64.rpm            28 kB/s |  46 kB     00:01
(5/12): httpd-filesystem-2.4.57-5.el9.noarch.rp  83 kB/s |  13 kB     00:00
(6/12): httpd-tools-2.4.57-5.el9.x86_64.rpm      47 kB/s |  80 kB     00:01
(7/12): apr-util-openssl-1.6.1-23.el9.x86_64.rp 9.5 kB/s |  14 kB     00:01
(8/12): apr-util-bdb-1.6.1-23.el9.x86_64.rpm    8.0 kB/s |  12 kB     00:01
(9/12): apr-util-1.6.1-23.el9.x86_64.rpm         58 kB/s |  94 kB     00:01
(10/12): apr-1.7.0-12.el9_3.x86_64.rpm           72 kB/s | 122 kB     00:01
(11/12): mod_http2-1.15.19-5.el9.x86_64.rpm      84 kB/s | 148 kB     00:01
(12/12): httpd-core-2.4.57-5.el9.x86_64.rpm     468 kB/s | 1.4 MB     00:02
--------------------------------------------------------------------------------
Total                                           287 kB/s | 2.0 MB     00:07
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1
  Installing       : apr-1.7.0-12.el9_3.x86_64                             1/12
  Installing       : apr-util-bdb-1.6.1-23.el9.x86_64                      2/12
  Installing       : apr-util-1.6.1-23.el9.x86_64                          3/12
  Installing       : apr-util-openssl-1.6.1-23.el9.x86_64                  4/12
  Installing       : httpd-tools-2.4.57-5.el9.x86_64                       5/12
  Running scriptlet: httpd-filesystem-2.4.57-5.el9.noarch                  6/12
  Installing       : httpd-filesystem-2.4.57-5.el9.noarch                  6/12
  Installing       : rocky-logos-httpd-90.15-2.el9.noarch                  7/12
  Installing       : mailcap-2.1.49-5.el9.noarch                           8/12
  Installing       : httpd-core-2.4.57-5.el9.x86_64                        9/12
  Installing       : mod_lua-2.4.57-5.el9.x86_64                          10/12
  Installing       : httpd-2.4.57-5.el9.x86_64                            11/12
  Running scriptlet: httpd-2.4.57-5.el9.x86_64                            11/12
  Installing       : mod_http2-1.15.19-5.el9.x86_64                       12/12
  Running scriptlet: httpd-2.4.57-5.el9.x86_64                            12/12
  Running scriptlet: mod_http2-1.15.19-5.el9.x86_64                       12/12
  Verifying        : mailcap-2.1.49-5.el9.noarch                           1/12
  Verifying        : rocky-logos-httpd-90.15-2.el9.noarch                  2/12
  Verifying        : mod_lua-2.4.57-5.el9.x86_64                           3/12
  Verifying        : httpd-tools-2.4.57-5.el9.x86_64                       4/12
  Verifying        : httpd-2.4.57-5.el9.x86_64                             5/12
  Verifying        : httpd-filesystem-2.4.57-5.el9.noarch                  6/12
  Verifying        : apr-util-openssl-1.6.1-23.el9.x86_64                  7/12
  Verifying        : apr-util-bdb-1.6.1-23.el9.x86_64                      8/12
  Verifying        : apr-util-1.6.1-23.el9.x86_64                          9/12
  Verifying        : mod_http2-1.15.19-5.el9.x86_64                       10/12
  Verifying        : apr-1.7.0-12.el9_3.x86_64                            11/12
  Verifying        : httpd-core-2.4.57-5.el9.x86_64                       12/12

Installed:
  apr-1.7.0-12.el9_3.x86_64              apr-util-1.6.1-23.el9.x86_64
  apr-util-bdb-1.6.1-23.el9.x86_64       apr-util-openssl-1.6.1-23.el9.x86_64
  httpd-2.4.57-5.el9.x86_64              httpd-core-2.4.57-5.el9.x86_64
  httpd-filesystem-2.4.57-5.el9.noarch   httpd-tools-2.4.57-5.el9.x86_64
  mailcap-2.1.49-5.el9.noarch            mod_http2-1.15.19-5.el9.x86_64
  mod_lua-2.4.57-5.el9.x86_64            rocky-logos-httpd-90.15-2.el9.noarch

Complete!
[vagrant@TP2 ~]$ sudo nano /etc/httpd/conf.d.pxeboot.conf
[vagrant@TP2 ~]$



```

**fichier conf**

```
Alias /rocky9 /var/pxe/rocky9
<Directory /var/pxe/rocky9>
    Options Indexes FollowSymLinks
    # access permission
    Require ip 127.0.0.1 10.10.10.0/24 # remplace 10.1.1.0/24 par le réseau dans>
</Directory>



```


**suivi de commande**
```
[vagrant@TP2 ~]$ sudo systemctl enable --now httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[vagrant@TP2 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[vagrant@TP2 ~]$ sudo fireswall-cmd --reload
sudo: fireswall-cmd: command not found
[vagrant@TP2 ~]$ sudo firewall-cmd --reload
success

```


# test

```
[vagrant@TP2 ~]$ sudo tcpdump
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on eth0, link-type EN10MB (Ethernet), snapshot length 262144 bytes
15:00:34.168504 IP efrei-xmg4agau1.etudiants.campus.villejuif.51133 > ntp-3.arkena.net.ntp: NTPv4, Client, length 48
15:00:34.198372 IP ntp-3.arkena.net.ntp > efrei-xmg4agau1.etudiants.campus.villejuif.51133: NTPv4, Server, length 48
15:00:34.264272 IP efrei-xmg4agau1.etudiants.campus.villejuif.49222 > b001-04.etudiants.campus.villejuif.domain: 34843+ PTR? 155.173.81.95.in-addr.arpa. (44)
15:00:34.289790 IP b001-04.etudiants.campus.villejuif.domain > efrei-xmg4agau1.etudiants.campus.villejuif.49222: 34843 1/0/0 PTR ntp-3.arkena.net. (74)
15:00:34.290095 IP efrei-xmg4agau1.etudiants.campus.villejuif.56578 > b001-04.etudiants.campus.villejuif.domain: 18369+ PTR? 15.2.0.10.in-addr.arpa. (40)
15:00:34.308607 IP b001-04.etudiants.campus.villejuif.domain > efrei-xmg4agau1.etudiants.campus.villejuif.56578: 18369* 2/0/0 PTR efrei-xmg4agau1.etudiants.campus.villejuif., PTR efrei-xmg4agau1.campus.villejuif. (126)
15:00:34.374033 IP efrei-xmg4agau1.etudiants.campus.villejuif.51968 > b001-04.etudiants.campus.villejuif.domain: 57500+ PTR? 3.2.0.10.in-addr.arpa. (39)
15:00:34.389655 IP b001-04.etudiants.campus.villejuif.domain > efrei-xmg4agau1.etudiants.campus.villejuif.51968: 57500* 1/0/0 PTR b001-04.etudiants.campus.villejuif. (87)
15:00:39.639646 ARP, Request who-has b002-09.etudiants.campus.villejuif tell efrei-xmg4agau1.etudiants.campus.villejuif, length 28
15:00:39.639834 ARP, Reply b002-09.etudiants.campus.villejuif is-at 52:54:00:12:35:02 (oui Unknown), length 46
15:00:39.639878 ARP, Request who-has b001-04.etudiants.campus.villejuif tell efrei-xmg4agau1.etudiants.campus.villejuif, length 28
15:00:39.640048 ARP, Reply b001-04.etudiants.campus.villejuif is-at 52:54:00:12:35:03 (oui Unknown), length 46
15:00:39.679883 IP efrei-xmg4agau1.etudiants.campus.villejuif.36817 > b001-04.etudiants.campus.villejuif.domain: 80+ PTR? 2.2.0.10.in-addr.arpa. (39)
15:00:39.698099 IP b001-04.etudiants.campus.villejuif.domain > efrei-xmg4agau1.etudiants.campus.villejuif.36817: 80* 1/0/0 PTR b002-09.etudiants.campus.villejuif. (87)
15:03:53.509501 IP efrei-xmg4agau1.etudiants.campus.villejuif.44882 > 82-64-81-218.subs.proxad.net.ntp: NTPv4, Client, length 48
15:03:53.558894 IP 82-64-81-218.subs.proxad.net.ntp > efrei-xmg4agau1.etudiants.campus.villejuif.44882: NTPv4, Server, length 48
15:03:53.559777 IP efrei-xmg4agau1.etudiants.campus.villejuif.55918 > b001-04.etudiants.campus.villejuif.domain: 13918+ PTR? 218.81.64.82.in-addr.arpa. (43)
15:03:53.579911 IP b001-04.etudiants.campus.villejuif.domain > efrei-xmg4agau1.etudiants.campus.villejuif.55918: 13918 1/0/0 PTR 82-64-81-218.subs.proxad.net. (85)
15:03:58.807577 ARP, Request who-has b002-09.etudiants.campus.villejuif tell efrei-xmg4agau1.etudiants.campus.villejuif, length 28
15:03:58.807682 ARP, Reply b002-09.etudiants.campus.villejuif is-at 52:54:00:12:35:02 (oui Unknown), length 46
15:03:58.807718 ARP, Request who-has b001-04.etudiants.campus.villejuif tell efrei-xmg4agau1.etudiants.campus.villejuif, length 28
15:03:58.
```



